UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'helmet_1') + 9),
				CASE
					WHEN DATA LIKE '%helmet_1%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'helmet_1') + 10,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'helmet_1') + 11, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'helmet_1') + 11, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'helmet_1') + 10 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'helmet_1') + 11, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'helmet_1') + 11, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_helmet'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'helmet_2') + 9),
				CASE
					WHEN DATA LIKE '%helmet_2%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'helmet_2') + 10,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'helmet_2') + 11, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'helmet_2') + 11, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'helmet_2') + 10 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'helmet_2') + 11, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'helmet_2') + 11, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_helmet'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'ears_1') + 7),
				CASE
					WHEN DATA LIKE '%ears_1%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'ears_1') + 8,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'ears_1') + 9, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'ears_1') + 9, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'ears_1') + 8 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'ears_1') + 9, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'ears_1') + 9, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_ears'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'ears_2') + 7),
				CASE
					WHEN DATA LIKE '%ears_2%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'ears_2') + 8,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'ears_2') + 9, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'ears_2') + 9, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'ears_2') + 8 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'ears_2') + 9, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'ears_2') + 9, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_ears'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'mask_1') + 7),
				CASE
					WHEN DATA LIKE '%mask_1%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'mask_1') + 8,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'mask_1') + 9, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'mask_1') + 9, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'mask_1') + 8 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'mask_1') + 9, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'mask_1') + 9, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_mask'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'mask_2') + 7),
				CASE
					WHEN DATA LIKE '%mask_2%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'mask_2') + 8,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'mask_2') + 9, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'mask_2') + 9, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'mask_2') + 8 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'mask_2') + 9, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'mask_2') + 9, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_mask'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'glasses_1') + 10),
				CASE
					WHEN DATA LIKE '%glasses_1%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'glasses_1') + 11,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'glasses_1') + 12, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'glasses_1') + 12, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'glasses_1') + 11 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'glasses_1') + 12, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'glasses_1') + 12, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_glasses'
);

UPDATE users
SET skin = (
	SELECT 
		CONCAT(
			CONCAT(
				SUBSTRING(skin, 1, INSTR(skin, 'glasses_2') + 10),
				CASE
					WHEN DATA LIKE '%glasses_2%'
					THEN
						SUBSTRING(
							DATA, 
							INSTR(DATA, 'glasses_2') + 11,
							CASE
								WHEN STRCMP(SUBSTRING( DATA, INSTR(DATA, 'glasses_2') + 12, 1), ',') = 0
								  OR STRCMP(SUBSTRING( DATA, INSTR(DATA, 'glasses_2') + 12, 1), '}') = 0
								THEN 1 
								ELSE 2 
							END
						)
					ELSE '-1'
				END
			),
			SUBSTRING(
				skin,
				INSTR(skin, 'glasses_2') + 11 + 
					CASE
						WHEN STRCMP(SUBSTRING(skin, INSTR(skin, 'glasses_2') + 12, 1), ',') = 0 
						  OR STRCMP(SUBSTRING(skin, INSTR(skin, 'glasses_2') + 12, 1), '}') = 0 
						THEN 1 
						ELSE 2 
					END,
				99999999
			)
		)
	FROM datastore_data
	WHERE users.identifier = datastore_data.owner
	  AND datastore_data.NAME = 'user_glasses'
);