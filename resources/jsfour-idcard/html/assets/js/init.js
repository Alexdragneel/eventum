$(document).ready(function(){
  // LUA listener
  window.addEventListener('message', function( event ) {
    if (event.data.action == 'open') {
      var type        = event.data.type;
      var userData    = event.data.array['user'][0];
      var licenseData = event.data.array['licenses'];
      var sex         = userData.sex;

      if ( type == 'driver' || type == null) {
        $('img').show();
        $('p').css('color', 'white');

        if ( sex.toLowerCase() == 'm' ) {
          $('img').attr('src', 'assets/images/male.png');
          $('#sex').text('male');
        } else {
          $('img').attr('src', 'assets/images/female.png');
          $('#sex').text('female');
        }

        $('#name').text(userData.firstname + ' ' + userData.lastname);
        $('#dob').text((new Date(userData.dateofbirth)).toLocaleDateString("fr-FR"));
        $('#height').text(userData.height);
        $('#signature').text(userData.firstname + ' ' + userData.lastname);

        if ( type == 'driver' ) {
          if ( licenseData != null ) {
          Object.keys(licenseData).forEach(function(key) {
            var type = licenseData[key].type;
            var points = licenseData[key].points;

            $('p').css('color', 'black');

            if ( type == 'drive_bike') {
              type = 'moto';
            } else if ( type == 'drive_truck' ) {
              type = 'camion';
            } else if ( type == 'drive' ) {
              type = 'voiture';
            }

            if ( type == 'moto' || type == 'camion' || type == 'voiture' ) {
              var content = type;
              if (points != undefined) {
                content += " (" + points + ")";
              }
              $('#licenses').append('<p>' + content + '</p>');
            }
          });
        }

          $('#id-card').css('background', 'url(assets/images/license.png)');
        } else {
          $('#id-card').css('background', 'url(assets/images/idcard.png)');
        }
      } else if ( type == 'weapon' ) {
        $('img').hide();
        $('p').css('color', 'white');
        $('#name').text(userData.firstname + ' ' + userData.lastname);
        $('#dob').text((new Date(userData.dateofbirth)).toLocaleDateString("fr-FR"));
        $('#signature').text(userData.firstname + ' ' + userData.lastname);

        $('#id-card').css('background', 'url(assets/images/firearm.png)');
      }

      $('#id-card').show();
    } else if (event.data.action == 'close') {
      $('#name').text('');
      $('#dob').text('');
      $('#height').text('');
      $('#signature').text('');
      $('#sex').text('');
      $('#id-card').hide();
      $('#licenses').html('');
    }
  });
});
