fx_version 'adamant'
game { 'gta5' }

this_is_a_map 'yes'

description 'Prison federale'


file 'desertos_prison_door_01_game.dat151.rel'
file 'interiorproxies.meta'

data_file 'AUDIO_GAMEDATA' 'desertos_prison_door_01_game.dat'
data_file 'INTERIOR_PROXY_ORDER_FILE' 'interiorproxies.meta'