description 'Catolich church interiors'
name 'Church'
author 'e_49'

files {
    "interiorproxies.meta"
}

client_script {
    "client.lua"
}

data_file 'INTERIOR_PROXY_ORDER_FILE' 'interiorproxies.meta'

this_is_a_map 'yes'

fx_version 'adamant'
games {'gta5'}