
        local blips = {
        
             {title="Eglise", colour=0, id=305, x = -1678.8, y = -282.9, z = 51.86},
          }
              
        Citizen.CreateThread(function()
        
            for _, info in pairs(blips) do
              info.blip = AddBlipForCoord(info.x, info.y, info.z)
              SetBlipSprite(info.blip, info.id)
              SetBlipDisplay(info.blip, 4)
              SetBlipScale(info.blip, 1.2)
              SetBlipColour(info.blip, info.colour)
              SetBlipAsShortRange(info.blip, true)
              BeginTextCommandSetBlipName("STRING")
              AddTextComponentString(info.title)
              EndTextCommandSetBlipName(info.blip)
            end
        end)