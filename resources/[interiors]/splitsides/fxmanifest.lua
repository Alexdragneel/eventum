fx_version 'adamant'
game 'gta5'

this_is_a_map 'yes'

data_file('DLC_ITYP_REQUEST')('stream/gta5interiors_splitsides.ytyp')

file 'interiorproxies.meta'

data_file 'INTERIOR_PROXY_ORDER_FILE' 'interiorproxies.meta'
