description 'bennys interior'

fx_version 'adamant'
games {'gta5'}

this_is_a_map 'yes'

data_file 'DLC_ITYP_REQUEST' 'stream/portebennysking_door_01.ytyp'
data_file "INTERIOR_PROXY_ORDER_FILE" "interiorproxies.meta"

files {
    "interiorproxies.meta",
}