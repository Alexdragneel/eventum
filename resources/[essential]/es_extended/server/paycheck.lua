ESX.StartPayCheck = function()
	function payCheck()
		local xPlayers = ESX.GetPlayers()
		local PlayersInDuty = {}
		local playersInDutyRetrieved = false
		local timeout = 0

		-- Get all players in duty
		TriggerEvent('esx_service:getAllInService', function(InService)
			PlayersInDuty = InService
			playersInDutyRetrieved = true
		end)

		-- Possiblement useless mais on sait jamais
		while playersInDutyRetrieved == false do
			Citizen.Wait(1000)
			timeout = timeout + 1
			if timeout >= 30 then
				break
			end
		end

		if timeout < 30 then

			for i=1, #xPlayers, 1 do
				local playerId = xPlayers[i]
				local xPlayer = ESX.GetPlayerFromId(playerId)
				local job     = xPlayer.job.grade_name
				local salary  = xPlayer.job.grade_salary

				if salary > 0 then
					if job == 'unemployed' then -- unemployed
						xPlayer.addAccountMoney('bank', salary)
						TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, _U('bank'), _U('received_paycheck'), _U('received_help', salary), 'CHAR_BANK_MAZE', 9)
					elseif Config.EnableSocietyPayouts then -- possibly a society
						if PlayersInDuty ~= nil then
							if PlayersInDuty[xPlayer.job.name] ~= nil then
								if PlayersInDuty[xPlayer.job.name][playerId] == true then
									TriggerEvent('esx_society:getSociety', xPlayer.job.name, function (society)
										if society ~= nil then -- verified society
											TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function (account)
												if account.money >= salary then -- does the society money to pay its employees?
													xPlayer.addAccountMoney('bank', salary)
													account.removeMoney(salary)

													TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, _U('bank'), _U('received_paycheck'), _U('received_salary', salary), 'CHAR_BANK_MAZE', 9)
												else
													TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, _U('bank'), '', _U('company_nomoney'), 'CHAR_BANK_MAZE', 1)
												end
											end)
										else -- not a society
											xPlayer.addAccountMoney('bank', salary)
											TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, _U('bank'), _U('received_paycheck'), _U('received_salary', salary), 'CHAR_BANK_MAZE', 9)
										end
									end)
								end
							end
						end
					else -- generic job
						xPlayer.addAccountMoney('bank', salary)
						TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, _U('bank'), _U('received_paycheck'), _U('received_salary', salary), 'CHAR_BANK_MAZE', 9)
					end
				end

			end

			SetTimeout(Config.PaycheckInterval, payCheck)
		else -- can't  players in duty
			SetTimeout(10 * 1000, payCheck)
		end
	end

	SetTimeout(Config.PaycheckInterval, payCheck)
end
