ESX = {}
ESX.Players = {}
ESX.UsableItemsCallbacks = {}
ESX.Items = {}
ESX.ServerCallbacks = {}
ESX.TimeoutCount = -1
ESX.CancelledTimeouts = {}
ESX.Pickups = {}
ESX.PickupId = 0
ESX.Jobs = {}
ESX.RegisteredCommands = {}

AddEventHandler('esx:getSharedObject', function(cb)
	cb(ESX)
end)

function getSharedObject()
	return ESX
end

MySQL.ready(function()
	MySQL.Async.fetchAll('SELECT * FROM lp_items', {}, function(result)
		local next = next

		for id, item in ipairs(result) do
			local actions = json.decode(item.actions)
			if next(actions) == nil then
				actions = nil
			end
			ESX.Items[item.name] = {
				label = item.label,
				weight = (item.weight / 1000) + 0.0,
				sellPrice = item.sellPrice,
				farmPrice = item.farmPrice,
				actions = actions,
				stackable = item.stackable,
				rarity = item.rarity,
				canRemove = item.canRemove,
				icon = item.icon
			}
		end

		for name, dbItem in pairs(ESX.Items) do
			if dbItem.actions ~= nil and ESX.UsableItemsCallbacks[name] == nil then
				ESX.RegisterUsableItem(name, function(source, itemId)
					local xPlayer = ESX.GetPlayerFromId(source)
					
					if dbItem.actions.trigger ~= nil then
						local trigger = dbItem.actions.trigger
						if trigger.type == 'server' then
							TriggerEvent(trigger.event)
						else
							xPlayer.triggerEvent(trigger.event)
						end
					elseif dbItem.actions.useCount ~= nil and dbItem.actions.useCount > 0 then
						xPlayer.consumeInventoryItem(itemId, 1)
					elseif dbItem.actions.becomes ~= nil then -- no useCount
						local becomesItem = ESX.Items[dbItem.actions.becomes]
						if becomesItem ~= nil then
							xPlayer.removeInventoryItem(name, 1, false)
							xPlayer.addInventoryItem(dbItem.actions.becomes, 1, true, false)
						end
					else
						xPlayer.removeInventoryItem(name, 1, false)
					end

					if dbItem.actions.anim ~= nil then
						TriggerClientEvent('lp_inventory:playAnim', source, dbItem.actions.anim)
					end 

					if dbItem.actions.effects ~= nil then
						for key, effect in ipairs(dbItem.actions.effects) do
							if effect.type == "add" then
								TriggerClientEvent('esx_status:add', source, effect.attribute, effect.amount)
							elseif effect.type == "remove" then
								TriggerClientEvent('esx_status:remove', source, effect.attribute, effect.amount)
							end
							-- TODOWIP: handle drugs/alcohol effects etc.
						end
					end

					if dbItem.actions.notification ~= nil then
						TriggerClientEvent('lp_inventory:notifAction', source, dbItem.actions.notification)
					end
				end)
			end
		end
	end)

	MySQL.Async.fetchAll('SELECT * FROM jobs', {}, function(jobs)
		for k,v in ipairs(jobs) do
			ESX.Jobs[v.name] = v
			ESX.Jobs[v.name].grades = {}
		end

		MySQL.Async.fetchAll('SELECT * FROM job_grades', {}, function(jobGrades)
			for k,v in ipairs(jobGrades) do
				if ESX.Jobs[v.job_name] then
					ESX.Jobs[v.job_name].grades[tostring(v.grade)] = v
				else
					print(('[es_extended] [^3WARNING^7] Ignoring job grades for "%s" due to missing job'):format(v.job_name))
				end
			end

			for k2,v2 in pairs(ESX.Jobs) do
				if ESX.Table.SizeOf(v2.grades) == 0 then
					ESX.Jobs[v2.name] = nil
					print(('[es_extended] [^3WARNING^7] Ignoring job "%s" due to no job grades found'):format(v2.name))
				end
			end
		end)
	end)

	print('[es_extended] [^2INFO^7] ESX developed by ESX-Org has been initialized')
end)

RegisterServerEvent('esx:clientLog')
AddEventHandler('esx:clientLog', function(msg)
	if Config.EnableDebug then
		print(('[es_extended] [^2TRACE^7] %s^7'):format(msg))
	end
end)

RegisterServerEvent('esx:triggerServerCallback')
AddEventHandler('esx:triggerServerCallback', function(name, requestId, ...)
	local playerId = source

	ESX.TriggerServerCallback(name, requestId, playerId, function(...)
		TriggerClientEvent('esx:serverCallback', playerId, requestId, ...)
	end, ...)
end)

AddEventHandler('txAdmin:events:scheduledRestart', function(eventData)
	if eventData.secondsRemaining == 60 then
        CreateThread(function()
            Wait(45000)
            print("15 seconds before restart... saving all players and vehicles!")
            ESX.SavePlayers()
        end)
    end
end)
