Config = {}

-- Disable/enable sounds
Config.Sounds = true
Config.LoopSound = true
Config.Volume = 0.6
Config.VolumeLoop = 0.5
-- Min: 0.0 Max: 1.0

-- Disable/enable Notifications
Config.Notification = true
Config.Strings = {seatbelt_on = 'Ceinture ~g~attachée', seatbelt_off = 'Ceinture ~r~détachée'}
-- Change to your own translations.

-- Disable/enable blinker image
Config.Blinker = true

-- Seatbelt button (docs.fivem.net/docs/game-references/controls)
Config.Control = 306

-- KM/H (must be have decimal value)
Config.Speed = 65.0
