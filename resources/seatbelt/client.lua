--- "Ramsus" ---

local isUiOpen = false 
local speedBuffer = {}
local velBuffer = {}
local SeatbeltON = false
local InVehicle = false
local lastHidden = false

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
  end
end)

function Notify(string)
  SetNotificationTextEntry("STRING")
  AddTextComponentString(string)
  DrawNotification(false, true)
end

AddEventHandler('seatbelt:sounds', function(soundFile, soundVolume, loop)
  local transactionType = 'playSound'
  if loop then
    transactionType = 'playSoundLoop'
  end
  TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', transactionType = transactionType, transactionFile = soundFile, transactionVolume = soundVolume})
end)

function IsCar(veh)
  local vc = GetVehicleClass(veh)
  local model = GetEntityModel(veh)
  return ((vc >= 0 and vc <= 7) or (vc >= 9 and vc <= 12) or (vc >= 17 and vc <= 20)) and model ~= GetHashKey('policeb') and model ~= GetHashKey('policeb1') and model ~= GetHashKey('policeb2')
end	

function Fwv(entity)
  local hr = GetEntityHeading(entity) + 90.0
  if hr < 0.0 then hr = 360.0 + hr end
  hr = hr * 0.0174533
  return { x = math.cos(hr) * 2.0, y = math.sin(hr) * 2.0 }
end
 
Citizen.CreateThread(function()
	while true do
	  Citizen.Wait(0)
  
    local ped = PlayerPedId()
    local car = GetVehiclePedIsIn(ped)

    if car ~= 0 and (InVehicle or IsCar(car)) then
      InVehicle = true
      if isUiOpen == false and not IsPlayerDead(PlayerId()) then
        if Config.Blinker then
          TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', displaySeatbelt = 'true', seatbeltOn = 'false' })
          isUiOpen = true
        end
      end

      if SeatbeltON then 
        DisableControlAction(0, 75, true)  -- Disable exit vehicle when stop
        DisableControlAction(27, 75, true) -- Disable exit vehicle when Driving
	    end

      speedBuffer[2] = speedBuffer[1]
      speedBuffer[1] = GetEntitySpeed(car)

      if not SeatbeltON and speedBuffer[2] ~= nil and GetEntitySpeedVector(car, true).y > 1.0 and speedBuffer[1] > (Config.Speed / 3.6) and (speedBuffer[2] - speedBuffer[1]) > (speedBuffer[1] * 0.255) then
        local co = GetEntityCoords(ped)
        local fw = Fwv(ped)
        SetEntityCoords(ped, co.x + fw.x, co.y + fw.y, co.z - 0.47, true, true, true)
        SetEntityVelocity(ped, velBuffer[2].x, velBuffer[2].y, velBuffer[2].z)
        Citizen.Wait(1)
        SetPedToRagdoll(ped, 1000, 1000, 0, 0, 0, 0)
      end
        
      velBuffer[2] = velBuffer[1]
      velBuffer[1] = GetEntityVelocity(car)
        
      if IsControlJustReleased(0, Config.Control) and GetLastInputMethod(0) and not ESX.UI.Menu.DialogIsOpen() and (not exports.gcphone:IsUsingPhone() or (exports.gcphone:IsUsingPhone() and not exports.gcphone:IsUsingMouse())) then
          if not SeatbeltON then
            if Config.Sounds then
              TriggerEvent("seatbelt:sounds", "buckle", Config.Volume, false)
              Citizen.Wait(2500)
            end

            SeatbeltON = not SeatbeltON
            
            if Config.Blinker then
              TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', seatbeltOn = 'true'})
            end
            isUiOpen = true 
          else 
            if Config.Sounds then
              TriggerEvent("seatbelt:sounds", "unbuckle", Config.Volume, false)
              Citizen.Wait(1000)
            end

            SeatbeltON = not SeatbeltON

            if Config.Blinker then
              TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', displaySeatbelt = 'true', seatbeltOn = 'false'})
            end
            isUiOpen = true  
          end
      end
    elseif InVehicle then
      InVehicle = false
      SeatbeltON = false
      speedBuffer[1], speedBuffer[2] = 0.0, 0.0
      if isUiOpen == true then
        if Config.Blinker then
          TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', displaySeatbelt = 'false'})
        end
        isUiOpen = false 
      end
    end
  end
end)

local pauseJustActive = false
local uiWasOpen = false

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)
    ShowWindow = true

    if IsPlayerDead(PlayerId()) or IsPauseMenuActive() then
      if isUiOpen == true then
        TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', displaySeatbelt = 'false'})
        pauseJustActive = true
        uiWasOpen = true
      end
    else
      if pauseJustActive and uiWasOpen then
        pauseJustActive = false
        uiWasOpen = false
        TriggerEvent('lp_hud:sendNui', {setSeatbelt = 'true', displaySeatbelt = 'true'})
      end
    end
  end
end)

-- ALERTE SONORE PAS DE CINTOURE
-- Citizen.CreateThread(function()
-- 	while true do
-- 		Citizen.Wait(1000)
--     if not SeatbeltON and InVehicle and not IsPauseMenuActive() and Config.LoopSound and ShowWindow then
--       TriggerEvent("seatbelt:sounds", "seatbelt", Config.VolumeLoop, true)
-- 		end    
-- 	end
-- end)
