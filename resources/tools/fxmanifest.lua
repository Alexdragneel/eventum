fx_version 'adamant'

game 'gta5'

description 'Tools'

version '1.0.0'

client_scripts {
	'config.lua',
	'client/no_vehicle_rewards-client.lua',
	'client/npc_drop-client.lua',
	'client/anti-shuffle.lua',
	'client/vehicle-lights.lua',
	'client/visible-weapons.lua',
	'client/veh-speed-limits.lua'
}

server_scripts {
	'config.lua',
	'server/main.lua'
}