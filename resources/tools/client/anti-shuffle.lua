local isShuffling = false -- please do not change this, it is internal to the script and not a config variable
local doors = {{"seat_dside_f", -1}, {"seat_pside_f", 0}, {"seat_dside_r", 1}, {"seat_pside_r", 2}}

local stopPassengerShuffle = true		--	Stop passenger shuffling to driver seat when driver seat is empty
local allowEntrySlide = false		--	Allow the player to slide into driver's seat when entering empty vehicle from passenger side
local allowKeyShuffle = false		--	If true, the player can shuffle to their neighboring seat when they press any of the 'exemptKeys' listed below
local wantShuffle = false

function getPedSeat(p, v)
    local seats = GetVehicleModelNumberOfSeats(GetEntityModel(v))
    for i = -1, seats do
        local t = GetPedInVehicleSeat(v, i)
        if (t == p) then
            return i
        end
    end
    return -2
end

function getVehicleInFront(p)
    local pos1, pos2 = GetEntityCoords(p), GetOffsetFromEntityInWorldCoords(p, 0.0, 7.0, 0.0)
    local ray = StartShapeTestRay(pos1.x, pos1.y, pos1.z, pos2.x, pos2.y, pos2.z, 2, p, 0)
    local ret, hit, endPos, hitPos, entityHit = GetShapeTestResult(ray)
    return entityHit
end

function enterRearSeat(p)
    local vehicle = getVehicleInFront(p)

    if vehicle ~= nil then
        local plyCoords = GetEntityCoords(p, false)
        local doorDistances = {}

        for k, door in pairs(doors) do
            local doorBone = GetEntityBoneIndexByName(vehicle, door[1])
            local doorPos = GetWorldPositionOfEntityBone(vehicle, doorBone)
            local distance = #(plyCoords - doorPos)

            table.insert(doorDistances, distance)
        end

        local key, min = 1, doorDistances[1]

        for k, v in ipairs(doorDistances) do
            if doorDistances[k] < min then
                key, min = k, v
            end
        end

        TaskEnterVehicle(p, vehicle, -1, doors[key][2], 1.5, 1, 0)
    end

end

Citizen.CreateThread(function()
    while true do
        local player = PlayerPedId()
        if (stopPassengerShuffle) then
            if (not GetPedConfigFlag(player, 184, 1)) then
                SetPedConfigFlag(player, 184, true)
            end
            if (IsPedInAnyVehicle(player, false)) then
                local v = GetVehiclePedIsIn(player, 0)
                if (wantShuffle) then
                    wantShuffle = false
                    if (GetPedConfigFlag(player, 184, 1)) then
                        SetPedConfigFlag(player, 184, false)
                    end
                    if (allowKeyShuffle) and (not isShuffling) then
                        local seatCurrent, seatTarget = getPedSeat(player, v), nil
                        if ((seatCurrent % 2) == 1) then
                            seatTarget = seatCurrent + 1
                        else
                            seatTarget = seatCurrent - 1
                        end
                        if (seatCurrent ~= 0) and (GetVehicleModelNumberOfSeats(GetEntityModel(v)) >= seatTarget + 2) then
                            if (IsVehicleSeatFree(v, seatTarget)) then
                                TaskShuffleToNextVehicleSeat(player, v)
                            end
                        end
                    end
                end
                if (GetIsTaskActive(player, 165)) then
                    isShuffling = true
                    if (not allowEntrySlide) then
                        if (GetSeatPedIsTryingToEnter(player) == -1) then
                            if (GetPedConfigFlag(player, 184, 1)) then
                                SetPedIntoVehicle(player, v, 0)
                                SetVehicleCloseDoorDeferedAction(v, 0)
                            end
                        end
                    end
                else
                    isShuffling = false
                end
            else
                if (IsControlJustPressed(0, 23)) and (IsInputDisabled(2)) then
                    enterRearSeat(player)
                end
            end
        else
            if (GetPedConfigFlag(player, 184, 1)) then
                SetPedConfigFlag(player, 184, false)
            end
        end
        Citizen.Wait(0)
    end
end)

RegisterCommand('shuffle', function()
    wantShuffle = true
end, false)

RegisterKeyMapping('shuffle', 'Changement de place', 'keyboard', 'B')
