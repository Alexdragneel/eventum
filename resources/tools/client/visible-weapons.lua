ESX = nil
local Weapons = {}
local Loaded = false
local spawned_object = {}

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end

    while not Loaded do
        Citizen.Wait(500)
    end

    local playerPed = PlayerPedId()

    while true do

        Citizen.Wait(500)

        for i, realWeapon in ipairs(Config.RealWeapons) do
            local weaponHash = GetHashKey(realWeapon.name)

            if HasPedGotWeapon(playerPed, weaponHash, false) then
                local onPlayer = false

                for weaponName, entity in pairs(Weapons) do
                    if weaponName == realWeapon.name then
                        onPlayer = true
                        break
                    end
                end

                if not onPlayer and weaponHash ~= GetSelectedPedWeapon(playerPed) then
                    SetGear(realWeapon.name)
                elseif onPlayer and weaponHash == GetSelectedPedWeapon(playerPed) then
                    RemoveGear(realWeapon.name)
                end

            end
        end
    end
end)

AddEventHandler('skinchanger:modelLoaded', function()
    SetGears()
    fixchar()
    Loaded = true
end)

RegisterNetEvent('esx:removeWeapon')
AddEventHandler('esx:removeWeapon', function(weaponName)
    RemoveGear(weaponName)
end)

-- Remove only one weapon that's on the ped
function RemoveGear(weapon)
    local _Weapons = {}

    for weaponName, entity in pairs(Weapons) do
        if weaponName ~= weapon then
            _Weapons[weaponName] = entity
        else
            ESX.Game.DeleteObject(entity)
            for i, object in ipairs(spawned_object) do
                if object == GetHashKey(weapon) then
                    table.remove(spawned_object, i) -- remove object from spawned object list
                    break
                end
            end
        end
    end

    Weapons = _Weapons
end

-- Remove all weapons that are on the ped
function RemoveGears()
    for weaponName, entity in pairs(Weapons) do
        ESX.Game.DeleteObject(entity)
    end
    Weapons = {}
    spawned_object = {} -- make sure reinitialize list
end

-- Add one weapon on the ped
function SetGear(weapon)
    if weapon ~= 'WEAPON_UNARMED' then
        local bone = nil
        local boneX = 0.0
        local boneY = 0.0
        local boneZ = 0.0
        local boneXRot = 0.0
        local boneYRot = 0.0
        local boneZRot = 0.0
        local playerPed = PlayerPedId()
        local model = nil
        local playerData = ESX.GetPlayerData()
        local model = nil
        local already_spawned = false
        local _wait = false

        for i, realWeapon in ipairs(Config.RealWeapons) do
            if realWeapon.name == weapon then
                bone = realWeapon.bone
                boneX = realWeapon.x
                boneY = realWeapon.y
                boneZ = realWeapon.z
                boneXRot = realWeapon.xRot
                boneYRot = realWeapon.yRot
                boneZRot = realWeapon.zRot
                model = realWeapon.model
                break
            end
        end

        -- if spawned object already attached, mark already_spawned
        for i, object in ipairs(spawned_object) do
            if object == GetHashKey(weapon) then
                already_spawned = true
                break
            end
        end

        -- if object already spawned (attached) dont need to spawnobject (HIGH CPU USAGE)
        if not already_spawned and model ~= nil then
            _wait = true
            ESX.Game.SpawnObject(model, {
                x = 0,
                y = 0,
                z = 0
            }, function(object)
                local boneIndex = GetPedBoneIndex(playerPed, bone)
                local bonePos = GetWorldPositionOfEntityBone(playerPed, boneIndex)
                AttachEntityToEntity(object, playerPed, boneIndex, boneX, boneY, boneZ, boneXRot, boneYRot, boneZRot,
                    false, false, false, false, 2, true)
                Weapons[weapon] = object
                -- list the spawned object to make sure to not reattached same object
                table.insert(spawned_object, GetHashKey(weapon))
                _wait = false
            end)
            while _wait == true do
                Citizen.Wait(100)
            end
        end
    end
end

-- Add all the weapons in the xPlayer's loadout
-- on the ped
function SetGears()
    local playerData = ESX.GetPlayerData()

    spawned_object = {} -- make sure reinitialize list
    for i, playerWeapon in ipairs(playerData.loadout) do
        for j, realWeapon in ipairs(Config.RealWeapons) do
            if realWeapon.name == playerWeapon.name then
                SetGear(realWeapon.name)
            end
        end
    end
end

function fixchar()
    local position = GetEntityCoords(GetPlayerPed(-1), false)
    Loaded = true

    for i, realWeapon in ipairs(Config.RealWeapons) do
        local object = GetClosestObjectOfType(position.x, position.y, position.z, 2.0, GetHashKey(realWeapon.model),
            false, false, false)
        if object ~= 0 then
            -- call RemoveGear function to use "spawned_object = {}" on that function, u can skip this.
            RemoveGear(realWeapon.name)
            -- re-delete object few times, coz sometimes object still available
            -- (5 is just small number, sometimes DeleteObject still not work, re /fixhar until everything looks okay)
            for x = 1, 5 do
                DeleteObject(object)
            end
        end
    end

    -- make sure to re-attach all owned weapon
    SetGears()
end

-- register /fixchar command to clear not owned weapon
RegisterCommand('fixchar', function(source)
    fixchar()
end)

AddEventHandler('playerDropped', function()
	local playerId = source
	-- Did the player ever join?
	if playerId then
        RemoveGears()
    end
end)
