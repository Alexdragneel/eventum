function LimitSpeed(model, vehicle)
    if model == -1842748181 or model == 55628203 or model == -1289178744 or model == GetHashKey('faggio4') then --Faggio
        SetVehicleMaxSpeed(vehicle, (70 / 3.6))
    elseif model == 92612664 then -- kalahari
        SetVehicleMaxSpeed(vehicle, (90 / 3.6))
    end
end

Citizen.CreateThread(function()
    local ped = nil
    local vehicle = nil
    local inVeh = false
    local wasInVeh = false

    while true do
        ped = GetPlayerPed(-1)
        inVeh = IsPedInAnyVehicle(ped, false)
        if inVeh then
            vehicle = GetVehiclePedIsIn(ped, false)
            local model = GetEntityModel(vehicle)
            LimitSpeed(model, vehicle)
        end
        wasInVeh = inVeh
        Citizen.Wait(1000)
    end
end)
