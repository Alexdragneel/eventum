local IndicatorL = false
local IndicatorR = false

AddEventHandler('Indicator', function(dir)
    Citizen.CreateThread(function()
        local Ped = GetPlayerPed(-1)
        if IsPedInAnyVehicle(Ped, true) then
            local Veh = GetVehiclePedIsIn(Ped, false)
            if GetPedInVehicleSeat(Veh, -1) == Ped then
                if dir == 'left' then
                    IndicatorL = not IndicatorL
                    TriggerServerEvent('IndicatorL', IndicatorL)
                elseif dir == 'right' then
                    IndicatorR = not IndicatorR
                    TriggerServerEvent('IndicatorR', IndicatorR)
                end
            end
        end
    end)
end)

AddEventHandler('Siren', function(hasSiren)
    Citizen.CreateThread(function()
        local Ped = GetPlayerPed(-1)
        if IsPedInAnyVehicle(Ped, true) then
            local veh = GetVehiclePedIsIn(Ped, false)
            if GetPedInVehicleSeat(veh, -1) == Ped then
                local vehNetId = NetworkGetNetworkIdFromEntity(veh)
                TriggerServerEvent('SyncSiren', hasSiren, vehNetId)
            end
        end
    end)
end)

RegisterNetEvent('updateIndicators')
AddEventHandler('updateIndicators', function(PID, dir, Toggle)
    -- if isPlayerOnline(PID) then
    local Veh = GetVehiclePedIsIn(GetPlayerPed(GetPlayerFromServerId(PID)), false)
    if dir == 'left' then
        SetVehicleIndicatorLights(Veh, 1, Toggle)
    elseif dir == 'right' then
        SetVehicleIndicatorLights(Veh, 0, Toggle)
    end
    -- end
end)

RegisterNetEvent('updateSiren')
AddEventHandler('updateSiren', function(vehNetId, hasSiren)
    -- if isPlayerOnline(PID) then
    local veh = NetworkGetEntityFromNetworkId(vehNetId)
    if veh then
        if hasSiren == true then
            SetVehicleHasMutedSirens(veh, false)
        else
            SetVehicleHasMutedSirens(veh, true)
        end
    end
    -- end
end)

RegisterCommand('indicator_left', function()
    if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
        TriggerEvent('Indicator', 'left')
    end
end, false)

RegisterKeyMapping('indicator_left', 'Clignotant gauche', 'keyboard', '1')

RegisterCommand('indicator_right', function()
    if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
        TriggerEvent('Indicator', 'right')
    end
end, false)

RegisterKeyMapping('indicator_right', 'Clignotant droit', 'keyboard', '\4')

RegisterCommand('code2', function()
    if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
        TriggerEvent('Siren', false)
    end
end, false)

RegisterKeyMapping('code2', 'Sirène code 2', 'keyboard', '2')

RegisterCommand('code3', function()
    if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
        TriggerEvent('Siren', true)
    end
end, false)

RegisterKeyMapping('code3', 'Sirène code 3', 'keyboard', '3')
