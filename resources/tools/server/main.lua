RegisterServerEvent('IndicatorL')
RegisterServerEvent('IndicatorR')
RegisterServerEvent('SyncSiren')

AddEventHandler('IndicatorL', function(IndicatorL)
    local netID = source
    TriggerClientEvent('updateIndicators', -1, netID, 'left', IndicatorL)
end)

AddEventHandler('IndicatorR', function(IndicatorR)
    local netID = source
    TriggerClientEvent('updateIndicators', -1, netID, 'right', IndicatorR)
end)

AddEventHandler('SyncSiren', function(hasSiren, vehNetId)
    TriggerClientEvent('updateSiren', -1, vehNetId, hasSiren)
end)
