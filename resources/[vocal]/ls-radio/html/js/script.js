var documentWidth = document.documentElement.clientWidth;
var documentHeight = document.documentElement.clientHeight;

var canals = {
    first: [0,0,0,0],
    second: [0,0,0,0]
}

var soundSettings = {
    first: {
        balance: 0,
        volume: 0,
    },
    second: {
        balance: 0,
        volume: 0,
    },
}

function Click(x, y) {
    var element = $(document.elementFromPoint(x, y));
    element.focus().click();
}

$(function() {
    window.addEventListener('message', function(event) {
        switch (event.data.type) {
            case 'enableui':
                document.body.style.display = event.data.enable ? "block" : "none";
                break;

            case 'joinedRadio':
                if (event.data.which === 'first') {
                    $('span#status_first').removeClass('disconnected');
                    $('span#status_first').addClass('connected_first');
                } else if (event.data.which === 'second') {
                    $('span#status_second').removeClass('disconnected');
                    $('span#status_second').addClass('connected_second');
                }
                break;

            case 'leftRadio':
                if (event.data.which === 'first') {
                    $('span#status_first').removeClass('connected_first');
                    $('span#status_first').addClass('disconnected');
                } else if (event.data.which === 'second') {
                    $('span#status_second').removeClass('connected_second');
                    $('span#status_second').addClass('disconnected');
                }
                break;
        }
    });

    document.onkeyup = function (data) {
        if (data.which == 114 || data.which == 8) { // Escape or backspace key
            $.post('http://ls-radio/escape', JSON.stringify({}));
        }
    };

    $("#channel").keydown(function(e) {
        const val = $("#channel").val();
        if (e.key !== "." && isNaN(e.key) && e.keyCode !== 8) {
            e.preventDefault();
        } else if (e.key === "." && val.indexOf(e.key) !== -1) {
            e.preventDefault();
        }
    });

    $('.chevron-top').click(function(e) {
        const input = $(`input#channel${e.target.dataset.freq}`)
        const curFreq = parseInt(input.val());
        if (curFreq < 9) {
            input.val(curFreq + 1)
            const selectedCanal = $(`input#selectedchannel`).val()
            const digit = parseInt(e.target.dataset.freq)
            canals[selectedCanal][digit - 1] = curFreq + 1
        }
    });

    $('.chevron-bottom').click(function(e) {
        const input = $(`input#channel${e.target.dataset.freq}`);
        const curFreq = parseInt(input.val());
        if (curFreq > 0) {
            input.val(curFreq - 1)
            const selectedCanal = $(`input#selectedchannel`).val()
            const digit = parseInt(e.target.dataset.freq)
            canals[selectedCanal][digit - 1] = curFreq - 1
        }
    });

    $(document).on('input', 'input#balance', function() {
        const selectedCanal = $(`input#selectedchannel`).val()
        soundSettings[selectedCanal].balance = parseInt($(this).val())

        $.post('http://ls-radio/updateSettings', JSON.stringify({
            balance: soundSettings[selectedCanal].balance,
            which: selectedCanal,
        }));
    });

    $(document).on('input', 'input#volume', function() {
        const selectedCanal = $(`input#selectedchannel`).val()
        soundSettings[selectedCanal].volume = parseInt($(this).val())

        $.post('http://ls-radio/updateSettings', JSON.stringify({
            volume: soundSettings[selectedCanal].volume,
            which: selectedCanal,
        }));
    });

    $('button#canal1').click(function(e) {
        $('button#canal2').removeClass('selected');
        $('button#canal1').addClass('selected');
        $(`input#selectedchannel`).val('first');
        $(`input#channel1`).val(canals.first[0]);
        $(`input#channel2`).val(canals.first[1]);
        $(`input#channel3`).val(canals.first[2]);
        $(`input#channel4`).val(canals.first[3]);
        $(`input#balance`).val(soundSettings.first.balance);
        $(`input#volume`).val(soundSettings.first.volume);
    });

    $('button#canal2').click(function(e) {
        $('button#canal1').removeClass('selected');
        $('button#canal2').addClass('selected');
        $(`input#selectedchannel`).val('second');
        $(`input#channel1`).val(canals.second[0]);
        $(`input#channel2`).val(canals.second[1]);
        $(`input#channel3`).val(canals.second[2]);
        $(`input#channel4`).val(canals.second[3])
        $(`input#balance`).val(soundSettings.second.balance);
        $(`input#volume`).val(soundSettings.second.volume);
    });

    $("#change-channel").submit(function(e) {
        e.preventDefault(); // Prevent form from submitting

        if (e.originalEvent.submitter.name === "disconnect") {
            $.post('http://ls-radio/leaveRadio', JSON.stringify({
                which: $("#selectedchannel").val()
            }));
        } else {
            // connect
            const input1 = $(`input#channel1`);
            const input2 = $(`input#channel2`);
            const input3 = $(`input#channel3`);
            const input4 = $(`input#channel4`);
            const channel = `${input1.val()}${input2.val()}${input3.val()}.${input4.val()}`

            $.post('http://ls-radio/joinRadio', JSON.stringify({
                channel: channel,
                which: $("#selectedchannel").val(),
            }));
        }

        
    });

    $("#onoff").submit(function(e) {
        e.preventDefault(); // Prevent form from submitting

        $.post('http://ls-radio/leaveRadio', JSON.stringify({

        }));
    });
});
