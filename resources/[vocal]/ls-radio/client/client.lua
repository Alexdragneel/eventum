
--===============================================================================
--=== Stworzone przez Alcapone aka suprisex. Zakaz rozpowszechniania skryptu! ===
--===================== na potrzeby LS-Story.pl =================================
--===============================================================================


-- ESX

ESX = nil
local PlayerData                = {}
local IsDead = false
local radioProp = 0
local radioModel = -1964402432

local currentStatus = 'out'
local lastDict = nil
local lastAnim = nil
local lastFlag = nil
local lastIsFreeze = false
local pauseAnim = false

local ANIMS = {
	['cellphone@'] = {
		['out'] = {
			['text'] = 'cellphone_cellphone_intro',
		},
		['text'] = {
			['out'] = 'cellphone_cellphone_outro',
		}
	},
	['cellphone@in_car@ps'] = {
		['out'] = {
			['text'] = 'cellphone_text_in',
		},
		['text'] = {
			['out'] = 'cellphone_text_out',
		}
	}
}

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)


local radioMenu = false

function PrintChatMessage(text)
    TriggerEvent('chatMessage', "system", { 255, 0, 0 }, text)
end

function newRadioProp()
	deleteRadio()
	RequestModel(radioModel)
	while not HasModelLoaded(radioModel) do
		Citizen.Wait(1)
	end
	radioProp = CreateObject(radioModel, 1.0, 1.0, 1.0, 1, 1, 0)
	local bone = GetPedBoneIndex(myPedId, 28422)
	local isUnarmed = GetCurrentPedWeapon(myPedId, 0xA2719263)
	if not isUnarmed then
		AttachEntityToEntity(radioProp, myPedId, bone, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
	else
		SetCurrentPedWeapon(myPedId, 0xA2719263, true)
		AttachEntityToEntity(radioProp, myPedId, bone, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
	end
end

function deleteRadio()
	if radioProp ~= 0 then
		Citizen.InvokeNative(0xAE3CBE5BF394C9C9 , Citizen.PointerValueIntInitialized(radioProp))
		radioProp = 0
	end
end

function enableRadio(enable)

  SetNuiFocus(true, true)
  radioMenu = enable

  SendNUIMessage({

    type = "enableui",
    enable = enable

  })

  if enable then
    RadioPlayIn()
  else
    RadioPlayOut()
  end
end

function RadioPlayAnim (status, freeze, force)
	if currentStatus == status and force ~= true then
		return
	end

	myPedId = GetPlayerPed(-1)
	local freeze = freeze or false

	local dict = "cellphone@"
	if IsPedInAnyVehicle(myPedId, false) then
		dict = "cellphone@in_car@ps"
	end

  RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end

	local anim = ANIMS[dict][currentStatus][status]
	if currentStatus ~= 'out' then
		StopAnimTask(myPedId, lastDict, lastAnim, 1.0)
	end
	local flag = 50
	if freeze == true then
		flag = 14
	end
	TaskPlayAnim(myPedId, dict, anim, 3.0, -1, -1, flag, 0, false, false, false)

	if status ~= 'out' and currentStatus == 'out' then
		Citizen.Wait(380)
		newRadioProp()
	end

	lastDict = dict
	lastAnim = anim
	lastIsFreeze = freeze
	lastFlag = flag
	currentStatus = status

	if status == 'out' then
		Citizen.Wait(180)
		deleteRadio()
		StopAnimTask(myPedId, lastDict, lastAnim, 1.0)
	end

end

function RadioPlayOut ()
	RadioPlayAnim('out')
end

function RadioPlayIn () 
	if currentStatus == 'out' then
		RadioPlayAnim('text')
	end
end

RegisterNUICallback('joinRadio', function(data, cb)
    local _source = source
    local PlayerData = ESX.GetPlayerData(_source)
    local playerServerId = GetPlayerServerId(PlayerId())
    local whichChannel = "radio:channel" .. "_" .. data.which -- radio:channel_first or radio:channel_second
    local getPlayerRadioChannel = exports.tokovoip_script:getPlayerData(playerServerId, whichChannel)
    local channel = math.floor(tonumber(data.channel) * 10)

    if channel >= 1 then
      if channel ~= tonumber(getPlayerRadioChannel) then
        if tonumber(getPlayerRadioChannel) ~= nil then
          exports.tokovoip_script:removePlayerFromRadio(getPlayerRadioChannel, data.which)
        end
        exports.tokovoip_script:setPlayerData(playerServerId, whichChannel, channel, true)
        exports.tokovoip_script:addPlayerToRadio(channel, data.which)
      end
    end
    cb('ok')
end)

RegisterNUICallback('updateSettings', function(data, cb)
  exports.tokovoip_script:updateSettings(data)

  cb('ok')
end)

-- opuszczanie radia

RegisterNUICallback('leaveRadio', function(data, cb)
  TriggerEvent('ls-radio:onRadioLost', data)
  cb('ok')
end)

RegisterNetEvent('ls-radio:onPlayerJoinChannel', function (which)
  SendNUIMessage({
    type = "joinedRadio",
    which = which
  })
end)

RegisterNetEvent('ls-radio:onRadioLost')
AddEventHandler('ls-radio:onRadioLost', function(data)
  local playerServerId = GetPlayerServerId(PlayerId())
  local getPlayerRadioChannel1 = exports.tokovoip_script:getPlayerData(playerServerId, "radio:channel_first")
  local getPlayerRadioChannel2 = exports.tokovoip_script:getPlayerData(playerServerId, "radio:channel_second")

  if getPlayerRadioChannel1 ~= -1 and (data == nil or data.which == nil or data.which == "first") then
    exports.tokovoip_script:removePlayerFromRadio(getPlayerRadioChannel1, "first")
    exports.tokovoip_script:setPlayerData(playerServerId, "radio:channel_first", -1, true)
    SendNUIMessage({
      type = "leftRadio",
      which = "first"
    })
  end
  if getPlayerRadioChannel2 ~= -1 and (data == nil or data.which == nil or data.which == "second") then
    exports.tokovoip_script:removePlayerFromRadio(getPlayerRadioChannel2, "second")
    exports.tokovoip_script:setPlayerData(playerServerId, "radio:channel_second", -1, true)
    SendNUIMessage({
      type = "leftRadio",
      which = "second"
    })
  end
end)

RegisterNUICallback('escape', function(data, cb)

    enableRadio(false)
    SetNuiFocus(false, false)


    cb('ok')
end)

-- net eventy

RegisterNetEvent('ls-radio:use')
AddEventHandler('ls-radio:use', function()
  enableRadio(true)
end)

RegisterNetEvent('ls-radio:onRadioDrop')
AddEventHandler('ls-radio:onRadioDrop', function()
  local playerServerId = GetPlayerServerId(PlayerId())
  local getPlayerRadioChannel1 = exports.tokovoip_script:getPlayerData(playerServerId, "radio:channel_first")
  local getPlayerRadioChannel2 = exports.tokovoip_script:getPlayerData(playerServerId, "radio:channel_second")

  if getPlayerRadioChannel1 ~= -1 then
    exports.tokovoip_script:removePlayerFromRadio(getPlayerRadioChannel1, "first")
    exports.tokovoip_script:setPlayerData(playerServerId, "radio:channel_first", -1, true)
  end
  if getPlayerRadioChannel1 ~= -1 then
    exports.tokovoip_script:removePlayerFromRadio(getPlayerRadioChannel2, "second")
    exports.tokovoip_script:setPlayerData(playerServerId, "radio:channel_second", -1, true)
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
	IsDead = true
  if radioMenu then
    enableRadio(false)
    RadioPlayOut()
  end
end)

RegisterNetEvent('ragdoll:playerRagdolling')
AddEventHandler('ragdoll:playerRagdolling', function ()
  if radioMenu then
    enableRadio(false)
    RadioPlayOut()
  end
end)

AddEventHandler('esx:onPlayerSpawn', function(spawn)
	IsDead = false
end)

function hasRadio(cb)
  if (ESX == nil) then return cb(0) end
  ESX.TriggerServerCallback('gcphone:getItemAmount', function(qtty)
    cb(qtty > 0)
  end, 'radio')
end

function OpenRadio()
  if not IsDead then
    hasRadio(function (hasRadio)
      if hasRadio == true and not radioMenu then
        enableRadio(true)
      end
    end)
  end
end

Citizen.CreateThread(function()
  while true do   
    if radioMenu then
        DisableControlAction(0, 1, guiEnabled) -- LookLeftRight
        DisableControlAction(0, 2, guiEnabled) -- LookUpDown

        DisableControlAction(0, 142, guiEnabled) -- MeleeAttackAlternate

        DisableControlAction(0, 106, guiEnabled) -- VehicleMouseControlOverride
    end
    if IsControlJustReleased(1, Config.KeyOpen) then -- On key press, will open the phone
      OpenRadio()
    end
    Citizen.Wait(0)
  end
end)
