fx_version "adamant"

game "gta5"

description "LS Radio"

version "1.0.0"

client_scripts {
  'config.lua',
  'client/client.lua'
}

server_scripts {
  'config.lua',
  'server/server.lua'
}

ui_page 'html/ui.html'

files {
    'html/ui.html',
    'html/js/script.js',
    'html/css/style.css',
    'html/img/chevron.png',
    'html/img/radio.png',
    'html/fonts/DS-DIGI.TTF',
    'html/fonts/DS-DIGIB.TTF',
    'html/fonts/DS-DIGII.TTF',
    'html/fonts/DS-DIGIT.TTF'
}
