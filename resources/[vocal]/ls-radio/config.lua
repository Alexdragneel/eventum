Config = {}

Config.enableCmd = false --  /radio command should be active or not (if not you have to carry the item "radio") true / false
Config.KeyOpen = 170 -- F3

Config.messages = {

  ['not_on_radio'] = 'Radio déconnectée',
  ['on_radio'] = 'Vous êtes sur le canal : <b>',
  ['joined_to_radio'] = 'Radio connectée sur le canal: <b>',
  ['restricted_channel_error'] = 'Vous ne pouvez pas vous connecter à un canal sécurisé',
  ['you_on_radio'] = 'Vous êtes déjà sur le canal: <b>',
  ['you_leave'] = 'Vous avez quitté le canal: <b>',
  ['no_radio'] = 'Vous n\'avez pas de ~r~radio~s~.',
}
