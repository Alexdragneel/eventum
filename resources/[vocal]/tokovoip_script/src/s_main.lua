------------------------------------------------------------
------------------------------------------------------------
---- Author: Dylan 'Itokoyamato' Thuillier              ----
----                                                    ----
---- Email: itokoyamato@hotmail.fr                      ----
----                                                    ----
---- Resource: tokovoip_script                          ----
----                                                    ----
---- File: s_main.lua                                   ----
------------------------------------------------------------
------------------------------------------------------------

--------------------------------------------------------------------------------
--	Server: radio functions
--------------------------------------------------------------------------------

function getRadioChannels()
	local _channels = {}
	for i = 1, 9999 do
		table.insert(_channels, i, {name = (i / 10) + 0.0 .. " MHz", subscribers = {}})
	end
	return _channels
end
local channels = getRadioChannels();
local phoneCalls = {}
local serverId;

SetConvarReplicated("gametype", "gta5");

RegisterServerEvent("TokoVoip:getActivePlayers");
AddEventHandler("TokoVoip:getActivePlayers", function (cb)
	TriggerClientEvent("TokoVoip:getActivePlayersCb", source, GetPlayers())
end)

function addPlayerToRadio(channelId, which, playerServerId)
	if (not channels[channelId]) then
		channels[channelId] = {id = channelId, name = (channelId / 10) + 0.0 .. " MHz", subscribers = {}};
	end
	if (not channels[channelId].id) then
		channels[channelId].id = channelId;
	end

	channels[channelId].subscribers[playerServerId] = playerServerId;
	print("[Radio] Added [" .. playerServerId .. "] " .. (GetPlayerName(playerServerId) or "") .. " to channel " .. channelId);

	for _, subscriberServerId in pairs(channels[channelId].subscribers) do
		if (subscriberServerId ~= playerServerId) then
			TriggerClientEvent("TokoVoip:onPlayerJoinChannel", subscriberServerId, channelId, which, playerServerId);
		else
			-- Send whole channel data to new subscriber
			TriggerClientEvent("TokoVoip:onPlayerJoinChannel", subscriberServerId, channelId, which, playerServerId, channels[channelId]);
		end
	end
end
RegisterServerEvent("TokoVoip:addPlayerToRadio");
AddEventHandler("TokoVoip:addPlayerToRadio", addPlayerToRadio);

function addPlayerToPhoneCall(callId, caller, playerServerId)
	if phoneCalls[playerServerId] == nil then
		phoneCalls[playerServerId] = callId
	end

	print("[Phone] Started Call [" .. callId .. "] between [" .. playerServerId .. "] " .. (GetPlayerName(playerServerId) or "") .. " and [" .. caller .. "] " .. (GetPlayerName(caller) or ""));

	TriggerClientEvent("TokoVoip:onPlayerJoinPhoneCall", playerServerId, callId, caller, playerServerId);
end
RegisterServerEvent("TokoVoip:addPlayerToPhoneCall");
AddEventHandler("TokoVoip:addPlayerToPhoneCall", addPlayerToPhoneCall);

function removePlayerFromRadio(channelId, which, playerServerId)
	if (channels[channelId] and channels[channelId].subscribers[playerServerId]) then
		channels[channelId].subscribers[playerServerId] = nil;
		if (tablelength(channels[channelId].subscribers) == 0) then
			channels[channelId] = nil;
		end
		print("[Radio] Removed [" .. playerServerId .. "] " .. (GetPlayerName(playerServerId) or "") .. " from channel " .. channelId);

		-- Tell unsubscribed player he's left the channel as well
		TriggerClientEvent("TokoVoip:onPlayerLeaveChannel", playerServerId, channelId, which, playerServerId);

		-- Channel does not exist, no need to update anyone else
		if (not channels[channelId]) then return end

		for _, subscriberServerId in pairs(channels[channelId].subscribers) do
			TriggerClientEvent("TokoVoip:onPlayerLeaveChannel", subscriberServerId, channelId, which, playerServerId);
		end
	end
end
RegisterServerEvent("TokoVoip:removePlayerFromRadio");
AddEventHandler("TokoVoip:removePlayerFromRadio", removePlayerFromRadio);

function removePlayerFromPhoneCall(callId, caller, playerServerId)
	if phoneCalls[playerServerId] == callId then
		phoneCalls[playerServerId] = nil
		print("[Phone Call] Finished Call [" .. callId .. "] between [" .. playerServerId .. "] " .. (GetPlayerName(playerServerId) or "") .. " and [" .. caller .. "] " .. (GetPlayerName(caller) or ""));
		
		TriggerClientEvent("TokoVoip:onPlayerLeavePhoneCall", playerServerId, callId, playerServerId);
		TriggerClientEvent("TokoVoip:onPlayerLeavePhoneCall", caller, callId, caller);
	end
end
RegisterServerEvent("TokoVoip:removePlayerFromPhoneCall");
AddEventHandler("TokoVoip:removePlayerFromPhoneCall", removePlayerFromPhoneCall);

function removePlayerFromAllRadio(playerServerId)
	for channelId, channel in pairs(channels) do
		if (channel.subscribers[playerServerId]) then
			removePlayerFromRadio(channelId, "first", playerServerId);
			removePlayerFromRadio(channelId, "second", playerServerId);
		end
	end
end
RegisterServerEvent("TokoVoip:removePlayerFromAllRadio");
AddEventHandler("TokoVoip:removePlayerFromAllRadio", removePlayerFromAllRadio);

AddEventHandler("playerDropped", function()
	removePlayerFromAllRadio(source);
end);

function getServerId() TriggerClientEvent("TokoVoip:onClientGetServerId", source, serverId); end
RegisterServerEvent("TokoVoip:getServerId");
AddEventHandler("TokoVoip:getServerId", getServerId);

function getEnvironment() TriggerClientEvent("TokoVoip:onClientGetEnvironment", source, GetConvar('lp_environment', 'dev')); end
RegisterServerEvent("TokoVoip:getEnvironment");
AddEventHandler("TokoVoip:getEnvironment", getEnvironment);

AddEventHandler("onResourceStart", function(resource)
	if (resource ~= GetCurrentResourceName()) then return end;
	serverId = randomString(32);
	print("TokoVOIP FiveM Server ID: " .. serverId);
end);