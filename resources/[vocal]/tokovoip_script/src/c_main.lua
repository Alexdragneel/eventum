------------------------------------------------------------
------------------------------------------------------------
---- Author: Dylan 'Itokoyamato' Thuillier              ----
----                                                    ----
---- Email: itokoyamato@hotmail.fr                      ----
----                                                    ----
---- Resource: tokovoip_script                          ----
----                                                    ----
---- File: c_main.lua                                   ----
------------------------------------------------------------
------------------------------------------------------------

--------------------------------------------------------------------------------
--	Client: Voip data processed before sending it to TS3Plugin
--------------------------------------------------------------------------------

local targetPed;
local useLocalPed = true;
local isRunning = false;
local scriptVersion = "1.5.6";
local animStates = {}
local displayingPluginScreen = false;
local HeadBone = 0x796e;
local radioVolume = 0;
local nuiLoaded = false

--------------------------------------------------------------------------------
--	Plugin functions
--------------------------------------------------------------------------------

-- Handles the talking state of other players to apply talking animation to them
local function setPlayerTalkingState(player, playerServerId)
	local talking = tonumber(getPlayerData(playerServerId, "voip:talking"));
	if (animStates[playerServerId] == 0 and talking == 1) then
		PlayFacialAnim(GetPlayerPed(player), "mic_chatter", "mp_facial");
	elseif (animStates[playerServerId] == 1 and talking == 0) then
		PlayFacialAnim(GetPlayerPed(player), "mood_normal_1", "facials@gen_male@base");
	end
	animStates[playerServerId] = talking;
end

local function PlayRedMFacialAnimation(player, animDict, animName)
	RequestAnimDict(animDict)
	while not HasAnimDictLoaded(animDict) do
		Wait(100)
	end
  SetFacialIdleAnimOverride(player, animName, animDict)
end

RegisterNUICallback("updatePluginData", function(data, cb)
	local payload = data.payload;
	if (voip[payload.key] == payload.data) then return end
	voip[payload.key] = payload.data;
	setPlayerData(voip.serverId, "voip:" .. payload.key, voip[payload.key], true);
	voip:updateConfig();
	voip:updateTokoVoipInfo(true);
	cb('ok');
end);

-- Receives data from the TS plugin on microphone toggle
RegisterNUICallback("setPlayerTalking", function(data, cb)
	voip.talking = tonumber(data.state);

	if (voip.talking == 1) then
		setPlayerData(voip.serverId, "voip:talking", 1, true);
		PlayFacialAnim(GetPlayerPed(PlayerId()), "mic_chatter", "mp_facial");
	else
		setPlayerData(voip.serverId, "voip:talking", 0, true);
		PlayFacialAnim(PlayerPedId(), "mood_normal_1", "facials@gen_male@base");
	end
	cb('ok');
end)

RegisterNUICallback("setPlayerMuted", function(data, cb)
	voip.localMuted = tonumber(data.state);

	if (voip.localMuted == 1) then
		setPlayerData(voip.serverId, "voip:localMuted", 1, true);
	else
		setPlayerData(voip.serverId, "voip:localMuted", 0, true);
	end
	cb('ok');
end)

local function clientProcessing()
	local playerList = voip.playerList;
	local usersdata = {};
	local localHeading;
	local ped = PlayerPedId();

	if (voip.headingType == 1) then
		localHeading = math.rad(GetEntityHeading(ped));
	else
		localHeading = math.rad(GetGameplayCamRot().z % 360);
	end
	local localPos;

	if useLocalPed then
		localPos = GetPedBoneCoords(ped, HeadBone);
	else
		localPos = GetPedBoneCoords(targetPed, HeadBone);
	end

	for _, playerId in ipairs(playerList) do
		local playerServerId = tonumber(playerId)
		local player = GetPlayerFromServerId(playerServerId)
		local playerPed = GetPlayerPed(player);
		local playerTalking = getPlayerData(playerServerId, "voip:talking");
			
		if player ~= -1 then
			setPlayerTalkingState(player, playerServerId);
		end
		
		if (voip.serverId == playerServerId or not playerPed or not playerTalking or playerTalking == 0) then goto continue end

		local playerPos = GetPedBoneCoords(playerPed, HeadBone);
		local dist = #(localPos - playerPos);
		local isOnPhoneWith = false

		if voip.myPhoneCall.callId then
			if voip.myPhoneCall.caller == playerServerId then
				isOnPhoneWith = true
			end
		end

		if ((player == -1 or dist > voip.distance[3]) and not isOnPhoneWith) then goto continue end

		local posX = 0
		local posY = 0
		local posZ = 0
		local volume = 0
		local muted = 1

		if not isOnPhoneWith and player ~= -1 then
			--	Process the volume for proximity voip
			if (not getPlayerData(playerServerId, "voip:mode")) then
				setPlayerData(playerServerId, "voip:mode", 1);
			end
			local mode = tonumber(getPlayerData(playerServerId, "voip:mode"));
			if (not mode or (mode ~= 1 and mode ~= 2 and mode ~= 3)) then mode = 1 end;
			volume = -30 + (30 - dist / voip.distance[mode] * 30);
			if (volume >= 0) then
				volume = 0;
			end

			local angleToTarget = localHeading - math.atan(playerPos.y - localPos.y, playerPos.x - localPos.x);
			posX = voip.plugin_data.enableStereoAudio and math.cos(angleToTarget) * dist or 0
			posY = voip.plugin_data.enableStereoAudio and math.sin(angleToTarget) * dist or 0
			posZ = voip.plugin_data.enableStereoAudio and playerPos.z or 0

			-- Process proximity
			if (dist >= voip.distance[mode]) then
				muted = 1;
			else
				muted = 0;
			end
		else
			local playerMuted = getPlayerData(playerServerId, "phone:muted")
			if playerMuted then
				muted = 1
			else
				muted = 0
			end
			volume = radioVolume
			posX = 0
			posY = 0
			posZ = voip.plugin_data.enableStereoAudio and localPos.z or 0
		end
		

		-- Set player's position
		local userData = {
			uuid = getPlayerData(playerServerId, "voip:pluginUUID"),
			volume = volume,
			muted = muted,
			radioEffect = false,
			posX = posX,
			posY = posY,
			posZ = posZ
		};
		--

		usersdata[#usersdata + 1] = userData;

		::continue::
	end

	-- Process channels
	for _, channel in pairs(voip.myChannels) do
		for _, subscriber in pairs(channel.subscribers) do
			if (subscriber == voip.serverId) then goto channelContinue end

			local remotePlayerUsingRadio1 = getPlayerData(subscriber, "radio:talking_first");
			local remotePlayerChannel1 = getPlayerData(subscriber, "radio:channel_first");
			local remotePlayerUsingRadio2 = getPlayerData(subscriber, "radio:talking_second");
			local remotePlayerChannel2 = getPlayerData(subscriber, "radio:channel_second");

			if (not remotePlayerUsingRadio1 or remotePlayerChannel1 ~= channel.id) and (not remotePlayerUsingRadio2 or remotePlayerChannel2 ~= channel.id) then
				goto channelContinue
			end

			local remotePlayerUuid = getPlayerData(subscriber, "voip:pluginUUID");
			
			local posX = 0
			local volume = 0
			if channel.id == voip.plugin_data.radioChannel.first then
				posX = voip.plugin_data.radioSettings.first.balance
				volume = voip.plugin_data.radioSettings.first.volume
			else
				posX = voip.plugin_data.radioSettings.second.balance
				volume = voip.plugin_data.radioSettings.second.volume
			end

			local userData = {
				uuid = remotePlayerUuid,
				radioEffect = false,
				muted = false,
				volume = volume,
				posX = posX,
				posY = 0,
				posZ = voip.plugin_data.enableStereoAudio and localPos.z or 0
			};

			local remoteChannel = nil

			if remotePlayerUsingRadio1 then 
				remoteChannel = remotePlayerChannel1
			else
				remoteChannel = remotePlayerChannel2
			end

			if ((type(remoteChannel) == "number" and remoteChannel <= voip.config.radioClickMaxChannel) or channel.radio) then
				userData.radioEffect = true;
			end

			for k, v in pairs(usersdata) do
				if (v.uuid == remotePlayerUuid) then
					usersdata[k] = userData;
					goto channelContinue;
				end
			end

			usersdata[#usersdata + 1] = userData;

			::channelContinue::
		end
	end

	voip.plugin_data.Users = usersdata; -- Update TokoVoip's data
	voip.plugin_data.posX = 0;
	voip.plugin_data.posY = 0;
	voip.plugin_data.posZ = voip.plugin_data.enableStereoAudio and localPos.z or 0;
end

RegisterNetEvent("initializeVoip");
AddEventHandler("initializeVoip", function()
	Citizen.Wait(1000);
	if (isRunning) then return Citizen.Trace("TokoVOIP is already running\n"); end
	isRunning = true;

	voip = TokoVoip:init(TokoVoipConfig); -- Initialize TokoVoip and set default settings

	-- Variables used script-side
	voip.plugin_data.Users = {};
	voip.plugin_data.whichChannel = 0;
	voip.plugin_data.radioTalking = false;
	voip.plugin_data.radioChannel = {
		first = -1,
		second = -1
	}
	voip.plugin_data.radioSettings = {
		first = {
			volume = 0,
			balance = 0
		},
		second = {
			volume = 0,
			balance = 0
		},
	}
	voip.plugin_data.localRadioClicks = false;
	voip.mode = 1;
	voip.talking = false;
	voip.localMuted = false;
	voip.pluginStatus = -1;
	voip.pluginVersion = "0";
	voip.serverId = GetPlayerServerId(PlayerId());

	-- Radio channels
	voip.myChannels = {};
	voip.myPhoneCall = {};

	-- Player data shared on the network
	setPlayerData(voip.serverId, "voip:mode", voip.mode, true);
	setPlayerData(voip.serverId, "voip:talking", voip.talking, true);
	setPlayerData(voip.serverId, "voip:localMuted", voip.localMuted, true);
	setPlayerData(voip.serverId, "radio:channel_first", voip.plugin_data.radioChannel.first, true);
	setPlayerData(voip.serverId, "radio:channel_second", voip.plugin_data.radioChannel.second, true);
	setPlayerData(voip.serverId, "radio:talking_first", voip.plugin_data.radioTalking, true);
	setPlayerData(voip.serverId, "radio:talking_second", voip.plugin_data.radioTalking, true);
	setPlayerData(voip.serverId, "voip:pluginStatus", voip.pluginStatus, true);
	setPlayerData(voip.serverId, "voip:pluginVersion", voip.pluginVersion, true);
	refreshAllPlayerData();

	-- Set targetped (used for spectator mod for admins)
	targetPed = GetPlayerPed(-1);

	-- Request this stuff here only one time
	RequestAnimDict("mp_facial");
	RequestAnimDict("facials@gen_male@base");


	Citizen.Trace("TokoVoip: Initialized script (" .. scriptVersion .. ")\n");

	local response;
	Citizen.CreateThread(function()
		local function handler(serverId) response = serverId or "N/A"; end
		RegisterNetEvent("TokoVoip:onClientGetServerId");
		AddEventHandler("TokoVoip:onClientGetServerId", handler);
		TriggerServerEvent("TokoVoip:getServerId");
		while (not response) do Wait(5) end

		voip.fivemServerId = response;
		print("TokoVoip: FiveM Server ID is " .. voip.fivemServerId);

		voip.processFunction = clientProcessing; -- Link the processing function that will be looped
		while not nuiLoaded do
			voip:initialize(); -- Initialize the websocket and controls
			Citizen.Wait(5000)
		end
		voip:loop(); -- Start TokoVoip's loop
	end);
end)
--------------------------------------------------------------------------------
--	Radio functions
--------------------------------------------------------------------------------

function addPlayerToRadio(channel, which)
	TriggerServerEvent("TokoVoip:addPlayerToRadio", channel, which, voip.serverId);
end
RegisterNetEvent("TokoVoip:addPlayerToRadio");
AddEventHandler("TokoVoip:addPlayerToRadio", addPlayerToRadio);

function addPlayerToPhoneCall(callId, caller)
	TriggerServerEvent("TokoVoip:addPlayerToPhoneCall", callId, caller, voip.serverId);
end
RegisterNetEvent("TokoVoip:addPlayerToPhoneCall");
AddEventHandler("TokoVoip:addPlayerToPhoneCall", addPlayerToPhoneCall);

function removePlayerFromRadio(channel, which)
	TriggerServerEvent("TokoVoip:removePlayerFromRadio", channel, which, voip.serverId);
end
RegisterNetEvent("TokoVoip:removePlayerFromRadio");
AddEventHandler("TokoVoip:removePlayerFromRadio", removePlayerFromRadio);

function updateSettings(data)
	if data.volume then
		voip.plugin_data.radioSettings[data.which].volume = data.volume
	end
	if data.balance then
		voip.plugin_data.radioSettings[data.which].balance = tonumber(data.balance) / 100 + 0.0
	end
end

function removePlayerFromPhoneCall(infoCall)
	TriggerServerEvent("TokoVoip:removePlayerFromPhoneCall", infoCall.callId, infoCall.caller, voip.serverId);
end
RegisterNetEvent("TokoVoip:removePlayerFromPhoneCall");
AddEventHandler("TokoVoip:removePlayerFromPhoneCall", removePlayerFromPhoneCall);

RegisterNetEvent("TokoVoip:onPlayerLeaveChannel");
AddEventHandler("TokoVoip:onPlayerLeaveChannel", function(channelId, which, playerServerId)
	-- Local player left channel
	if (playerServerId == voip.serverId and voip.myChannels[channelId]) then
		local previousChannel = voip.plugin_data.radioChannel[which];
		voip.myChannels[channelId] = nil;
		if (voip.plugin_data.radioChannel[which] == channelId) then
			voip.plugin_data.radioChannel[which] = -1;
		end

		if (previousChannel ~= voip.plugin_data.radioChannel[which]) then -- Update network data only if we actually changed radio channel
			setPlayerData(voip.serverId, "radio:channel" .. "_" .. which, voip.plugin_data.radioChannel[which], true);
		end

	-- Remote player left channel we are subscribed to
	elseif (voip.myChannels[channelId]) then
		voip.myChannels[channelId].subscribers[playerServerId] = nil;
	end
end)

RegisterNetEvent("TokoVoip:onPlayerLeavePhoneCall");
AddEventHandler("TokoVoip:onPlayerLeavePhoneCall", function(callId, playerServerId)
	if (playerServerId == voip.serverId) then
		voip.myPhoneCall = {};
		setPlayerData(voip.serverId, "phone:call", voip.myPhoneCall, true);
		setPlayerData(voip.serverId, "phone:muted", false, true);
	end
end)

RegisterNetEvent("TokoVoip:mutePhone");
AddEventHandler("TokoVoip:mutePhone", function(playerServerId)
	if (playerServerId == voip.serverId) then
		setPlayerData(voip.serverId, "phone:muted", true, true);
	end
end)

RegisterNetEvent("TokoVoip:unmutePhone");
AddEventHandler("TokoVoip:unmutePhone", function(playerServerId)
	if (playerServerId == voip.serverId) then
		setPlayerData(voip.serverId, "phone:muted", false, true);
	end
end)

RegisterNetEvent("TokoVoip:onPlayerJoinChannel");
AddEventHandler("TokoVoip:onPlayerJoinChannel", function(channelId, which, playerServerId, channelData)
	-- Local player joined channel
	if (playerServerId == voip.serverId and channelData) then
		local previousChannel = voip.plugin_data.radioChannel[which];

		voip.plugin_data.radioChannel[which] = channelData.id;
		voip.myChannels[channelData.id] = channelData;

		if (previousChannel ~= voip.plugin_data.radioChannel[which]) then -- Update network data only if we actually changed radio channel
			setPlayerData(voip.serverId, "radio:channel" .. "_" .. which, voip.plugin_data.radioChannel[which], true);
			TriggerEvent('ls-radio:onPlayerJoinChannel', which)
		end

	-- Remote player joined a channel we are subscribed to
	elseif (voip.myChannels[channelId]) then
		voip.myChannels[channelId].subscribers[playerServerId] = playerServerId;
	end
end)

RegisterNetEvent("TokoVoip:onPlayerJoinPhoneCall");
AddEventHandler("TokoVoip:onPlayerJoinPhoneCall", function(callId, caller, playerServerId)
	if (playerServerId == voip.serverId) then
		local previousCall = voip.myPhoneCall

		voip.myPhoneCall = {
			callId = callId,
			caller = caller
		}
		
		if (previousCall.callId ~= voip.myPhoneCall.callId) then
			setPlayerData(voip.serverId, "phone:call", voip.myPhoneCall, true);
		end
	end
end)

function setRadioVolume(volume)
	radioVolume = volume;
end
RegisterNetEvent('TokoVoip:setRadioVolume');
AddEventHandler('TokoVoip:setRadioVolume', setRadioVolume);

--------------------------------------------------------------------------------
--	Specific utils
--------------------------------------------------------------------------------

RegisterCommand("tokovoiplatency", function()
	SendNUIMessage({ type = "toggleLatency" });
end);

-- Toggle the blocking screen with usage explanation
-- Not used
function displayPluginScreen(toggle)
	if (displayingPluginScreen ~= toggle) then
		SendNUIMessage(
			{
				type = "displayPluginScreen",
				data = toggle
			}
		);
		displayingPluginScreen = toggle;
	end
end

-- Used for admin spectator feature
AddEventHandler("updateVoipTargetPed", function(newTargetPed, useLocal)
	targetPed = newTargetPed
	useLocalPed = useLocal
end)

-- Used to prevent bad nui loading
RegisterNUICallback("nuiLoaded", function(data, cb)
	nuiLoaded = true
	cb("ok")
end)

-- Make exports available on first tick
exports("addPlayerToRadio", addPlayerToRadio);
exports("addPlayerToPhoneCall", addPlayerToPhoneCall);
exports("removePlayerFromRadio", removePlayerFromRadio);
exports("removePlayerFromPhoneCall", removePlayerFromPhoneCall);
exports("setRadioVolume", setRadioVolume);
exports("updateSettings", updateSettings);