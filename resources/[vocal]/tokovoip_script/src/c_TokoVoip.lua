------------------------------------------------------------
------------------------------------------------------------
---- Author: Dylan 'Itokoyamato' Thuillier              ----
----                                                    ----
---- Email: itokoyamato@hotmail.fr                      ----
----                                                    ----
---- Resource: tokovoip_script                          ----
----                                                    ----
---- File: c_TokoVoip.lua                               ----\
------------------------------------------------------------
------------------------------------------------------------

--------------------------------------------------------------------------------
--	Client: TokoVoip functions
--------------------------------------------------------------------------------

TokoVoip = {};
TokoVoip.__index = TokoVoip;
local lastTalkState = false
local isDead = false

function TokoVoip.init(self, config)
	local self = setmetatable(config, TokoVoip);
	self.config = json.decode(json.encode(config));
	self.lastNetworkUpdate = 0;
	self.lastPlayerListUpdate = self.playerListRefreshRate;
	self.playerList = {};
	self.justSwitchedProximity = false;
	return (self);
end

function TokoVoip.loop(self)
	Citizen.CreateThread(function()
		while (true) do
			Citizen.Wait(self.refreshRate);
			self:processFunction();
			self:sendDataToTS3();

			self.lastNetworkUpdate = self.lastNetworkUpdate + self.refreshRate;
			self.lastPlayerListUpdate = self.lastPlayerListUpdate + self.refreshRate;
			if (self.lastNetworkUpdate >= self.networkRefreshRate) then
				self.lastNetworkUpdate = 0;
				self:updateTokoVoipInfo();
			end
			if (self.lastPlayerListUpdate >= self.playerListRefreshRate) then
				self.lastPlayerListUpdate = 0;
				local _playerList;
				local function handler(response) _playerList = response or {}; end
				RegisterNetEvent("TokoVoip:getActivePlayersCb");
				AddEventHandler("TokoVoip:getActivePlayersCb", handler);
				TriggerServerEvent("TokoVoip:getActivePlayers");
				while (not _playerList) do Citizen.Wait(5) end
				self.playerList = _playerList
			end
		end
	end);
end

function TokoVoip.sendDataToTS3(self) -- Send usersdata to the Javascript Websocket
	if (self.pluginStatus == -1) then return end;
	self:updatePlugin("updateTokoVoip", self.plugin_data);
end

function TokoVoip.updateTokoVoipInfo(self, forceUpdate) -- Update the top-left info
	local info = " "
	if (self.plugin_data.radioTalking) then
		if self.plugin_data.whichChannel == self.plugin_data.radioChannel.first then
			info = "<font class='base_talk radio_first'><i class='fa fa-volume'></i></font>"
		elseif self.plugin_data.whichChannel == self.plugin_data.radioChannel.second then
			info = "<font class='base_talk radio_second'><i class='fa fa-volume'></i></font>"
		end
	elseif (self.talking == 1 or self.justSwitchedProximity) then
		if (self.mode == 1) then
			info = "<font class='base_talk normal'><i class='fa fa-volume'></i></font>";
		elseif (self.mode == 2) then
			info = "<font class='base_talk normal'><i class='fa fa-volume-down'></i></font>";
		elseif (self.mode == 3) then
			info = "<font class='base_talk normal'><i class='fa fa-volume-up'></i></font>";
		end
	end
	if (self.localMuted == 1) then
		info = "<font class='base_talk muted'><i class='fa fa-microphone-slash'></i></font>"
	end
	if (info == self.screenInfo and not forceUpdate) then return end
	self.screenInfo = info;
	self:updatePlugin("updateTokovoipInfo", "" .. info);
end

function TokoVoip.updatePlugin(self, event, payload)
	--exports.tokovoip_script:doSendNuiMessage(event, payload);
	SendNUIMessage({
			type = event,
			payload = payload
	})
end

function TokoVoip.updateConfig(self)
	local data = self.config;
	data.plugin_data = self.plugin_data;
	data.pluginVersion = self.pluginVersion;
	data.pluginStatus = self.pluginStatus;
	data.pluginUUID = self.pluginUUID;
	self:updatePlugin("updateConfig", data);
end

function TokoVoip.initialize(self)
	self:updateConfig();
	self:updatePlugin("initializeSocket", self.wsServer);

	RegisterCommand('switchProximity', function()
		if not isDead and not self.justSwitchedProximity then
			if (not self.mode) then
				self.mode = 1;
			end
			self.mode = self.mode + 1;
			if (self.mode > 3) then
				self.mode = 1;
			end
			self.justSwitchedProximity = true
			setPlayerData(self.serverId, "voip:mode", self.mode, true);
			self:updateTokoVoipInfo();
			Citizen.Wait(500)
			self.justSwitchedProximity = false
			self:updateTokoVoipInfo();
		end
	end, false)

	RegisterCommand('+talkRadio', function()
		if not isDead then
			if self.plugin_data.radioChannel.first ~= -1 and self.config.radioEnabled then
				self.plugin_data.radioTalking = true;
				self.plugin_data.whichChannel = self.plugin_data.radioChannel.first;
				self.plugin_data.localRadioClicks = true;
				if (self.plugin_data.radioChannel.first > self.config.radioClickMaxChannel) then
					self.plugin_data.localRadioClicks = false;
				end
				if (not getPlayerData(self.serverId, "radio:talking_first")) then
					setPlayerData(self.serverId, "radio:talking_first", true, true);
				end
				self:updateTokoVoipInfo();
				if (lastTalkState == false and self.myChannels[self.plugin_data.radioChannel.first] and self.config.radioAnim) then
					if (not string.match(self.myChannels[self.plugin_data.radioChannel.first].name, "Call") and not IsPedSittingInAnyVehicle(PlayerPedId())) then
						RequestAnimDict("random@arrests");
						while not HasAnimDictLoaded("random@arrests") do
							Wait(5);
						end
						TaskPlayAnim(PlayerPedId(),"random@arrests","generic_radio_chatter", 8.0, 0.0, -1, 49, 0, 0, 0, 0);
					end
					lastTalkState = true
				end
			end
		end
	end, false)

	RegisterCommand('-talkRadio', function()
		if not isDead then
			self.plugin_data.radioTalking = false;
			self.plugin_data.whichChannel = 0;
			if (getPlayerData(self.serverId, "radio:talking_first")) then
				setPlayerData(self.serverId, "radio:talking_first", false, true);
			end
			self:updateTokoVoipInfo();

			if lastTalkState == true and self.config.radioAnim then
				lastTalkState = false
				StopAnimTask(PlayerPedId(), "random@arrests","generic_radio_chatter", -4.0);
			end
		end
	end, false)

	RegisterCommand('+talkRadio2', function()
		if not isDead then
			if self.plugin_data.radioChannel.second ~= -1 and self.config.radioEnabled then
				self.plugin_data.radioTalking = true;
				self.plugin_data.whichChannel = self.plugin_data.radioChannel.second;
				self.plugin_data.localRadioClicks = true;
				if (self.plugin_data.radioChannel.second > self.config.radioClickMaxChannel) then
					self.plugin_data.localRadioClicks = false;
				end
				if (not getPlayerData(self.serverId, "radio:talking_second")) then
					setPlayerData(self.serverId, "radio:talking_second", true, true);
				end
				self:updateTokoVoipInfo();
				if (lastTalkState == false and self.myChannels[self.plugin_data.radioChannel.second] and self.config.radioAnim) then
					if (not string.match(self.myChannels[self.plugin_data.radioChannel.second].name, "Call") and not IsPedSittingInAnyVehicle(PlayerPedId())) then
						RequestAnimDict("random@arrests");
						while not HasAnimDictLoaded("random@arrests") do
							Wait(5);
						end
						TaskPlayAnim(PlayerPedId(),"random@arrests","generic_radio_chatter", 8.0, 0.0, -1, 49, 0, 0, 0, 0);
					end
					lastTalkState = true
				end
			end
		end
	end, false)

	RegisterCommand('-talkRadio2', function()
		if not isDead then
			self.plugin_data.radioTalking = false;
			self.plugin_data.whichChannel = 0;
			if (getPlayerData(self.serverId, "radio:talking_second")) then
				setPlayerData(self.serverId, "radio:talking_second", false, true);
			end
			self:updateTokoVoipInfo();

			if lastTalkState == true and self.config.radioAnim then
				lastTalkState = false
				StopAnimTask(PlayerPedId(), "random@arrests","generic_radio_chatter", -4.0);
			end
		end
	end, false)

	RegisterKeyMapping('switchProximity', 'Portée de la voix', 'keyboard', self.keyProximity)
	RegisterKeyMapping('+talkRadio', 'Parler à la radio (Canal 1)', 'keyboard', self.radioKey)
	RegisterKeyMapping('+talkRadio2', 'Parler à la radio (Canal 2)', 'keyboard', self.radioKey2)
end

function TokoVoip.disconnect(self)
	self:updatePlugin("disconnect");
end

AddEventHandler('esx:onPlayerDeath', function()
	isDead = true
end)

AddEventHandler('playerSpawned', function()
	isDead = false
end)