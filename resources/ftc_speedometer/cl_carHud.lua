  -- @Project: FiveM Tools
  -- @License: GNU General Public License v3.0
display = true
local displayHud = true
local isDisplayed = false

Citizen.CreateThread(function()

	local text = "KM/H"
	local speedMultiplicator = 3.6

	while true do
		Citizen.Wait(30)
		if display == true then
			local player = GetPlayerPed(-1) -- current ped

			if IsPedInAnyVehicle(player, false) and displayHud and not IsPauseMenuActive() then
				isDisplayed = true
				local vehicle = GetVehiclePedIsIn(player, false) -- Current Vehicle ped is in
				local pasInSeat = GetPedInVehicleSeat(vehicle, -1) -- Driver Seat

				if player == pasInSeat then
					local vehClass = GetVehicleClass(vehicle)
					if vehClass == 15 or vehClass == 16 then
						local verticalSpeed = math.abs(math.floor(GetEntitySpeedVector(vehicle).z))  -- convert in km/h or mph
						local horizontalSpeed = math.floor((GetEntitySpeed(vehicle) * speedMultiplicator) - (math.sqrt((verticalSpeed*speedMultiplicator)*(verticalSpeed*speedMultiplicator))))
						local verticalSpeedPercentage = math.floor(verticalSpeed * 100 / 100)
						local horizontalSpeedPercentage = math.floor(horizontalSpeed * 100 / 240)
						local entityHeight = math.floor(GetEntityHeightAboveGround(vehicle))

						local vehicleNailVerticalSpeed
						if verticalSpeed > 100 then
							vehicleNailVerticalSpeed = math.ceil(  280 - math.ceil( math.ceil(100 * 205) / 100) )
						else
							vehicleNailVerticalSpeed = math.ceil(  280 - math.ceil( math.ceil(verticalSpeed * 205) / 100) )
						end
						local vehicleNailHorizontalSpeed
						if horizontalSpeed > 240 then
							vehicleNailHorizontalSpeed = math.ceil(  280 - math.ceil( math.ceil(240 * 205) / 240) )
						else
							vehicleNailHorizontalSpeed = math.ceil(  280 - math.ceil( math.ceil(horizontalSpeed * 205) / 240) )
						end
						TriggerEvent('lp_hud:sendNui', { speedometer = 'plane', verticalSpeed = verticalSpeed, verticalSpeedPercentage = verticalSpeedPercentage, horizontalSpeed = horizontalSpeed, horizontalSpeedPercentage = horizontalSpeedPercentage, vehicleNailVerticalSpeed = vehicleNailVerticalSpeed, vehicleNailHorizontalSpeed = vehicleNailHorizontalSpeed, entityHeight = entityHeight })
					elseif not (vehClass == 13 or vehClass == 14 or vehClass == 21) then
						local carSpeed = math.floor(GetEntitySpeed(vehicle) * speedMultiplicator) -- convert in km/h or mph
						local carSpeedPercentage = math.floor(carSpeed * 100 / 240)
						local vehicleGear = math.floor(GetVehicleCurrentGear(vehicle))
						local vehicleRPM = GetVehicleCurrentRpm(vehicle)
						local maxRPM = GetMaxRPM(vehClass)
						if not GetIsVehicleEngineRunning(vehicle) then
							vehicleRPM = 0
						end
						if vehicleRPM > 0.99 then
							vehicleRPM = vehicleRPM*100
							vehicleRPM = vehicleRPM+math.random(-2,2)
							vehicleRPM = vehicleRPM/100
						end
						vehicleRPM = math.floor(vehicleRPM * maxRPM)
						local RPMPercentage = math.floor(vehicleRPM * 100 / maxRPM)
						local vehicleNailSpeed
						if carSpeed > 240 then
							vehicleNailSpeed = math.ceil(  280 - math.ceil( math.ceil(240 * 205) / 240) )
						else
							vehicleNailSpeed = math.ceil(  280 - math.ceil( math.ceil(carSpeed * 205) / 240) )
						end
						local vehicleNailRpm
						if vehicleRPM > maxRPM then
							vehicleNailRpm = math.ceil(  280 - math.ceil( math.ceil(maxRPM * 205) / maxRPM) )
						else
							vehicleNailRpm = math.ceil(  280 - math.ceil( math.ceil(vehicleRPM * 205) / maxRPM) )
						end
						local retLights, lightsOn, highbeamsOn = GetVehicleLightsState(vehicle)
						local blinkerLights = GetVehicleIndicatorLights(vehicle)
						local vehicleSirens = false
						local vehicleSirensSound = false
						if vehClass == 18 then
							vehicleSirens = IsVehicleSirenOn(vehicle)
							vehicleSirensSound = IsVehicleSirenAudioOn(vehicle)
						end
						TriggerEvent('lp_hud:sendNui', { speedometer = 'car', vehicleSirens = vehicleSirens, vehicleSirensSound = vehicleSirensSound, blinkerLights = blinkerLights, lightsOn = lightsOn, highbeamsOn = highbeamsOn, carSpeed = carSpeed, carSpeedPercentage = carSpeedPercentage, vehicleNailSpeed = vehicleNailSpeed, vehicleNailRpm = vehicleNailRpm, RPMPercentage = RPMPercentage, vehicleGear = vehicleGear, vehicleRPM = vehicleRPM })
					end
				end
			elseif (not IsPedInAnyVehicle(player, false) or IsPauseMenuActive()) and isDisplayed then
				isDisplayed = false
				TriggerEvent('lp_hud:sendNui', { speedometer = 'hide' })
			else
				Citizen.Wait(500)
			end
		end
	end
end)

RegisterNetEvent("ftc_speedometer:State")
AddEventHandler('ftc_speedometer:State', function(state)
	if state ~= nil then
		if state == false then
			display = false
		else
			display = true
		end
	else
		if display == true then
			display = false
		else
			display = true
		end
	end
end)

AddEventHandler('camera:displayHUD', function(_display)
	displayHud = _display
end)

function RoundValue(value, numDecimalPlaces)
	if numDecimalPlaces then
		local power = 10^numDecimalPlaces
		return math.floor((value * power) + 0.5) / (power)
	else
		return math.floor(value + 0.5)
	end
end

function ChangeState(state)
	if state ~= nil then
		if state == false then
			display = false
		else
			display = true
		end
	else
		if display == true then
			display = false
		else
			display = true
		end
	end
end

function GetMaxRPM(vehClass)
	if vehClass == 0 or vehClass == 1 or vehClass == 18 then
		return 6000
	elseif vehClass == 2 or vehClass == 9 then
		return 5000
	elseif vehClass == 3 then
		return 7500
	elseif vehClass == 4 or vehClass == 5 then
		return 7000
	elseif vehClass == 6 then
		return 8000
	elseif vehClass == 7 then
		return 8500
	elseif vehClass == 8 then
		return 11000
	elseif vehClass == 10 or vehClass == 11 or vehClass == 20 then
		return 2500
	elseif vehClass == 12 or vehClass == 17 then
		return 3000
	elseif vehClass == 19 then
		return 4500
	end

	return 6000
end