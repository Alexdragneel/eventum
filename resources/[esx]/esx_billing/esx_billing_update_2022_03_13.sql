alter table billing
    add created_at datetime default now() null;

alter table billing
    add paid_at datetime default null;

alter table billing
    add is_deleted tinyint(1) DEFAULT '0';