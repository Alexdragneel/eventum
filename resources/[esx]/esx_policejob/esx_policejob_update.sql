START TRANSACTION;

INSERT INTO `job_vehicles` (`job_name`, `min_job_grade`, `vehicle_model`, `vehicle_trailer`, `vehicle_livery`, `vehicle_type`, `price`) VALUES
('police', 0, 'policeb2', NULL, 2, 'car', '18000'),
('police', 0, 'policeb1', NULL, 2, 'car', '18000');

UPDATE `job_grades` 
SET `skin_male` =  REPLACE(skin_male, "\"tshirt_1\":58", "\"tshirt_1\":122"),
`skin_female` =  REPLACE(skin_female, "\"tshirt_1\":35", "\"tshirt_1\":152")
WHERE `job_grades`.`job_name` = 'police' AND `job_grades`.`grade` > 0;

UPDATE `job_grades` 
SET `skin_male` =  REPLACE(skin_male, "\"ears_1\":2", "\"ears_1\":-1"),
`skin_female` =  REPLACE(skin_female, "\"ears_1\":2", "\"ears_1\":-1")
WHERE `job_grades`.`job_name` = 'police';

UPDATE `job_grades` 
SET `skin_male` =  REPLACE(skin_male, "\"mask_1\":-1", "\"mask_1\":121"),
`skin_female` =  REPLACE(skin_female, "\"mask_1\":-1", "\"mask_1\":121")
WHERE `job_grades`.`job_name` = 'police';

COMMIT;