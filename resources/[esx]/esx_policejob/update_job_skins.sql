START TRANSACTION;
#Cadets
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":57,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":46,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 0 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":34,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":45,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 0 AND `job_grades`.`job_name`='police';

#Officier
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 1 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 1 AND `job_grades`.`job_name`='police';

#Officier Sup
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":0,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 2 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":0,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 2 AND `job_grades`.`job_name`='police';

#Sergent
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":1,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 3 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":1,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0}' WHERE `job_grades`.`grade` = 3 AND `job_grades`.`job_name`='police';

#Sergent Chef
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":1,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0}' WHERE `job_grades`.`grade` = 4 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":1,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 4 AND `job_grades`.`job_name`='police';

#Inspecteur
UPDATE `job_grades` SET `skin_male` = '{}' WHERE `job_grades`.`grade` = 5 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{}' WHERE `job_grades`.`grade` = 5 AND `job_grades`.`job_name`='police';

#Lieutenant
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":2,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 6 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":2,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 6 AND `job_grades`.`job_name`='police';

#Captain
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":3,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 7 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":3,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 7 AND `job_grades`.`job_name`='police';

#Boss
UPDATE `job_grades` SET `skin_male` = '{\"sex\":0,\"tshirt_1\":58,\"tshirt_2\":0,\"torso_1\":55,\"torso_2\":0,\"decals_1\":8,\"decals_2\":3,\"arms\":41,\"pants_1\":25,\"pants_2\":0,\"shoes_1\":25,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 8 AND `job_grades`.`job_name`='police';

UPDATE `job_grades` SET `skin_female` = '{\"sex\":1,\"tshirt_1\":35,\"tshirt_2\":0,\"torso_1\":48,\"torso_2\":0,\"decals_1\":7,\"decals_2\":3,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"helmet_1\":-1,\"helmet_2\":0,\"chain_1\":0,\"chain_2\":0,\"ears_1\":2,\"ears_2\":0,\"mask_1\":-1,\"mask_2\":0,\"bracelets_1\":-1,\"bracelets_2\":0}' WHERE `job_grades`.`grade` = 8 AND `job_grades`.`job_name`='police';
COMMIT;