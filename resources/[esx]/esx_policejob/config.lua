Config                            = {}

Config.DrawDistance               = 10.0 -- How close do you need to be for the markers to be drawn (in GTA units).
Config.MarkerType                 = {Cloakrooms = 20, Armories = 21, BossActions = 22}
Config.MarkerSize                 = {x = 1.5, y = 1.5, z = 0.5}
Config.MarkerColor                = {r = 50, g = 50, b = 204}

Config.EnablePlayerManagement     = true -- Enable if you want society managing.
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- Enable if you're using esx_identity.
Config.EnableLicenses             = true -- Enable if you're using esx_license.

Config.EnableHandcuffTimer        = false -- Enable handcuff timer? will unrestrain player after the time ends.
Config.HandcuffTimer              = 20 * 60000 -- 20 minutes.

Config.EnableJobBlip              = false -- Enable blips for cops on duty, requires esx_society.
Config.EnableCustomPeds           = false -- Enable custom peds in cloak room? See Config.CustomPeds below to customize peds.

Config.EnableESXService           = true -- Enable esx service?
Config.MaxInService               = 200 -- How much people can be in service at once?

Config.Locale                     = 'fr'

Config.PoliceStations = {

	LSPD = {

		Blip = {
			Coords  = vector3(425.1, -979.5, 30.7),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(452.6, -992.8, 30.6)
		},

		Vehicles = {
			{
				Spawner = vector3(454.6, -1017.4, 28.4),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{coords = vector3(438.4, -1018.3, 27.7), heading = 90.0, radius = 6.0},
					{coords = vector3(441.0, -1024.2, 28.3), heading = 90.0, radius = 6.0},
					{coords = vector3(453.5, -1022.2, 28.0), heading = 90.0, radius = 6.0},
					{coords = vector3(450.9, -1016.5, 28.1), heading = 90.0, radius = 6.0}
				}
			},

			{
				Spawner = vector3(473.3, -1018.8, 28.0),
				InsideShop = vector3(228.5, -993.5, -99.0),
				SpawnPoints = {
					{coords = vector3(475.9, -1021.6, 28.0), heading = 276.1, radius = 6.0},
					{coords = vector3(484.1, -1023.1, 27.5), heading = 302.5, radius = 6.0}
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(461.1, -981.5, 43.6),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{coords = vector3(449.5, -981.2, 43.6), heading = 92.6, radius = 10.0}
				}
			}
		},

		BossActions = {
			vector3(452.0, -973.2, 30.6)
		}

	}

}

Config.AuthorizedWeapons = {
	cadet = {},

	officer = {},

	officer_sup = {},

	sergeant = {},

	sergeant_chief = {},

	inspector = {},

	lieutenant = {},

	captain = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, nil}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, nil}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	boss = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, nil}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, nil}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	}
}

Config.CustomPeds = {
	shared = {
		{label = 'Sheriff Ped', maleModel = 's_m_y_sheriff_01', femaleModel = 's_f_y_sheriff_01'},
		{label = 'Police Ped', maleModel = 's_m_y_cop_01', femaleModel = 's_f_y_cop_01'}
	},

	cadet = {},

	officer = {},

	officer_sup = {},

	sergeant = {},
	
	sergeant_chief = {},

	inspector = {},
	
	lieutenant = {},

	captain = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	boss = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements
Config.Uniforms = {

	bullet_wear = {
		male = {
			bproof_1 = 11,  bproof_2 = 1
		},
		female = {
			bproof_1 = 13,  bproof_2 = 1
		}
	},

	gilet_wear = {
		male = {
			tshirt_1 = 59,  tshirt_2 = 0
		},
		female = {
			tshirt_1 = 36,  tshirt_2 = 0
		}
	},

	swat_wear = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 320,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 17,
			pants_1 = 121,   pants_2 = 0,
			shoes_1 = 24,   shoes_2 = 0,
			helmet_1 = 141,  helmet_2 = 0,
			mask_1 = 56,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 331,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 18,
			pants_1 = 127,   pants_2 = 0,
			shoes_1 = 24,   shoes_2 = 0,
			helmet_1 = 140,  helmet_2 = 0,
			mask_1 = 56,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},
	bike_wear = {
		male = {
			tshirt_1 = 153,  tshirt_2 = 0,
			torso_1 = 316,   torso_2 = 5,
			decals_1 = 70,   decals_2 = 1,
			arms = 17,		arms_2 = 0,
			pants_1 = 59,   pants_2 = 9,
			shoes_1 = 25,   shoes_2 = 0,
			helmet_1 = 17,  helmet_2 = 7,
			mask_1 = -1,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0,
			bproof_1 = 11,  bproof_2 = 1
		},
		female = {
			tshirt_1 = 189,  tshirt_2 = 0,
			torso_1 = 327,   torso_2 = 5,
			decals_1 = 79,   decals_2 = 1,
			arms = 18,		arms_2 = 0,
			pants_1 = 61,   pants_2 = 9,
			shoes_1 = 25,   shoes_2 = 0,
			helmet_1 = 17,  helmet_2 = 7,
			mask_1 = -1,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0,
			bproof_1 = 13,  bproof_2 = 1
		}
	},
	pilot_wear = {
		male = {
			tshirt_1 = 129,  tshirt_2 = 0,
			torso_1 = 228,   torso_2 = 5,
			decals_1 = -1,
			arms = 42,		arms_2 = 0,
			pants_1 = 92,   pants_2 = 4,
			shoes_1 = 25,   shoes_2 = 0,
			helmet_1 = 19,  helmet_2 = 0,
			mask_1 = -1,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0,
			bproof_1 = -1
		},
		female = {
			tshirt_1 = 159,  tshirt_2 = 0,
			torso_1 = 238,   torso_2 = 5,
			decals_1 = -1,
			arms = 18,		arms_2 = 0,
			pants_1 = 95,   pants_2 = 4,
			shoes_1 = 25,   shoes_2 = 0,
			helmet_1 = 19,  helmet_2 = 0,
			mask_1 = -1,	mask_2 = 1,
			chain_1 = -1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0,
			bproof_1 = -1
		}
	},
	be_wear = {
		male = {
			chain_1 = 125, chain_2 = 0,
			bproof_1 = -1
		},
		female = {
			chain_1 = 95, chain_2 = 0,
			bproof_1 = -1
		}
	}
}
