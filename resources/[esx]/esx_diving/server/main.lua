ESX = nil

TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)

Citizen.CreateThread(function()

    if Config.UseReusableObject then

        -------------Light Diving suit
        ESX.RegisterUsableItem('diving_suit1', function(source)
            local _source = source
            TriggerEvent('esx_diving:useDivingSuit', _source, 'light')
        end)

        -------------diving suit
        ESX.RegisterUsableItem('diving_suit2', function(source)
            local _source = source
            TriggerEvent('esx_diving:useDivingSuit', _source, 'heavy')
        end)

        ESX.RegisterUsableItem('skin', function(source)
            local _source = source
            TriggerEvent('esx_diving:useSkin', _source)
        end)

    end
end)

RegisterNetEvent('esx_diving:useDivingSuit')
AddEventHandler('esx_diving:useDivingSuit', function(source, type)
    local _source = source
    local _type = type
    local xPlayer = ESX.GetPlayerFromId(_source)
    if (_type == 'light') then
        xPlayer.removeInventoryItem('diving_suit1', 1, true)
    else
        xPlayer.removeInventoryItem('diving_suit2', 1, true)
    end
    xPlayer.addInventoryItem('skin', 1, false, true)
    TriggerClientEvent('esx_diving:setDivingSuit', _source, _type)
end)

RegisterNetEvent('esx_diving:useSkin')
AddEventHandler('esx_diving:useSkin', function(source)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    xPlayer.removeInventoryItem('skin', 1, true)
    TriggerClientEvent('esx_diving:removeDivingSuit', _source)
end)
