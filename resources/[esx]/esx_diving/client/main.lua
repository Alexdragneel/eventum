ESX = nil
local lastSkin = {}

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end
end)
------------- Diving suit
RegisterNetEvent('esx_diving:setDivingSuit')
AddEventHandler('esx_diving:setDivingSuit', function(type)
    TriggerEvent('skinchanger:getSkin', function(skin)
        lastSkin = skin
        local clothesSkin = {}
        if skin.sex == 0 then
            if (type == 'light') then
                clothesSkin = {
                    ['tshirt_1'] = 15,
                    ['tshirt_2'] = 0,
                    ['torso_1'] = 15,
                    ['torso_2'] = 0,
                    ['decals_1'] = 0,
                    ['decals_2'] = 0,
                    ['mask_1'] = 36,
                    ['mask_2'] = 0,
                    ['arms'] = 15,
                    ['pants_1'] = 16,
                    ['pants_2'] = 0,
                    ['shoes_1'] = 34,
                    ['shoes_2'] = 0,
                    ['helmet_1'] = 8,
                    ['helmet_2'] = 0,
                    ['bags_1'] = 43,
                    ['bags_2'] = 0,
                    ['glasses_1'] = 6,
                    ['glasses_2'] = 0,
                    ['chain_1'] = 0,
                    ['chain_2'] = 0,
                    ['bproof_1'] = 0,
                    ['bproof_2'] = 0
                }
            else
                clothesSkin = {
                    ['tshirt_1'] = 15,
                    ['tshirt_2'] = 0,
                    ['torso_1'] = 243,
                    ['torso_2'] = 5,
                    ['decals_1'] = 0,
                    ['decals_2'] = 0,
                    ['arms'] = 35,
                    ['pants_1'] = 94,
                    ['pants_2'] = 0,
                    ['shoes_1'] = 67,
                    ['shoes_2'] = 0,
                    ['glasses_1'] = 26,
                    ['glasses_2'] = 0,
                    ['helmet_1'] = -1,
                    ['helmet_2'] = 0,
                    ['chain_1'] = 0,
                    ['chain_2'] = 0,
                    ['ears_1'] = -1,
                    ['ears_2'] = 0,
                    ['bproof_1'] = 0,
                    ['bproof_2'] = 0
                }
            end
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        else
            if (type == 'light') then
                clothesSkin = {
                    ['tshirt_1'] = 15,
                    ['tshirt_2'] = 0,
                    ['ears_1'] = -1,
                    ['ears_2'] = 0,
                    ['torso_1'] = 15,
                    ['torso_2'] = 0,
                    ['decals_1'] = 0,
                    ['decals_2'] = 0,
                    ['mask_1'] = 36,
                    ['mask_2'] = 0,
                    ['arms'] = 15,
                    ['pants_1'] = 15,
                    ['pants_2'] = 0,
                    ['shoes_1'] = 35,
                    ['shoes_2'] = 0,
                    ['helmet_1'] = -1,
                    ['helmet_2'] = 0,
                    ['bags_1'] = 43,
                    ['bags_2'] = 0,
                    ['glasses_1'] = 5,
                    ['glasses_2'] = 0,
                    ['chain_1'] = 0,
                    ['chain_2'] = 0,
                    ['bproof_1'] = 0,
                    ['bproof_2'] = 0
                }
            else
                clothesSkin = {
                    ['tshirt_1'] = 1,
                    ['tshirt_2'] = 0,
                    ['torso_1'] = 251,
                    ['torso_2'] = 5,
                    ['decals_1'] = 0,
                    ['decals_2'] = 0,
                    ['arms'] = 40,
                    ['pants_1'] = 97,
                    ['pants_2'] = 5,
                    ['shoes_1'] = 70,
                    ['shoes_2'] = 0,
                    ['glasses_1'] = 28,
                    ['glasses_2'] = 0,
                    ['helmet_1'] = -1,
                    ['helmet_2'] = 0,
                    ['chain_1'] = 0,
                    ['chain_2'] = 0,
                    ['ears_1'] = -1,
                    ['ears_2'] = 0,
                    ['bproof_1'] = 0,
                    ['bproof_2'] = 0
                }
            end
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        end
        SetEnableScuba(GetPlayerPed(-1), true)
        if type == "light" then
            SetPedMaxTimeUnderwater(GetPlayerPed(-1), 30.00) --30sec
        else
            SetPedMaxTimeUnderwater(GetPlayerPed(-1), 600.00) --10min
        end
    end)
end)

--- Restore skin
RegisterNetEvent('esx_diving:removeDivingSuit')
AddEventHandler('esx_diving:removeDivingSuit', function()
    if (lastSkin ~= nil) then
        TriggerEvent('skinchanger:getSkin', function(skin)
            TriggerEvent('skinchanger:loadClothes', skin, lastSkin)
            lastSkin = nil
            SetPedMaxTimeUnderwater(GetPlayerPed(-1), -1.0) --10min
        end)
    end
end)
