Config              = {}

Config.ZDiff        = 2.0
Config.BlipSprite   = 431

Config.Locale       = 'fr'

Config.ATMModels = {
	[-870868698] = true,
	[-1126237515] = true,
	[-1364697528] = true,
	[506770882] = true
}
