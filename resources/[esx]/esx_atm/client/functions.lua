function FindNearestATM()
    local coords = GetEntityCoords(PlayerPedId())
    local ATMs = {}
    local handle, object = FindFirstObject()
    local success

    repeat
        if Config.ATMModels[GetEntityModel(object)] then
            table.insert(ATMs, object)
        end

        success, object = FindNextObject(handle, object)
    until not success

    EndFindObject(handle)

    local ATMObject = 0
    local ATMDistance = 1000

    for _, ATMTempoObject in pairs(ATMs) do
        local dstcheck = GetDistanceBetweenCoords(coords, GetEntityCoords(ATMTempoObject))

        if dstcheck < ATMDistance then
            ATMDistance = dstcheck
            ATMObject = ATMTempoObject
        end
    end

    return ATMObject, ATMDistance
end