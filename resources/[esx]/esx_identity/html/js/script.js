$(function() {
	window.addEventListener('message', function(event) {
		if (event.data.type == "enableui") {
			document.body.style.display = event.data.enable ? "block" : "none";
		}
	});

	//On cache la partie confirmation si on appuie sur annuler
	$( document ).ready(function() {
		$("#second").hide();
	});

	$("#register").submit(function(event) {
		event.preventDefault(); // Prevent form from submitting
		// Verify date
		var date = $("#dateofbirth").val();
		var dateCheck = new Date($("#dateofbirth").val());
		if (dateCheck == "Invalid Date") {
			date == "invalid";
		} else {
			const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(dateCheck)
			const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(dateCheck)
			const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(dateCheck)
			var formattedDate = `${mo}/${da}/${ye}`;
			//on repasse les cm en inches
			var height = parseInt($("#height").val() * 0.393701);
			$.post('http://esx_identity/register', JSON.stringify({
				firstname: $("#firstname").val(),
				lastname: $("#lastname").val(),
				dateofbirth: formattedDate,
				sex: $("input[type='radio'][name='sex']:checked").val(),
				height: height
			}));
		}
	});

	$("#validation").click(function(event) {
		event.preventDefault();
		if (checkFirstName() && checkLastName() && checkDOB()) {
			//nom et prénom
			$("#firstnameLastname").text($("#firstname").val() + ' ' + $("#lastname").val())
			//Gestion du genre
			if($("input[type='radio'][name='sex']:checked").val() == 'm'){
				$("#genre").text('un homme');
			} else {
				$("#genre").text('une femme');
			}
			//Gestion de la date de naissance
			var dateCheck = new Date($("#dateofbirth").val());
			const da2 = new Intl.DateTimeFormat('fr', { day: '2-digit' }).format(dateCheck);
			const mo2 = new Intl.DateTimeFormat('fr', { month: 'long' }).format(dateCheck);
			const ye2 = new Intl.DateTimeFormat('fr', { year: 'numeric' }).format(dateCheck);
			var datebirth = `${da2} ${mo2} ${ye2}`;
			$("#datebirth").text(datebirth);
			//Gestion de la taille
			$("#taille").text($("#height").val());
			$("#main").hide();
			$("#second").show();
		}
	})

	$("#cancel").click(function(event) {
		event.preventDefault();
		$("#main").show();
		$("#second").hide();
	})
});
