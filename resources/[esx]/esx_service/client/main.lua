ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx_service:notifyAllInService')
AddEventHandler('esx_service:notifyAllInService', function(notification, target, notifyMe)
	target = GetPlayerFromServerId(target)
	if target == PlayerId() and notifyMe ~= true then return end

	local mugshot, mugshotStr = nil
	if notification.picture == nil then
		local targetPed = GetPlayerPed(target)
		mugshot, mugshotStr = ESX.Game.GetPedMugshot(targetPed)
		notification.picture = mugshotStr
	end

	ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg, notification.picture, notification.iconType)
	if mugshot ~= nil then
		UnregisterPedheadshot(mugshot)
	end
end)