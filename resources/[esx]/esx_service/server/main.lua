ESX                = nil
local InService    = {}
local MaxInService = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetInServiceCount(name)
	local count = 0

	for k,v in pairs(InService[name]) do
		if v == true then
			count = count + 1
		end
	end

	return count
end

RegisterServerEvent('esx_service:activateService')
AddEventHandler('esx_service:activateService', function(name, max)
	InService[name] = {}
	MaxInService[name] = max
end)

RegisterServerEvent('esx_service:disableService')
AddEventHandler('esx_service:disableService', function(name)
	InService[name][source] = nil
	TriggerClientEvent('esx_service:disableService', source)

	local inServiceCount = GetInServiceCount(name)
	if name == 'bennys' and inServiceCount == 0 then
		TriggerEvent("esx_lscustom:toggleLSCustoms", true)
	end
end)

RegisterServerEvent('esx_service:notifyAllInService')
AddEventHandler('esx_service:notifyAllInService', function(notification, name, notifyMe)
    if name ~= nil and name ~= "" then
        if InService[name] ~= nil then
            for k, v in pairs(InService[name]) do
                if v == true then
                    TriggerClientEvent('esx_service:notifyAllInService', k, notification, source, notifyMe)
                end
            end
        end
    end
end)

RegisterNetEvent('esx_service:getAllInService')
AddEventHandler('esx_service:getAllInService', function(cb)
	cb(InService)
end)

RegisterNetEvent('esx_service:InServiceCount')
AddEventHandler('esx_service:InServiceCount', function(name, cb)
	cb(GetInServiceCount(name))
end)

ESX.RegisterServerCallback('esx_service:enableService', function(source, cb, name)
	local inServiceCount = GetInServiceCount(name)

	if inServiceCount >= MaxInService[name] then
		cb(false, MaxInService[name], inServiceCount)
	else
		TriggerClientEvent('esx_service:enableService', source)
		InService[name][source] = true

		local newInServiceCount = GetInServiceCount(name)
		if name == 'bennys' and newInServiceCount >= 1 then
			TriggerEvent("esx_lscustom:toggleLSCustoms", false)
		end

		cb(true, MaxInService[name], inServiceCount)
	end
end)

ESX.RegisterServerCallback('esx_service:isInService', function(source, cb, name)
	local isInService = false

	if InService[name] ~= nil then
		if InService[name][source] then
			isInService = true
		end
	else
		print(('[esx_service] [^3WARNING^7] A service "%s" is not activated'):format(name))
	end

	cb(isInService)
end)

ESX.RegisterServerCallback('esx_service:isPlayerInService', function(source, cb, name, target)
	local isPlayerInService = false
	local targetXPlayer = ESX.GetPlayerFromId(target)

	if InService[name][targetXPlayer.source] then
		isPlayerInService = true
	end

	cb(isPlayerInService)
end)

ESX.RegisterServerCallback('esx_service:getInServiceList', function(source, cb, name)
	cb(InService[name])
end)

AddEventHandler('playerDropped', function()
	local _source = source
		
	for k,v in pairs(InService) do
		if v[_source] == true then
			v[_source] = nil
		end
	end
end)
