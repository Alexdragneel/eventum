var tableauQuestion = [
	{
		question: "Vous vous engagez sur la freeway qui fait le tour de l'île, à quelle vitesse êtes-vous limité :",
		propositionA: "120 km/h",
		propositionB: "140 km/h",
		propositionC: "160 km/h",
		propositionD: "180 km/h",
		reponse: "C"
	},

	{
		question: "Vous vous apprêtez à tourner à droite, mais vous voyez un piéton qui traverse :",
		propositionA: "Vous passez avant le piéton",
		propositionB: "Vous vérifiez qu'il n'y a pas d'autre véhicule et vous passez",
		propositionC: "Vous attendez que le piéton ait traversé",
		propositionD: "Vous tirez sur le piéton pour passer",
		reponse: "C"
	},

	{
		question: "Sans aucune indication : La vitesse en ville est limitée à :",
		propositionA: "70 km/h",
		propositionB: "80 km/h",
		propositionC: "90 km/h",
		propositionD: "100 km/h",
		reponse: "C"
	},

	{
		question: "Avant chaque changement de file, vous devez :",
		propositionA: "Vérifiez vos rétroviseurs",
		propositionB: "Vérifiez vos angles morts",
		propositionC: "Signalez vos intentions",
		propositionD: "Toutes les réponses précédentes",
		reponse: "D"
	},

	{
		question: "Quel taux d'alcoolémie (dans le sang) est considéré comme étant une conduite en état d'ébriété ?",
		propositionA: "0.10%",
		propositionB: "0.15%",
		propositionC: "0.20%",
		propositionD: "0.30%",
		reponse: "D"
	},

	{
		question: "Qu'implique l'utilisation du téléphone au volant ?",
		propositionA: "Une diminution du temps de réaction",
		propositionB: "Une tendance à zigzaguer",
		propositionC: "Une augmentation de la concentration",
		propositionD: "Toutes les réponses précédentes",
		reponse: "B"
	},

	{
		question: "Un piéton attend à son feu. Il est rouge :",
		propositionA: "Vous le laissez passer",
		propositionB: "Vous observez avant de continuer",
		propositionC: "Vous lui faites un signe de la main",
		propositionD: "Vous continuez votre chemin",
		reponse: "B"
	},

	{
		question: "Qu'est-ce qui est permis quand vous dépassez un autre véhicule ?",
		propositionA: "Le suivre de près pour le doubler plus vite",
		propositionB: "Le doubler en quittant la route",
		propositionC: "Conduire sur la route opposée pour le dépasser",
		propositionD: "Dépasser la vitesse limite",
		reponse: "C"
	},

	{
		question: "Vous conduisez hors de la ville, la vitesse maximale est de 110 km/h. La plupart du trafic roule à 120 km/h, alors vous ne devriez pas conduire plus vite que :",
		propositionA: "80 km/h",
		propositionB: "110 km/h",
		propositionC: "120 km/h",
		propositionD: "140 km/h",
		reponse: "B"
	},

	{
		question: "Quand vous êtes dépassé par un autre véhicule, il est important de ne PAS :",
		propositionA: "Ralentir",
		propositionB: "Vérifiez vos rétroviseurs",
		propositionC: "Regarder les autres conducteurs",
		propositionD: "Augmenter votre vitesse",
		reponse: "D"
	},
]
