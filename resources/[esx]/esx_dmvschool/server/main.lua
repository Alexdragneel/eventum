ESX = nil
local lastErrorTime = {}
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('esx_dmvschool:canYouPay', function(source, cb, type)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.getMoney() >= Config.Prices[type] then
		xPlayer.removeMoney(Config.Prices[type])
		TriggerClientEvent('esx:showNotification', source, _U('you_paid', Config.Prices[type]))
		cb(true)
	else
		cb(false)
	end
end)

AddEventHandler('esx:playerLoaded', function(source)
	TriggerEvent('esx_license:getLicenses', source, function(licenses)
		TriggerClientEvent('esx_dmvschool:loadLicenses', source, licenses)
	end)
end)

RegisterNetEvent('esx_dmvschool:addLicense')
AddEventHandler('esx_dmvschool:addLicense', function(type, target)
    local _source = source
    if target ~= nil then
        _source = target
    end
    TriggerEvent('esx_license:getLicenses', _source, function(licenses)
        local hasLicence = false
		local points = 12
        for i, license in ipairs(licenses) do
            if license.type == type then
                hasLicence = true
                break
            end
        end
		if type == 'dmv' then
			points = nil
		end
        if hasLicence == true then
            TriggerEvent('esx_license:setPoints', _source, type, points, function()
                TriggerEvent('esx_license:getLicenses', _source, function(licenses)
                    TriggerClientEvent('esx_dmvschool:loadLicenses', _source, licenses)
                end)
            end)
        else
            TriggerEvent('esx_license:addLicense', _source, type, function()
                TriggerEvent('esx_license:setPoints', _source, type, points, function()
					TriggerEvent('esx_license:getLicenses', _source, function(licenses)
						TriggerClientEvent('esx_dmvschool:loadLicenses', _source, licenses)
					end)
				end)
            end)
        end
    end)
end)

RegisterNetEvent('esx_dmvschool:licenceRemoved')
AddEventHandler('esx_dmvschool:licenceRemoved', function(target, type)
	local _target = target
	TriggerEvent('esx_license:getLicenses', _target, function(licenses)
		TriggerClientEvent('esx_dmvschool:loadLicenses', _target, licenses)
	end)
end)

RegisterNetEvent('esx_dmvschool:initErrorTimeout')
AddEventHandler('esx_dmvschool:initErrorTimeout', function(cb)
	local playerId = source
	local alreadyInit = false
	for i = 1, #lastErrorTime, 1 do
		if lastErrorTime[i].PlayerId == playerId then
			if cb ~= nil then
				cb()
			end
			alreadyInit = true
			lastErrorTime[i].time = os.time()
		end
	end
	if alreadyInit == false then
		table.insert(lastErrorTime, {
			PlayerId = playerId,
			time = os.time()
		})
	end
end)

RegisterNetEvent('esx_dmvschool:clearErrorTimeout')
AddEventHandler('esx_dmvschool:clearErrorTimeout', function(cb)
	local playerId = source

	for i = 1, #lastErrorTime, 1 do
		if lastErrorTime[i].PlayerId == playerId then
			if cb ~= nil then
				cb()
			end
			table.remove(lastErrorTime, i)
		end
	end
end)

ESX.RegisterServerCallback('esx_dmvschool:canCountError', function(source, cb)
	local playerId = source
	for i = 1, #lastErrorTime, 1 do
		local lastError = lastErrorTime[i];
		if lastError.PlayerId == playerId then
			if (os.time() - lastError.time) > Config.SecondsBetweenErrors then
				lastError.time = os.time()
				cb(true)
			else
				cb(false)
			end
		end
	end
end)

ESX.RegisterCommand('removelicense', 'admin', function(xPlayer, args, showError)
    local playerId = args.playerId
    local type = args.type
    TriggerEvent('esx_license:getLicenses', playerId, function(licenses)
        for i = 1, #licenses, 1 do
            if licenses[i].type == type then
                TriggerEvent('esx_license:removeLicense', playerId, type, function()
                    TriggerEvent('esx_dmvschool:licenceRemoved', playerId, type)
                end)
            end
        end
    end)
end, true, {
    help = '',
    validate = true,
    arguments = {{
        name = 'playerId',
        help = '',
        type = 'number'
    }, {
        name = 'type',
        help = '',
        type = 'string'
    }}
})

ESX.RegisterCommand('addlicense', 'admin', function(xPlayer, args, showError)
    local playerId = args.playerId
	TriggerEvent('esx_dmvschool:addLicense', args.type, playerId)
end, true, {
    help = '',
    validate = true,
    arguments = {{
        name = 'playerId',
        help = '',
        type = 'number'
    }, {
        name = 'type',
        help = '',
        type = 'string'
    }}
})
