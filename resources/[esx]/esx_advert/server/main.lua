ESX               = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('global:advert')
AddEventHandler('global:advert', function(args)
	TriggerClientEvent('esx:showAdvancedNotification', -1, _U('company_name'), _U('news'), table.concat(args, " "), 'CHAR_LIFEINVADER', 1)
end)

RegisterServerEvent('wait:advert')
AddEventHandler('wait:advert', function()
	TriggerClientEvent('esx:showAdvancedNotification', source, _U('company_name'), _U('error'), _U('wait_before_ad'), 'CHAR_LIFEINVADER', 1)
end)

RegisterServerEvent('job:advert')
AddEventHandler('job:advert', function()
	TriggerClientEvent('esx:showAdvancedNotification', source, _U('company_name'), _U('error'), _U('contact_journalist'), 'CHAR_LIFEINVADER', 1)
end)