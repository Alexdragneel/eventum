Config = {}
Config.Locale = 'fr'

Config.Price = 250
Config.AccessoryPrice = 100

Config.DrawDistance = 100.0
Config.MarkerSize   = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor  = {r = 102, g = 102, b = 204}
Config.MarkerType   = 1

Config.Shops = {
	{ pos = vector3(72.3, -1399.1, 28.4), type = 'CLOTHES' },
	{ pos = vector3(-703.8, -152.3, 36.4), type = 'CLOTHES' },
	{ pos = vector3(-167.9, -299.0, 38.7), type = 'CLOTHES' },
	{ pos = vector3(428.7, -800.1, 28.5), type = 'CLOTHES' },
	{ pos = vector3(-829.4, -1073.7, 10.3), type = 'CLOTHES' },
	{ pos = vector3(-1447.8, -242.5, 48.8), type = 'CLOTHES' },
	{ pos = vector3(11.6, 6514.2, 30.9), type = 'CLOTHES' },
	{ pos = vector3(123.6, -219.4, 53.6), type = 'CLOTHES' },
	{ pos = vector3(1696.3, 4829.3, 41.1), type = 'CLOTHES' },
	{ pos = vector3(618.1, 2759.6, 41.1), type = 'CLOTHES' },
	{ pos = vector3(1190.6, 2713.4, 37.2), type = 'CLOTHES' },
	{ pos = vector3(-1193.4, -772.3, 16.3), type = 'CLOTHES' },
	{ pos = vector3(-3172.5, 1048.1, 19.9), type = 'CLOTHES' },
	{ pos = vector3(-1108.4, 2708.9, 18.1), type = 'CLOTHES' },

	{ pos = vector3(80.3, -1389.4, 28.4), type = 'EARS' },
	{ pos = vector3(-163.0, -302.0, 38.8), type = 'EARS' },
	{ pos = vector3(-163.0,-302.0, 38.8), type = 'EARS' },
	{ pos = vector3(420.7, -809.6, 28.6), type = 'EARS' },
	{ pos = vector3(-817.0, -1075.9, 10.4), type = 'EARS' },
	{ pos = vector3(-1451.3, -238.2, 48.9), type = 'EARS' },
	{ pos = vector3(-0.7, 6513.6, 30.9), type = 'EARS' },
	{ pos = vector3(123.4, -208.0, 53.6), type = 'EARS' },
	{ pos = vector3(1687.3, 4827.6, 41.1), type = 'EARS' },
	{ pos = vector3(622.8, 2749.2, 41.2), type = 'EARS' },
	{ pos = vector3(1200.0, 2705.4,	37.3), type = 'EARS' },
	{ pos = vector3(-1199.9, -782.5, 16.4), type = 'EARS' },
	{ pos = vector3(-3171.8, 1059.6, 19.9), type = 'EARS' },
	{ pos = vector3(-1095.6, 2709.2, 18.2), type = 'EARS' },

	{ pos = vector3(-1338.1, -1278.2, 3.8), type = 'MASK' },

	{ pos = vector3(81.5, -1400.6, 28.4), type = 'HELMET' },
	{ pos = vector3(-705.8, -159.0,	36.5), type = 'HELMET' },
	{ pos = vector3(-161.3, -295.7,	38.8), type = 'HELMET' },
	{ pos = vector3(419.3, -800.6, 28.6), type = 'HELMET' },
	{ pos = vector3(-824.3, -1081.7, 10.4), type = 'HELMET' },
	{ pos = vector3(-1454.8, -242.9, 48.9), type = 'HELMET' },
	{ pos = vector3(4.7, 6520.9, 30.9), type = 'HELMET' },
	{ pos = vector3(121.0, -223.2, 53.3), type = 'HELMET' },
	{ pos = vector3(1689.6, 4818.8,	41.1), type = 'HELMET' },
	{ pos = vector3(613.9, 2749.9, 41.2), type = 'HELMET' },
	{ pos = vector3(1189.5, 2703.9,	37.3), type = 'HELMET' },
	{ pos = vector3(-1204.0, -774.4, 16.4), type = 'HELMET' },
	{ pos = vector3(-3164.2, 1054.7, 19.9), type = 'HELMET' },
	{ pos = vector3(-1103.1, 2700.5, 18.2), type = 'HELMET' },

	{ pos = vector3(75.2, -1391.1, 28.4), type = 'GLASSES' },
	{ pos = vector3(-713.1, -160.1, 36.5), type = 'GLASSES' },
	{ pos = vector3(-156.1, -300.5,	38.8), type = 'GLASSES' },
	{ pos = vector3(425.4, -807.8, 28.6), type = 'GLASSES' },
	{ pos = vector3(-820.8, -1072.9, 10.4), type = 'GLASSES' },
	{ pos = vector3(-1458.0, -236.7, 48.9), type = 'GLASSES' },
	{ pos = vector3(3.5, 6511.5, 30.9), type = 'GLASSES' },
	{ pos = vector3(131.3, -212.3, 53.6), type = 'GLASSES' },
	{ pos = vector3(1694.9, 4820.8,	41.1), type = 'GLASSES' },
	{ pos = vector3(613.9, 2768.8, 41.2), type = 'GLASSES' },
	{ pos = vector3(1198.6, 2711.0, 37.3), type = 'GLASSES' },
	{ pos = vector3(-1188.2, -764.5, 16.4), type = 'GLASSES' },
	{ pos = vector3(-3173.1,  1038.2, 19.9), type = 'GLASSES' },
	{ pos = vector3(-1100.4, 2712.4, 18.2), type = 'GLASSES' },
}
