
local hasAlreadyEnteredMarker, hasPaid, currentActionData, hasOpenedMenu = false, false, {}, false
local lastZone, currentAction, currentActionMsg, currentType
ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function OpenShopMenu()
	hasPaid = false
	hasOpenedMenu = true

	local isAccessory = true

	local restrictList = {}
	if currentType == 'CLOTHES' then
		restrictList = {
			'tshirt_1', 'tshirt_2',
			'torso_1', 'torso_2',
			'decals_1', 'decals_2',
			'arms', "arms_2",
			'pants_1', 'pants_2',
			'shoes_1', 'shoes_2',
			'bags_1', 'bags_2',
			'chain_1', 'chain_2',
			'watches_1', 'watches_2',
			'bracelets_1', 'bracelets_2',
		}
		isAccessory = false
	elseif currentType == 'EARS' then
		restrictList = {
			'ears_1', 'ears_2',
		}
	elseif currentType == 'MASK' then
		restrictList = {
			'mask_1', 'mask_2',
		}
	elseif currentType == 'HELMET' then
		restrictList = {
			'helmet_1', 'helmet_2',
		}
	elseif currentType == 'GLASSES' then
		restrictList = {
			'glasses_1', 'glasses_2',
		}
	end

	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)
		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
			title = _U('valid_this_purchase'),
			align = 'top-left',
			elements = {
				{label = _U('no'), value = 'no'},
				{label = _U('yes'), value = 'yes'}
		}}, function(data, menu)
			menu.close()

			if currentType == 'EARS' then
				TriggerEvent('skinchanger:setWearEars', true)
			end
			if currentType == 'MASK' then
				TriggerEvent('skinchanger:setWearMask', true)
			end
			if currentType == 'HELMET' then
				TriggerEvent('skinchanger:setWearHelmet', true)
			end
			if currentType == 'GLASSES' then
				TriggerEvent('skinchanger:setWearGlasses', true)
			end

			if data.current.value == 'yes' then
				ESX.TriggerServerCallback('esx_clotheshop:buyClothes', function(bought)
					if bought then
						TriggerEvent('skinchanger:getSkin', function(skin)
							ESX.TriggerServerCallback("esx_skin:getPlayerSkin", function(aSkin, jobSkin)
								if currentType ~= 'CLOTHES' then
									skin.tshirt_1 = aSkin.tshirt_1
									skin.tshirt_2 = aSkin.tshirt_2
									skin.torso_1 = aSkin.torso_1
									skin.torso_2 = aSkin.torso_2
									skin.decals_1 = aSkin.decals_1
									skin.decals_2 = aSkin.decals_2
									skin.arms = aSkin.arms
									skin.arms_2 = aSkin.arms_2
									skin.pants_1 = aSkin.pants_1
									skin.pants_2 = aSkin.pants_2
									skin.shoes_1 = aSkin.shoes_1
									skin.shoes_2 = aSkin.shoes_2
									skin.bags_1 = aSkin.bags_1
									skin.bags_2 = aSkin.bags_2
									skin.chain_1 = aSkin.chain_1
									skin.chain_2 = aSkin.chain_2
									skin.watches_1 = aSkin.watches_1
									skin.watches_2 = aSkin.watches_2
									skin.bracelets_1 = aSkin.bracelets_1
									skin.bracelets_2 = aSkin.bracelets_2
								end
								if currentType ~= 'EARS' then
									skin.ears_1 = aSkin.ears_1
									skin.ears_2 = aSkin.ears_2
								end
								if currentType ~= 'MASK' then
									skin.mask_1 = aSkin.mask_1
									skin.mask_2 = aSkin.mask_2
								end
								if currentType ~= 'HELMET' then
									skin.helmet_1 = aSkin.helmet_1
									skin.helmet_2 = aSkin.helmet_2
								end
								if currentType ~= 'GLASSES' then
									skin.glasses_1 = aSkin.glasses_1
									skin.glasses_2 = aSkin.glasses_2
								end
								TriggerServerEvent('esx_skin:save', skin)
							end)
						end)

						hasPaid = true

						--[[ESX.TriggerServerCallback('esx_clotheshop:checkPropertyDataStore', function(foundStore)
							if foundStore then
								ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'save_dressing', {
									title = _U('save_in_dressing'),
									align = 'top-left',
									elements = {
										{label = _U('no'),  value = 'no'},
										{label = _U('yes'), value = 'yes'}
								}}, function(data2, menu2)
									menu2.close()

									if data2.current.value == 'yes' then
										ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'outfit_name', {
											title = _U('name_outfit')
										}, function(data3, menu3)
											menu3.close()

											TriggerEvent('skinchanger:getSkin', function(skin)
												TriggerServerEvent('esx_clotheshop:saveOutfit', data3.value, skin)
												ESX.ShowNotification(_U('saved_outfit'))
											end)
										end, function(data3, menu3)
											menu3.close()
										end)
									end
								end)
							end
						end)]]--

					else
						ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
							TriggerEvent('skinchanger:loadSkin', skin)
						end)

						ESX.ShowNotification(_U('not_enough_money'))
					end
				end, isAccessory)
			elseif data.current.value == 'no' then
				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
					TriggerEvent('skinchanger:loadSkin', skin)
				end)
			end

			currentAction     = 'shop_menu'
			currentActionMsg  = _U('press_menu')
			currentActionData = {}
		end, function(data, menu)
			menu.close()

			currentAction     = 'shop_menu'
			currentActionMsg  = _U('press_menu')
			currentActionData = {}
		end)

	end, function(data, menu)
		menu.close()

		currentAction     = 'shop_menu'
		currentActionMsg  = _U('press_menu')
		currentActionData = {}
	end, restrictList)
end

AddEventHandler('esx_clotheshop:hasEnteredMarker', function(zone)
	currentAction     = 'shop_menu'
	currentActionMsg  = _U('press_menu')
	currentActionData = {}
end)

AddEventHandler('esx_clotheshop:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	currentAction = nil

	if not hasPaid and hasOpenedMenu  then
		hasOpenedMenu = false
		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
			TriggerEvent('skinchanger:loadSkin', skin)
		end)
	end
end)

-- Create Blips
Citizen.CreateThread(function()
	for k,v in ipairs(Config.Shops) do
		if v.type == 'CLOTHES' then
			local blip = AddBlipForCoord(v.pos)

			SetBlipSprite (blip, 73)
			SetBlipColour (blip, 47)
			SetBlipAsShortRange(blip, true)

			BeginTextCommandSetBlipName('STRING')
			AddTextComponentSubstringPlayerName(_U('clothes'))
			EndTextCommandSetBlipName(blip)
		end
		if v.type == 'MASK' then
			local blip = AddBlipForCoord(v.pos)

			SetBlipSprite (blip, 362)
			SetBlipColour (blip, 2)
			SetBlipAsShortRange(blip, true)

			BeginTextCommandSetBlipName('STRING')
			AddTextComponentSubstringPlayerName(_U('clothes'))
			EndTextCommandSetBlipName(blip)
		end
	end
end)

-- Enter / Exit marker events & draw markers
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		local playerCoords, isInMarker, currentZone, letSleep = GetEntityCoords(PlayerPedId()), false, nil, true

		for k,v in pairs(Config.Shops) do
			local distance = #(playerCoords - v.pos)

			if distance < Config.DrawDistance then
				letSleep = false
				DrawMarker(Config.MarkerType, v.pos, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, nil, nil, false)

				if distance < Config.MarkerSize.x then
					isInMarker, currentZone, currentType = true, k, v.type
				end
			end
		end

		if (isInMarker and not hasAlreadyEnteredMarker) or (isInMarker and lastZone ~= currentZone) then
			hasAlreadyEnteredMarker, lastZone = true, currentZone
			TriggerEvent('esx_clotheshop:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_clotheshop:hasExitedMarker', lastZone)
		end

		if letSleep then
			Citizen.Wait(500)
		end
	end
end)

-- Key controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if currentAction then
			ESX.ShowHelpNotification(currentActionMsg)

			if IsControlJustReleased(0, 38) then
				if currentAction == 'shop_menu' then
					ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin, nil, currentType == 'EARS', currentType == 'GLASSES', currentType == 'HELMET', currentType == 'MASK')
						OpenShopMenu()
					end)
				end

				currentAction = nil
			end
		else
			Citizen.Wait(500)
		end
	end
end)
