ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function AddLicense(target, type, cb)
	local xPlayer = ESX.GetPlayerFromId(target)

	if xPlayer then
		MySQL.Async.execute('INSERT INTO user_licenses (type, owner) VALUES (@type, @owner)', {
			['@type']  = type,
			['@owner'] = xPlayer.identifier
		}, function(rowsChanged)
			if cb then
				cb()
			end
		end)
	else
		if cb then
			cb()
		end
	end
end

function RemoveLicense(target, type, cb)
	local xPlayer = ESX.GetPlayerFromId(target)

	if xPlayer then
		MySQL.Async.execute('DELETE FROM user_licenses WHERE type = @type AND owner = @owner', {
			['@type'] = type,
			['@owner'] = xPlayer.identifier
		}, function(rowsChanged)
			if cb then
				cb()
			end
		end)
	else
		if cb then
			cb()
		end
	end
end

function GetLicense(type, cb)
	MySQL.Async.fetchAll('SELECT label FROM licenses WHERE type = @type', {
		['@type'] = type
	}, function(result)
		local data = {
			type  = type,
			label = result[1].label
		}

		cb(data)
	end)
end

function GetOwnerLicense(owner, type, cb)
	MySQL.Async.fetchAll('SELECT type, points FROM user_licenses WHERE type = @type AND owner = @owner', {
		['@type'] = type,
		['@owner'] = owner
	}, function(result)
		if #result > 0 then
			cb(result[1])
		else
			cb()
		end
	end)
end


function GetLicenses(target, cb)
    local xPlayer = ESX.GetPlayerFromId(target)

    MySQL.Async.fetchAll('SELECT type, points FROM user_licenses WHERE owner = @owner', {
        ['@owner'] = xPlayer.identifier
    }, function(result)
        local licenses, asyncTasks = {}, {}

        for i = 1, #result, 1 do
            local scope = function(type)
                table.insert(asyncTasks, function(cb)
                    MySQL.Async.fetchAll('SELECT label FROM licenses WHERE type = @type', {
                        ['@type'] = type
                    }, function(result2)
                        table.insert(licenses, {
                            type = type,
                            label = result2[1].label,
                            points = result[i].points
                        })

                        cb()
                    end)
                end)
            end

            scope(result[i].type)
        end

        Async.parallel(asyncTasks, function(results)
            cb(licenses)
        end)
    end)
end

function CheckLicense(target, type, cb)
	local xPlayer = ESX.GetPlayerFromId(target)

	if xPlayer then
		MySQL.Async.fetchAll('SELECT COUNT(*) as count FROM user_licenses WHERE type = @type AND owner = @owner', {
			['@type'] = type,
			['@owner'] = xPlayer.identifier
		}, function(result)
			if tonumber(result[1].count) > 0 then
				cb(true)
			else
				cb(false)
			end
		end)
	else
		cb(false)
	end
end

function GetLicensesList(cb)
	MySQL.Async.fetchAll('SELECT type, label FROM licenses', {
		['@type'] = type
	}, function(result)
		local licenses = {}

		for i=1, #result, 1 do
			table.insert(licenses, {
				type  = result[i].type,
				label = result[i].label
			})
		end

		cb(licenses)
	end)
end

function RemovePoints(target, type, amount, cb)
    local xPlayer = ESX.GetPlayerFromId(target)
    GetLicenses(target, function(licenses)
        for i, license in ipairs(licenses) do
            if license.type == type then
                if license.points >= amount then
                    license.points = license.points - amount
                else
                    license.points = 0
                end
                MySQL.Async.execute('UPDATE user_licenses SET points = @points WHERE owner = @owner AND type = @type',
                    {
                        ['@type'] = type,
                        ['@owner'] = xPlayer.identifier,
                        ['@points'] = license.points
                    }, function(result)
                        if result > 0 then
                            cb(license.points)
                        else
                            cb(-1)
                        end
                    end)
            end
        end
    end)
end

function SetPoints(target, type, amount, cb)
    local xPlayer = ESX.GetPlayerFromId(target)
    GetLicenses(target, function(licenses)
        for i, license in ipairs(licenses) do
            if license.type == type then
                MySQL.Async.execute('UPDATE user_licenses SET points = @points WHERE owner = @owner AND type = @type',
                    {
                        ['@type'] = type,
                        ['@owner'] = xPlayer.identifier,
                        ['@points'] = amount
                    }, function(result)
                        if result > 0 then
                            cb(amount)
                        else
                            cb(-1)
                        end
                    end)
            end
        end
    end)
end

RegisterNetEvent('esx_license:addLicense')
AddEventHandler('esx_license:addLicense', function(target, type, cb)
	AddLicense(target, type, cb)
end)

RegisterNetEvent('esx_license:removeLicense')
AddEventHandler('esx_license:removeLicense', function(target, type, cb)
	RemoveLicense(target, type, cb)
end)

AddEventHandler('esx_license:getLicense', function(type, cb)
	GetLicense(type, cb)
end)

RegisterNetEvent('esx_license:getOwnerLicense')
AddEventHandler('esx_license:getOwnerLicense', function(target, type, cb)
	GetOwnerLicense(target, type, cb)
end)

AddEventHandler('esx_license:getLicenses', function(target, cb)
	GetLicenses(target, cb)
end)

AddEventHandler('esx_license:checkLicense', function(target, type, cb)
	CheckLicense(target, type, cb)
end)

AddEventHandler('esx_license:getLicensesList', function(cb)
	GetLicensesList(cb)
end)

RegisterNetEvent('esx_license:removePoints')
AddEventHandler('esx_license:removePoints', function(target, type, amount, cb)
    RemovePoints(target, type, amount, cb)
end)

RegisterNetEvent('esx_license:setPoints')
AddEventHandler('esx_license:setPoints', function(target, type, amount, cb)
    SetPoints(target, type, amount, cb)
end)

ESX.RegisterServerCallback('esx_license:getLicense', function(source, cb, type)
	GetLicense(type, cb)
end)

ESX.RegisterServerCallback('esx_license:getOwnerLicense', function(source, cb, target, type)
	GetOwnerLicense(target, type, cb)
end)

ESX.RegisterServerCallback('esx_license:getLicenses', function(source, cb, target)
	GetLicenses(target, cb)
end)

ESX.RegisterServerCallback('esx_license:checkLicense', function(source, cb, target, type)
	CheckLicense(target, type, cb)
end)

ESX.RegisterServerCallback('esx_license:getLicensesList', function(source, cb)
	GetLicensesList(cb)
end)

ESX.RegisterServerCallback('esx_license:removePoints', function(source, cb, target, type, amount)
    RemovePoints(target, type, amount, cb)
end)

ESX.RegisterServerCallback('esx_license:setPoints', function(source, cb, target, type, amount)
    SetPoints(target, type, amount, cb)
end)