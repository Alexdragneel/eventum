ESX               = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


ESX.RegisterServerCallback('carlock:isVehicleOwnerSociety', function(source, cb, plate)
	local xPlayer = ESX.GetPlayerFromId(source)
	local job = nil

	if xPlayer.job.name == 'police' then
		job = 'police'
	elseif xPlayer.job.name == 'ambulance' then
		job = 'police'
	elseif xPlayer.job.name == 'journaliste' then
		job = 'journaliste'
	else
		job = 'unemployed'
	end

	MySQL.Async.fetchAll('SELECT owner FROM owned_vehicles WHERE owner = @owner AND plate = @plate', {
		['@owner'] = 'society:' .. job,
		['@plate'] = plate
	}, function(result)
		if result[1] then
			cb(result[1].owner == 'society:' .. job)
		else
			cb(false)
		end
	end)
end)


ESX.RegisterServerCallback('carlock:isVehicleOwner', function(source, cb, plate)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT owner FROM owned_vehicles WHERE owner = @owner AND plate = @plate', {
		['@owner'] = xPlayer.identifier,
		['@plate'] = plate
	}, function(result)
		if result[1] then
			cb(result[1].owner == xPlayer.identifier)
		else
			cb(false)
		end
	end)
end)