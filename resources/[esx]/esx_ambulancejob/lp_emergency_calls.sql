USE `fivem_server`;

CREATE TABLE `emergency_calls` (
  `id` int(11) NOT NULL,
  `player` varchar(100) NOT NULL,
  `position` varchar(255) NOT NULL,
  `reason` longtext DEFAULT NULL,
  `taken_by` varchar(40) DEFAULT NULL,
  `taken_by_name` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `resolved` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `emergency_calls` ADD PRIMARY KEY (`id`);
