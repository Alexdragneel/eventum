Config = {}

Config.DrawDistance = 20.0 -- How close do you need to be in order for the markers to be drawn (in GTA units).

Config.Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 171, g = 60, b = 230, a = 100, rotate = false}

Config.ReviveReward = 0 -- Revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog = true -- Enable anti-combat logging? (Removes Items when a player logs back after intentionally logging out while dead.)
Config.LoadIpl = true -- Disable if you're using fivem-ipl or other IPL loaders

Config.Locale = "fr"

Config.EarlyRespawnTimer = 60000 * 0.05 -- time til respawn is available
Config.BleedoutTimer = 60000 * 10 -- time til the player bleeds out

Config.EnablePlayerManagement = true -- Enable society managing (If you are using esx_society).
Config.MaxInService = 200 -- ESX Service max in service

Config.RemoveWeaponsAfterRPDeath = true
Config.RemoveCashAfterRPDeath = true
Config.RemoveItemsAfterRPDeath = true

-- Let the player pay for respawning early, only if he can afford it.
Config.EarlyRespawnFine = false
Config.EarlyRespawnFineAmount = 5000

Config.RespawnPoint = {coords = vector3(361.29, -580.82, 28.83), heading = 230.15}

Config.Hospitals = {
	CentralLosSantos = {
		Blip = {
			coords = vector3(295.02, -583.64, 42.16),
			sprite = 61,
			scale = 1.2,
			color = 2
		},
		AmbulanceActions = {
			vector3(301.53, -599.3, 42.28)
		},
		BossActions = {
			vector3(339.26, -595.71, 42.28)
		},
		Pharmacies = {
			vector3(344.83, -592.21, 42.28),
			vector3(356.36, -594.73, 42.28),
			vector3(324.32, -573.38, 42.28),
			vector3(321.41, -572.22, 42.28),
			vector3(314.95, -570.88, 42.28),
			vector3(310.82, -563.78, 42.28)
		},
		FastTravels = {
			{
				-- Bas vers le haut (elevator)
				From = vector3(346.84, -582.82, 27.8),
				To = {coords = vector3(331.43, -595.53, 42.28), heading = 71.2},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = false}
			},
			{
				-- haut vers le bas (elevator)
				From = vector3(330.05, -601.02, 42.28),
				To = {coords = vector3(345.79, -586.63, 27.8), heading = 0.0},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = false}
			},
			{
				-- garage 1 vers le haut (elevator)
				From = vector3(339.58, -584.66, 27.8),
				To = {coords = vector3(331.43, -595.53, 42.28), heading = 71.2},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = false}
			},
			{
				-- garage 2 vers le haut (elevator)
				From = vector3(340.92, -580.97, 27.8),
				To = {coords = vector3(331.43, -595.53, 42.28), heading = 71.2},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = false}
			},
			{
				-- bas vers le haut (helico)
				From = vector3(327.17, -603.88, 42.50),
				To = {coords = vector3(338.62, -586.84, 74.0), heading = 244.08},
				Marker = {type = 34, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = true}
			},
			{
				-- bas vers le haut (helico)
				From = vector3(340.19, -583.02, 73.20),
				To = {coords = vector3(331.43, -595.53, 42.28), heading = 244.08},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = false}
			}
				
		},
		FastTravelsPrompt = {
			{
				From = vector3(237.4, -1373.8, 26.0),
				To = {coords = vector3(251.9, -1363.3, 38.5), heading = 0.0},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false},
				Prompt = _U("fast_travel")
			},
			{
				From = vector3(256.5, -1357.7, 36.0),
				To = {coords = vector3(235.4, -1372.8, 26.3), heading = 0.0},
				Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false},
				Prompt = _U("fast_travel")
			}
		}
	}
}
