ESX = nil
local playersHealing, deadPlayers = {}, {}

TriggerEvent(
	"esx:getSharedObject",
	function(obj)
		ESX = obj
	end
)

TriggerEvent("esx_phone:registerNumber", "ambulance", _U("alert_ambulance"), true, true)

TriggerEvent(
	"esx_society:registerSociety",
	"ambulance",
	"Ambulance",
	"society_ambulance",
	"society_ambulance",
	"society_ambulance",
	{type = "public"}
)

TriggerEvent('esx_service:activateService', 'ambulance', Config.MaxInService)

RegisterNetEvent("esx_ambulancejob:revive")
AddEventHandler(
	"esx_ambulancejob:revive",
	function(playerId)
		local xPlayer = ESX.GetPlayerFromId(source)
		local isEMS = xPlayer.job.name == "ambulance"
		local isCop = xPlayer.job.name == "police"

		if xPlayer and (isEMS or isCop) then
			local xTarget = ESX.GetPlayerFromId(playerId)

			if xTarget then
				if deadPlayers[playerId] then
					if Config.ReviveReward > 0 and isEMS then
						xPlayer.showNotification(_U("revive_complete_award", xTarget.name, Config.ReviveReward))
						xPlayer.addMoney(Config.ReviveReward)
					else
						xPlayer.showNotification(_U("revive_complete", xTarget.name))
					end
					xTarget.triggerEvent("esx_ambulancejob:revive")
				else
					xPlayer.showNotification(_U("player_not_unconscious"))
				end
			else
				xPlayer.showNotification(_U("revive_fail_offline"))
			end
		end
	end
)

RegisterNetEvent("esx:onPlayerDeath")
AddEventHandler(
	"esx:onPlayerDeath",
	function(data)
		deadPlayers[source] = "dead"
		TriggerClientEvent("esx_ambulancejob:setDeadPlayers", -1, deadPlayers)
	end
)

-- callsback false if 0 ems on duty online
ESX.RegisterServerCallback(
	"esx_ambulancejob:isAnyEMSOnDuty",
	function(source, cb)
		local count = 0
		TriggerEvent('esx_service:InServiceCount', 'ambulance', function(count)
			cb(count > 0)
		end)
	end
)

RegisterServerEvent("esx_ambulancejob:svsearch")
AddEventHandler(
	"esx_ambulancejob:svsearch",
	function()
		TriggerClientEvent("esx_ambulancejob:clsearch", -1, source)
	end
)

RegisterNetEvent("esx_ambulancejob:onPlayerDistress")
AddEventHandler(
	"esx_ambulancejob:onPlayerDistress",
	function(playerId, coords, reason, street_name)
		if deadPlayers[source] then
			deadPlayers[source] = "distress"
			TriggerClientEvent("esx_ambulancejob:setDeadPlayers", -1, deadPlayers)
			local xPlayer = ESX.GetPlayerFromId(playerId)
			local phone_number = 'n/a'
			exports.gcphone:getPhoneNumber(playerId, function (_phone_number)
				if _phone_number then
					phone_number = _phone_number
				end

				local time = os.time()
				MySQL.Async.insert('INSERT INTO emergency_calls (player, position, reason, time, street_name) VALUES (@player, @position, @reason, @time, @street_name)', {
					['@player'] = xPlayer.get('firstName') .. ' ' .. xPlayer.get('lastName') .. ' - #' .. phone_number,
					['@position'] = coords,
					['@reason'] = reason,
					['@time'] = time,
					['@street_name'] = street_name
				}, function(id)
					xPlayer.showNotification(_U('distress_sent'))
					MySQL.Async.fetchAll('SELECT * FROM emergency_calls WHERE id = @id', { ['@id'] = id }, function (response)
						TriggerClientEvent("gcPhone:receiveEmergency", -1, response[1])
					end)
				end)
			end)
		end
	end
)

RegisterNetEvent("esx:onPlayerSpawn")
AddEventHandler(
	"esx:onPlayerSpawn",
	function()
		if deadPlayers[source] then
			deadPlayers[source] = nil
			TriggerClientEvent("esx_ambulancejob:setDeadPlayers", -1, deadPlayers)
		end
	end
)

AddEventHandler(
	"esx:playerDropped",
	function(playerId, reason)
		if deadPlayers[playerId] then
			deadPlayers[playerId] = nil
			TriggerClientEvent("esx_ambulancejob:setDeadPlayers", -1, deadPlayers)
		end
		TriggerEvent("esx_service:disableService", "ambulance")
	end
)

RegisterNetEvent("esx_ambulancejob:heal")
AddEventHandler(
	"esx_ambulancejob:heal",
	function(target, type)
		local xPlayer = ESX.GetPlayerFromId(source)

		if xPlayer.job.name == "ambulance" then
			TriggerClientEvent("esx_ambulancejob:heal", target, type)
		end
	end
)

RegisterNetEvent("esx_ambulancejob:adminHeal")
AddEventHandler("esx_ambulancejob:adminHeal", function(target)
	TriggerClientEvent("esx_ambulancejob:heal", target, 'big')
end)

RegisterNetEvent("esx_ambulancejob:putInVehicle")
AddEventHandler(
	"esx_ambulancejob:putInVehicle",
	function(target)
		local xPlayer = ESX.GetPlayerFromId(source)

		if xPlayer.job.name == "ambulance" then
			TriggerClientEvent("esx_ambulancejob:putInVehicle", target)
		end
	end
)

RegisterNetEvent('esx_ambulancejob:OutVehicle')
AddEventHandler('esx_ambulancejob:OutVehicle', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'ambulance' then
		TriggerClientEvent('esx_ambulancejob:OutVehicle', target)
	else
		print(('esx_ambulancejob: %s attempted to drag out from vehicle (not EMS)!'):format(xPlayer.identifier))
	end
end)

RegisterNetEvent('esx_ambulancejob:removeTattoo')
AddEventHandler('esx_ambulancejob:removeTattoo', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'ambulance' then
		TriggerClientEvent('esx_ambulancejob:removeTattoo', source, target)
	else
		print(('esx_ambulancejob: %s attempted to remove tattoo (not EMS)!'):format(xPlayer.identifier))
	end
end)

ESX.RegisterServerCallback(
	"esx_ambulancejob:removeItemsAfterRPDeath",
	function(source, cb, respawnInPlace)
		local xPlayer = ESX.GetPlayerFromId(source)

		if Config.RemoveCashAfterRPDeath and not respawnInPlace then
			if xPlayer.getMoney() > 0 then
				xPlayer.removeMoney(xPlayer.getMoney())
			end

			if xPlayer.getAccount("black_money").money > 0 then
				xPlayer.setAccountMoney("black_money", 0)
			end
		end

		if Config.RemoveItemsAfterRPDeath and not respawnInPlace then
			for i = 1, #xPlayer.inventory, 1 do
				-- remove all items except phone
				if xPlayer.inventory[i].count > 0 and xPlayer.inventory[i].name ~= "phone" then
					xPlayer.setInventoryItem(xPlayer.inventory[i].name, 0)
				end
			end
		end

		local playerLoadout = {}
		if Config.RemoveWeaponsAfterRPDeath and not respawnInPlace then
			for i = 1, #xPlayer.loadout, 1 do
				xPlayer.removeWeapon(xPlayer.loadout[i].name)
			end
		else -- save weapons & restore em' since spawnmanager removes them
			for i = 1, #xPlayer.loadout, 1 do
				table.insert(playerLoadout, xPlayer.loadout[i])
			end

			-- give back wepaons after a couple of seconds
			Citizen.CreateThread(
				function()
					Citizen.Wait(5000)
					for i = 1, #playerLoadout, 1 do
						if playerLoadout[i].label ~= nil then
							xPlayer.addWeapon(playerLoadout[i].name, playerLoadout[i].ammo)
						end
					end
				end
			)
		end

		cb()
	end
)

if Config.EarlyRespawnFine then
	ESX.RegisterServerCallback(
		"esx_ambulancejob:checkBalance",
		function(source, cb)
			local xPlayer = ESX.GetPlayerFromId(source)
			local bankBalance = xPlayer.getAccount("bank").money

			cb(bankBalance >= Config.EarlyRespawnFineAmount)
		end
	)

	RegisterNetEvent("esx_ambulancejob:payFine")
	AddEventHandler(
		"esx_ambulancejob:payFine",
		function()
			local xPlayer = ESX.GetPlayerFromId(source)
			local fineAmount = Config.EarlyRespawnFineAmount

			xPlayer.showNotification(_U("respawn_bleedout_fine_msg", ESX.Math.GroupDigits(fineAmount)))
			xPlayer.removeAccountMoney("bank", fineAmount)
		end
	)
end

ESX.RegisterServerCallback(
	"esx_ambulancejob:getItemAmount",
	function(source, cb, item)
		local xPlayer = ESX.GetPlayerFromId(source)
		local invItem = xPlayer.getInventoryItem(item)
		local quantity = 0
		if invItem then
			quantity = invItem.count
		end

		cb(quantity)
	end
)

RegisterNetEvent("esx_ambulancejob:removeItem")
AddEventHandler(
	"esx_ambulancejob:removeItem",
	function(item)
		local xPlayer = ESX.GetPlayerFromId(source)
		xPlayer.removeInventoryItem(item, 1, true)

		if item == "bandage" then
			xPlayer.showNotification(_U("used_bandage"))
		elseif item == "medikit" then
			xPlayer.showNotification(_U("used_medikit"))
		end
	end
)

RegisterNetEvent("esx_ambulancejob:giveItem")
AddEventHandler(
	"esx_ambulancejob:giveItem",
	function(itemName, amount)
		local xPlayer = ESX.GetPlayerFromId(source)

		if xPlayer.job.name ~= "ambulance" then
			print(('[esx_ambulancejob] [^2INFO^7] "%s" attempted to spawn in an item!'):format(xPlayer.identifier))
			return
		elseif (itemName ~= "medikit" and itemName ~= "bandage" and itemName ~= 'diving_suit2') then
			print(('[esx_ambulancejob] [^2INFO^7] "%s" attempted to spawn in an item!'):format(xPlayer.identifier))
			return
		end

		if xPlayer.canCarryItem(itemName, amount) then
			xPlayer.addInventoryItem(itemName, amount, false, true)
		else
			xPlayer.showNotification(_U("max_item"))
		end
	end
)

ESX.RegisterCommand(
	"revive",
	"admin",
	function(xPlayer, args, showError)
		if xPlayer then
			print(('[ADMIN CMD] "%s" a utilisé la commande "revive" sur "%s" (%d)'):format(xPlayer.getName(), args.playerId.getName(), os.time()))
		else
			print(('[ADMIN CMD] "(console)" a utilisé la commande "revive" sur "%s" (%d)'):format(args.playerId.getName(), os.time()))
		end
		args.playerId.triggerEvent("esx_ambulancejob:revive")
	end,
	true,
	{
		help = _U("revive_help"),
		validate = true,
		arguments = {
			{name = "playerId", help = "The player id", type = "player"}
		}
	}
)

ESX.RegisterUsableItem(
	"medikit",
	function(source)
		if not playersHealing[source] then
			local xPlayer = ESX.GetPlayerFromId(source)
			xPlayer.removeInventoryItem("medikit", 1, true)

			playersHealing[source] = true
			TriggerClientEvent("esx_ambulancejob:useItem", source, "medikit")

			Citizen.Wait(10000)
			playersHealing[source] = nil
		end
	end
)

ESX.RegisterUsableItem(
	"bandage",
	function(source)
		if not playersHealing[source] then
			local xPlayer = ESX.GetPlayerFromId(source)
			xPlayer.removeInventoryItem("bandage", 1, true)

			playersHealing[source] = true
			TriggerClientEvent("esx_ambulancejob:useItem", source, "bandage")

			Citizen.Wait(10000)
			playersHealing[source] = nil
		end
	end
)

ESX.RegisterServerCallback(
	"esx_ambulancejob:getDeathStatus",
	function(source, cb)
		local xPlayer = ESX.GetPlayerFromId(source)

		MySQL.Async.fetchScalar(
			"SELECT is_dead FROM users WHERE identifier = @identifier",
			{
				["@identifier"] = xPlayer.identifier
			},
			function(isDead)
				if isDead then
					print(('[esx_ambulancejob] [^2INFO^7] "%s" attempted combat logging'):format(xPlayer.identifier))
				end

				cb(isDead)
			end
		)
	end
)

RegisterNetEvent("esx_ambulancejob:setDeathStatus")
AddEventHandler(
	"esx_ambulancejob:setDeathStatus",
	function(isDead)
		local xPlayer = ESX.GetPlayerFromId(source)

		if type(isDead) == "boolean" then
			MySQL.Sync.execute(
				"UPDATE users SET is_dead = @isDead WHERE identifier = @identifier",
				{
					["@identifier"] = xPlayer.identifier,
					["@isDead"] = isDead
				}
			)
		end
	end
)

RegisterNetEvent('esx_ambulancejob:drag')
AddEventHandler('esx_ambulancejob:drag', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'ambulance' then
		TriggerClientEvent('esx_ambulancejob:drag', target, source)
	else
		print(('esx_ambulancejob: %s attempted to drag (not an EMS)!'):format(xPlayer.identifier))
	end
end)