local CurrentAction, CurrentActionMsg, CurrentActionData = nil, "", {}
local HasAlreadyEnteredMarker, LastHospital, LastPart, LastPartNum
local isBusy, deadPlayers, isOnDuty = false, {}, false
isInShopMenu = false

function OpenAmbulanceActionsMenu()
	local elements = {{label = _U("cloakroom"), value = "cloakroom"}}

	if Config.EnablePlayerManagement and ESX.PlayerData.job.grade_name == "boss" then
		table.insert(elements, {label = _U("boss_actions"), value = "boss_actions"})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		"ambulance_actions",
		{
			title = _U("ambulance"),
			align = "top-left",
			elements = elements
		},
		function(data, menu)
			if data.current.value == "cloakroom" then
				OpenCloakroomMenu()
			elseif data.current.value == "boss_actions" then
				TriggerEvent(
					"esx_society:openBossMenu",
					"ambulance",
					function(data, menu)
						menu.close()
					end,
					{wash = false}
				)
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenMobileAmbulanceActionsMenu()
	ESX.UI.Menu.CloseAll()

	local elems = {
		{label = _U("ems_menu"), value = "citizen_interaction"},
		{label = _U("billing"), value = "billing_menu"}
	}
	if(ESX.PlayerData.job.grade > 0) then
		table.insert(elems, {label = _U("bill_management"), value = "bill_management"})
	end

	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		"mobile_ambulance_actions",
		{
			title = _U("ambulance"),
			align = "top-left",
			elements = elems
		},
		function(data, menu)
			if data.current.value == "citizen_interaction" then
				ESX.UI.Menu.Open(
					"default",
					GetCurrentResourceName(),
					"citizen_interaction",
					{
						title = _U("ems_menu_title"),
						align = "top-left",
						elements = {
							{label = _U("ems_menu_revive"), value = "revive"},
							{label = _U("ems_menu_small"), value = "small"},
							{label = _U("ems_menu_big"), value = "big"},
							{label = _U('ems_menu_drag'), value = 'drag'},
							{label = _U("ems_menu_putincar"), value = "put_in_vehicle"},
							{label = _U("ems_menu_putoutcar"), value = "put_out_vehicle"},
							{label = _U("ems_menu_search"), value = "search"},
							{label = _U("ems_menu_remove_tattoo"), value = "remove_tattoo"},
						}
					},
					function(data, menu)
						if isBusy then
							return
						end

						local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
						local closestDeadPed, closestDeadPedDistance = ESX.Game.GetClosestDeadPed()

						if data.current.value == "search" then
							TriggerServerEvent("esx_ambulancejob:svsearch")
						elseif closestDeadPed > -1 and closestDeadPedDistance < 1.5 and data.current.value == "revive" then
							revivePed(closestDeadPed)
						elseif closestPlayer == -1 or closestDistance > 1.5 then
							ESX.ShowNotification(_U("no_players"))
						else
							if data.current.value == "revive" then
								revivePlayer(closestPlayer)
							elseif data.current.value == "small" then
								ESX.TriggerServerCallback(
									"esx_ambulancejob:getItemAmount",
									function(quantity)
										if quantity > 0 then
											local closestPlayerPed = GetPlayerPed(closestPlayer)
											local health = GetEntityHealth(closestPlayerPed)

											if health > 0 then
												local playerPed = PlayerPedId()

												isBusy = true
												ESX.ShowNotification(_U("heal_inprogress"))
												TaskStartScenarioInPlace(playerPed, "CODE_HUMAN_MEDIC_TEND_TO_DEAD", 0, true)
												TriggerEvent('rprogress:start', _U("heal_inprogress"), 10000)
												Citizen.Wait(10000)
												TriggerEvent('rprogress:stop')
												ClearPedTasks(playerPed)

												TriggerServerEvent("esx_ambulancejob:removeItem", "bandage")
												TriggerServerEvent("esx_ambulancejob:heal", GetPlayerServerId(closestPlayer), "small")
												ESX.ShowNotification(_U("heal_complete", GetPlayerName(closestPlayer)))
												isBusy = false
											else
												ESX.ShowNotification(_U("player_not_conscious"))
											end
										else
											ESX.ShowNotification(_U("not_enough_bandage"))
										end
									end,
									"bandage"
								)
							elseif data.current.value == "big" then
								ESX.TriggerServerCallback(
									"esx_ambulancejob:getItemAmount",
									function(quantity)
										if quantity > 0 then
											local closestPlayerPed = GetPlayerPed(closestPlayer)
											local health = GetEntityHealth(closestPlayerPed)

											if health > 0 then
												local playerPed = PlayerPedId()

												isBusy = true
												ESX.ShowNotification(_U("heal_inprogress"))
												TaskStartScenarioInPlace(playerPed, "CODE_HUMAN_MEDIC_TEND_TO_DEAD", 0, true)
												TriggerEvent('rprogress:start', _U("heal_inprogress"), 10000)
												Citizen.Wait(10000)
												TriggerEvent('rprogress:stop')
												ClearPedTasks(playerPed)

												TriggerServerEvent("esx_ambulancejob:removeItem", "medikit")
												TriggerServerEvent("esx_ambulancejob:heal", GetPlayerServerId(closestPlayer), "big")
												ESX.ShowNotification(_U("heal_complete", GetPlayerName(closestPlayer)))
												isBusy = false
											else
												ESX.ShowNotification(_U("player_not_conscious"))
											end
										else
											ESX.ShowNotification(_U("not_enough_medikit"))
										end
									end,
									"medikit"
								)
							elseif data.current.value == 'drag' then
								TriggerServerEvent('esx_ambulancejob:drag', GetPlayerServerId(closestPlayer))
							elseif data.current.value == "put_in_vehicle" then
								TriggerServerEvent("esx_ambulancejob:putInVehicle", GetPlayerServerId(closestPlayer))
							elseif data.current.value == "put_out_vehicle" then
								TriggerServerEvent("esx_ambulancejob:OutVehicle", GetPlayerServerId(closestPlayer))
							elseif data.current.value == "remove_tattoo" then
								TriggerServerEvent("esx_ambulancejob:removeTattoo", GetPlayerServerId(closestPlayer))
							end
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
			elseif data.current.value == "billing_menu" then
				TriggerEvent('lp_core:openBillingMenu')
			elseif data.current.value == 'bill_management' then
				TriggerEvent(
					"esx_society:openBillManagement",
					ESX.PlayerData.job.name
				)
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function revivePlayer(closestPlayer)
	isBusy = true
	local closestPlayerPed = GetPlayerPed(closestPlayer)
	local playerServerId = GetPlayerServerId(closestPlayer)
	if deadPlayers[playerServerId] then
		local playerPed = PlayerPedId()
		local lib, anim = "mini@cpr@char_a@cpr_str", "cpr_pumpchest"
		ESX.ShowNotification(_U("revive_inprogress"))

		RotateEntityToEntity(playerPed, closestPlayerPed)

		TriggerEvent('rprogress:start', _U("revive_inprogress"), 900 * 15)
		for i = 1, 15 do
			Citizen.Wait(900)

			ESX.Streaming.RequestAnimDict(
				lib,
				function()
					TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0.0, false, false, false)
				end
			)
		end
		TriggerEvent('rprogress:stop')

		TriggerServerEvent("esx_ambulancejob:revive", GetPlayerServerId(closestPlayer))
	else
		ESX.ShowNotification(_U("player_not_unconscious"))
	end
	isBusy = false
end

function revivePed(closestPed)
	isBusy = true
	
	local playerPed = PlayerPedId()

	ResurrectPed(closestPed)
	SetEntityHealth(closestPed, 200)
	ClearPedTasksImmediately(closestPed)
	SetEntityCollision(closestPed, true, true)
	SetBlockingOfNonTemporaryEvents(closestPed, true)

	local libDead, animDead = "dead", "dead_a"
	ESX.Streaming.RequestAnimDict(libDead, function()
		TaskPlayAnim(closestPed, libDead, animDead, 8.0, -8.0, -1, 1, 0.0, false, false, false)
	end)

	RotateEntityToEntity(playerPed, closestPed)

	local lib, anim = "mini@cpr@char_a@cpr_str", "cpr_pumpchest"
	ESX.ShowNotification(_U("revive_inprogress"))

	TriggerEvent('rprogress:start', _U("revive_inprogress"), 900 * 15)
	for i = 1, 15 do
		Citizen.Wait(900)

		ESX.Streaming.RequestAnimDict(lib, function()
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0.0, false, false, false)
		end)
	end
	TriggerEvent('rprogress:stop')

	local pedCoords = GetEntityCoords(closestPed, true)
	local libStandUp, animStandUp = "get_up@directional@transition@prone_to_seated@injured", "back_armsdown"
	ESX.Streaming.RequestAnimDict(libStandUp, function()
		TaskPlayAnim(closestPed, libStandUp, animStandUp, 8.0, -8.0, -1, 0, 0.0, false, false, false)
	end)
	SetBlockingOfNonTemporaryEvents(closestPed, false)
		
	isBusy = false
end

function RotateEntityToEntity(e1, e2)
	local p1 = GetEntityCoords(e1, true)
	local p2 = GetEntityCoords(e2, true)
	local dx = p2.x - p1.x
	local dy = p2.y - p1.y
	local heading = GetHeadingFromVector_2d(dx, dy)
	SetEntityHeading(e1, heading)
end

function FastTravel(coords, heading)
	local playerPed = PlayerPedId()

	DoScreenFadeOut(800)

	while not IsScreenFadedOut() do
		Citizen.Wait(500)
	end

	ESX.Game.Teleport(
		playerPed,
		coords,
		function()
			DoScreenFadeIn(800)

			if heading then
				SetEntityHeading(playerPed, heading)
			end
		end
	)
end

-- Draw markers & Marker logic
Citizen.CreateThread(
	function()
		while true do
			Citizen.Wait(0)

			if ESX.PlayerData.job and ESX.PlayerData.job.name == "ambulance" then
				local playerCoords = GetEntityCoords(PlayerPedId())
				local letSleep, isInMarker, hasExited = true, false, false
				local currentHospital, currentPart, currentPartNum

				for hospitalNum, hospital in pairs(Config.Hospitals) do
					-- Ambulance Actions
					for k, v in ipairs(hospital.AmbulanceActions) do
						local distance = #(playerCoords - v)

						if distance < Config.DrawDistance then
							DrawMarker(
								Config.Marker.type,
								v,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								Config.Marker.x,
								Config.Marker.y,
								Config.Marker.z,
								Config.Marker.r,
								Config.Marker.g,
								Config.Marker.b,
								Config.Marker.a,
								false,
								false,
								2,
								Config.Marker.rotate,
								nil,
								nil,
								false
							)
							letSleep = false

							if distance < Config.Marker.x then
								isInMarker, currentHospital, currentPart, currentPartNum = true, hospitalNum, "AmbulanceActions", k
							end
						end
					end
					if ESX.PlayerData.job.grade_name == "boss" then
						for k, v in ipairs(hospital.BossActions) do
							local distance = #(playerCoords - v)

							if distance < Config.DrawDistance then
								DrawMarker(
									Config.Marker.type,
									v,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									Config.Marker.x,
									Config.Marker.y,
									Config.Marker.z,
									Config.Marker.r,
									Config.Marker.g,
									Config.Marker.b,
									Config.Marker.a,
									false,
									false,
									2,
									Config.Marker.rotate,
									nil,
									nil,
									false
								)
								letSleep = false

								if distance < Config.Marker.x then
									isInMarker, currentHospital, currentPart, currentPartNum = true, hospitalNum, "BossActions", k
								end
							end
						end
					end

					-- Pharmacies
					for k, v in ipairs(hospital.Pharmacies) do
						local distance = #(playerCoords - v)

						if distance < Config.DrawDistance then
							DrawMarker(
								Config.Marker.type,
								v,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								Config.Marker.x,
								Config.Marker.y,
								Config.Marker.z,
								Config.Marker.r,
								Config.Marker.g,
								Config.Marker.b,
								Config.Marker.a,
								false,
								false,
								2,
								Config.Marker.rotate,
								nil,
								nil,
								false
							)
							letSleep = false

							if distance < Config.Marker.x then
								isInMarker, currentHospital, currentPart, currentPartNum = true, hospitalNum, "Pharmacy", k
							end
						end
					end

					-- Fast Travels (Prompt)
					for k, v in ipairs(hospital.FastTravelsPrompt) do
						local distance = #(playerCoords - v.From)

						if distance < Config.DrawDistance then
							DrawMarker(
								v.Marker.type,
								v.From,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								v.Marker.x,
								v.Marker.y,
								v.Marker.z,
								v.Marker.r,
								v.Marker.g,
								v.Marker.b,
								v.Marker.a,
								false,
								false,
								2,
								v.Marker.rotate,
								nil,
								nil,
								false
							)
							letSleep = false

							if distance < v.Marker.x then
								isInMarker, currentHospital, currentPart, currentPartNum = true, hospitalNum, "FastTravelsPrompt", k
							end
						end
					end
				end

				-- Logic for exiting & entering markers
				if
					isInMarker and not HasAlreadyEnteredMarker or
						(isInMarker and (LastHospital ~= currentHospital or LastPart ~= currentPart or LastPartNum ~= currentPartNum))
				 then
					if
						(LastHospital ~= nil and LastPart ~= nil and LastPartNum ~= nil) and
							(LastHospital ~= currentHospital or LastPart ~= currentPart or LastPartNum ~= currentPartNum)
					 then
						TriggerEvent("esx_ambulancejob:hasExitedMarker", LastHospital, LastPart, LastPartNum)
						hasExited = true
					end

					HasAlreadyEnteredMarker, LastHospital, LastPart, LastPartNum = true, currentHospital, currentPart, currentPartNum

					TriggerEvent("esx_ambulancejob:hasEnteredMarker", currentHospital, currentPart, currentPartNum)
				end

				if not hasExited and not isInMarker and HasAlreadyEnteredMarker then
					HasAlreadyEnteredMarker = false
					TriggerEvent("esx_ambulancejob:hasExitedMarker", LastHospital, LastPart, LastPartNum)
				end

				if letSleep then
					Citizen.Wait(500)
				end
			else
				Citizen.Wait(500)
			end
		end
	end
)

-- Fast travels
Citizen.CreateThread(
	function()
		while true do
			Citizen.Wait(0)
			local playerCoords, letSleep = GetEntityCoords(PlayerPedId()), true

			for hospitalNum, hospital in pairs(Config.Hospitals) do
				-- Fast Travels
				for k, v in ipairs(hospital.FastTravels) do
					local distance = #(playerCoords - v.From)

					if distance < Config.DrawDistance then
						DrawMarker(
							v.Marker.type,
							v.From,
							0.0,
							0.0,
							0.0,
							0.0,
							0.0,
							0.0,
							v.Marker.x,
							v.Marker.y,
							v.Marker.z,
							v.Marker.r,
							v.Marker.g,
							v.Marker.b,
							v.Marker.a,
							false,
							false,
							2,
							v.Marker.rotate,
							nil,
							nil,
							false
						)
						letSleep = false

						if distance < v.Marker.x then
							ESX.ShowHelpNotification(_U('use_elevator'))
							if IsControlJustReleased(0, 38) then
								FastTravel(v.To.coords, v.To.heading)
							end
						end
					end
				end
			end

			if letSleep then
				Citizen.Wait(500)
			end
		end
	end
)

AddEventHandler(
	"esx_ambulancejob:hasEnteredMarker",
	function(hospital, part, partNum)
		if part == "AmbulanceActions" then
			CurrentAction = part
			CurrentActionMsg = _U("actions_prompt")
			CurrentActionData = {}
		elseif part == "BossActions" then
			CurrentAction = part
			CurrentActionMsg = _U("boass_actions_prompt")
			CurrentActionData = {}
		elseif part == "Pharmacy" then
			CurrentAction = part
			CurrentActionMsg = _U("open_pharmacy")
			CurrentActionData = {}
		elseif part == "FastTravelsPrompt" then
			local travelItem = Config.Hospitals[hospital][part][partNum]

			CurrentAction = part
			CurrentActionMsg = travelItem.Prompt
			CurrentActionData = {to = travelItem.To.coords, heading = travelItem.To.heading}
		end
	end
)

AddEventHandler(
	"esx_ambulancejob:hasExitedMarker",
	function(hospital, part, partNum)
		if not isInShopMenu then
			ESX.UI.Menu.CloseAll()
		end

		CurrentAction = nil
	end
)

-- Key Controls
Citizen.CreateThread(
	function()
		while true do
			Citizen.Wait(0)

			if CurrentAction then
				ESX.ShowHelpNotification(CurrentActionMsg)

				if IsControlJustReleased(0, 38) then
					if CurrentAction == "AmbulanceActions" then
						OpenCloakroomMenu()
					elseif CurrentAction == "BossActions" then
						TriggerEvent(
							"esx_society:openBossMenu",
							"ambulance",
							function(data, menu)
								menu.close()
							end,
							{wash = false}
						)
					elseif CurrentAction == "Pharmacy" then
						OpenPharmacyMenu()
					elseif CurrentAction == "FastTravelsPrompt" then
						FastTravel(CurrentActionData.to, CurrentActionData.heading)
					end

					CurrentAction = nil
				end
			elseif ESX.PlayerData.job and ESX.PlayerData.job.name == "ambulance" and not isDead then
				if IsControlJustReleased(0, 167) then
					OpenMobileAmbulanceActionsMenu()
				end
			else
				Citizen.Wait(500)
			end
		end
	end
)

RegisterNetEvent("esx_ambulancejob:putInVehicle")
AddEventHandler(
	"esx_ambulancejob:putInVehicle",
	function()
		local playerPed = PlayerPedId()
		local coords = GetEntityCoords(playerPed)

		if IsAnyVehicleNearPoint(coords, 5.0) then
			local vehicle, distance = ESX.Game.GetClosestVehicle(coords)

			if DoesEntityExist(vehicle) and distance < 5.0 then
				local hasControl = ESX.Game.RequestControl(vehicle)
				if not hasControl then
					ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
					return
				end
				local maxSeats, freeSeat = GetVehicleMaxNumberOfPassengers(vehicle)

				for i = maxSeats - 1, 0, -1 do
					if IsVehicleSeatFree(vehicle, i) then
						freeSeat = i
						break
					end
				end

				if freeSeat then
					TaskWarpPedIntoVehicle(playerPed, vehicle, freeSeat)
				end
			end
		end
	end
)

RegisterNetEvent('esx_ambulancejob:OutVehicle')
AddEventHandler('esx_ambulancejob:OutVehicle', function()
	local playerPed = PlayerPedId()

	if IsPedSittingInAnyVehicle(playerPed) then
		local vehicle = GetVehiclePedIsIn(playerPed, false)
		local hasControl = ESX.Game.RequestControl(vehicle)
		if not hasControl then
			ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
			return
		end
		TaskLeaveVehicle(playerPed, vehicle, 64)
	end
end)

RegisterNetEvent('esx_ambulancejob:removeTattoo')
AddEventHandler('esx_ambulancejob:removeTattoo', function(target)
	TriggerEvent("lp_tattooshops:openTattooMenu", true, target)
end)

function GetIsOnDuty()
	return isOnDuty
end

function OpenCloakroomMenu()
    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "cloakroom", {
        title = _U("cloakroom"),
        align = "top-left",
        elements = {
          {label = _U("ems_clothes_civil"), value = "citizen_wear"},
          {label = _U("ems_clothes_ems"), value = "ambulance_wear"},
          {label = _U('ems_divingsuit'), value = 'divingsuit'},
        }
    }, function(data, menu)
        if data.current.value == "citizen_wear" then
			ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
				if isInService then
					ESX.TriggerServerCallback("esx_skin:getPlayerSkin", function(skin, jobSkin)
						startAnimAction('switch@franklin@getting_ready', '002334_02_fras_v2_11_getting_dressed_exit')
						Citizen.Wait(1000)
						ClearPedTasks(PlayerPedId())
						TriggerEvent("skinchanger:loadSkin", skin)
						isOnDuty = false
						-- disables the EMS app on the phone
						TriggerEvent("gcPhone:displayEMSApp", isOnDuty)
						-- notifies the server the player is off duty
						TriggerServerEvent('esx_service:disableService', 'ambulance')
					end)
				end
			end, 'ambulance')
        elseif data.current.value == "ambulance_wear" then
			ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
				if not isInService then
					ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
						if canTakeService == true then
							ESX.TriggerServerCallback("esx_skin:getPlayerSkin", function(skin, jobSkin)
								startAnimAction('switch@franklin@getting_ready', '002334_02_fras_v2_11_getting_dressed_exit')
								Citizen.Wait(1000)
								ClearPedTasks(PlayerPedId())
								if skin.sex == 0 then
									TriggerEvent("skinchanger:loadClothes", skin, jobSkin.skin_male)
								else
									TriggerEvent("skinchanger:loadClothes", skin, jobSkin.skin_female)
								end

								isOnDuty = true
								-- enables the EMS app on the phone
								TriggerEvent("gcPhone:displayEMSApp", isOnDuty)
								-- notifies the server the player is on duty
								TriggerServerEvent('esx_service:enableService', 'ambulance')
							end)
						else
							ESX.ShowNotification(_U('service_max', inServiceCount, maxInService))
						end
					end, 'ambulance')
				end
			end, 'ambulance')

        elseif data.current.value == 'divingsuit' then
            TriggerServerEvent('esx_ambulancejob:giveItem', 'diving_suit2', 1)
        end
        menu.close()
    end, function(data, menu)
        menu.close()
    end)
end


function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		"pharmacy",
		{
			title = _U("pharmacy_menu_title"),
			align = "top-left",
			elements = {
				{label = _U("pharmacy_take", _U("medikit")), item = "medikit", type = "slider", value = 1, min = 1, max = 100},
				{label = _U("pharmacy_take", _U("bandage")), item = "bandage", type = "slider", value = 1, min = 1, max = 100}
			}
		},
		function(data, menu)
			TriggerServerEvent("esx_ambulancejob:giveItem", data.current.item, data.current.value)
		end,
		function(data, menu)
			menu.close()
		end
	)
end

RegisterNetEvent("esx_ambulancejob:heal")
AddEventHandler(
	"esx_ambulancejob:heal",
	function(healType, quiet)
		local playerPed = PlayerPedId()
		local maxHealth = GetEntityMaxHealth(playerPed)

		if healType == "small" then
			local health = GetEntityHealth(playerPed)
			local newHealth = math.min(maxHealth, math.floor(health + maxHealth / 8))
			SetEntityHealth(playerPed, newHealth)
		elseif healType == "big" then
			SetEntityHealth(playerPed, maxHealth)
		end

		if not quiet then
			ESX.ShowNotification(_U("healed"))
		end
	end
)

RegisterNetEvent("esx:setJob")
AddEventHandler(
	"esx:setJob",
	function(job)
		if isOnDuty and job ~= "ambulance" then
			isOnDuty = false
			-- disables the EMS app on the phone
			TriggerEvent("gcPhone:displayEMSApp", isOnDuty)
			-- notifies the server the player is off duty
			TriggerServerEvent('esx_service:disableService', 'ambulance')
		end
	end
)

RegisterNetEvent('esx_service:enableService')
AddEventHandler('esx_service:enableService', function(jobName)
	isOnDuty = true
end)

RegisterNetEvent('esx_service:disableService')
AddEventHandler('esx_service:disableService', function(jobName)
	isOnDuty = false
end)

RegisterNetEvent("esx_ambulancejob:setDeadPlayers")
AddEventHandler(
	"esx_ambulancejob:setDeadPlayers",
	function(_deadPlayers)
		deadPlayers = _deadPlayers
	end
)

-- Divers --

function startAnimAction(lib, anim)
	ESX.Streaming.RequestAnimDict(lib, function()
		TaskPlayAnim(PlayerPedId(), lib, anim, 8.0, 1.0, -1, 49, 0, false, false, false)
		RemoveAnimDict(lib)
	end)
end

-- --
