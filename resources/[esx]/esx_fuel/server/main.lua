ESX = nil

local Vehicles = {}

function DeleteVehicleSync()
    for i = 1, #Vehicles, 1 do
        local vehicle = Vehicles[i]
        if vehicle == nil then
            table.remove(Vehicles, i)
        end
    end
end

TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)

RegisterServerEvent('fuel:pay')
AddEventHandler('fuel:pay', function(price)
    local xPlayer = ESX.GetPlayerFromId(source)
    local amount = ESX.Math.Round(price)

    if price > 0 then
        xPlayer.removeMoney(amount)
    end
end)

ESX.RegisterServerCallback('fuel:getJerryCan', function(source, cb)
    local xPlayer = ESX.GetPlayerFromId(source)
    local loadoutNum, weapon
    if xPlayer.hasWeapon('WEAPON_PETROLCAN') then
        cb()
    else
        xPlayer.addWeapon('WEAPON_PETROLCAN', math.floor(Config.JerryCanMaxCap))
        loadoutNum, weapon = xPlayer.getWeapon('WEAPON_PETROLCAN')
        cb(math.floor(weapon.ammo))
    end
end)

ESX.RegisterServerCallback('fuel:refillJerryCan', function(source, cb)
    local xPlayer = ESX.GetPlayerFromId(source)
    if xPlayer.hasWeapon('WEAPON_PETROLCAN') then
        local loadoutNum, weapon = xPlayer.getWeapon('WEAPON_PETROLCAN')
        xPlayer.addWeaponAmmo('WEAPON_PETROLCAN', math.floor(Config.JerryCanMaxCap - weapon.ammo))
        loadoutNum, weapon = xPlayer.getWeapon('WEAPON_PETROLCAN')
        cb(math.floor(weapon.ammo))
    else
        cb()
    end
end)

ESX.RegisterServerCallback('fuel:removeToJerryCan', function(source, cb, ammo)
    local xPlayer = ESX.GetPlayerFromId(source)
    if xPlayer.hasWeapon('WEAPON_PETROLCAN') then
        local loadoutNum, weapon = xPlayer.getWeapon('WEAPON_PETROLCAN')
        if weapon.ammo < ammo then
            xPlayer.removeWeaponAmmo('WEAPON_PETROLCAN', math.floor(weapon.ammo))
        else
            xPlayer.removeWeaponAmmo('WEAPON_PETROLCAN', math.floor(ammo))
        end
        loadoutNum, weapon = xPlayer.getWeapon('WEAPON_PETROLCAN')
        cb(math.floor(weapon.ammo))
    else
        cb()
    end
end)

RegisterServerEvent('esx_fuel:SetFuelLevel')
AddEventHandler('esx_fuel:SetFuelLevel', function(plate, fuelLevel)
    local curVehicle = {}
    local trimmedPlate = ESX.Math.Trim(plate)
    if Vehicles[trimmedPlate] ~= nil then
        curVehicle = Vehicles[trimmedPlate]
        curVehicle.fuelLevel = fuelLevel
        curVehicle.lastUpdate = os.time()
        Vehicles[trimmedPlate] = curVehicle
        TriggerClientEvent('esx_fuel:SyncFuel', -1, trimmedPlate, fuelLevel)
    else
        TriggerEvent('lp_core:GetVehicleByPlate', trimmedPlate, function(vehicle, owner)
            local curVehicle = {
                fuelLevel = fuelLevel,
                lastUpdate = os.time()
            }
            if vehicle then
                curVehicle.isPed = false
            else
                curVehicle.isPed = true
            end
            Vehicles[trimmedPlate] = curVehicle
            TriggerClientEvent('esx_fuel:SyncFuel', -1, trimmedPlate, fuelLevel)
        end)
    end
end)

ESX.RegisterServerCallback('esx_fuel:GetFuelLevel', function(source, cb, plate)
    local trimmedPlate = ESX.Math.Trim(plate)
    if Vehicles[trimmedPlate] ~= nil then
        cb(Vehicles[trimmedPlate].fuelLevel)
    else
        cb()
    end
end)

RegisterServerEvent('esx_fuel:DeleteSyncVehicle')
AddEventHandler('esx_fuel:DeleteSyncVehicle', function(plate)
    local trimmedPlate = ESX.Math.Trim(plate)
    printDebug('[event] esx_fuel:DeleteSyncVehicle', 'plate', trimmedPlate, 'Vehicles[plate]', json.encode(Vehicles[trimmedPlate]))
    if Vehicles[trimmedPlate] ~= nil then
        if not Vehicles[trimmedPlate].isPed then
            SaveFuelLevel(trimmedPlate, Vehicles[trimmedPlate].fuelLevel)
        end
        Vehicles[trimmedPlate] = nil
    end
    DeleteVehicleSync()
end)

Citizen.CreateThread(function()
    local countTime = 0
    local time = 0
    while true do
        Citizen.Wait(1000) -- tempo 1 sec
        countTime = countTime + 1
        if countTime == 300 then -- 5 min
            time = os.time()
            countTime = 0
            printDebug('5min elapsed, saving fuelLevel')
            printDebug('Vehicles', json.encode(Vehicles))
            for plate, vehicle in pairs(Vehicles) do
                printDebug('plate', plate, 'vehicle', json.encode(vehicle))
                if not vehicle.isPed then
                    SaveFuelLevel(plate, vehicle.fuelLevel)
                else
                    if (time - vehicle.lastUpdate) > 1800 then -- 30 min last update
                        Vehicles[plate] = nil
                        DeleteVehicleSync()
                    end
                end
            end
        end
    end
end)

function SaveFuelLevel(plate, fuelLevel)
    local trimmedPlate = ESX.Math.Trim(plate)
    MySQL.Async.execute('UPDATE `owned_vehicles` SET `fuel` = @fuel WHERE `plate` = @plate', {
        ['@fuel'] = fuelLevel,
        ['@plate'] = trimmedPlate
    }, function(rowsChanged)
        if rowsChanged > 0 then
            printDebug(('[esx_fuel] Niveau d\'essence du véhicule %s sauvegardé (%s)'):format(trimmedPlate, fuelLevel))
        else
            print(('[esx_fuel] Le véhicule "%s" n\'exite pas en base de donnée'):format(trimmedPlate))
            Vehicles[trimmedPlate] = nil
            DeleteVehicleSync()
        end
    end)
end

function printDebug(str, ...)
    if Config.Debug then
        print(str, ...)
    end
end
