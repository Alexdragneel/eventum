Locales['en'] = {
    ['exit_to_refuel'] = 'exit the vehicle to refuel',
    ['refuel_key'] = '~g~E~w~ refuel vehicle',
    ['cancel_fueling'] = '~g~E~w~ cancel the fueling',
    ['full_tank'] = 'tank is full',
    ['jerrycan_already_owned'] = 'you already bought à jerrycan!',
    ['jerrycan_not_owned'] = 'jerrycan not owned !',

    ['exit_to_recharge'] = 'exit the vehicle to recharge',
    ['recharge_key'] = '~g~E~w~ start charge',
    ['cancel_recharge'] = '~g~E~w~ cancel charge',
    ['full_batteries'] = 'batteries are full',

    ['wrong_engine'] = 'impossible to use this kind of energy for this vehicle',
    ['jerrycan'] = 'jerry can',
    ['vehicle'] = 'vehicle',
    ['purchase_jerrycan'] = "~g~E~w~ purchase a jerry can for ~g~$%s",
    ['refill_jerrycan'] = '~g~E~w~ refill the jerry can for ~g~$%s',
    ['jerrycan_full'] = 'jerry can is full',
    ['jerrycan_empty'] = 'jerry can is empty',
    ['not_enough_cash'] = 'not enough cash',
    ['total_cost'] = 'cost',
    ['gas_station'] = 'gas station',
    ['charge_station'] = 'charge station'
}
