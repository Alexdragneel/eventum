TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)

function GetFuel(vehicle)
    local fuel = ESX.Game.GetVehicleProperties(vehicle).fuelLevel;
    if fuel == 0 then
        fuel = DecorGetFloat(vehicle, Config.FuelDecor)
    end
    return fuel
end

function isElectricVehicle(vehicle)
    local model = GetEntityModel(vehicle)
    local displaytext = GetDisplayNameFromVehicleModel(model)
    for k, v in pairs(Config.ElectricalVehicle) do
        if displaytext == k then
            return v
        end
    end
    return false
end

function SetFuel(vehicle, fuel)
    ESX.Game.SetVehicleProperties(vehicle, {
        fuelLevel = fuel
    });
    DecorSetFloat(vehicle, Config.FuelDecor, fuel)
end

function LoadAnimDict(dict)
    if not HasAnimDictLoaded(dict) then
        RequestAnimDict(dict)

        while not HasAnimDictLoaded(dict) do
            Citizen.Wait(1)
        end
    end
end

function DrawText3Ds(x, y, z, text)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local _, count = string.gsub(text, "\n", "")

    if onScreen then
        SetTextScale(0.35, 0.35)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 215)
        BeginTextCommandDisplayText("STRING")
        SetTextCentre(true)
        AddTextComponentSubstringPlayerName(text)
        SetDrawOrigin(x, y, z, 0)
        DrawText(0.0, 0.0)
        local factor = (string.len(text)) / 370
        DrawRect(0.0, 0.0125 * (count + 1), 0.017 + factor, 0.03 + (0.03 * count), 0, 0, 0, 75)
        ClearDrawOrigin()
    end
end

function Round(num, numDecimalPlaces)
    local mult = 10 ^ (numDecimalPlaces or 0)

    return math.floor(num * mult + 0.5) / mult
end

function CreateBlip(coords)
    local blip = AddBlipForCoord(coords)

    SetBlipSprite(blip, 361)
    SetBlipScale(blip, 0.9)
    SetBlipColour(blip, 4)
    SetBlipDisplay(blip, 4)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('gas_station'))
    EndTextCommandSetBlipName(blip)

    return blip
end

function CreateChargerBlip(coords)
    local blip = AddBlipForCoord(coords)

    SetBlipSprite(blip, 354)
    SetBlipScale(blip, 1.5)
    SetBlipColour(blip, 4)
    SetBlipDisplay(blip, 4)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('charge_station'))
    EndTextCommandSetBlipName(blip)

    return blip
end

function FindNearestFuelPump()
    local coords = GetEntityCoords(PlayerPedId())
    local fuelPumps = {}
    local handle, object = FindFirstObject()
    local success

    repeat
        if Config.PumpModels[GetEntityModel(object)] then
            table.insert(fuelPumps, object)
        end

        success, object = FindNextObject(handle, object)
    until not success

    EndFindObject(handle)

    local pumpObject = 0
    local pumpDistance = 1000

    for _, fuelPumpObject in pairs(fuelPumps) do
        local dstcheck = GetDistanceBetweenCoords(coords, GetEntityCoords(fuelPumpObject))

        if dstcheck < pumpDistance then
            pumpDistance = dstcheck
            pumpObject = fuelPumpObject
        end
    end

    return pumpObject, pumpDistance
end

function FindNearestCharger()
    local coords = GetEntityCoords(PlayerPedId())
    local chargerObject = 0
    local chargerDistance = 1000

    for _, chargerLocation in pairs(Config.ChargerModelsLocation) do
        local dstcheck = GetDistanceBetweenCoords(coords, chargerLocation)

        if dstcheck < chargerDistance then
            chargerDistance = dstcheck
            chargerObject = chargerLocation
        end
    end

    return chargerObject, chargerDistance
end

function GetRandomFuelLevel(tankCap)
    return (math.random(0, tankCap * 100) + 0.0) / 100.0 -- Multiply by 100 to get more digits and return between 0 and 100% of tank cap
end

function printDebug(str, ...)
    if Config.Debug then
        print(str, ...)
    end
end
