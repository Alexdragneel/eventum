local isNearPump = false
local isNearCharger = false
local isFueling = false
local currentFuel = 0.0
local totalCost = 0.0
local currentCash = 0
local fuelSynced = false
local inBlacklisted = true

local vehicle = nil
local vehicleTankCap = 0.0
local vehicleFuelLevel = 0.0
local vehicleFuelLevelPercent = 0.0
local isPedInVeh = false
local wasPedInVeh = false
local fuelType = nil
local waitRefresh = 0
local isDriver = false
local plate = nil
local hasHUD = true

ESX = nil

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end

    RegisterCommand("fuel", function(source, args, raw)
        ESX.TriggerServerCallback('lp_personalmenu:Admin_getUsergroup', function(group)
            if group == 'admin' or group == 'superadmin' then
                if isPedInVeh then
                    local fuelLevel = vehicleTankCap * (tonumber(args[1]) / 100)
                    printDebug('[command] /fuel', 'fuelLevel', args[1], 'plate', plate)
                    TriggerServerEvent('esx_fuel:SetFuelLevel', plate, fuelLevel)
                    TriggerEvent('esx_fuel:Refuel', plate, fuelLevel)
                end
            end
        end)
    end, false)
end)

function ManageFuelUsage(vehicle)
    printDebug('[fn] ManageFuelLevel', 'IsVehicleEngineOn(vehicle)', IsVehicleEngineOn(vehicle))
    if IsVehicleEngineOn(vehicle) then
        local newFuelLevel = vehicleFuelLevel - Config.FuelUsage[Round(GetVehicleCurrentRpm(vehicle), 1)] *
                                 (Config.Classes[GetVehicleClass(vehicle)] or 1.0) / 10
        printDebug('[fn] ManageFuelLevel', 'plate', plate, 'newFuelLevel', newFuelLevel)
        TriggerServerEvent('esx_fuel:SetFuelLevel', plate, newFuelLevel)
    end
end

Citizen.CreateThread(function()
    DecorRegister(Config.FuelDecor, 1)

    for index = 1, #Config.Blacklist do
        if type(Config.Blacklist[index]) == 'string' then
            Config.Blacklist[GetHashKey(Config.Blacklist[index])] = true
        else
            Config.Blacklist[Config.Blacklist[index]] = true
        end
    end

    for index = #Config.Blacklist, 1, -1 do
        table.remove(Config.Blacklist, index)
    end

    while true do
        local ped = PlayerPedId()
        isPedInVeh = IsPedInAnyVehicle(ped, false)
        if isPedInVeh then
            if GetVehiclePedIsIn(ped) ~= vehicle then
                vehicle = GetVehiclePedIsIn(ped)
                wasPedInVeh = false
                fuelType = nil
                waitRefresh = 2
            end
            isDriver = (GetPedInVehicleSeat(vehicle, -1) == ped)
            if not wasPedInVeh and waitRefresh == 0 then
                wasPedInVeh = true
                plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))
                vehicleTankCap = GetVehicleHandlingFloat(vehicle, "CHandlingData", "fPetrolTankVolume")
                if vehicleTankCap == 0.0 then
                    vehicleTankCap = 65.0
                end
                ESX.TriggerServerCallback('esx_fuel:GetFuelLevel', function(fuelLevel)
                    printDebug('[callback] esx_fuel:GetFuelLevel', 'fuel', fuelLevel)
                    if fuelLevel == nil then
                        fuelLevel = GetRandomFuelLevel(vehicleTankCap)
                        printDebug('[callback] esx_fuel:GetFuelLevel', 'randomFuelLevel', fuelLevel)
                        TriggerServerEvent('esx_fuel:SetFuelLevel', plate, fuelLevel)
                    elseif (fuelLevel + 0.0) == -1.0 then -- Set full fuel
                        fuelLevel = vehicleTankCap
                        TriggerServerEvent('esx_fuel:SetFuelLevel', plate, fuelLevel)
                    end
                    vehicleFuelLevel = fuelLevel
                    vehicleFuelLevelPercent = (fuelLevel / vehicleTankCap) * 100
                end, plate)
            end

            if Config.Blacklist[GetEntityModel(vehicle)] then
                inBlacklisted = true
            else
                inBlacklisted = false
            end

            for k, v in pairs(Config.BlacklistClass) do
                if k == GetVehicleClass(vehicle) then
                    if v == true then
                        inBlacklisted = true
                        break
                    end
                end
            end
        else
            if wasPedInVeh then
                wasPedInVeh = false
                TriggerServerEvent('esx_fuel:SetFuelLevel', plate, vehicleFuelLevel)
            end
            if fuelSynced then
                fuelSynced = false
            end

            if inBlacklisted then
                inBlacklisted = false
            end
        end
        if waitRefresh > 0 then
            waitRefresh = waitRefresh -1
        end
        Citizen.Wait(500)
    end
end)

-- Stop engine when no fuel
Citizen.CreateThread(function()
    while true do
        if isPedInVeh and isDriver then
            if vehicleFuelLevelPercent <= 5.0 then
                SetVehicleEngineOn(vehicle, false, true, false)
            end
        end
        Citizen.Wait(10)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(10000)
        if isPedInVeh then
            if not inBlacklisted and isDriver then
                ManageFuelUsage(vehicle)
            end
        end
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(250)

        local pumpObject, pumpDistance = FindNearestFuelPump()
        local chargerObject, chargerDistance = FindNearestCharger()

        if pumpDistance < 2.5 then
            isNearPump = pumpObject
            isNearCharger = false
        elseif chargerDistance < 5 then
            isNearPump = false
            isNearCharger = chargerObject
        else
            isNearPump = false
            isNearCharger = false
        end
        if not isNearPump and not isNearCharger then
            if pumpDistance < chargerDistance then
                Citizen.Wait(math.ceil(pumpDistance * 20))
            else
                Citizen.Wait(math.ceil(chargerDistance * 20))
            end
        else
            local playerData = ESX.GetPlayerData()
            for i = 1, #playerData.accounts, 1 do
                if playerData.accounts[i].name == 'money' then
                    currentCash = playerData.accounts[i].money
                    break
                end
            end
        end
    end
end)

AddEventHandler('fuel:startFuelUpTick', function(pumpObject, ped, vehicle)

    printDebug('[event] fuel:startFuelUpTick', 'GetVehicleNumberPlateText(vehicle)', GetVehicleNumberPlateText(vehicle))
    ESX.TriggerServerCallback('esx_fuel:GetFuelLevel', function(fuelLevel)
        printDebug('[callback] esx_fuel:GetFuelLevel', 'fuelLevel', fuelLevel)
        local tankCap = GetVehicleHandlingFloat(vehicle, "CHandlingData", "fPetrolTankVolume") -- Get Tank volume
        local currentFuel = fuelLevel
        local currentCost = 0.0
        totalCost = 0
        if tankCap == 0 then
            tankCap = 65
        end
        while isFueling do
            Citizen.Wait(500)
            local oldFuel = currentFuel
            local fuelToAdd = math.random(10, 20) / 10.0
            local extraCost = Round((fuelToAdd / 1.5 * Config.CostMultiplier), 1)
            local isEV = isElectricVehicle(vehicle)

            if not pumpObject then
                if isEV == false then
                    if GetAmmoInPedWeapon(ped, 883325847) - fuelToAdd * 100 >= 0 then
                        currentFuel = oldFuel + fuelToAdd
                        ESX.TriggerServerCallback('fuel:removeToJerryCan', function(ammo)
                            if ammo ~= nil then
                                SetPedAmmo(ped, 883325847, math.floor(ammo))
                            end
                        end, (fuelToAdd * 100))
                    else
                        isFueling = false
                    end
                else
                    isFueling = false
                end
            else
                if isEV then
                    if oldFuel >= (tankCap * 0.8) then
                        fuelToAdd = math.random(2, 5) / 10.0 -- 80 pourcent de charge, charge plus lente
                        extraCost = Round((fuelToAdd / 1.5 * Config.CostMultiplier), 1)
                    end
                end
                currentFuel = oldFuel + fuelToAdd
            end

            if currentFuel > tankCap then
                currentFuel = tankCap
                isFueling = false
            end

            if pumpObject then
                currentCost = currentCost + extraCost

                if fuelToAdd and currentCost < Config.CostMultiplier then
                    currentCost = Config.CostMultiplier
                end
                totalCost = totalCost + ESX.Math.Round(currentCost)

                if currentCash >= totalCost then
                    TriggerServerEvent('fuel:pay', currentCost)
                else
                    isFueling = false
                end
            end
            currentCost = 0.0
            TriggerServerEvent('esx_fuel:SetFuelLevel', plate, currentFuel)
            TriggerEvent('esx_fuel:Refuel', plate, currentFuel)
        end
    end, ESX.Math.Trim(GetVehicleNumberPlateText(vehicle)))
end)

AddEventHandler('fuel:refuelFromPump', function(pumpObject, ped, vehicle)
    local isEV = isElectricVehicle(vehicle)
    TaskTurnPedToFaceEntity(ped, vehicle, 1000)
    Citizen.Wait(1000)
    if pumpObject then
        SetCurrentPedWeapon(ped, -1569615261, true)
    end
    LoadAnimDict("timetable@gardener@filling_can")
    if isNearPump then
        TaskPlayAnim(ped, "timetable@gardener@filling_can", "gar_ig_5_filling_can", 2.0, 8.0, -1, 50, 0, 0, 0, 0)
    elseif isNearCharger then
        TaskPlayAnim(ped, "timetable@gardener@filling_can", "gar_ig_5_filling_can", 2.0, 8.0, 1000, 2, 0, 0, 0, 0)
    end

    TriggerEvent('fuel:startFuelUpTick', pumpObject, ped, vehicle)

    while isFueling do
        local currentFuel = GetPercentFuelLevel(vehicle)
        if isNearPump then
            for _, controlIndex in pairs(Config.DisableKeys) do
                DisableControlAction(0, controlIndex)
            end
        end

        local vehicleCoords = GetEntityCoords(vehicle)

        if pumpObject then
            local stringCoords = GetEntityCoords(pumpObject)
            local extraString = ""

            if isNearPump then
                stringCoords = GetEntityCoords(pumpObject)
            elseif isNearCharger then
                stringCoords = isNearCharger
            end

            extraString = string.format('\n %s: ~g~$%g', _U('total_cost'), Round(totalCost, 1))
            if isEV == false then
                DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('cancel_fueling') .. extraString)
            else
                DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('cancel_recharge') .. extraString)
            end
            DrawText3Ds(vehicleCoords.x, vehicleCoords.y, vehicleCoords.z + 0.5, Round(currentFuel, 1) .. "%")
        else
            DrawText3Ds(vehicleCoords.x, vehicleCoords.y, vehicleCoords.z + 0.5,
                string.format('%s\n%s: ~g~%g%%~w~ | %s: ~g~%g%%~w~', _U('cancel_fueling'), _U('jerrycan'),
                    Round(GetAmmoInPedWeapon(ped, 883325847) / Config.JerryCanMaxCap * 100, 1), _U('vehicle'),
                    Round(currentFuel, 1)))
        end

        if isNearPump then
            if not IsEntityPlayingAnim(ped, "timetable@gardener@filling_can", "gar_ig_5_filling_can", 3) then
                TaskPlayAnim(ped, "timetable@gardener@filling_can", "gar_ig_5_filling_can", 2.0, 8.0, -1, 50, 0, 0, 0, 0)
            end
        end

        if IsControlJustReleased(0, 38) or DoesEntityExist(GetPedInVehicleSeat(vehicle, -1)) or
            (isNearPump and GetEntityHealth(pumpObject) <= 0) then
            isFueling = false
        end

        Citizen.Wait(0)
    end
    ESX.ShowNotification(_U('paid_fuel', ESX.Math.Round(totalCost)))
    ClearPedTasks(ped)
    RemoveAnimDict("timetable@gardener@filling_can")
end)

Citizen.CreateThread(function()
    while true do
        local ped = PlayerPedId()
        local hasJerryCan = (GetSelectedPedWeapon(ped) == 883325847)

        if not isFueling then
            if (isNearPump and (GetEntityHealth(isNearPump) > 0)) or (isNearCharger) or (hasJerryCan) then
                if IsPedInAnyVehicle(ped) and GetPedInVehicleSeat(GetVehiclePedIsIn(ped), -1) == ped then
                    if isNearPump then
                        local pumpCoords = GetEntityCoords(isNearPump)
                        DrawText3Ds(pumpCoords.x, pumpCoords.y, pumpCoords.z + 1.2, _U('exit_to_refuel'))
                    elseif isNearCharger then
                        DrawText3Ds(isNearCharger.x, isNearCharger.y, isNearCharger.z + 1.2, _U('exit_to_recharge'))
                    end
                else
                    local vehicle = GetPlayersLastVehicle()
                    local vehicleCoords = GetEntityCoords(vehicle)
                    if DoesEntityExist(vehicle) and GetDistanceBetweenCoords(GetEntityCoords(ped), vehicleCoords) < 2.5 then
                        if not DoesEntityExist(GetPedInVehicleSeat(vehicle, -1)) then
                            local stringCoords = nil
                            local isEV = isElectricVehicle(vehicle)
                            local canFuel = true

                            if isNearPump then
                                stringCoords = GetEntityCoords(isNearPump)
                            elseif isNearCharger then
                                stringCoords = isNearCharger
                            elseif hasJerryCan then
                                stringCoords = vehicleCoords
                            end

                            if GetPercentFuelLevel(vehicle) < 99 then
                                if hasJerryCan then
                                    if GetAmmoInPedWeapon(ped, 883325847) > 0 then
                                        canFuel = (isEV == false)
                                    else
                                        canFuel = false
                                        if isEV == false then
                                            DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                                _U('jerrycan_empty'))
                                        end
                                    end
                                else
                                    if (isNearPump and isEV) or (isNearCharger and not isEV) then
                                        DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                            _U('wrong_engine'))
                                        canFuel = false
                                    else
                                        if currentCash > 0 then
                                            canFuel = true
                                        else
                                            canFuel = false
                                            DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                                _U('not_enough_cash'))
                                        end
                                    end
                                end
                            else
                                canFuel = false
                                if isEV == false then
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('full_tank'))
                                else
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                        _U('full_batteries'))
                                end
                            end
                            if canFuel then
                                if isEV == false then
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('refuel_key'))
                                else
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('recharge_key'))
                                end
                                if IsControlJustReleased(0, 38) then
                                    isFueling = true
                                    TriggerEvent('fuel:refuelFromPump', isNearPump or isNearCharger, ped, vehicle)
                                    LoadAnimDict("timetable@gardener@filling_can")
                                end
                            end
                        end
                    elseif isNearPump then
                        local stringCoords = GetEntityCoords(isNearPump)
                        if not hasJerryCan then
                            if currentCash >= Config.JerryCanCost then
                                DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                    _U('purchase_jerrycan', Config.JerryCanCost))
                                if IsControlJustReleased(0, 38) then
                                    ESX.TriggerServerCallback('fuel:getJerryCan', function(ammo)
                                        if ammo ~= nil then
                                            TriggerServerEvent('fuel:pay', Config.JerryCanCost)
                                            GiveWeaponToPed(ped, 883325847, Config.JerryCanMaxCap, false, true)
                                        else
                                            ESX.ShowNotification(_U('jerrycan_already_owned'))
                                        end
                                    end)
                                end
                            else
                                DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                    string.format("%s (~r~$%d~w~)", _U('not_enough_cash'), Config.JerryCanCost))
                            end
                        else
                            local refillCost = Round(Config.RefillCost *
                                                         (1 - GetAmmoInPedWeapon(ped, 883325847) / Config.JerryCanMaxCap))
                            if refillCost > 0 then
                                if currentCash >= refillCost then
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                        _U('refill_jerrycan', refillCost))

                                    if IsControlJustReleased(0, 38) then
                                        ESX.TriggerServerCallback('fuel:refillJerryCan', function(ammo)
                                            if ammo ~= nil then
                                                TriggerServerEvent('fuel:pay', refillCost)
                                                SetPedAmmo(ped, 883325847, math.floor(ammo))
                                            else
                                                ESX.ShowNotification(_U('jerrycan_not_owned'))
                                            end
                                        end)
                                    end
                                else
                                    DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2,
                                        string.format("%s (~r~$%d~w~)", _U('not_enough_cash'), refillCost))
                                end
                            else
                                DrawText3Ds(stringCoords.x, stringCoords.y, stringCoords.z + 1.2, _U('jerrycan_full'))
                            end
                        end
                    else
                        Citizen.Wait(250)
                    end
                end
            end
        else
            Citizen.Wait(250)
        end
        Citizen.Wait(0)
    end
end)

if Config.ShowNearestGasStationOnly then
    Citizen.CreateThread(function()
        while true do
            local currentGasBlip = 0
            local currentChargerBlip = 0
            local coords = GetEntityCoords(PlayerPedId())
            local closest = 1000
            local closestCoords

            for _, gasStationCoords in pairs(Config.GasStations) do
                local dstcheck = GetDistanceBetweenCoords(coords, gasStationCoords)

                if dstcheck < closest then
                    closest = dstcheck
                    closestCoords = gasStationCoords
                end
            end

            if DoesBlipExist(currentGasBlip) then
                RemoveBlip(currentGasBlip)
            end

            currentGasBlip = CreateBlip(closestCoords)

            for _, chargerStationCoords in pairs(Config.ChargerStations) do
                local dstcheck = GetDistanceBetweenCoords(coords, chargerStationCoords)

                if dstcheck < closest then
                    closest = dstcheck
                    closestCoords = chargerStationCoords
                end
            end

            if DoesBlipExist(currentChargerBlip) then
                RemoveBlip(currentChargerBlip)
            end

            currentChargerBlip = CreateChargerBlip(closestCoords)

            Citizen.Wait(10000)
        end
    end)
elseif Config.ShowAllGasStations then
    Citizen.CreateThread(function()
        for _, gasStationCoords in pairs(Config.GasStations) do
            CreateBlip(gasStationCoords)
            Citizen.Wait(0)
        end
        for _, chargerStationCoords in pairs(Config.ChargerStations) do
            CreateChargerBlip(chargerStationCoords)
            Citizen.Wait(0)
        end
    end)
end

Citizen.CreateThread(function()
    if Config.EnableHUD then
        local level = 0
        local displayHud = false
        local color = "#b71c1c"
        local icon = 'local_gas_station'
        local x = 0.01135
        local y = 0.002
        local wasPaused = false

        while true do
            local ped = PlayerPedId()
            local isInVeh = IsPedInAnyVehicle(ped, false)
            if isInVeh and not (Config.RemoveHUDForBlacklistedVehicle and inBlacklisted) and hasHUD then
                if IsPauseMenuActive() then
                    if wasPaused == false then
                        wasPaused = true
                        TriggerEvent('lp_hud:sendNui', {
                            setFuel = true,
                            setDisplay = true,
                            display = 0
                        })
                    end
                    displayHud = false
                else
                    wasPaused = false
                    if fuelType == nil then
                        if isElectricVehicle(vehicle) then
                            color = "#ffeb3b"
                            fuelType = 'electric'
                            icon = 'bolt'
                        else
                            color = '#b71c1c'
                            fuelType = 'fuel'
                            icon = 'local_gas_station'
                        end
                    end
                    
                    TriggerEvent('lp_hud:sendNui', {
                        setFuel = true,
                        setDisplay = true,
                        display = 1,
                        update = true,
                        fuels = {{
                            val = math.ceil(  280 - math.ceil( math.ceil(vehicleFuelLevelPercent * 205) / 100) ),
                            color = color,
                            icon = icon
                        }}
                    })
                end
            else
                displayHud = false
                fuelType = nil
                TriggerEvent('lp_hud:sendNui', {
                    setFuel = true,
                    setDisplay = true,
                    display = 0
                })
            end
            Citizen.Wait(30)
        end
    end
    Citizen.Wait(0)
end)

function GetPercentFuelLevel(targetVehicle)
    if targetVehicle == vehicle then
        return vehicleFuelLevelPercent
    else
        return 0.0
    end
end

RegisterNetEvent('esx_fuel:SyncFuel')
AddEventHandler('esx_fuel:SyncFuel', function(targetPlate, fuelLevel)
    local trimmedPlate = ESX.Math.Trim(targetPlate)
    printDebug('[event] esx_fuel:SyncFuel', 'isPedInVeh', isPedInVeh, 'targetPlate', trimmedPlate, 'plate', plate,
        'fuelLevel', fuelLevel, 'vehicleTankCap', vehicleTankCap)
    if isPedInVeh and trimmedPlate == plate then
        if (fuelLevel + 0.0) == -1.0 then
            vehicleFuelLevel = vehicleTankCap
        else
            vehicleFuelLevel = fuelLevel
        end
        vehicleFuelLevelPercent = (fuelLevel / vehicleTankCap) * 100
        if isDriver then
            SetFuel(vehicle, fuelLevel)
        end
    end
end)

RegisterNetEvent('esx_fuel:Refuel')
AddEventHandler('esx_fuel:Refuel', function(targetPlate, fuelLevel)
    local trimmedPlate = ESX.Math.Trim(targetPlate)
    printDebug('[event] esx_fuel:Refuel', 'targetPlate', trimmedPlate, 'plate', plate, 'fuelLevel', fuelLevel, 'vehicleTankCap', vehicleTankCap)
    if trimmedPlate == plate then
        vehicleFuelLevel = fuelLevel
        vehicleFuelLevelPercent = (fuelLevel / vehicleTankCap) * 100
        if isDriver then
            SetFuel(vehicle, fuelLevel)
        end
    end
end)

AddEventHandler('camera:displayHUD', function(_display)
    hasHUD = _display
end)
