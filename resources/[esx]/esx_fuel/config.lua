Config = {}

Config.Locale = 'fr'

Config.Debug = false
-- Modify the fuel-cost here, using a multiplier value. Setting the value to 2.0 would cause a doubled increase.
Config.CostMultiplier = 1.0
-- What should the price of jerry cans be?
Config.JerryCanCost = 100
Config.JerryCanMaxCap = 1000
Config.RefillCost = (Config.JerryCanMaxCap / 100) * Config.CostMultiplier -- If it is missing half of it capacity, this amount will be divided in half, and so on.

-- Fuel decor - No need to change this, just leave it.
Config.FuelDecor = "_FUEL_LEVEL"

-- What keys are disabled while you're fueling.
Config.DisableKeys = {0, 22, 23, 24, 29, 30, 31, 37, 44, 56, 82, 140, 166, 167, 168, 170, 288, 289, 311, 323}

-- Want to use the HUD? Turn this to true.
Config.EnableHUD = true

-- Configure blips here. Turn both to false to disable blips all together.
Config.ShowNearestGasStationOnly = false
Config.ShowAllGasStations = true

Config.PumpModels = {
	[-2007231801] = true,
	[1339433404] = true,
	[1694452750] = true,
	[1933174915] = true,
	[-462817101] = true,
	[-469694731] = true,
	[-164877493] = true
}

--[[
-- May be useful later, dunno
Config.ChargerModels = {
	[2237903458] = true,
	[-2057063838] = true
}
--]]

Config.ChargerModelsLocation = {
	vector3(303.4531, -331.4046, 43.91336),
	vector3(302.3029, -334.5728, 43.91336),
	vector3(301.2106, -337.579, 43.91336),
	vector3(299.9449, -341.066, 43.91336),
	vector3(298.7392, -344.3794, 43.91336),
	vector3(297.6898, -347.2671, 43.91336),
	vector3(296.3323, -351.0045, 43.91336),
	vector3(262.5701, -330.9058, 43.91919),
	vector3(263.8021, -327.5172, 43.91919),
	vector3(265.0009, -324.2299, 43.91919),
	vector3(266.1052, -321.192, 43.91919),
	vector3(267.2607, -318.0233, 43.91919),
	vector3(283.8868, -331.4021, 43.91919),
	vector3(282.6644, -334.7607, 43.91919),
	vector3(281.5362, -337.8604, 43.91919),
	vector3(280.3078, -341.2352, 43.91919)
}

-- Blacklist certain vehicles. Use names or hashes. https://wiki.gtanet.work/index.php?title=Vehicle_Models
Config.Blacklist = {
	"rcbandito"
	--"Adder",
	--276773164
}

Config.BlacklistClass = {
	[13] = true
}

--List of electrical vehicle
Config.ElectricalVehicle = {
	['NEON'] = true,
	['RAIDEN'] = true,
	['CYCLONE'] = true,
	['VOLTIC'] = true,
	['TEZERACT'] = true,
	['IMORGON'] = true,
	['KHAMEL'] = true,
	['CADDY'] = true,
	['CADDY3'] = true,
	['AIRTUG'] = true,
	['SURGE'] = true,

}

-- Do you want the HUD removed from showing in blacklisted vehicles?
Config.RemoveHUDForBlacklistedVehicle = true

-- Class multipliers. If you want SUVs to use less fuel, you can change it to anything under 1.0, and vise versa.
--[[
	Consommation pour un réservoir de 65L avec une vitesse constante
	2.5 -> 1h à 90  (1.18)
	3.75 -> 57min à 90 (0.19)
	4.0 -> 54min à 90 (0.20)
	4.5 -> 48min à 90 (0.225)
	5.0 -> 43min à 90 (0.250)
	6.0 -> 36min à 90 (0.300)
	7.0 -> 30min à 90 (0.350)
--]]
Config.Classes = {
	[0] = 2.5, -- Compacts
	[1] = 2.5, -- Sedans
	[2] = 3.75, -- SUVs
	[3] = 3.75, -- Coupes
	[4] = 4.0, -- Muscle
	[5] = 5.0, -- Sports Classics
	[6] = 4.5, -- Sports
	[7] = 6.0, -- Super
	[8] = 4.0, -- Motorcycles
	[9] = 2.5, -- Off-road
	[10] = 5.0, -- Industrial
	[11] = 3.75, -- Utility
	[12] = 3.75, -- Vans
	[13] = 0.0, -- Cycles
	[14] = 0.1, -- Boats
	[15] = 1.0, -- Helicopters
	[16] = 1.0, -- Planes
	[17] = 5.0, -- Service
	[18] = 2.5, -- Emergency
	[19] = 5.0, -- Military
	[20] = 3.75, -- Commercial
	[21] = 5.0, -- Trains
}

-- The left part is at percentage RPM, and the right is how much fuel (divided by 10) you want to remove from the tank every second
Config.FuelUsage = {
	[1.0] = 1.4,
	[0.9] = 1.2,
	[0.8] = 1.0,
	[0.7] = 0.9,
	[0.6] = 0.8,
	[0.5] = 0.7,
	[0.4] = 0.5,
	[0.3] = 0.4,
	[0.2] = 0.2,
	[0.1] = 0.1,
	[0.0] = 0.05,
}

Config.GasStations = {
	vector3(49.4187, 2778.793, 58.043),
	vector3(263.894, 2606.463, 44.983),
	vector3(1039.958, 2671.134, 39.550),
	vector3(1207.260, 2660.175, 37.899),
	vector3(2539.685, 2594.192, 37.944),
	vector3(2679.858, 3263.946, 55.240),
	vector3(2005.055, 3773.887, 32.403),
	vector3(1687.156, 4929.392, 42.078),
	vector3(1701.314, 6416.028, 32.763),
	vector3(179.857, 6602.839, 31.868),
	vector3(-94.4619, 6419.594, 31.489),
	vector3(-2554.996, 2334.40, 33.078),
	vector3(-1800.375, 803.661, 138.651),
	vector3(-1437.622, -276.747, 46.207),
	vector3(-2096.243, -320.286, 13.168),
	vector3(-724.619, -935.1631, 19.213),
	vector3(-526.019, -1211.003, 18.184),
	vector3(-70.2148, -1761.792, 29.534),
	vector3(265.648, -1261.309, 29.292),
	vector3(819.653, -1028.846, 26.403),
	vector3(1208.951, -1402.567,35.224),
	vector3(1181.381, -330.847, 69.316),
	vector3(620.843, 269.100, 103.089),
	vector3(2581.321, 362.039, 108.468),
	vector3(176.631, -1562.025, 29.263),
	vector3(-319.292, -1471.715, 30.549),
	vector3(1784.324, 3330.55, 41.253)
}

Config.ChargerStations = {
	vector3(285.95, -337.95, 45.53)
}