Config = {}

Config.Locale = 'fr'

Config.BackpackWeight = {
	[40] = 20, [41] = 20, [44] = 20, [45] = 20, [81] = 20, [82] = 20, [85] = 20, [86] = 20
}

exports('Config', function() return Config end)