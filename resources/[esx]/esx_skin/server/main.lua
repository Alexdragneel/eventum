ESX = nil
local prisoners = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterNetEvent('lp_jail:setPlayerIsPrisoner')
AddEventHandler('lp_jail:setPlayerIsPrisoner', function(playerId, isPrisoner)
	prisoners[playerId] = isPrisoner
end)

RegisterServerEvent('esx_skin:save')
AddEventHandler('esx_skin:save', function(skin)
	local xPlayer = ESX.GetPlayerFromId(source)
	local defaultMaxWeight = ESX.GetConfig().MaxWeight
	local backpackModifier = Config.BackpackWeight[skin.bags_1]

	if backpackModifier then
		xPlayer.setMaxWeight(defaultMaxWeight + backpackModifier)
	else
		xPlayer.setMaxWeight(defaultMaxWeight)
	end

	MySQL.Async.execute('UPDATE users SET skin = @skin WHERE identifier = @identifier', {
		['@skin'] = json.encode(skin),
		['@identifier'] = xPlayer.identifier
	})
end)

RegisterServerEvent('esx_skin:responseSaveSkin')
AddEventHandler('esx_skin:responseSaveSkin', function(skin)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.getGroup() == 'admin' then
		local file = io.open('resources/[esx]/esx_skin/skins.txt', "a")

		file:write(json.encode(skin) .. "\n\n")
		file:flush()
		file:close()
	else
		print(('esx_skin: %s attempted saving skin to file'):format(xPlayer.getIdentifier()))
	end
end)

ESX.RegisterServerCallback('esx_skin:getPlayerSkin', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT skin FROM users WHERE identifier = @identifier', {
		['@identifier'] = xPlayer.identifier
	}, function(users)
		local user, skin = users[1]

		local jobSkin = {
			skin_male   = xPlayer.job.skin_male,
			skin_female = xPlayer.job.skin_female
		}

		if user.skin then
			skin = json.decode(user.skin)
		end

		cb(skin, jobSkin)
	end)
end)

ESX.RegisterServerCallback('esx_skin:getPlayerSkinAndCompleteJobSkin', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT skin FROM users WHERE identifier = @identifier', {
		['@identifier'] = xPlayer.identifier
	}, function(users)
		local user, skin = users[1]

		local jobSkin = nil
		local prisonerSkin = nil

		if user.skin then
			skin = json.decode(user.skin)
		end

		if prisoners[source] then
			TriggerEvent('lp_jail:getPrisonerSkin', skin.sex, function(pSkin)
				prisonerSkin = pSkin
			end)
			while prisonerSkin == nil do
				Citizen.Wait(10)
			end
		end
		if skin.sex == 0 then
			jobSkin = xPlayer.job.skin_male
		else
			jobSkin = xPlayer.job.skin_female
		end
		local completeJobSkin = jobSkin

		if prisonerSkin ~= nil then
			for k,v in pairs(skin) do
				local prisonerSkinSet = false
				for l,b in pairs(prisonerSkin) do
					if l == k then
						skin[k] = b
						break
					end
				end
			end
		else
			for k,v in pairs(skin) do
				local clotheIsSetInJobSkin = false
				for l,b in pairs(jobSkin) do
					if l == k then
						clotheIsSetInJobSkin = true
						break
					end
				end
				if not clotheIsSetInJobSkin then
					completeJobSkin[k] = v
				end
			end
		end

		cb(skin, completeJobSkin)
	end)
end)

ESX.RegisterCommand('skin', 'admin', function(xPlayer, args, showError)
	xPlayer.triggerEvent('esx_skin:openSaveableMenu')
end, false, {help = _U('skin')})

ESX.RegisterCommand('skinsave', 'admin', function(xPlayer, args, showError)
	xPlayer.triggerEvent('esx_skin:requestSaveSkin')
end, false, {help = _U('saveskin')})
