ESX = nil
local lastSkin, playerLoaded, cam, isCameraActive
local firstSpawn, zoomOffset, camOffset, heading, playerRegistered, walkPlayerOut = true, 0.0, 0.0, 90.0, false, false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

function OpenMenu(submitCb, cancelCb, restrict)
    local playerPed = PlayerPedId()

    TriggerEvent('skinchanger:getSkin', function(skin) lastSkin = skin end)
    TriggerEvent('skinchanger:getData', function(components, maxVals)
        local elements = {}
        local _components = {}

        -- Restrict menu
        if restrict == nil then
            for i=1, #components, 1 do
                _components[i] = components[i]
            end
        else
            for i=1, #components, 1 do
                local found = false

                for j=1, #restrict, 1 do
                    if components[i].name == restrict[j] then
                        found = true
                    end
                end

                if found then
                    table.insert(_components, components[i])
                end
            end
        end
        -- Insert elements
        for i=1, #_components, 1 do
            local value = _components[i].value
            local componentId = _components[i].componentId

            if componentId == 0 then
                value = GetPedPropIndex(playerPed, _components[i].componentId)
            end

            local data = {
                label = _components[i].label,
                name = _components[i].name,
                value = value,
                min = _components[i].min,
                textureof = _components[i].textureof,
                zoomOffset= _components[i].zoomOffset,
                camOffset = _components[i].camOffset,
                type = 'slider'
            }

            for k,v in pairs(maxVals) do
                if k == _components[i].name then
                    data.max = v
                    break
                end
            end

            table.insert(elements, data)
        end

        -- waits for player to create their identity before displaying skin gui
        local playerWasRegistered = playerRegistered
        while not playerRegistered do
            Citizen.Wait(100)
        end

        CreateSkinCam()
        zoomOffset = _components[1].zoomOffset
        camOffset = _components[1].camOffset

        ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'skin', {
            title = _U('skin_menu'),
            align = 'top-left',
            elements = elements
        }, function(data, menu)
            -- submit
            -- box de confirmation
            if not playerWasRegistered then
                ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'charac_confirm', {
                    title    = _U('charac_confirm'),
                    align    = 'center',
                    elements = {
                        {label = _U('no'),  value = 'no'},
                        {label = _U('yes'), value = 'yes'}
                }}, function(data2, menu2)
                    menu2.close()
                    if data2.current.value == 'yes' then
                        TriggerEvent('skinchanger:getSkin', function(skin) lastSkin = skin end)
                        submitCb(data, menu)
                        DeleteSkinCam()
                    elseif data2.current.value == 'no' then
                        menu.refresh()
                    end
                end, function(data2, menu2)
                    menu2.close()
                    menu.refresh()
                end)
            else
                TriggerEvent('skinchanger:getSkin', function(skin) lastSkin = skin end)
                TriggerServerEvent('lp_jail:removePrisonerClothes')
                submitCb(data, menu)
                DeleteSkinCam()
            end
        end, function(data, menu)
            -- cancel
            if playerWasRegistered then
                menu.close()
                DeleteSkinCam()
                TriggerEvent('skinchanger:loadSkin', lastSkin)

                if cancelCb ~= nil then
                    cancelCb(data, menu)
                end
            end
        end, function(data, menu)
            -- change
            local skin, components, maxVals

            TriggerEvent('skinchanger:getSkin', function(getSkin) skin = getSkin end)

            zoomOffset = data.current.zoomOffset
            camOffset = data.current.camOffset

            if skin[data.current.name] ~= data.current.value then

                if data.current.name == 'tshirt_1' then
                    TriggerEvent('skinchanger:change', 'tshirt_2', 0)
                end
                if data.current.name == 'torso_1' then
                    TriggerEvent('skinchanger:change', 'torso_2', 0)
                end
                if data.current.name == 'decals_1' then
                    TriggerEvent('skinchanger:change', 'decals_2', 0)
                end
                if data.current.name == 'arms' then
                    TriggerEvent('skinchanger:change', 'arms_2', 0)
                end
                if data.current.name == 'pants_1' then
                    TriggerEvent('skinchanger:change', 'pants_2', 0)
                end
                if data.current.name == 'shoes_1' then
                    TriggerEvent('skinchanger:change', 'shoes_2', 0)
                end
                if data.current.name == 'chain_1' then
                    TriggerEvent('skinchanger:change', 'chain_2', 0)
                end
                if data.current.name == 'watches_1' then
                    TriggerEvent('skinchanger:change', 'watches_2', 0)
                end
                if data.current.name == 'bracelets_1' then
                    TriggerEvent('skinchanger:change', 'bracelets_2', 0)
                end
                if data.current.name == 'bags_1' then
                    TriggerEvent('skinchanger:change', 'bags_2', 0)
                end
                if data.current.name == 'helmet_1' then
                    TriggerEvent('skinchanger:change', 'helmet_2', 0)
                end
                if data.current.name == 'glasses_1' then
                    TriggerEvent('skinchanger:change', 'glasses_2', 0)
                end
                if data.current.name == 'ears_1' then
                    TriggerEvent('skinchanger:change', 'ears_2', 0)
                end
                if data.current.name == 'mask_1' then
                    TriggerEvent('skinchanger:change', 'mask_2', 0)
                end

                -- Change skin element
                TriggerEvent('skinchanger:change', data.current.name, data.current.value)

                -- Update max values
                TriggerEvent('skinchanger:getData', function(comp, max)
                    components, maxVals = comp, max
                end)

                local newData = {}

                for i=1, #elements, 1 do
                    newData = {}
                    newData.max = maxVals[elements[i].name]

                    if elements[i].textureof ~= nil and data.current.name == elements[i].textureof then
                        newData.value = 0
                    end

                    menu.update({name = elements[i].name}, newData)
                end

                menu.refresh()
            end
        end, function(data, menu)
            -- close
            if playerWasRegistered then
                DeleteSkinCam()
            end
        end)
    end)
end

function CreateSkinCam()
    if not DoesCamExist(cam) then
        cam = CreateCam('DEFAULT_SCRIPTED_CAMERA', true)
    end

    local playerPed = PlayerPedId()

    SetCamActive(cam, true)
    RenderScriptCams(true, true, 500, true, true)

    isCameraActive = true
    SetCamCoord(cam, GetEntityCoords(playerPed))
    --SetCamRot(cam, 0.0, 0.0, 270.0, true)
end

function DeleteSkinCam()
    isCameraActive = false
    SetCamActive(cam, false)
    RenderScriptCams(false, true, 500, true, true)
    cam = nil
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        if isCameraActive then
            DisableAllControlActions(0)
            EnableControlAction(0, 109, true) -- 6 NUMPAD
			EnableControlAction(0, 108, true) -- 4 NUMPAD
			EnableControlAction(0, 172, true) -- UP ARROW
			EnableControlAction(0, 173, true) -- DOWN ARROW
			EnableControlAction(0, 174, true) -- LEFT ARROW
			EnableControlAction(0, 175, true) -- RIGHT ARROW
			EnableControlAction(0, 191, true) -- ENTER
			--EnableControlAction(0, 215, true) -- ENTER
			--EnableControlAction(0, 18, true) -- ENTER
            EnableControlAction(0, 177, true) --BackSpace, Esc, Right MB

            local playerPed = PlayerPedId()
            local coords    = GetEntityCoords(playerPed)

            local angle = heading * math.pi / 180.0
            local theta = {
                x = math.cos(angle),
                y = math.sin(angle)
            }

            local pos = {
                x = coords.x + (zoomOffset * theta.x),
                y = coords.y + (zoomOffset * theta.y)
            }

            local angleToLook = heading - 140.0
            if angleToLook > 360 then
                angleToLook = angleToLook - 360
            elseif angleToLook < 0 then
                angleToLook = angleToLook + 360
            end

            angleToLook = angleToLook * math.pi / 180.0
            local thetaToLook = {
                x = math.cos(angleToLook),
                y = math.sin(angleToLook)
            }

            local posToLook = {
                x = coords.x + (zoomOffset * thetaToLook.x),
                y = coords.y + (zoomOffset * thetaToLook.y)
            }

            SetCamCoord(cam, pos.x, pos.y, coords.z + camOffset)
            PointCamAtCoord(cam, posToLook.x, posToLook.y, coords.z + camOffset)

            ESX.ShowHelpNotification(_U('use_rotate_view'))
        else
            Citizen.Wait(500)
        end
    end
end)

local walkingOut = false
local unlockPlayerWalk = false
local walkoutTimeout = 10
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local playerPed = GetPlayerPed(-1)
        local coords = GetEntityCoords(playerPed)

        if walkPlayerOut and not walkingOut then
            walkingOut = true
            local heading = GetEntityHeading(playerPed)
            TaskGoToCoordAnyMeans(GetPlayerPed(-1), -1037.5, -2738.3, 20.2, 1.0, 0, 0, 786603, 1.0)
        elseif walkPlayerOut and walkingOut then
            DisableAllControlActions(0)
            TriggerEvent('lp_welcome:fadeIn')
            if ESX.Math.Round(coords.x,0) == -1038.0 or unlockPlayerWalk then
                walkPlayerOut = false
                walkingOut = false
                TriggerEvent("lp_cinematiccam:set", false)
                TriggerEvent('esx_status:setDisplay', 1.0)
                ESX.UI.HUD.SetDisplay(1.0)
            end
        else
            Citizen.Wait(500)
        end
    end
end)
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if walkPlayerOut and walkingOut then
            walkoutTimeout = walkoutTimeout - 1
            Citizen.Wait(1000)
            if walkoutTimeout == 0 then
                unlockPlayerWalk = true
            end
        end 
    end
end)

Citizen.CreateThread(function()
    local angle = 250

    while true do
        Citizen.Wait(0)

        if isCameraActive then
            if IsControlPressed(0, 108) then
                angle = angle - 1
            elseif IsControlPressed(0, 109) then
                angle = angle + 1
            end

            if angle > 360 then
                angle = angle - 360
            elseif angle < 0 then
                angle = angle + 360
            end

            heading = angle + 0.0
        else
            Citizen.Wait(500)
        end
    end
end)

function ChangeInstance(separateInstance, coords)
    local playerPed = PlayerPedId()
    local arrivalVehicle = {
        model = 92612664 --kalahari
    }
    -- move player to instance
    if separateInstance then
        -- set skin creation instance
        TriggerServerEvent('lp_instance:set')
    else
        -- set cinematic camera
        TriggerEvent("lp_cinematiccam:set", true)
        arrivalVehicle.plate = exports["esx_vehicleshop"]:GeneratePlate()
        TriggerServerEvent("lp_garage:addNewCitizenCar", arrivalVehicle)
        Citizen.Wait(800)

        -- fade out
        DoScreenFadeOut(1000)
        while not IsScreenFadedOut() do
            Citizen.Wait(10)
        end

        -- set real world instance
        TriggerServerEvent('lp_instance:set', 0)

        -- teleporting player to airport
        local airport = { x = -1044.00, y = -2748.6, z = 21.4, heading = 324.3 }
        SetEntityCoords(playerPed, airport.x, airport.y, airport.z, false, false, false, false)
        SetEntityHeading(playerPed, airport.heading)
        Citizen.Wait(800)

        -- fade in
        DoScreenFadeIn(1000)
        while not IsScreenFadedIn() do
            Citizen.Wait(10)
        end
    end
end

function OpenSaveableMenu(submitCb, cancelCb, restrict, instance)
    TriggerEvent('skinchanger:getSkin', function(skin) lastSkin = skin end)

    if instance == nil or instance == true then
        ChangeInstance(true)
    end

    OpenMenu(function(data, menu)
        menu.close()
        DeleteSkinCam()
        if instance == nil or instance == true then 
            ChangeInstance(false)
            walkPlayerOut = true
        end

        TriggerEvent('skinchanger:getSkin', function(skin)
            TriggerServerEvent('esx_skin:save', skin)
            if submitCb ~= nil then
                submitCb(data, menu)
            end
        end)

    end, cancelCb, restrict)
end

AddEventHandler('esx:onPlayerSpawn', function()
    Citizen.CreateThread(function()
        while not playerLoaded do
            Citizen.Wait(100)
        end

        if firstSpawn then
            ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
                if skin == nil then
                    -- open skin menu
                    TriggerEvent('skinchanger:loadSkin', {sex = 0}, OpenSaveableMenu)
                else
                    TriggerEvent('skinchanger:loadSkin', skin)
                end
            end)

            firstSpawn = false
        end
    end)
end)

AddEventHandler('esx_skin:resetFirstSpawn', function()
    firstSpawn = true
end)

AddEventHandler('esx_skin:playerRegistered', function()
    playerRegistered = true
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    playerLoaded = true
end)

AddEventHandler('esx_skin:getLastSkin', function(cb) cb(lastSkin) end)
AddEventHandler('esx_skin:setLastSkin', function(skin) lastSkin = skin end)

RegisterNetEvent('esx_skin:openMenu')
AddEventHandler('esx_skin:openMenu', function(submitCb, cancelCb)
    OpenMenu(submitCb, cancelCb, nil)
end)

RegisterNetEvent('esx_skin:openRestrictedMenu')
AddEventHandler('esx_skin:openRestrictedMenu', function(submitCb, cancelCb, restrict)
    OpenMenu(submitCb, cancelCb, restrict)
end)

RegisterNetEvent('esx_skin:openSaveableMenu')
AddEventHandler('esx_skin:openSaveableMenu', function(submitCb, cancelCb)
    OpenSaveableMenu(submitCb, cancelCb, nil, false)
end)

RegisterNetEvent('esx_skin:openSaveableRestrictedMenu')
AddEventHandler('esx_skin:openSaveableRestrictedMenu', function(submitCb, cancelCb, restrict)
    OpenSaveableMenu(submitCb, cancelCb, restrict, false)
end)

RegisterNetEvent('esx_skin:requestSaveSkin')
AddEventHandler('esx_skin:requestSaveSkin', function()
    TriggerEvent('skinchanger:getSkin', function(skin)
        TriggerServerEvent('esx_skin:responseSaveSkin', skin)
    end)
end)
