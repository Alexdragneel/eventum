fx_version 'cerulean'

games { 'gta5' }

description 'ESX Reporter Job'

version '1.0.4'

server_scripts {
  '@mysql-async/lib/MySQL.lua',
  '@es_extended/locale.lua',
  'config.lua',
  'server/main.lua',
  'locales/fr.lua'
}

client_scripts {
  '@es_extended/locale.lua',
  'config.lua',
  'client/main.lua',
  'locales/fr.lua'  
}
