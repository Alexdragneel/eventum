Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 102, g = 0, b = 102 }
Config.EnablePlayerManagement     = true
Config.EnableVaultManagement      = true
Config.EnableSocietyOwnedVehicles = true
Config.EnableHelicopters          = true
Config.EnableMoneyWash            = true
Config.MaxInService               = 200
Config.Locale                     = 'fr'

Config.Blip = {
	Pos     = { x = -1087.048, y = -249.330, z = 36.947},
	Sprite  = 184,
	Display = 2,
	Scale   = 0.85,
	Colour  = 0,
}

Config.Zones = {

    BossActions = {
        Pos   = { x = -1053.683, y = -230.554, z = 43.020 },
        Size  = { x = 1.5, y = 1.5, z = 1.0 },
        Color = { r = 0, g = 100, b = 0 },
        Type  = 20,
    },
	
	Cloakrooms = {
		Pos = { x = -1068.600, y = -241.440, z = 38.833},
		Size = { x = 1.5, y = 1.5, z = 1.0 },
        Color = { r = 0, g = 255, b = 128 },
		Type = 20,
	},

    Vaults = {
        Pos   = { x = -1051.591, y = -232.954, z = 43.050 },
        Size  = { x = 1.3, y = 1.3, z = 1.0 },
        Color        = { r = 0, g = 255, b = 128 },
        Type  = 20,
    },	
}
