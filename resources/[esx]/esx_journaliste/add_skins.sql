UPDATE `job_grades` SET 
`skin_male` = '{\"sex\":0,\"tshirt_1\":15,\"tshirt_2\":0,\"torso_1\":111,\"torso_2\":1,\"decals_1\":0,\"decals_2\":0,\"arms\":4,\"pants_1\":13,\"pants_2\":0,\"shoes_1\":57,\"shoes_2\":10,\"chain_1\":0,\"chain_2\":0}',
`skin_female` = '{\"sex\":1,\"tshirt_1\":14,\"tshirt_2\":0,\"torso_1\":27,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":0,\"pants_1\":0,\"pants_2\":8,\"shoes_1\":3,\"shoes_2\":2,\"chain_1\":2,\"chain_2\":1}'
WHERE `job_name` = 'journaliste' AND `grade` = 0;

UPDATE `job_grades` SET 
`skin_male` = '{\"sex\":0,\"tshirt_1\":6,\"tshirt_2\":1,\"torso_1\":25,\"torso_2\":3,\"decals_1\":0,\"decals_2\":0,\"arms\":11,\"pants_1\":13,\"pants_2\":0,\"shoes_1\":51,\"shoes_2\":1,\"chain_1\":24,\"chain_2\":5}',
`skin_female` = '{\"sex\":1,\"tshirt_1\":24,\"tshirt_2\":0,\"torso_1\":28,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":0,\"pants_1\":6,\"pants_2\":0,\"shoes_1\":13,\"shoes_2\":0,\"chain_1\":0,\"chain_2\":0}'
WHERE `job_name` = 'journaliste' AND `grade` = 1;

UPDATE `job_grades` SET 
`skin_male` = '{\"sex\":0,\"tshirt_1\":33,\"tshirt_2\":0,\"torso_1\":77,\"torso_2\":1,\"decals_1\":0,\"decals_2\":0,\"arms\":11,\"pants_1\":13,\"pants_2\":0,\"shoes_1\":51,\"shoes_2\":1,\"chain_1\":27,\"chain_2\":5}',
`skin_female` = '{\"sex\":1,\"tshirt_1\":40,\"tshirt_2\":7,\"torso_1\":64,\"torso_2\":1,\"decals_1\":0,\"decals_2\":0,\"arms\":0,\"pants_1\":6,\"pants_2\":0,\"shoes_1\":13,\"shoes_2\":0,\"chain_1\":0,\"chain_2\":0}'
WHERE `job_name` = 'journaliste' AND `grade` = 2;

UPDATE `job_grades` SET 
`skin_male` = '{\"sex\":0,\"tshirt_1\":33,\"tshirt_2\":0,\"torso_1\":31,\"torso_2\":0,\"decals_1\":0,\"decals_2\":0,\"arms\":4,\"pants_1\":10,\"pants_2\":0,\"shoes_1\":10,\"shoes_2\":0,\"chain_1\":27,\"chain_2\":5}',
`skin_female` = '{\"sex\":1,\"tshirt_1\":20,\"tshirt_2\":2,\"torso_1\":24,\"torso_2\":3,\"decals_1\":0,\"decals_2\":0,\"arms\":5,\"pants_1\":6,\"pants_2\":0,\"shoes_1\":13,\"shoes_2\":0,\"chain_1\":0,\"chain_2\":0}'
WHERE `job_name` = 'journaliste' AND `grade` = 3;

INSERT INTO `lp_garages` (`name`, `spawnerCoordsX`, `spawnerCoordsY`, `spawnerCoordsZ`, `spawnpointCoordsX`, `spawnpointCoordsY`, `spawnpointCoordsZ`, `spawnpointCoordsHeading`, `type`, `hidden`, `active`, `category`) VALUES
('Garage Weazel News', -1095.98, -254.73, 37.68, -1100.0, -262.77, 37.67, 198.4, 'journaliste', 0, 1, 'car');
