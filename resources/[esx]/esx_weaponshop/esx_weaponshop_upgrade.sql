START TRANSACTION;
DROP TABLE IF EXISTS `weashops`;
--
-- Structure de la table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `license` varchar(60) DEFAULT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `weashops`
--

INSERT INTO `weashops` (`zone`, `item`, `price`, `available`, `license`) VALUES
('GunShop', 'WEAPON_PISTOL', 300, 1, 'weapon'),
('BlackWeashop', 'WEAPON_PISTOL', 500, 1, 'weapon'),
('GunShop', 'WEAPON_FLASHLIGHT', 60, 1, NULL),
('BlackWeashop', 'WEAPON_FLASHLIGHT', 70, 0, NULL),
('GunShop', 'WEAPON_MACHETE', 90, 1, NULL),
('BlackWeashop', 'WEAPON_MACHETE', 110, 0, NULL),
('GunShop', 'WEAPON_NIGHTSTICK', 150, 0, NULL),
('BlackWeashop', 'WEAPON_NIGHTSTICK', 150, 0, NULL),
('GunShop', 'WEAPON_BAT', 100, 1, NULL),
('BlackWeashop', 'WEAPON_BAT', 100, 0, NULL),
('GunShop', 'WEAPON_STUNGUN', 50, 1, 'weapon'),
('BlackWeashop', 'WEAPON_STUNGUN', 50, 1, 'weapon'),
('GunShop', 'WEAPON_MICROSMG', 1400, 0, 'weapon'),
('BlackWeashop', 'WEAPON_MICROSMG', 1700, 1, 'weapon'),
('GunShop', 'WEAPON_PUMPSHOTGUN', 3400, 0, 'weapon'),
('BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500, 1, 'weapon'),
('GunShop', 'WEAPON_ASSAULTRIFLE', 10000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000, 0, 'weapon'),
('GunShop', 'WEAPON_SPECIALCARBINE', 15000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500, 0, 'weapon'),
('GunShop', 'WEAPON_SNIPERRIFLE', 22000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000, 0, 'weapon'),
('GunShop', 'WEAPON_FIREWORK', 18000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_FIREWORK', 20000, 0, 'weapon'),
('GunShop', 'WEAPON_GRENADE', 500, 0, 'weapon'),
('BlackWeashop', 'WEAPON_GRENADE', 650, 0, 'weapon'),
('GunShop', 'WEAPON_BZGAS', 200, 0, 'weapon'),
('BlackWeashop', 'WEAPON_BZGAS', 350, 0, 'weapon'),
('GunShop', 'WEAPON_FIREEXTINGUISHER', 100, 1, NULL),
('BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100, 0, NULL),
('GunShop', 'WEAPON_BALL', 50, 0, NULL),
('BlackWeashop', 'WEAPON_BALL', 50, 0, NULL),
('GunShop', 'WEAPON_SMOKEGRENADE', 100, 0, 'weapon'),
('BlackWeashop', 'WEAPON_SMOKEGRENADE', 100, 0, 'weapon'),
('BlackWeashop', 'WEAPON_APPISTOL', 1100, 0, 'weapon'),
('BlackWeashop', 'WEAPON_CARBINERIFLE', 12000, 1, 'weapon'),
('BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_MINIGUN', 45000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_RAILGUN', 50000, 0, 'weapon'),
('BlackWeashop', 'WEAPON_STICKYBOMB', 500, 0, 'weapon'),
('GunShop', 'WEAPON_KNIFE', 50, 1, NULL),
('GunShop', 'WEAPON_SWITCHBLADE', 75, 1, NULL),
('GunShop', 'GADGET_PARACHUTE', 500, 1, NULL),
('GunShop', 'WEAPON_FLARE', 111, 1, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `weashops`
--
ALTER TABLE `weashops`
  ADD KEY `fk_license` (`license`) USING BTREE;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `weashops`
--
ALTER TABLE `weashops`
  ADD CONSTRAINT `fk_license` FOREIGN KEY (`license`) REFERENCES `licenses` (`type`) ON DELETE SET NULL ON UPDATE CASCADE;

COMMIT;