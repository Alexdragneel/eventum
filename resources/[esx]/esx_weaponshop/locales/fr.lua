Locales ['fr'] = {
  ['buy_license'] = 'acheter une license?',
  ['yes'] = '%s',
  ['no'] = 'non',
  ['weapon_bought'] = 'acheté pour $%s',
  ['not_enough_black'] = 'Vous n\'avez ~r~pas assez~s~ d\'argent sale',
  ['not_enough'] = 'vous n\'avez ~r~pas assez~s~ d\'argent',
  ['already_owned'] = 'vous possédez déjà cette arme!',
  ['shop_menu_title'] = 'magasin',
  ['shop_menu_prompt'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au magasin.',
  ['shop_menu_item'] = '$%s',
  ['map_blip'] = 'armurerie',
  ['max_ammo'] = 'vous possédez le maximum de munition pour cette arme!',
  ['not_owned'] = 'vous ne possédez pas cette arme!'
}
