# GCPHONE

> A Vue.js project - Phone for GTA RP

## Build Setup

``` bash
# move to right folder
cd {RESOURCE_PWD}/gcphone/html_src

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification (will copy build files to resources/gcphone/html folder)
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
