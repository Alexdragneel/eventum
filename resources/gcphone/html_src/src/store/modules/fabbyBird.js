const state = {
  highestScore: []
}

const getters = {
  highestScore: ({ highestScore }) => highestScore
}

const actions = {
  resetBourse ({ commit }) {
    commit('SET_HIGHEST_SCORE', [])
  }
}

const mutations = {
  SET_HIGHEST_SCORE (state, highestScore) {
    state.highestScore = highestScore
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line
  state.highestScore = 12
}
