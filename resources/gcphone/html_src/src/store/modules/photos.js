import PhoneAPI from '../../PhoneAPI'

const state = {
  photos: []
}

const getters = {
  photos: ({ photos }) => photos
}

const actions = {
  async takePhoto () {
    try {
      const { url } = await PhoneAPI.takePhoto()
      if (url !== null && url !== undefined) {
        await PhoneAPI.addPhoto(url)
      }
    } catch (e) {}
  },
  deletePhoto (context, { id }) {
    PhoneAPI.deletePhoto(id)
  },
  resetPhoto ({ commit }) {
    commit('SET_PHOTOS', [])
  }
}

const mutations = {
  SET_PHOTOS (state, photos) {
    state.photos = photos.sort((a, b) => b.id - a.id)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line
  state.photos = [{
    id: 1,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 2,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 3,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 4,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 5,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 6,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 7,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 8,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 9,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 10,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 11,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 12,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 13,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 12,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 13,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 12,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 13,
    url: 'https://picsum.photos/1920/1080'
  },
  {
    id: 12,
    url: 'https://picsum.photos/1920/1080'
  }]
}
