const state = {
  bankAmount: '0'
}

const getters = {
  bankAmount: ({ bankAmount }) => bankAmount
}

const actions = {}

const mutations = {
  SET_PRO_BANK_AMOUNT (state, bankAmount) {
    state.bankAmount = bankAmount
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

