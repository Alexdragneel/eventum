ALTER TABLE `phone_messages` ADD COLUMN `deleted` TINYINT(1) DEFAULT 0;
ALTER TABLE `phone_calls` ADD COLUMN `deleted` TINYINT(1) DEFAULT 0;