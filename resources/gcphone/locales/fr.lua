Locales['fr'] = {
    ["mission_row"] = "LSPD Mission Row",
    ["phone_booth"] = "Cabine téléphonique",
    ["key_answer"] = "Appuyez sur ~INPUT_PICKUP~ pour répondre au téléphone",
    ["new_message"] = "~o~Nouveau message",
    ["new_message_from"] = "~o~Nouveau message de ~y~%s",
    ["new_message_transmitter"] = "~o~Nouveau message de ~g~%s",
    ["use_fixed"] = "~g~%s ~o~(%s) ~n~~INPUT_PICKUP~~w~ Utiliser le téléphone",
    ['enter_phone_number'] = "numéro de téléphone",
    ["new_emergency"] = "~r~NOUVELLE URGENCE!",
    ["new_emergency_noems"] = "~r~NOUVELLE URGENCE!~w~ Il n'y a pas d'ambulancier en service, on a besoin de vous",
    ['ems_gps_marker_added'] = 'La position de l\'urgence a été ajoutée à votre GPS.',
    
    -- Social
    ["new_tweet"] = "Nouveau tweet!",
    
    -- Warning
    ["no_phone"] = "Vous n'avez pas de ~r~téléphone~s~."
}
