ESX = nil

local PlayerPed = false
local PlayerPos = false
local PlayerLastPos = 0

local InUse = false
local currentObject = nil
local coordsObjectInUse

-- // ANIMATION
local Anim = 'dos'
local AnimScroll = 0
local dict, anim =  nil, nil
local playerAnimationsList = {}

local heading = 0.0

local isDead = false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

AddEventHandler('esx:onPlayerSpawn', function() isDead = false end)
AddEventHandler('esx:onPlayerDeath', function() isDead = true end)

function GetAnimation(dict, anim)
    RequestAnimDict(dict)
    while not HasAnimDictLoaded(dict) do
        Citizen.Wait(0)
    end
end

function GetSittingAnimations()
	local elements = {}

    for i = 1, #Config.Animations.Sitting do
        table.insert(elements, {
            label = Config.Animations.Sitting[i].name,
            dict = Config.Animations.Sitting[i].dict,
            anim = Config.Animations.Sitting[i].anim,
            value = i
        })
    end
    return elements
end

function GetBarstoolAnimations()
	local elements = {}

    for i = 1, #Config.Animations.Barstool do
        table.insert(elements, {
            label = Config.Animations.Barstool[i].name,
            dict = Config.Animations.Barstool[i].dict,
            anim = Config.Animations.Barstool[i].anim,
            value = i
        })
    end
    return elements
end

function PlayAnimWithScenario(objectcoords, vertz, dir)
    TaskStartScenarioAtPosition(PlayerPed, Config.objects.SitAnimation.anim, objectcoords.x, objectcoords.y, objectcoords.z + vertz, heading + dir, -1, true, true)
    Citizen.Wait(1350)
    GetAnimation(dict, anim)
    ClearPedTasksImmediately(PlayerPed)
    --SetEntityVisible(PlayerPed, false)
    TriggerServerEvent('ChairBedSystem:Server:PlaySittingAnimation', dict, anim)
    Citizen.Wait(105)
    --SetEntityVisible(PlayerPed, true)
end

-- Menu de sélection des poses assises

function ShowSittingAnimationsMenu()
	local playerPed = PlayerPedId()
	local elements = GetSittingAnimations()
		
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'sittingAnimations', {
		title    = "Poses",
		align    = 'top-left',
		elements = elements,
	}, function (data, menu)
        dict = data.current.dict
        anim = data.current.anim
        if dict ~= nil and anim ~= nil then
            GetAnimation(dict, anim)
            ClearPedTasksImmediately(PlayerPed)
            --SetEntityVisible(PlayerPed, false)
            TriggerServerEvent('ChairBedSystem:Server:PlaySittingAnimation', dict, anim)
            Citizen.Wait(105)
            --SetEntityVisible(PlayerPed, true)
        end	    
	end, function (data, menu)
		ESX.UI.Menu.CloseAll()
	end)
    
end

function ShowBarstoolAnimationsMenu()
	local playerPed = PlayerPedId()
	local elements = GetBarstoolAnimations()
		
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'barstoolAnimations', {
		title    = "Poses",
		align    = 'top-left',
		elements = elements,
	}, function (data, menu)
        dict = data.current.dict
        anim = data.current.anim
        if dict ~= nil and anim ~= nil then
            GetAnimation(dict, anim)
            ClearPedTasksImmediately(PlayerPed)
            --SetEntityVisible(PlayerPed, false)
            TriggerServerEvent('ChairBedSystem:Server:PlaySittingAnimation', dict, anim)
            Citizen.Wait(105)
           -- SetEntityVisible(PlayerPed, true)
        end	    
	end, function (data, menu)
		ESX.UI.Menu.CloseAll()
	end)
    
end

---------------------------------------

Citizen.CreateThread(function()
    while true do
        PlayerPed = PlayerPedId()
        --FreezeEntityPosition(PlayerPed, false)
        PlayerPos = GetEntityCoords(PlayerPedId())
        
        if InUse then

           if Anim == 'dos' and not IsEntityPlayingAnim(PlayerPed, Config.objects.BedBackAnimation.dict, Config.objects.BedBackAnimation.anim, 3) then
                Animation(Config.objects.BedBackAnimation.dict, Config.objects.BedBackAnimation.anim, PlayerPed, 20.0)
            end
    
            if Anim == 'ventre' and not IsEntityPlayingAnim(PlayerPed, Config.objects.BedStomachAnimation.dict, Config.objects.BedStomachAnimation.anim, 3) then
                Animation(Config.objects.BedStomachAnimation.dict, Config.objects.BedStomachAnimation.anim, PlayerPed, 20.0)
            end
        end

        Citizen.Wait(100)
    end
end)

Citizen.CreateThread(function()
    while true do
        local canSit = false
        if not IsPedInAnyVehicle(PlayerPedId(), false) then
            local hasMarkedOne = false
            for i = 1, #Config.objects.locations do
                local current = Config.objects.locations[i]
                local offSetDualSit = current.offSetDualSit
                local offSetTripleSit = current.offSetTripleSit
                local coordsObject = GetEntityCoords(current.object)
                local dist = #(PlayerPos - vector3(coordsObject.x, coordsObject.y, coordsObject.z))
                local forward = GetEntityForwardVector(current.object)
                if current.forward then         
                    coordsObject = coordsObject - forward * current.forward
                    dist = #(PlayerPos - vector3(coordsObject.x, coordsObject.y, coordsObject.z))  
                end

                -- Création/placement des textes 3D

                if dist <= 3.0 then
                    canSit = true
                    local vertz = current.verticalOffsetZ
                    if dist <= 1.5 and not InUse and not hasMarkedOne then

                        -- Lits

                        if current.bed then
                            DrawText3Ds(coordsObject.x, coordsObject.y, coordsObject.z + vertz, Config.Text.LieOnBed)
                            if IsControlJustPressed(0, Config.objects.ButtonToLayOnBed) then
                                Anim = "dos"
                                TriggerServerEvent('ChairBedSystem:Server:Enter', current, coordsObject)
                            elseif IsControlJustPressed(0, Config.objects.ButtonToLayOnBedReverse) then
                                Anim = "ventre"
                                TriggerServerEvent('ChairBedSystem:Server:Enter', current, coordsObject)
                            end
                        
                        -- Sièges

                                -- 2/3 places
                        elseif offSetDualSit ~= nil or offSetTripleSit ~= nil then
                            local currentOffSet = nil
                            if offSetDualSit == nil then
                                currentOffSet = offSetTripleSit
                            else 
                                currentOffSet = offSetDualSit
                            end
                            local positionLeft = GetOffsetFromEntityInWorldCoords(current.object, currentOffSet.x, currentOffSet.y, 0.0)
                            local positionRight = GetOffsetFromEntityInWorldCoords(current.object, -currentOffSet.x, -currentOffSet.y, 0.0)
                            local distanceRight = #(PlayerPos.xy - positionRight.xy)
                            local distanceLeft = #(PlayerPos.xy - positionLeft.xy)
                            local position = nil
                            if dist < distanceRight + 0.5 and dist < distanceLeft + 0.5 and currentOffSet == offSetTripleSit then
                                DrawText3Ds(coordsObject.x, coordsObject.y, coordsObject.z + vertz, Config.Text.SitOnChair)
                                position = coordsObject
                            elseif distanceLeft < distanceRight and distanceLeft < dist and distanceLeft <= 1.5 then
                                DrawText3Ds(positionLeft.x, positionLeft.y, positionLeft.z + vertz, Config.Text.SitOnChair)
                                if current.forward then
                                    positionLeft = positionLeft - forward * current.forward
                                end
                                position = positionLeft
                            elseif distanceRight < distanceLeft and distanceRight < dist and distanceRight <= 1.5 then
                                DrawText3Ds(positionRight.x, positionRight.y, positionRight.z + vertz, Config.Text.SitOnChair)
                                if current.forward then
                                    positionRight = positionRight - forward * current.forward
                                end
                                position = positionRight
                            end
                            if IsControlJustPressed(0, Config.objects.ButtonToSitOnChair) and position ~= nil then
                                Anim = 'sit'
                                TriggerServerEvent('ChairBedSystem:Server:Enter', current, position)
                            end

                                -- 1 place
                        elseif not current.bed and offSetDualSit == nil and offSetTripleSit == nil then
                            DrawText3Ds(coordsObject.x, coordsObject.y, coordsObject.z + vertz, Config.Text.SitOnChair)
                            if IsControlJustPressed(0, Config.objects.ButtonToSitOnChair) then
                                Anim = 'sit'
                                TriggerServerEvent('ChairBedSystem:Server:Enter', current, coordsObject)
                            end 
                        end
                        break
                    end

                    -- Déclenchements des actions après installation du ped

                    if InUse and currentObject.objName == current.objName then
                        vertz = currentObject.verticalOffsetZ
                        local PlayerPos = GetEntityCoords(PlayerPed)
                       
                        -- Changement de pose sur les sièges

                        if not currentObject.bed and not currentObject.barstool then
                            ESX.ShowHelpNotification(Config.Text.InfoCommandsSitting)
                            FreezeEntityPosition(PlayerPed, true)
                            if IsControlJustPressed(0, 38) then
                                ShowSittingAnimationsMenu()   
                            end
                        elseif not currentObject.bed and currentObject.barstool then
                            ESX.ShowHelpNotification(Config.Text.InfoCommandsSitting)
                            FreezeEntityPosition(PlayerPed, true)
                            if IsControlJustPressed(0, 38) then
                                ShowBarstoolAnimationsMenu()  
                            end   
                        end

                        -- Changement de pose sur les lits
                        
                        if (IsControlJustPressed(0, 34) or IsControlJustPressed(0, 35)) and currentObject.bed == true then
                            if Anim == 'dos' then
                                Anim = 'ventre'
                                SetEntityVisible(PlayerPed, false)
                                SetEntityHeading(PlayerPed, GetEntityHeading(PlayerPed) + 180.0)
                                Citizen.Wait(150)
                                SetEntityVisible(PlayerPed, true)
                            else
                                Anim = 'dos'
                                SetEntityVisible(PlayerPed, false)
                                SetEntityHeading(PlayerPed, GetEntityHeading(PlayerPed) - 180.0)
                                Citizen.Wait(150)
                                SetEntityVisible(PlayerPed, true)
                            end
                        end

                        -- Se lever 

                        if IsControlJustPressed(0, Config.objects.ButtonToStandUp) or isDead then
                            InUse = false
                            FreezeEntityPosition(PlayerPed, false)
                            TriggerServerEvent('ChairBedSystem:Server:Leave', coordsObjectInUse)

                            -- Lits 

                            if currentObject.bed then
                                ClearPedTasksImmediately(PlayerPed)
                                SetEntityCoords(PlayerPed, PlayerLastPos.x, PlayerLastPos.y, PlayerLastPos.z - vertz)
                                SetEntityHeading(PlayerPed, GetEntityHeading(currentObject.object) + currentObject.direction)
                            
                            -- Sièges avec enregistrement de la dernière pose

                            elseif not currentObject.bed and not currentObject.barstool then
                                ESX.UI.Menu.CloseAll()
                                playerAnimationsList['animSit'] = {dict = dict, anim = anim}
                                ClearPedTasksImmediately(PlayerPed)
                                SetEntityCoords(PlayerPed, PlayerLastPos.x, PlayerLastPos.y, PlayerLastPos.z - vertz)
                                SetEntityHeading(PlayerPed, GetEntityHeading(currentObject.object) + currentObject.direction)
                                TriggerServerEvent('lp_personalmenu:updateAnimationShortcuts', playerAnimationsList)

                            elseif not currentObject.bed and currentObject.barstool then
                                ESX.UI.Menu.CloseAll()
                                playerAnimationsList['animBar'] = {dict = dict, anim = anim}
                                ClearPedTasksImmediately(PlayerPed)
                                SetEntityCoords(PlayerPed, PlayerLastPos.x, PlayerLastPos.y, PlayerLastPos.z - vertz)
                                SetEntityHeading(PlayerPed, GetEntityHeading(currentObject.object) + currentObject.direction)
                                TriggerServerEvent('lp_personalmenu:updateAnimationShortcuts', playerAnimationsList)
                            
                            --Autres sièges

                            else
                                ClearPedTasksImmediately(PlayerPed)
                                SetEntityCoords(PlayerPed, PlayerLastPos.x, PlayerLastPos.y, PlayerLastPos.z - vertz)
                                SetEntityHeading(PlayerPed, GetEntityHeading(currentObject.object) + currentObject.direction)
                            end
                        end
                    end
                end
            end
        end

        if not canSit then
            Citizen.Wait(100)
        end
        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
	while true do
        local object = 0
		Citizen.Wait(500)
		for i = 1, #Config.objects.locations do
			local current = Config.objects.locations[i]
            object = GetClosestObjectOfType(PlayerPos, 1.0, GetHashKey(current.objName), false, false, false)
            if object ~= 0 then
                current.object = object
            end
		end
	end
end)

RegisterNetEvent('ChairBedSystem:Client:Animation')
AddEventHandler('ChairBedSystem:Client:Animation', function(v, coords)
    local object = v.object
    local vertz = v.verticalOffsetZ
    local dir = v.direction
    local isBed = v.bed
    local objectcoords = coords
    local isBarstool = v.barstool
    coordsObjectInUse = coords

    if Anim == 'ventre' then
        dir = dir + 90
    end
    
    print("current object : " .. v.objName)

    local ped = PlayerPed
    heading = GetEntityHeading(object)
    PlayerLastPos = GetEntityCoords(ped)
    FreezeEntityPosition(object, true)
    currentObject = v
    InUse = true

    -- Gestion des chaises/bancs/tabourets en hauteur
    if isBed == false then
        SetEntityNoCollisionEntity(ped, object, false)
        
        -- Retrouve et réalise la dernière position assise choisie
        ESX.TriggerServerCallback('lp_personalmenu:requestPlayerAnimationShortcuts', function(anAnimationShortcutsList)
            if anAnimationShortcutsList then
                playerAnimationsList = anAnimationShortcutsList
            end
            if not isBarstool and playerAnimationsList['animSit'] then
                dict = playerAnimationsList['animSit'].dict
                anim = playerAnimationsList['animSit'].anim
                PlayAnimWithScenario(objectcoords, vertz, dir)
            elseif not isBarstool and not playerAnimationsList['animSit'] then
                TaskStartScenarioAtPosition(ped, Config.objects.SitAnimation.anim, objectcoords.x, objectcoords.y, objectcoords.z + vertz, heading + dir, -1, true, true)
            elseif isBarstool and playerAnimationsList['animBar'] then
                SetEntityNoCollisionEntity(ped, object, true)
                FreezeEntityPosition(ped, true)
                dict = playerAnimationsList['animBar'].dict
                anim = playerAnimationsList['animBar'].anim
                PlayAnimWithScenario(objectcoords, vertz, dir)
            else
                SetEntityNoCollisionEntity(ped, object, true)
                FreezeEntityPosition(ped, true)
                dict = Config.Animations.Barstool[1].dict
                anim = Config.Animations.Barstool[1].anim
                PlayAnimWithScenario(objectcoords, vertz, dir)
            end
        end)

    -- Gestion des lits
    else
        FreezeEntityPosition(ped, true)
        dict = Config.objects.BedBackAnimation.dict
        anim = Config.objects.BedBackAnimation.anim
        GetAnimation(dict, anim)
        if Anim == 'dos' then
            TriggerServerEvent('ChairBedSystem:Server:BackAnimation', objectcoords, heading, vertz, dir, dict, anim)
        elseif Anim == 'ventre' then
            TriggerServerEvent('ChairBedSystem:Server:StomachAnimation', objectcoords, heading, vertz, dir, dict, anim)
        end
        heading = GetEntityHeading(object) + 135 + dir
    end
end)

-- Spawn Prop

RegisterNetEvent('ChairBedSystem:Client:SpawnProp')
AddEventHandler('ChairBedSystem:Client:SpawnProp', function(propname)
	RequestModel(propname)
    local iter_for_request = 1
    while not HasModelLoaded(propname) and iter_for_request < 5 do
        Citizen.Wait(500)				
        iter_for_request = iter_for_request + 1
    end
    if not HasModelLoaded(propname) then
        SetModelAsNoLongerNeeded(propname)
    else
        local ped = PlayerPedId()
        local x,y,z = table.unpack(GetEntityCoords(ped))
        local created_object = CreateObjectNoOffset(propname, x, y, z, 1, 0, 1)
        PlaceObjectOnGroundProperly(created_object)
        FreezeEntityPosition(created_object,true)
        SetModelAsNoLongerNeeded(propname)
    end
end)