local oArray = {}
local oPlayerUse = {}
local playersLastSittingPose = {}

ESX = {}
AddEventHandler('esx:getSharedObject', function(cb)
	cb(ESX)
end)

function getSharedObject()
	return ESX
end

AddEventHandler('playerDropped', function()
    local oSource = source
    if oPlayerUse[oSource] ~= nil then
        oArray[oPlayerUse[oSource]] = nil
        oPlayerUse[oSource] = nil
    end
end)

RegisterServerEvent('ChairBedSystem:Server:Enter')
AddEventHandler('ChairBedSystem:Server:Enter', function(object, objectcoords)
    local oSource = source
    if oArray[objectcoords] == nil  then
        oPlayerUse[oSource] = objectcoords
        oArray[objectcoords] = true
        TriggerClientEvent('ChairBedSystem:Client:Animation', oSource, object, objectcoords)
    end
end)

RegisterServerEvent('ChairBedSystem:Server:Leave')
AddEventHandler('ChairBedSystem:Server:Leave', function(objectcoords)
    local oSource = source
    oPlayerUse[oSource] = nil
    oArray[objectcoords] = nil
end)

RegisterServerEvent('ChairBedSystem:Server:BackAnimation')
AddEventHandler('ChairBedSystem:Server:BackAnimation', function(objectcoords, heading, vertz, dir, dict, anim)
    SetEntityCoords(source, objectcoords.x, objectcoords.y, objectcoords.z + vertz)
    SetEntityHeading(source, heading + 90 + dir)
    TaskPlayAnim(source, dict, anim, 8.0, 1.0, -1, 1, 0, 0, 0, 0)
end)

RegisterServerEvent('ChairBedSystem:Server:StomachAnimation')
AddEventHandler('ChairBedSystem:Server:StomachAnimation', function(objectcoords, heading, vertz, dir, dict, anim)
    SetEntityCoords(source, objectcoords.x, objectcoords.y, objectcoords.z + vertz)
    SetEntityHeading(source, heading + 180.0 + dir)
    TaskPlayAnim(source, dict, anim, 8.0, 1.0, -1, 1, 0, 0, 0, 0)
end)

RegisterServerEvent('ChairBedSystem:Server:PlaySittingAnimation')
AddEventHandler('ChairBedSystem:Server:PlaySittingAnimation', function(dict, anim)
    TaskPlayAnim(source, dict, anim, 20.0, 1.0, -1, 1, 0, 0, 0, 0)
end)

RegisterCommand('spawnprop', function(source, args)
    TriggerClientEvent('ChairBedSystem:Client:SpawnProp', source, args[1])
end, false)
