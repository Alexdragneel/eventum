Config = {}

--[[

local object_model = “v_res_skateboard”
local function create_object()
   Citizen.CreateThread(function()
        RequestModel(object_model)
    	local iter_for_request = 1
    	while not HasModelLoaded(object_model) and iter_for_request < 5 do
    		Citizen.Wait(500)				
            iter_for_request = iter_for_request + 1
        end
    	if not HasModelLoaded(object_model) then
    		SetModelAsNoLongerNeeded(object_model)
    	else
            local ped = PlayerPedId()
            local x,y,z = table.unpack(GetEntityCoords(ped))
            local created_object = CreateObjectNoOffset(object_model, x, y, z, 1, 0, 1)
            PlaceObjectOnGroundProperly(created_object)
            FreezeEntityPosition(created_object,true)
            SetModelAsNoLongerNeeded(object_model)
         end
     end)
end

]]

Config.objects = {
	Object = nil, ObjectVertX = nil, ObjectVertY = nil, ObjectVertZ = nil, ObjectDir = nil, isBed = nil, -- // Please don't change this line!;)
	ButtonToSitOnChair = 38,
	ButtonToLayOnBed = 38,
	ButtonToLayOnBedReverse = 23, 
	ButtonToStandUp = 73,
	SitAnimation = {anim='PROP_HUMAN_SEAT_CHAIR_MP_PLAYER'},
	BedBackAnimation = {dict='anim@gangops@morgue@table@', anim='ko_front'},
	BedStomachAnimation = {dict='amb@world_human_sunbathe@male@front@base', anim='base'},
	BedSitAnimation = {anim='WORLD_HUMAN_PICNIC'},
	locations = {
		-- Les verticalOffsetX et verticalOffsetY sont mal gérés pour l'instant et sont donc ignorés (ils ne sont pas relatifs à la direction de l'objet)
		-- Seul verticalOffsetZ et direction sont pris en comptes

		-- Objets oks 

			-- Chaises et fauteuils
		{objName="prop_off_chair_05", verticalOffsetZ=0.4, direction=178.0, bed=false},
		{objName="v_ilev_leath_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_serv_ct_chair02", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="v_corp_offchair", verticalOffsetZ=0.5, direction=168.0, bed=false},
		{objName="prop_off_chair_03", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_wheelchair_01", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="v_med_cor_medstool", verticalOffsetZ=0.67, direction=180.0, bed=false, forward = -0.12},
		{objName="prop_chair_01a", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_table_03_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="p_soloffchair_s", verticalOffsetZ=-0.1, direction=180.0, bed=false},
		{objName="prop_ld_farm_chair01", verticalOffsetZ=0.0, direction=0.0, bed=false},
		{objName="prop_skid_chair_01", verticalOffsetZ=0.05, direction=180.0, bed=false},
		{objName="prop_gc_chair02", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="prop_rock_chair_01", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="p_yacht_chair_01_s", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="prop_direct_chair_02", verticalOffsetZ=0.2, direction=180.0, bed=false},
		{objName="prop_yaught_chair_01", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="prop_armchair_01", verticalOffsetZ=0.45, direction=180.0, bed=false},
		{objName="prop_chair_08", verticalOffsetZ=0.00, direction=180.0, bed=false},
		{objName="prop_chateau_chair_01", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="prop_chair_07", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_chair_01b", verticalOffsetZ=0.55, direction=180.0, bed=false},
		{objName="prop_chair_pile_01", verticalOffsetZ=0.8, direction=180.0, bed=false},
		{objName="prop_old_deck_chair", verticalOffsetZ=-0.15, direction=180.0, bed=false},
		{objName="prop_chair_10", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_old_wood_chair", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="prop_chair_03", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_chair_09", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_old_wood_chair_lod", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="prop_chair_06", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_off_chair_04b", verticalOffsetZ=0.42, direction=180.0, bed=false},
		{objName="v_club_officechair", verticalOffsetZ=0.4, direction=180.0, bed=false},
		{objName="prop_skid_chair_03", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="prop_skid_chair_02", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="hei_prop_hei_skid_chair", verticalOffsetZ=0.1, direction=180.0, bed=false},
		{objName="p_clb_officechair_s", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="p_dinechair_01_s", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="p_ilev_p_easychair_s", verticalOffsetZ=0.55, direction=180.0, bed=false},
		{objName="prop_wheelchair_01_s", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_ilev_chair02_ped", verticalOffsetZ=-0.02, direction=180.0, bed=false},
		{objName="v_ilev_hd_chair", verticalOffsetZ=0.68, direction=180.0, bed=false},
		{objName="v_ilev_m_dinechair", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_ilev_p_easychair", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_table_01_chr_b", verticalOffsetZ=0.0, direction=270.0, bed=false},
		{objName="v_res_m_l_chair1", verticalOffsetZ=0.5, direction=180.0, bed=false, forward = 0.02},
		{objName="v_ilev_m_dinechair", verticalOffsetZ=0.52, direction=180.0, bed=false},
		{objName="hei_prop_heist_off_chair", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_chair_02", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_chair_04a", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_chair_04b", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_cs_office_chair", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="Prop_Off_Chair_01", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_off_chair_04", verticalOffsetZ=0.47, direction=168.0, bed=false, forward = 1.0},
		{objName="prop_off_chair_04_s", verticalOffsetZ=0.47, direction=180.0, bed=false},
		{objName="prop_sol_chair", verticalOffsetZ=0.5, direction=180.0, bed=false},	
		{objName="prop_table_01_chr_a", verticalOffsetZ=0.02, direction=180.0, bed=false},
		{objName="prop_table_02_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_table_03b_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_table_04_chr", verticalOffsetZ=0.55, direction=180.0, bed=false, forward = 0.01},
		{objName="prop_table_05_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_table_06_chr", verticalOffsetZ=0.45, direction=180.0, bed=false},
		{objName="prop_torture_ch_01", verticalOffsetZ=0.13, direction=180.0, bed=false},
		{objName="prop_ven_market_stool", verticalOffsetZ=0.25, direction=180.0, bed=false},
		{objName="prop_waiting_seat_01", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_corp_cd_chair", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_ilev_fh_dineeamesa", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_ilev_leath_chr", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="v_ret_gc_chair03", verticalOffsetZ=-0.17, direction=180.0, bed=false},
		{objName="hei_prop_yah_seat_01", verticalOffsetZ=0.65, direction=180.0, bed=false, forward = 0.05},
		{objName="hei_prop_yah_seat_02", verticalOffsetZ=0.55, direction=180.0, bed=false},
		{objName="prop_yacht_seat_01", verticalOffsetZ=0.57, direction=180.0, bed=false},
		{objName="prop_yacht_seat_03", verticalOffsetZ=0.65, direction=180.0, bed=false, forward = 0.05},
		{objName="prop_hobo_seat_01", verticalOffsetZ=0.35, direction=180.0, bed=false, forward = -0.07},
		{objName="prop_chair_05", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="prop_off_chair_01", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="ex_prop_offchair_exec_01", verticalOffsetZ=-0.05, direction=180.0, bed=false},
		{objName="vw_prop_vw_offchair_02", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="vw_prop_casino_track_chair_01", verticalOffsetZ=0.5, direction=180.0, bed=false},
		{objName="ex_mp_h_stn_chairarm_24", verticalOffsetZ=0.52, direction=180.0, bed=false, forward = 0.50}, -- fauteuil un peu partout dans hopital, capté en spawn mais pas dans l'hôpital
		{objName="prop_toilet_01", verticalOffsetZ=0.55, direction=180.0, bed=false},
		{objName="hei_heist_bench02", verticalOffsetZ=0.0, direction=180.0, bed=false},
		{objName="p_armchair_01_s", verticalOffsetZ=0.50, direction=180.0, bed=false, forward = 0.20},
		{objName="prop_rub_couch03", verticalOffsetZ=0.47, direction=180.0, bed=false, forward = 0.25},
		{objName="ex_prop_offchair_exec_04", verticalOffsetZ=-0.1, direction=180.0, bed=false, forward = 0.2},
		{objName="v_res_fa_chair01", verticalOffsetZ=0.32, direction=180.0, bed=false, forward = 0.01},
		--{objName="bkr_prop_clubhouse_chair_01", verticalOffsetZ=-0.1, direction=180.0, bed=false, forward = 0.01}, pb en se relevant sur une des chaises au taxi, passe sous la map
		
		
			-- Canapés/bancs doubles
		{objName="prop_bench_01a", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.5, y=0.0}},
		{objName="prop_bench_08", verticalOffsetZ=0.47, direction=180.0, bed=false, offSetDualSit= {x=0.70, y=0.0}},
		{objName="prop_ld_farm_couch02", verticalOffsetZ=0.0, direction=270.0, bed=false, offSetDualSit= {x=0.00, y=0.50}},
		{objName="prop_bench_01b", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="prop_bench_01c", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="prop_bench_02", verticalOffsetZ=0.46, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}, forward = 0.05},
		{objName="prop_bench_03", verticalOffsetZ=0.45, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="prop_bench_04", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="prop_ld_bench01", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.60, y=0.00}, forward = -0.20},	
		{objName="prop_rub_couch01", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.60, y=0.00}},
		{objName="prop_rub_couch04", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.60, y=0.00}},
		{objName="p_yacht_sofa_01_s", verticalOffsetZ=0.0, direction=180.0, bed=false, offSetDualSit= {x=0.40, y=0.00}},
		{objName="v_tre_sofa_mess_b_s", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="v_tre_sofa_mess_c_s", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="p_yacht_sofa_01_s", verticalOffsetZ=0.0, direction=180.0, bed=false, offSetDualSit= {x=0.40, y=0.00}},
		{objName="v_tre_sofa_mess_b_s", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="v_tre_sofa_mess_c_s", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="prop_table_08_chr", verticalOffsetZ=0.30, direction=180.0, bed=false, offSetDualSit= {x=0.50, y=0.00}},
		{objName="hei_prop_yah_seat_03", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.30, y=0.00}},
		{objName="prop_yacht_seat_02", verticalOffsetZ=0.57, direction=180.0, bed=false, offSetDualSit= {x=0.30, y=0.00}},
		{objName="p_res_sofa_l_s", verticalOffsetZ=0.65, direction=180.0, bed=false, offSetDualSit= {x=0.00, y=0.50}},

		
			-- Canapés/bancs triples
		{objName="prop_bench_09", verticalOffsetZ=0.3, direction=180.0, bed=false, offSetTripleSit= {x=0.9, y=0.0}},
		{objName="prop_bench_05", verticalOffsetZ=0.43, direction=180.0, bed=false, offSetTripleSit= {x=1.0, y=0.0}},
		{objName="prop_ld_farm_couch01", verticalOffsetZ=0.0, direction=90.0, bed=false, offSetTripleSit={x=0.0, y=0.7}}, -- prop mal orientée, forward impossible
		{objName="prop_bench_10", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=1.0, y=0.0}, forward = 0.1},
		{objName="prop_bench_06", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=1.0, y=0.0}},
		{objName="prop_bench_11", verticalOffsetZ=0.4, direction=180.0, bed=false, offSetTripleSit={x=1.0, y=0.0}},
		{objName="prop_fib_3b_bench", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=1.1, y=0.0}},
		{objName="prop_wait_bench_01", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=1.1, y=0.0}},
		{objName="p_v_med_p_sofa_s", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=0.9, y=0.0}},
		{objName="v_ilev_ph_bench", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=1.3, y=0.0}},
		{objName="v_med_p_sofa", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=0.85, y=0.0}, forward = 0.2}, -- canapé dans bureau capitaine lspd, capté en spawn mais pas dans le LSPD
		--{objName="prop_bench_07", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetTripleSit={x=0.85, y=0.0}, forward = 0.2}, -- prop mal faite

			-- Tabourets en hauteur
		{objName="prop_direct_chair_01", verticalOffsetZ=0.18, direction=180.0, bed=false, barstool= true},
		{objName="v_corp_bk_chair3", verticalOffsetZ=0.5, direction=180.0, bed=false, barstool = true},
		{objName="v_ilev_fh_kitchenstool", verticalOffsetZ=0.9, direction=180.0, bed=false, barstool = true, forward = -0.1}, -- tabouret du vignoble, capté en spawn mais pas dans le LSPD
		{objName="prop_stool_01", verticalOffsetZ=0.85, direction=180.0, bed=false, barstool = true, forward = -0.1},
		{objName="v_ilev_tort_stool", verticalOffsetZ=0.15, direction=180.0, bed=false, barstool = true, forward = -0.12},
		{objName="vw_prop_casino_chair_01a", verticalOffsetZ=0.8, direction=0.0, bed=false, barstool = true, forward = 0.12},
		{objName="prop_bar_stool_01", verticalOffsetZ=0.9, direction=0.0, bed=false, barstool = true, forward = 0.10},
		{objName="v_ret_ta_stool", verticalOffsetZ=0.57, direction=180.0, bed=false, barstool = true, forward = -0.05},
		{objName="v_ret_gc_chair01", verticalOffsetZ=0.42, direction=180.0, bed=false, barstool = true, forward = -0.05},
		
			-- Lits
		{objName="v_med_bed2", verticalOffsetZ=0.35, direction=90.0, bed=true},
		{objName="v_med_emptybed", verticalOffsetZ=0.12, direction=85.0, bed=true},
		{objName="gabz_pillbox_diagnostics_bed_02", verticalOffsetZ=0.92, direction=87.0, bed=true},
		{objName="gabz_pillbox_diagnostics_bed_03", verticalOffsetZ=0.95, direction=87.0, bed=true},
		{objName="v_med_bed1", verticalOffsetZ=0.33, direction=90.0, bed=true},

		
		
		-- Objets ajoutés mais non détectés, à voir plus tard ?
		--[[{objName="gabz_pillbox_diagnostics_bed_01", verticalOffsetZ=1.2, direction=90.0, bed=true}, -- géré avec le bed_02 et bed_03
		{objName="hei_heist_bench02", verticalOffsetZ=0.0, direction=180.0, bed=false}, --police banc cellule
		{objName="hei_heist_beds02", verticalOffsetZ=1.2, direction=90.0, bed=true}, --police lit cellule]]--



		-- Objets non conformes
		
		--{objName="prop_clown_chair", verticalOffsetZ=0.5, direction=180.0, bed=false}, prop mal fait
		--{objName="v_club_officechair", verticalOffsetZ=0.5, direction=180.0, bed=false}, pose trop basse, verticalOffSetZ qui ne change rien
		--{objName="miss_rub_couch_01", verticalOffsetZ=0.5, direction=180.0, bed=false, offSetDualSit= {x=0.60, y=0.00}}, assise trop basse
		--{objName="prop_rub_couch02", verticalOffsetZ=0.5, direction=180.0, bed=false}, assise trop basse
		--{objName="p_lev_sofa_s", verticalOffsetZ=0.5, direction=180.0, bed=false}, aucune collision, canapé d'angle
		--{objName="v_ilev_m_sofa", verticalOffsetZ=0.5, direction=180.0, bed=false}, canapé d'angle collision ok
		--{objName="v_res_tre_sofa_s", verticalOffsetZ=0.5, direction=180.0, bed=false}, canapé sans collision
		--{objName="v_tre_sofa_mess_a_s", verticalOffsetZ=0.5, direction=180.0, bed=false},
		--{objName="prop_roller_car_01", verticalOffsetZ=0.5, direction=180.0, bed=false},
		--{objName="prop_roller_car_02", verticalOffsetZ=0.5, direction=180.0, bed=false},
	}
}

Config.Text = {
	SitOnChair = '~p~E~w~ pour s\'Asseoir',
	SitOnBed = '~p~E~w~ pour s\'Asseoir sur le lit',
	LieOnBed = '~p~E~w~ pour s\'Allonger',
	Standup = '~INPUT_VEH_DUCK~ pour se Lever',
	InfoCommandsBed = '~INPUT_VEH_DUCK~ pour se Lever\n~INPUT_MOVE_LEFT_ONLY~ ~INPUT_MOVE_RIGHT_ONLY~ pour se Retourner',
	InfoCommandsSitting = '~INPUT_PICKUP~ pour ouvrir le menu des poses\n~INPUT_VEH_DUCK~ pour se Lever'
}

Config.Animations = {
	Sitting = {
		{dict = 'amb@prop_human_seat_chair_mp@male@generic@base', anim = 'base', name = "Assise masculine par défaut"},
		{dict = 'amb@prop_human_seat_chair@male@elbows_on_knees@base', anim = 'base', name = "Coudes sur les genoux"},
		{dict = 'amb@prop_human_seat_chair@male@left_elbow_on_knee@base', anim = 'base', name = "Appui sur le genou gauche"},
		{dict = 'amb@prop_human_seat_chair@male@recline_b@base_b', anim = 'base_b', name = "Incliné en arrière"},
		{dict = 'amb@prop_human_seat_chair@male@right_foot_out@base', anim = 'base', name = "Jambe droite étendue"},
		{dict = 'amb@prop_human_seat_strip_watch@bit_thuggy@base', anim = 'base', name = "Assise dansante"},
		{dict = 'amb@prop_human_seat_chair_mp@female@proper@base', anim = 'base', name = "Assise féminine par défaut"},
		{dict = 'amb@prop_human_seat_chair@female@arms_folded@base', anim = 'base', name = "Bras croisés"},
		{dict = 'amb@prop_human_seat_chair@female@legs_crossed@base', anim = 'base', name = "Jambes croisées"}
	},
	Barstool = {
		{dict = 'amb@prop_human_seat_chair@male@right_foot_out@base', anim = 'base', name = "Jambe droite dans le vide"},
		{dict = 'amb@prop_human_seat_chair@female@legs_crossed@base', anim = 'base', name = "Jambes croisées"},
		{dict = 'amb@prop_human_seat_chair@male@elbows_on_knees@base', anim = 'base', name = "Coudes sur les genoux"},
	}
}