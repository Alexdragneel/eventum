fx_version 'adamant'

game 'gta5'

description 'Police Bike Sounds'

version '1.0'

files {
	'audio/config/pbike_game.dat151.rel'
}

data_file 'AUDIO_GAMEDATA' 'audio/config/pbike_game.dat'
