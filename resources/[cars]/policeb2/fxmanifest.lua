fx_version 'adamant'

game 'gta5'

description 'Police BF400'

version '1.0'

client_scripts {
	"client/names.lua",
}

files {
	'data/vehicles.meta',
	'data/carvariations.meta',
	'data/carcols.meta'
}

data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'
