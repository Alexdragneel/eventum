fx_version 'adamant'

game 'gta5'

description 'Weazel News Vehicles'

version '1.0'

client_scripts {
	"client/names.lua",
}

files {
	'data/vehicles.meta',
	'data/carvariations.meta',
}

data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'
