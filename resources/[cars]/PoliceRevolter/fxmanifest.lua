fx_version 'adamant'

game 'gta5'

description 'Police Revolter'

version '1.0'

client_scripts {
	"client/names.lua",
}

files {
	'data/vehicles.meta',
	'data/carvariations.meta',
	'data/carcols.meta',
	'data/handling.meta',
	'audio/config/policerevolter_game.dat151.rel'
}

data_file 'AUDIO_GAMEDATA' 'audio/config/policerevolter_game.dat'

data_file 'HANDLING_FILE' 'data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'
