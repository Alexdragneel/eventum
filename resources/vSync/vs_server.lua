------------------ change this -------------------

admins = {
    -- 'steam:110000105959047',
    "license:33d85fdea000f071e5850b525044d31ba3fe0841", -- Darkus3 id
    "license:63a67eac04a9cb0bd2e8f21aaadc51a2772539d4", -- ksajan
    "licence:c24a5adfc9c9178df05ae4ee442b4789d2a06016" -- n0x
    -- Add more admin id here (users identifiers in database)
}

-- Set this to false if you don't want the weather to change automatically every 10 minutes.
DynamicWeather = true

-- Set this to false to let Xmas end on jan 3rd naturally
DisableXmas = true

--------------------------------------------------
debugprint = true -- don't touch this unless you know what you're doing or you're being asked by Vespura to turn this on.
--------------------------------------------------

-------------------- DON'T CHANGE THIS --------------------
AvailableWeatherTypes = {
    "EXTRASUNNY",
    "CLEAR",
    "NEUTRAL",
    "SMOG",
    "FOGGY",
    "OVERCAST",
    "CLOUDS",
    "CLEARING",
    "RAIN",
    "THUNDER",
    "SNOW",
    "BLIZZARD",
    "SNOWLIGHT",
    "XMAS",
    "HALLOWEEN"
}
CurrentWeather = "EXTRASUNNY"
local baseTime = 0
local timeOffset = 0
local freezeTime = false
local blackout = false
local newWeatherTimer = math.random(10, 15)
local nextWeather = newWeatherTimer
local calendar = {}
local events = {}

AddEventHandler('onResourceStop', function(resourceName)
    if (GetCurrentResourceName() == resourceName) then
        calendar.month = nil
        calendar.day = nil
    end
end)

AddEventHandler('onResourceStart', function(resourceName)
    if (GetCurrentResourceName() == resourceName and not DisableXmas) then
        calendar.month = tonumber(os.date("%m"))
        calendar.day = tonumber(os.date("%d"))
        calendar.hour = tonumber(os.date("%H"))
        if calendar.month == 12 then
            events.canSnow = true
            if calendar.day >= 15 then
                events.isXmas = true
                DynamicWeather = false
                CurrentWeather = "XMAS"
            end
        elseif calendar.month == 1 then
            if calendar.day < 3 then
                calendar.canSnow = true
            end
        end
    end
end)

RegisterServerEvent("vSync:requestSync")
AddEventHandler(
    "vSync:requestSync",
    function()
        TriggerClientEvent("vSync:updateWeather", -1, CurrentWeather, blackout)
        TriggerClientEvent("vSync:updateTime", -1, baseTime, timeOffset, freezeTime)
        TriggerClientEvent("vSync:setEvents", -1, events)
    end
)

RegisterNetEvent('vSync:freezeTime')
AddEventHandler('vSync:freezeTime', function(freeze)
    freezeTime = freeze
end)

RegisterNetEvent('vSync:freezeWeather')
AddEventHandler('vSync:freezeWeather', function(freeze)
    DynamicWeather = not freeze
end)

function isAllowedToChange(player)
    local allowed = false
    for i, id in ipairs(admins) do
        for x, pid in ipairs(GetPlayerIdentifiers(player)) do
            if debugprint then
                print("admin id: " .. id .. "\nplayer id:" .. pid)
            end
            if string.lower(pid) == string.lower(id) then
                allowed = true
            end
        end
    end
    return allowed
end

RegisterCommand(
    "freezetime",
    function(source, args)
        if source ~= 0 then
            if isAllowedToChange(source) then
                freezeTime = not freezeTime
                if freezeTime then
                    TriggerClientEvent("vSync:notify", source, "L'heure est maintenant ~b~bloqué~s~.")
                else
                    TriggerClientEvent("vSync:notify", source, "L'heure n'est maintenant~y~plus bloqué~s~.")
                end
            else
                TriggerClientEvent(
                    "chatMessage",
                    source,
                    "",
                    {255, 255, 255},
                    "^8Error: ^1Vous n'êtes pas autorisé à utiliser cette commande"
                )
            end
        else
            freezeTime = not freezeTime
            if freezeTime then
                print("L'heure est maintenant bloqué")
            else
                print("L'heure n'est maintenant plus bloqué")
            end
        end
    end
)

RegisterCommand(
    "freezeweather",
    function(source, args)
        if source ~= 0 then
            if isAllowedToChange(source) then
                DynamicWeather = not DynamicWeather
                if not DynamicWeather then
                    TriggerClientEvent(
                        "vSync:notify",
                        source,
                        "Le changement de météo dynamique est maintenant ~r~desactivé~s~."
                    )
                else
                    TriggerClientEvent(
                        "vSync:notify",
                        source,
                        "Le changement de météo dynamique est maintenant ~b~activé~s~."
                    )
                end
            else
                TriggerClientEvent(
                    "chatMessage",
                    source,
                    "",
                    {255, 255, 255},
                    "^8Error: ^1Vous n'êtes pas autorisé à utiliser cette commande."
                )
            end
        else
            DynamicWeather = not DynamicWeather
            if not DynamicWeather then
                print("La météo est maintenant figé")
            else
                print("La météo n'est plus figé.")
            end
        end
    end
)

RegisterCommand(
    "weather",
    function(source, args)
        if source == 0 then
            local validWeatherType = false
            if args[1] == nil then
                print("Syntaxe incorrecte, la bonne syntaxe est: /weather <weathertype> ")
                return
            else
                for i, wtype in ipairs(AvailableWeatherTypes) do
                    if wtype == string.upper(args[1]) then
                        validWeatherType = true
                    end
                end
                if validWeatherType then
                    print("La météo à été changé.")
                    CurrentWeather = string.upper(args[1])
                    newWeatherTimer = math.random(10, 15)
                    nextWeather = newWeatherTimer
                    TriggerEvent("vSync:requestSync")
                else
                    print(
                        "Type de météo invalide, les types de météo valide sont: \nEXTRASUNNY CLEAR NEUTRAL SMOG FOGGY OVERCAST CLOUDS CLEARING RAIN THUNDER SNOW BLIZZARD SNOWLIGHT XMAS HALLOWEEN "
                    )
                end
            end
        else
            if isAllowedToChange(source) then
                local validWeatherType = false
                if args[1] == nil then
                    TriggerClientEvent(
                        "chatMessage",
                        source,
                        "",
                        {255, 255, 255},
                        "^8Error: ^1Syntaxe invalide, utiliser ^0/weather <weatherType> ^1à la place!"
                    )
                else
                    for i, wtype in ipairs(AvailableWeatherTypes) do
                        if wtype == string.upper(args[1]) then
                            validWeatherType = true
                        end
                    end
                    if validWeatherType then
                        TriggerClientEvent(
                            "vSync:notify",
                            source,
                            "La météo va changer pour: ~y~" .. string.lower(args[1]) .. "~s~."
                        )
                        CurrentWeather = string.upper(args[1])
                        newWeatherTimer = math.random(10, 15)
                        nextWeather = newWeatherTimer
                        TriggerEvent("vSync:requestSync")
                    else
                        TriggerClientEvent(
                            "chatMessage",
                            source,
                            "",
                            {255, 255, 255},
                            "^8Error: ^1Type de météo invalide, les types de météo valide sont: ^0\nEXTRASUNNY CLEAR NEUTRAL SMOG FOGGY OVERCAST CLOUDS CLEARING RAIN THUNDER SNOW BLIZZARD SNOWLIGHT XMAS HALLOWEEN "
                        )
                    end
                end
            else
                TriggerClientEvent(
                    "chatMessage",
                    source,
                    "",
                    {255, 255, 255},
                    "^8Error: ^1Vous n'avez pas accès à cette commande."
                )
                print("Accès rejeté pour la commande /weather")
            end
        end
    end,
    false
)

RegisterCommand(
    "blackout",
    function(source)
        if source == 0 then
            blackout = not blackout
            TriggerEvent("vSync:requestSync")
            if blackout then
                print("Panne d'électricité activée.")
            else
                print("Panne d'électricité désactivée.")
            end
        else
            if isAllowedToChange(source) then
                blackout = not blackout
                if blackout then
                    TriggerClientEvent("vSync:notify", source, "La panne d'électricité est maintenant ~b~activée~s~.")
                else
                    TriggerClientEvent("vSync:notify", source, "La panne d'électricité est maintenant ~r~désactivée~s~.")
                end
                TriggerEvent("vSync:requestSync")
            end
        end
    end
)

RegisterCommand(
    "morning",
    function(source)
        if source == 0 then
            print('Pour la console utiliser "/time <hh> <mm>" à la place')
            return
        end
        if isAllowedToChange(source) then
            ShiftToMinute(0)
            ShiftToHour(9)
            TriggerClientEvent("vSync:notify", source, "Heure réglée sur ~y~matinée~s~.")
            TriggerEvent("vSync:requestSync")
        end
    end
)
RegisterCommand(
    "noon",
    function(source)
        if source == 0 then
            print('Pour la console utiliser "/time <hh> <mm>" à la place')
            return
        end
        if isAllowedToChange(source) then
            ShiftToMinute(0)
            ShiftToHour(12)
            TriggerClientEvent("vSync:notify", source, "Heure réglée sur ~y~midi~s~.")
            TriggerEvent("vSync:requestSync")
        end
    end
)
RegisterCommand(
    "evening",
    function(source)
        if source == 0 then
            print('Pour la console utiliser "/time <hh> <mm>" à la place')
            return
        end
        if isAllowedToChange(source) then
            ShiftToMinute(0)
            ShiftToHour(18)
            TriggerClientEvent("vSync:notify", source, "Heure réglée sur ~y~soirée~s~.")
            TriggerEvent("vSync:requestSync")
        end
    end
)
RegisterCommand(
    "night",
    function(source)
        if source == 0 then
            print('Pour la console utiliser "/time <hh> <mm>" à la place')
            return
        end
        if isAllowedToChange(source) then
            ShiftToMinute(0)
            ShiftToHour(23)
            TriggerClientEvent("vSync:notify", source, "Heure réglée sur ~y~nuit~s~.")
            TriggerEvent("vSync:requestSync")
        end
    end
)

function ShiftToMinute(minute)
    timeOffset = timeOffset - (((baseTime + timeOffset) % 60) - minute)
end

function ShiftToHour(hour)
    timeOffset = timeOffset - ((((baseTime + timeOffset) / 60) % 24) - hour) * 60
end

RegisterCommand(
    "time",
    function(source, args, rawCommand)
        if source == 0 then
            if tonumber(args[1]) ~= nil and tonumber(args[2]) ~= nil then
                local argh = tonumber(args[1])
                local argm = tonumber(args[2])
                if argh < 24 then
                    ShiftToHour(argh)
                else
                    ShiftToHour(0)
                end
                if argm < 60 then
                    ShiftToMinute(argm)
                else
                    ShiftToMinute(0)
                end
                print("L'heure à changer pour " .. argh .. ":" .. argm .. ".")
                TriggerEvent("vSync:requestSync")
            else
                print("Syntaxe incorrecte, la bonne syntaxe est: time <hour> <minute> !")
            end
        elseif source ~= 0 then
            if isAllowedToChange(source) then
                if tonumber(args[1]) ~= nil and tonumber(args[2]) ~= nil then
                    local argh = tonumber(args[1])
                    local argm = tonumber(args[2])
                    if argh < 24 then
                        ShiftToHour(argh)
                    else
                        ShiftToHour(0)
                    end
                    if argm < 60 then
                        ShiftToMinute(argm)
                    else
                        ShiftToMinute(0)
                    end
                    local newtime = math.floor(((baseTime + timeOffset) / 60) % 24) .. ":"
                    local minute = math.floor((baseTime + timeOffset) % 60)
                    if minute < 10 then
                        newtime = newtime .. "0" .. minute
                    else
                        newtime = newtime .. minute
                    end
                    TriggerClientEvent("vSync:notify", source, "L'heure à changer pour: ~y~" .. newtime .. "~s~!")
                    TriggerEvent("vSync:requestSync")
                else
                    TriggerClientEvent(
                        "chatMessage",
                        source,
                        "",
                        {255, 255, 255},
                        "^8Error: ^1Syntaxe invalide. Utiliser ^0/time <hour> <minute> ^1à la place!"
                    )
                end
            else
                TriggerClientEvent(
                    "chatMessage",
                    source,
                    "",
                    {255, 255, 255},
                    "^8Error: ^1Vous n\avez pas accès à cette commande."
                )
                print("L'accès à la commande /time vous est refusé.")
            end
        end
    end
)

Citizen.CreateThread(
    function()
        while true do
            Citizen.Wait(0)
            local newBaseTime = os.time(os.date("!*t")) / 2 + 360
            if freezeTime then
                timeOffset = timeOffset + baseTime - newBaseTime
            end
            baseTime = newBaseTime
        end
    end
)

Citizen.CreateThread(
    function()
        while true do
            Citizen.Wait(5000)
            TriggerClientEvent("vSync:updateTime", -1, baseTime, timeOffset, freezeTime)
        end
    end
)

Citizen.CreateThread(
    function()
        local cpt5min = 0
        while true do
            Citizen.Wait(300000)
            cpt5min = cpt5min + 1;
            if cpt5min >= 12 then
                cpt5min = 0
                calendar.hour = tonumber(os.date("%H"))
                if calendar.month == 10 and calendar.day == 31 and calendar.hour == 22 then
                    CurrentWeather = 'FOGGY'
                    DynamicWeather = false
                    events.freeze = {
                        hour = 23
                    }
                    TriggerClientEvent('vSync:setEvents', -1, events)
                elseif calendar.month == 12 and not DisableXmas then
                    if (calendar.day == 24 or calendar.day == 31) and calendar.hour == 22 then
                        events.freeze = {
                            hour = 23
                        }
                        TriggerClientEvent('vSync:setEvents', -1, events)
                    end
                end
            end
            TriggerClientEvent("vSync:updateWeather", -1, CurrentWeather, blackout)
        end
    end
)

Citizen.CreateThread(
    function()
        while true do
            newWeatherTimer = newWeatherTimer - 1
            Citizen.Wait(60000)
            if newWeatherTimer == 0 then
                if DynamicWeather then
                    NextWeatherStage()
                end
                newWeatherTimer = math.random(10, 15)
                nextWeather = newWeatherTimer
            end
        end
    end
)

function NextWeatherStage()
    local new = math.random(1, 10)

    if events.canSnow then
        if CurrentWeather == "EXTRASUNNY" or CurrentWeather == "CLEAR" then
            if new <= 7 then
                CurrentWeather = "CLOUDS" -- Quelques nuages blancs éparses
            else
                CurrentWeather = "CLEAR" -- Ciel dégagé
            end
        elseif CurrentWeather == "CLOUDS" then
            if new <= 3 then
                CurrentWeather = "CLEAR" -- Ciel dégagé
            else
                CurrentWeather = "SNOWLIGHT" -- Neige faible
            end
        elseif CurrentWeather == "SNOWLIGHT" then
            if new >= 1 and new <= 5 then
                CurrentWeather = "CLEAR" -- Ciel dégagé
            else
                CurrentWeather = "CLOUDS"
            end
        end
    else
        if CurrentWeather == "EXTRASUNNY" then
            if new >= 1 and new <= 2 then
                CurrentWeather = "CLOUDS" -- Quelques nuages blancs éparses
            elseif new >= 3 and new <= 7 then
                CurrentWeather = "CLEAR" -- Ciel dégagé
            elseif new >= 8 and new <= 9 then
                CurrentWeather = "SMOG" -- Temps lourd / brouillard de forte chaleur/pollution
            end -- reste en EXTRASUNNY pour new == 10
        elseif CurrentWeather == "CLOUDS" then
            if new >= 1 and new <= 4 then
                CurrentWeather = "OVERCAST" -- Ciel gris
            elseif new >= 5 and new <= 9 then
                CurrentWeather = "CLEAR" -- Ciel dégagé
            end -- reste en CLOUDS pour new == 10
        elseif CurrentWeather == "CLEAR" then
            if new >= 1 and new <= 3 then
                CurrentWeather = "EXTRASUNNY" -- Ciel dégagé et grand soleil
            elseif new >= 4 and new <= 8 then
                CurrentWeather = "CLOUDS" -- Quelques nuages blancs éparses
            end -- reste en CLEAR pour new >= 9 and new <= 10
        elseif CurrentWeather == "SMOG" then
            CurrentWeather = "EXTRASUNNY"
        elseif CurrentWeather == "OVERCAST" then
            if new >= 1 and new <= 6 then
                CurrentWeather = "CLEARING" -- Averses
            elseif new >= 7 and new <= 9 then
                CurrentWeather = "CLOUDS" -- Quelques nuages blancs éparses
            end -- reste en OVERCAST pour new == 10
        elseif CurrentWeather == "CLEARING" then
            if new >= 1 and new <= 3 then
                CurrentWeather = "RAIN" -- Fortes pluies
            else
                CurrentWeather = "CLEAR" -- Quelques nuages blancs éparses
            end
        elseif CurrentWeather == "RAIN" then
            if new >= 1 and new <= 3 then
                CurrentWeather = "CLOUDS" -- Quelques nuages blancs éparses
            elseif new >= 4 and new <= 8 then
                CurrentWeather = "CLEAR" -- Ciel dégagé
            else
                CurrentWeather = "FOGGY" -- Brouillard
            end
        elseif CurrentWeather == "FOGGY" or CurrentWeather == "THUNDER" then
            CurrentWeather = "CLEAR" -- Ciel dégagé
        end
    end

    TriggerEvent("vSync:requestSync")
    if debugprint then
        print("[vSync] Nouvelle météo aléatoire générée: " .. CurrentWeather .. " (" .. new .. ")")
        print("[vSync] Changement de météo dans " .. nextWeather .. "min.")
    end
end

AddEventHandler('txAdmin:events:scheduledRestart', function(eventData)
    if eventData.secondsRemaining == 900 then
        print("Tempête en approche !")
        if events.isXmas then
            CurrentWeather = "BLIZZARD"
        else
            CurrentWeather = "THUNDER"
        end
        DynamicWeather = false
        TriggerEvent("vSync:requestSync")
        DynamicWeather = false
        TriggerEvent("vSync:requestSync")
    end
end)

RegisterCommand("tempete", function(source, args, rawCommand)
    local isAllowed = false
    if source ~= 0 then
        isAllowed = isAllowedToChange(source)
    else
        isAllowed = true
    end
    if isAllowed then
        TriggerEvent('txAdmin:events:scheduledRestart', {secondsRemaining = 900})
        local args = {
            "txAdmin",
            "Tempête en cours ! Rentrez chez vous !"
        }
        while true do
            print("^5[txAdminClient]^0 Admin Broadcast - "..args[1]..": "..args[2])
            TriggerClientEvent(
                "chat:addMessage",
                -1,
                {
                    args = {
                        "(Broadcast) "..args[1],
                        args[2],
                    },
                    color = {255, 0, 0}
                }
            )
            TriggerEvent('txaLogger:internalChatMessage', -1, "(Broadcast) "..args[1], args[2])
            Citizen.Wait(1000 * 60 * 5)
        end
    end
end, true)