ESX = nil

local IndexMenuSelected = {}

Citizen.CreateThread(function()

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	local Keys = {
		["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
		["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
		["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
		["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
		["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
		["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
		["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
		["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
		["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
	}

	local GUI      = {}
	GUI.Time       = 0
	local MenuType = 'default'

	local openMenu = function(namespace, name, data)
		-- number = #data.elements
		-- table.insert(data, number)

		local menu = ESX.UI.Menu.GetOpened(MenuType, namespace, name)
		local val = IndexMenuSelected[name]
		if val ~= nil then
			local oneIsSelected = false
			for i=1, #data.elements, 1 do
				if data.elements[i].selected then
					oneIsSelected = true
				end
			end
			if not oneIsSelected then
				local indexToSelect = -1
				local nbEquivalents = 0
				for i=1, #data.elements, 1 do
					if IsSameMenuElement(val, data.elements[i]) then
						nbEquivalents = nbEquivalents + 1
						indexToSelect = i
					end
				end
				if nbEquivalents == 1 then
					data.elements[indexToSelect].selected = true
				end
			end
		end

		SendNUIMessage({
			action    = 'openMenu',
			namespace = namespace,
			name      = name,
			data      = data,
		})

		-- FreezeEntityPosition(PlayerPedId(), true)
		-- SetPedCombatAttributes(PlayerPedId(), 292, true)
	end

	local closeMenu = function(namespace, name)

		SendNUIMessage({
			action    = 'closeMenu',
			namespace = namespace,
			name      = name,
			data      = data,
		})

		-- FreezeEntityPosition(PlayerPedId(), false)
		-- SetPedCombatAttributes(PlayerPedId(), 292, false)
	end

	ESX.UI.Menu.RegisterType(MenuType, openMenu, closeMenu)

	RegisterNUICallback('menu_submit', function(data, cb)
		local menu = ESX.UI.Menu.GetOpened(MenuType, data._namespace, data._name)
		
		if menu and menu.submit ~= nil and (data.current.disabled == nil or not data.current.disabled) then
			menu.submit(data, menu)
			PlaySoundFrontend(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", false)
		end

		cb('OK')
	end)

	RegisterNUICallback('menu_cancel', function(data, cb)
		
		local menu = ESX.UI.Menu.GetOpened(MenuType, data._namespace, data._name)
		
		if menu and menu.cancel ~= nil then
			menu.cancel(data, menu)
			PlaySoundFrontend(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET", false)					
		end

		cb('OK')
	end)

	RegisterNUICallback('menu_change', function(data, cb)

		local menu = ESX.UI.Menu.GetOpened(MenuType, data._namespace, data._name)
		
		for i=1, #data.elements, 1 do
			
			menu.setElement(i, 'value', data.elements[i].value)

			if data.elements[i].selected then
				menu.setElement(i, 'selected', true)
				IndexMenuSelected[data._name] = data.elements[i]
				PlaySoundFrontend(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", false)						
			else
				menu.setElement(i, 'selected', false)
			end

		end

		if menu and menu.change ~= nil then
			menu.change(data, menu)
		end

		cb('OK')
	end)

	Citizen.CreateThread(function()
		--GUI.Time = GetGameTimer()
		while true do

	  		Wait(0)

			if IsControlPressed(0, 191) and (GetGameTimer() - GUI.Time) > 150 then -- 150 et Keys['ENTER']

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'ENTER'
				})

				GUI.Time = GetGameTimer()

			end

			if IsControlPressed(0, Keys['BACKSPACE']) and (GetGameTimer() - GUI.Time) > 150 then

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'BACKSPACE'
				})

				GUI.Time = GetGameTimer()

			end

			if IsDisabledControlPressed(0, Keys['TOP']) and (GetGameTimer() - GUI.Time) > 150 then

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'TOP'
				})

				GUI.Time = GetGameTimer()

			end

			if IsControlPressed(0, Keys['DOWN']) and (GetGameTimer() - GUI.Time) > 150 then

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'DOWN'
				})

				GUI.Time = GetGameTimer()

			end

			if IsControlPressed(0, Keys['LEFT']) and (GetGameTimer() - GUI.Time) > 150 then

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'LEFT'
				})

				GUI.Time = GetGameTimer()

			end

			if IsControlPressed(0, Keys['RIGHT']) and (GetGameTimer() - GUI.Time) > 150 then

				SendNUIMessage({
					action  = 'controlPressed',
					control = 'RIGHT'
				})

				GUI.Time = GetGameTimer()

			end

	  end
	end)

end)

function IsSameMenuElement(t1,t2)
	
	for k2,v2 in pairs(t2) do
		if k2 ~= 'label' then
			local v1 = t1[k2]
			local v2 = t2[k2]
			if (type(v1) == 'number' or type(v1) == 'string') and type(v1) == type(v2) then
				if v1 ~= v2 then
					return false
				end
			elseif type(v1) ~= type(v2) then
				return false
			end
		end
	end
	return true
end