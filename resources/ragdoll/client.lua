local isRagdolling = false
local isDead = false

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(10)
        if isRagdolling then
            SetPedToRagdoll(GetPlayerPed(-1), 1000, 1000, 0, 0, 0, 0)
            ResetPedRagdollTimer(GetPlayerPed(-1))
        end
    end
end)

AddEventHandler('esx:onPlayerDeath', function()
    isDead = true
end)

AddEventHandler('esx:onPlayerSpawn', function(spawn)
    isDead = false
end)

RegisterCommand('ragdoll', function()
    local Player = GetPlayerPed(-1)
    if CanPedRagdoll(Player) and not isDead and not IsPedInAnyVehicle(Player, false) then
        isRagdolling = not isRagdolling
        if isRagdolling then
            TriggerEvent('ragdoll:playerRagdolling')
        end
    end
end, false)

RegisterKeyMapping('ragdoll', 'Ragdoll', 'keyboard', 'EQUALS')
