ESX = nil
local Radars = {}
local Fines = {}

-- Init ESX stuff --
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
----

AddEventHandler('onResourceStop', function(resourceName)
    if (GetCurrentResourceName() == resourceName) then
        Radars = {}
        Fines = {}
    end
end)

AddEventHandler('onResourceStart', function(resourceName)
    if (GetCurrentResourceName() == resourceName) then
        MySQL.Async.fetchAll(
            'SELECT `lp_radars`.`id`, `lp_radars`.`position`, `lp_road_types`.`road_type`, `lp_road_types`.`speed_limit` \
            FROM `lp_radars` \
            LEFT JOIN `lp_road_types` ON `lp_radars`.`road_type_id`=`lp_road_types`.`id` \
            WHERE `lp_radars`.`active` = 1',
            nil,
            function(radars)
                for _, radar in pairs(radars) do
                    local position = json.decode(radar.position)
                    table.insert(Radars, {
                        id = radar.id,
                        position = vector3(position.x, position.y, position.z),
                        road_type = radar.road_type,
                        speed_limit = radar.speed_limit,
                        broken = false,
                        brokenTime = 0,
                        someoneBreaking = false
                    })
                end
            end
        )
        MySQL.Async.fetchAll(
            'SELECT `fine_types`.`label`,`fine_types`.`amount` FROM `fine_types` WHERE label LIKE "%Excès de vitesse%"',
            nil,
            function(fines)
                for _, fine in pairs(fines) do
                    local label = fine.label
                    local amount = fine.amount
                    if string.find(label, '< 5') then
                        table.insert(Fines, {
                            speed = 5,
                            amount = amount,
                            points = 0
                        })
                    elseif string.find(label, '5-15') then
                        table.insert(Fines, {
                            speed = 15,
                            amount = amount,
                            points = 0
                        })
                    elseif string.find(label, '15-30') then
                        table.insert(Fines, {
                            speed = 30,
                            amount = amount,
                            points = 0
                        })
                    elseif string.find(label, '> 30') then
                        table.insert(Fines, {
                            speed = 31,
                            amount = amount,
                            points = 1
                        })
                    end
                end
            end
        )
    end
end)

RegisterNetEvent('lp_speedradars:getRadars')
AddEventHandler('lp_speedradars:getRadars', function(cb)
    cb(Radars)
end)

ESX.RegisterServerCallback('lp_speedradars:getRadars', function(source, cb)
    cb(Radars)
end)

ESX.RegisterServerCallback('lp_speedradars:getFineList', function(source, cb)
    cb(Fines)
end)

ESX.RegisterServerCallback('lp_speedradars:getFineForSpeed', function(source, cb, speed)
    local fine = Fines[1]
    for i = 1, #Fines, 1 do
        if speed > Fines[i].speed then
            fine = Fines[i]
        end
    end
    cb(fine)
end)

RegisterNetEvent('lp_speedradars:getRoadType')
AddEventHandler('lp_speedradars:getRoadType', function(roadType, cb)
    local type = roadType
    MySQL.Async.fetchAll(
        'SELECT `lp_road_types`.* FROM `lp_road_types` WHERE `lp_road_types`.`road_type`=@type LIMIT 1', {
            ['@type'] = type
        }, function(roadType)
            if #roadType > 0 then
                cb(roadType[1])
            else
                cb(nil)
            end
        end)
end)

RegisterNetEvent('lp_speedradars:addRadar')
AddEventHandler('lp_speedradars:addRadar', function(radarToAdd, cb)
    MySQL.Async.insert("INSERT INTO lp_radars (`position`, `road_type_id`) VALUES(@position, @road_type_id);", {
        ["@position"] = json.encode(radarToAdd.position),
        ["@road_type_id"] = radarToAdd.road_type_id
    }, function(row)
        if row > 0 then
            radarToAdd.id = row
            table.insert(Radars, radarToAdd)
            cb(true)
        else
            cb(false)
        end
    end)
end)

RegisterNetEvent('lp_speedradars:removeRadar')
AddEventHandler('lp_speedradars:removeRadar', function(radarToRemove, cb)
    MySQL.Async.execute("UPDATE `lp_radars` SET `active` = 0 WHERE `id` = @id;", {
        ["@id"] = radarToRemove.id
    }, function(row)
        if row > 0 then
            for index, radar in ipairs(Radars) do
                if radar.id == radarToRemove.id then
                    table.remove(Radars, index)
                    break
                end
            end
            cb(true)
        else
            cb(false)
        end
    end)
end)

RegisterNetEvent('lp_speedradars:showNotification')
AddEventHandler('lp_speedradars:showNotification', function(target, notification)
    TriggerClientEvent('esx:showAdvancedNotification', target.source, notification.title, notification.subject,
        notification.msg, notification.picture, notification.iconType)
end)

RegisterNetEvent('lp_speedradars:someoneBreakingRadar')
AddEventHandler('lp_speedradars:someoneBreakingRadar', function(radarBeingBroken, someoneBreaking)
    for index, radar in ipairs(Radars) do
        if radar.id == radarBeingBroken.id then
            radar.someoneBreaking = someoneBreaking
            break
        end
    end
    TriggerClientEvent('lp_speedradars:updateRadars', -1, Radars)
end)

RegisterNetEvent('lp_speedradars:radarBroken')
AddEventHandler('lp_speedradars:radarBroken', function(radarBroken)
    for index, radar in ipairs(Radars) do
        if radar.id == radarBroken.id then
            radar.broken = true
            radar.brokenTime = 3600
            radar.someoneBreaking = false
            break
        end
    end
    TriggerClientEvent('lp_speedradars:updateRadars', -1, Radars)
end)

RegisterNetEvent('lp_speedradars:newBlipWithRadius')
AddEventHandler('lp_speedradars:newBlipWithRadius', function(coords)
    TriggerEvent('esx_service:getAllInService', function(InService)
        if InService['police'] ~= nil then
            for k, v in pairs(InService['police']) do
                if v == true then
                    TriggerClientEvent('lp_core:newBlipWithRadius', k, coords, { radius = 30.0, duration = 45 })
                end
            end
        end
    end)
end)

Citizen.CreateThread(function()
    local radarChanged = false
    while true do
        Citizen.Wait(1000)
        for index, radar in ipairs(Radars) do
            if radar.brokenTime > 0 then
                radar.brokenTime = radar.brokenTime - 1
                if radar.brokenTime <= 0 then
                    radar.brokenTime = 0
                    radar.broken = false
                end
                radarChanged = true
            end
        end
        if radarChanged then
            TriggerClientEvent('lp_speedradars:updateRadars', -1, Radars)
            radarChanged = false
        end
    end
end)