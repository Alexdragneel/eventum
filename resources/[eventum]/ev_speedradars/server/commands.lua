ESX = nil

-- Init ESX stuff --
TriggerEvent("esx:getSharedObject", function(obj)
    ESX = obj
end)
----

ESX.RegisterCommand('add_radar', 'admin', function(xPlayer, args, showError)
    local source = xPlayer
    TriggerEvent('lp_speedradars:getRoadType', args.road_type, function(roadType)
        if (roadType ~= nil) then
            local coords = source.getCoords(true)
            local location = vector3(coords.x, coords.y,coords.z)
            local radar = {
                position = location,
                road_type = roadType.road_type,
                road_type_id = roadType.id,
                speed_limit = roadType.speed_limit,
                broken = false,
                brokenTime = 0,
                someoneBreaking = false
            }
            TriggerEvent('lp_speedradars:addRadar', radar, function(success)
                if success then
                    TriggerClientEvent('lp_speedradars:radarAdded', -1, radar)
                else
                    showError('Erreur lors de l\'ajout du radar')
                end
            end)
        end
    end)
end, false, {
    help = 'Ajoute un radar à votre position',
    validate = false,
    arguments = {{
        name = 'road_type',
        help = 'Le type de route [city/extra-urban/highway]',
        type = 'any'
    }}
})

ESX.RegisterCommand('remove_radar', 'admin', function(xPlayer, args, showError)
    local source = xPlayer
    local position = source.getCoords(true)
    local closestRadar = nil
    local radars = {}

    TriggerEvent('lp_speedradars:getRadars', function(radars)
        for _, radar in pairs(radars) do
            if #(radar.position - position) < 5 then
                if closestRadar == nil then
                    closestRadar = radar
                    break
                end
            end
        end
        if closestRadar ~= nil then
            TriggerEvent('lp_speedradars:removeRadar', closestRadar, function(success)
                if success then
                    TriggerClientEvent('lp_speedradars:radarRemoved', -1, closestRadar)
                else
                    showError('Erreur lors de la suppression du radar')
                end
            end)
        else
            showError('Aucun radar à proximité')
        end
    end)
end, false, {
    help = 'Supprime le radar le plus proche de vous',
    validate = false,
    arguments = {}
})

ESX.RegisterCommand('radars', nil, function(xPlayer, args, showError)
    local source = xPlayer
    local job = xPlayer.job.name
    local group = xPlayer.getGroup()

    if job == 'police' or group == 'admin' or group == 'superadmin' then
        if args.show == 'show' then
            xPlayer.triggerEvent('lp_speedradars:showRadars')
        elseif args.show == 'hide' then
            xPlayer.triggerEvent('lp_speedradars:hideRadars')
        end
    end
end, false, {
    help = 'Affiche/masque les radars sur la carte',
    validate = false,
    arguments = {{
        name = 'show',
        help = '[show] Affiche les radars, [hide] Masque les radars',
        type = 'string'
    }}
})
