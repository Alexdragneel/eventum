START TRANSACTION;
--
-- Structure de la table `lp_road_types`
--

CREATE TABLE IF NOT EXISTS `lp_road_types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `road_type` varchar(100) NOT NULL,
  `speed_limit` int(11) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Données de la table `lp_road_types`
--

INSERT INTO `lp_road_types` (`road_type`, `speed_limit`) VALUES
('city', 90),
('extra-urban', 110),
('highway', 160);

--
-- Structure de la table `lp_radars`
--

CREATE TABLE `lp_radars` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `position` JSON NOT NULL,
  `road_type_id` INT(11) NOT NULL,
  `active` TINYINT(1) DEFAULT 1,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE lp_radars
ADD CONSTRAINT `fk_road_type` FOREIGN KEY(`road_type_id`)
REFERENCES `lp_road_types`(`id`)
ON DELETE CASCADE;

INSERT INTO `lp_radars` (`position`, `road_type_id`, `active`) VALUES
('{\"x\": 379.68807983398, \"y\": -1048.3527832031, \"z\": 29.250692367554}', 1, 1),
('{\"x\": 182, \"y\": 210.3000030517578, \"z\": 105.5}', 1, 1),
('{\"x\": 97.9000015258789, \"y\": 243.8999938964844, \"z\": 108.0999984741211}', 1, 0),
('{\"x\": 392.2999877929688, \"y\": -610.5999755859375, \"z\": 28.5}', 1, 1),
('{\"x\": 68.30000305175781, \"y\": -979.5999755859376, \"z\": 28.70000076293945}', 1, 1);

ALTER TABLE `user_licenses`
ADD COLUMN points INT(11) DEFAULT NULL AFTER `owner`;
COMMIT;