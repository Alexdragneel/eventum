Locales['fr'] = {
    ['speed_unit'] = 'km/h',
    ['notification_title_speedradar']  = 'radar automatique',
    ['just_flashed_self'] = 'votre véhicule vient d\'être flashé',
    ['just_flashed_enterprise'] = 'un véhicule de votre entreprise vient d\'être flashé',
    ['just_flashed_lspd'] = 'un véhicule vient d\'être flashé',
    ['company_title'] = 'Entreprise',
    ['authorized_vehicle'] = '~g~Véhicule autorisé~w~',
    ['company_vehicle'] = '~y~Véhicule entreprise',
    ['stolen_vehicle'] = '~r~Véhicule volé~w',
    ['no_license'] = '~r~Sans permis~w~',
    ['notification_subject'] = 'Portion %d%s', --%d -> speed, -%s speed unit
    ['notification_message_location'] = '~y~[%s]~w~',
    ['notification_message_speed'] = 'Vitesse : ~y~%d~w~ %s, retenue ~r~%d~w~ %s', -- %d real speed, %s unit, %d retained speed, %s unit
    ['notification_message_plate'] = 'Plaque : ~b~%s~w~', --%s plate number
    ['notification_fine_amount'] = 'Amende : ~g~$%d~w~', --%d fine amount
    -- /!\ NE JAMAIS SUPPRIMER LES ESPACES QUI PARAISSENT USELESS C'EST PAS LE CAS ÇA BUG /!\
    ['notification_license_point_removed'] = 'Retrait~r~ %d~w~, restant :~g~ %d~w~', --%d point removed, %d new points on license
    --
    ['error_remove_points'] = 'Une erreur vous est favorable durant le traitement de votre retrait de point',
    ['break_radar'] = '~g~E~s~ Détruire le radar',
    ['stop_breaking_radar'] = '~r~X~s~ Abandonner',
    ['cant_break_radar'] = '~g~E ~c~Détruire le radar (~r~arme requise~c~)~s~',
    ['notification_message_radar_anomaly'] = '~r~Anomalie détectée~s~',
    ['notification_message_radar_anomaly_msg'] = 'Incident en cours sur un radar'
}