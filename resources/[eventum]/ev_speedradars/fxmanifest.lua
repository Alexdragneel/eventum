fx_version "adamant"

game "gta5"

description "Los Plantos Speed radars"

version "1.0.0"

server_scripts {
    "@mysql-async/lib/MySQL.lua",
    "@es_extended/locale.lua",
    "locales/fr.lua",
    "config.lua",
    "server/commands.lua",
    "server/main.lua"
}

client_scripts {
    "@es_extended/locale.lua",
    "locales/fr.lua",
    "config.lua",
    "client/main.lua"
}

ui_page 'html/ui.html'

files {
	'html/ui.html',
	'html/sounds/sound_alarm.mp3',
	'html/sounds/electric_spark.mp3'
}

dependencies {
    "es_extended"
}
