ESX = nil
local Radars = {}
local PlayerData = {}
local blips = {}
local isBlipsShown = false
local justFlashed = false

local disabledPicking = false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(10)
    end
    RequestModel("prop_cctv_pole_03")
    while not HasModelLoaded("prop_cctv_pole_03") do
        Citizen.Wait(1)
    end
    ESX.TriggerServerCallback('lp_speedradars:getRadars', function(radars)
        for i, radar in ipairs(radars) do
            local radarProp = CreateObject(GetHashKey('prop_cctv_pole_03'), radar.position.x, radar.position.y, (radar.position.z - 2.0), false, true, true)
            radar.prop = radarProp
            FreezeEntityPosition(radar.prop, true)
            SetEntityCanBeDamaged(radar.prop, false)
        end
        Radars = radars
    end)
end)

RegisterCommand('deleteradarprops', function()
    for i, radar in ipairs(Radars) do
        if radar.prop then
            DeleteObject(radar.prop)
            radar.prop = nil
        end
    end
end, false)

RegisterNetEvent('lp_speedradars:updateRadars')
AddEventHandler('lp_speedradars:updateRadars', function (newRadars)
    for i, radar in ipairs(Radars) do
        for i, newRadar in ipairs(newRadars) do
            if newRadar.id == radar.id then
                newRadar.prop = radar.prop
                break
            end
        end
    end
    Radars = newRadars
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

Citizen.CreateThread(function()
    local lastDiff = nil
    local diff = nil
    local playerCoords = nil
    while true do
        local player = GetPlayerPed(-1)
        Citizen.Wait(100)
        for i, radar in ipairs(Radars) do
            if not radar.broken then
                playerCoords = GetEntityCoords(player, true)
                diff = #(playerCoords - radar.position)
                if diff < 20 and not justFlashed then
                    ControlSpeed(player, radar)
                    if lastDiff ~= diff then
                        lastDiff = diff
                    end
                elseif justFlashed then
                    Citizen.Wait(3000)
                    justFlashed = false
                end
            end
        end
    end
end)

Citizen.CreateThread(function()
    local playerCoords = nil
    local diff = nil
    local radarHealth = 0
    local isBreakingRadar = false
    while true do
        local player = GetPlayerPed(-1)
        Citizen.Wait(0)
        for i, radar in ipairs(Radars) do
            if not radar.broken then
                playerCoords = GetEntityCoords(player, true)
                diff = #(playerCoords - radar.position)
                if diff < 2 and (not radar.someoneBreaking or isBreakingRadar) and not IsPedInAnyVehicle(player, true) then
                    local hasMachete = HasPedGotWeapon(player, GetHashKey("WEAPON_MACHETE"), false)
                    local hasBat = HasPedGotWeapon(player, GetHashKey("WEAPON_BAT"), false)
                    local hasGolfclub = HasPedGotWeapon(player, GetHashKey("WEAPON_GOLFCLUB"), false)
                    local hasCrowbar = HasPedGotWeapon(player, GetHashKey("WEAPON_CROWBAR"), false)
                    if hasMachete or hasBat or hasGolfclub or hasCrowbar then
                        if not isBreakingRadar then
                            DrawText3Ds(radar.position.x, radar.position.y, radar.position.z + 1.0, _U('break_radar'))
                            if IsControlJustReleased(0, 38) then
                                isBreakingRadar = true
                                TriggerServerEvent('lp_speedradars:someoneBreakingRadar', radar, true)
                                if hasMachete then 
                                    SetCurrentPedWeapon(player, GetHashKey("WEAPON_MACHETE"), true)
                                elseif hasBat then
                                    SetCurrentPedWeapon(player, GetHashKey("WEAPON_BAT"), true)
                                elseif hasGolfclub then
                                    SetCurrentPedWeapon(player, GetHashKey("WEAPON_GOLFCLUB"), true)
                                elseif hasCrowbar then
                                    SetCurrentPedWeapon(player, GetHashKey("WEAPON_CROWBAR"), true)
                                end
                                ESX.Streaming.RequestAnimDict('melee@large_wpn@streamed_core', function()
                                    TaskPlayAnim(player, 'melee@large_wpn@streamed_core', 'car_side_attack_a', 8.0, -8.0, -1, 1, 0, false, false, false)
                                    RemoveAnimDict('melee@large_wpn@streamed_core')
                                end)
                                TriggerEvent('rprogress:custom', {
                                    Duration = 45000,
                                    Label = '',
                                    onProgress = function (step)
                                        if step == 10 then
                                            if not isAlarmPlaying then
                                                isAlarmPlaying = true
                                                SendNUIMessage({ event = 'playAlarm' })
                                            end
                                        end
                                        if step == 15 then
                                            local streetHash, crossingHash = GetStreetNameAtCoord(radar.position.x + 0.0, radar.position.y + 0.0, radar.position.z + 0.0)
                                            local streetName = GetStreetNameFromHashKey(streetHash)
                                            local crossingName = GetStreetNameFromHashKey(crossingHash)
                                            local location = nil
                                            if crossingName ~= "" then
                                                location = string.format("%s/%s", streetName, crossingName)
                                            else
                                                location = streetName
                                            end
                                            local options = {
                                                location = location,
                                                coords = radar.position
                                            }
                                            BuildNotificationRadarBroken(options)

                                        end
                                    end,
                                    onProgressSteps = {10, 15},
                                    onComplete = function ()
                                        ClearPedTasks(player)
                                        TriggerServerEvent('lp_speedradars:radarBroken', radar)
                                        if isAlarmPlaying then
                                            SendNUIMessage({ event = 'playAlarmEnd' })
                                            Citizen.Wait(1000)
                                            isAlarmPlaying = false
                                        end
                                        isBreakingRadar = false
                                        radar.broken = true
                                        radar.someoneBreaking = false
                                    end,
                                })
                            end
                        else
                            DrawText3Ds(radar.position.x, radar.position.y, radar.position.z + 1.0, _U('stop_breaking_radar'))
                            if IsControlJustReleased(0, 73) then
                                isBreakingRadar = false
                                radar.broken = false
                                TriggerServerEvent('lp_speedradars:someoneBreakingRadar', radar, false)
                                ClearPedTasks(player)
                                if isAlarmPlaying then
                                    isAlarmPlaying = false
                                    SendNUIMessage({ event = 'playAlarmStop' })
                                end
                                TriggerEvent('rprogress:stop')
                            end
                        end
                    else
                        DrawText3Ds(radar.position.x, radar.position.y, radar.position.z + 1.0, _U('cant_break_radar'))
                    end
                elseif diff >= 2 and diff <= 22 then
                    local volume = nil
                    if diff >= 2 and diff < 3 and radar.someoneBreaking then
                        volume = 0.18
                    elseif diff >= 3 and diff < 4 and radar.someoneBreaking then
                        volume = 0.17
                    elseif diff >= 4 and diff < 5 and radar.someoneBreaking then
                        volume = 0.16
                    elseif diff >= 5 and diff < 6 and radar.someoneBreaking then
                        volume = 0.15
                    elseif diff >= 6 and diff < 7 and radar.someoneBreaking then
                        volume = 0.14
                    elseif diff >= 7 and diff < 8 and radar.someoneBreaking then
                        volume = 0.13
                    elseif diff >= 8 and diff < 9 and radar.someoneBreaking then
                        volume = 0.12
                    elseif diff >= 9 and diff < 10 and radar.someoneBreaking then
                        volume = 0.11
                    elseif diff >= 10 and diff < 11 and radar.someoneBreaking then
                        volume = 0.10
                    elseif diff >= 11 and diff < 12 and radar.someoneBreaking then
                        volume = 0.09
                    elseif diff >= 12 and diff < 13 and radar.someoneBreaking then
                        volume = 0.08
                    elseif diff >= 13 and diff < 14 and radar.someoneBreaking then
                        volume = 0.07
                    elseif diff >= 14 and diff < 15 and radar.someoneBreaking then
                        volume = 0.06
                    elseif diff >= 15 and diff < 16 and radar.someoneBreaking then
                        volume = 0.05
                    elseif diff >= 16 and diff < 17 and radar.someoneBreaking then
                        volume = 0.04
                    elseif diff >= 17 and diff < 18 and radar.someoneBreaking then
                        volume = 0.03
                    elseif diff >= 18 and diff < 19 and radar.someoneBreaking then
                        volume = 0.02
                    elseif diff >= 19 and diff < 20 and radar.someoneBreaking then
                        volume = 0.01
                    elseif diff >= 21 and diff < 22 and radar.someoneBreaking then
                        volume = 0.0
                    end
                    if radar.someoneBreaking and not isAlarmPlaying then
                        isAlarmPlaying = true
                        SendNUIMessage({ event = 'playAlarm' })
                    elseif not radar.someoneBreaking and isAlarmPlaying then
                        isAlarmPlaying = false
                        SendNUIMessage({ event = 'playAlarmEnd' })
                    end
                    if volume ~= nil then
                        SendNUIMessage({ event = 'updateVolume', volume = volume })
                    end
                end
            end
        end
    end
end)

function ControlSpeed(player, radar)
    local speed = GetEntitySpeed(player)
    local vehicle = GetVehiclePedIsIn(player, false)
    local streetHash, crossingHash = GetStreetNameAtCoord(radar.position.x + 0.0, radar.position.y + 0.0,
        radar.position.z + 0.0)
    local streetName = GetStreetNameFromHashKey(streetHash)
    local crossingName = GetStreetNameFromHashKey(crossingHash)
    local kphspeed = math.ceil(speed * 3.6)
    local fineamount = nil
    local finelevel = nil
    local truespeed = kphspeed
    local location = nil

    if vehicle > 0 then
        local driver = GetPedInVehicleSeat(vehicle, -1)
        local plate = GetVehicleNumberPlateText(vehicle)
        if driver == player then
            if crossingName ~= "" then
                location = string.format("%s/%s", streetName, crossingName)
            else
                location = streetName
            end
            if kphspeed >= Config.SpeedThreshold then
                kphspeed = kphspeed - Config.SpeedThreshold
            end
            if (kphspeed > radar.speed_limit) then
                justFlashed = true
                ESX.TriggerServerCallback('lp_speedradars:getFineForSpeed', function(fine)
                    if fine ~= nil then
                        flash(radar, GetVehicleClass(vehicle), location, truespeed, kphspeed, plate, fine)
                        TriggerServerEvent('lp_speedradars:newBlipWithRadius', radar.position)
                        StartScreenEffect('DefaultFlash', 500, false)
                    end
                end, (kphspeed - radar.speed_limit))
            end
        end
    end
end

function flash(radar, class, location, truespeed, speed, plate, fine)
    ESX.TriggerServerCallback('lp_core:GetVehicleByPlate', function(vehicle, xPlayerOwner)
        local points = 0
        local licenseType = 'drive'
        local options = {
            location = location,
            isAuthorized = false,
            isCompany = false,
            isVehicleStolen = false,
            vehicleOwner = nil,
            speed = {
                real = truespeed,
                retained = speed,
                limit = radar.speed_limit
            },
            fine = {
                amount = fine.amount,
                points = fine.points
            },
            license = {
                owned = false,
                newPoints = 0
            },
            plate = plate
        }
        if class == 8 then -- Moto
            licenseType = 'drive_bike'
        end
        if vehicle ~= nil then
            if vehicle.job ~= "NULL" and vehicle.job ~= nil then
                options.isCompany = true
                options.company = vehicle.job
                options.vehicleOwner = xPlayerOwner
                if vehicle.job == 'police' or vehicle.job == 'ambulance' then
                    options.isAuthorized = true
                    options.fine.points = 0
                    options.fine.amount = 0
                else
                    if vehicle.caution then
                        options.isCompany = false
                        if xPlayerOwner ~= nil then -- Player online
                            options.vehicleOwner = xPlayerOwner
                            TriggerServerEvent('lp_core:removeFromAccount', "bank", xPlayerOwner.source, fine.amount, true)
                        else
                            TriggerServerEvent('lp_core:removeFromAccount', "bank", vehicle.owner, fine.amount, false)
                        end
                    else
                        options.fine.points = 0
                        TriggerServerEvent('lp_core:removeSocietyMoney', vehicle.job, fine.amount)
                    end
                end
            else
                if xPlayerOwner ~= nil then -- Player online
                    options.vehicleOwner = xPlayerOwner
                    TriggerServerEvent('lp_core:removeFromAccount', "bank", xPlayerOwner.source, fine.amount, true)
                else
                    TriggerServerEvent('lp_core:removeFromAccount', "bank", vehicle.owner, fine.amount, false)
                end
            end
            ESX.TriggerServerCallback('esx_license:getOwnerLicense', function(license)
                if license ~= nil and license.points > 0 then
                    options.license.points = license.points
                    options.license.owned = true
                    if options.fine.points > 0 then
                        if xPlayerOwner then -- Pas encore possible de retirer de points si le joueur n'est pas en ligne
                            ESX.TriggerServerCallback('esx_license:removePoints', function(newPoints)
                                if newPoints > -1 then
                                    options.license.newPoints = newPoints
                                    license.points = newPoints
                                    TriggerEvent('esx_dmvschool:updateLicense', license)
                                    BuildNotification(options)
                                else -- Comprenpa
                                    ESX.ShowAdvancedNotification(_U('error_remove_points'))
                                end
                            end, xPlayerOwner.source, licenseType, fine.points)
                        else
                            BuildNotification(options) -- Joueur hors ligne
                        end
                    else
                        BuildNotification(options) -- Pas de retrait de points
                    end
                else
                    BuildNotification(options) -- Pas de permis ou plus dde points
                end
            end, vehicle.owner, licenseType)
            TriggerServerEvent('lp_core:addSocietyMoney', 'police', fine.amount)
        else
            options.isVehicleStolen = true
            BuildNotification(options)
        end
        BuildNotification(options, true)
    end, plate)
end

function BuildNotificationRadarBroken(options)
    local notification = {
        title = 'LSPD',
        subject = _U('notification_message_radar_anomaly'),
        picture = 'CHAR_CALL911',
        iconType = 1,
        msg = _('notification_message_location', options.location) .. '\n' .. _U('notification_message_radar_anomaly_msg')
    }
    
    TriggerServerEvent('esx_service:notifyAllInService', notification, 'police', true)
    TriggerServerEvent('lp_speedradars:newBlipWithRadius', options.coords)
end

function BuildNotification(options, forLSPD)
    local notification = {
        title = _U('notification_title_speedradar'),
        subject = _U('notification_subject', options.speed.limit, _('speed_unit')),
        picture = 'CHAR_CALL911',
        iconType = 1
    }
    local preMessage = nil
    local baseMessage = nil
    local vehicleInformationMessage = nil
    local fineMessage = nil
    local hasLicense = options.license.owned ~= nil and options.license.owned == true and options.license.points > 0

    local locationMessage = _('notification_message_location', options.location)
    baseMessage = _U('notification_message_speed', options.speed.real, _('speed_unit'), options.speed.retained,
            _('speed_unit'))
    baseMessage = baseMessage .. '\n' .. _U('notification_message_plate', options.plate)

    if options.isAuthorized == true then
        vehicleInformationMessage = _U('authorized_vehicle')
    else
        if options.isVehicleStolen == true then
            vehicleInformationMessage = _U('stolen_vehicle')
        else
            if options.isCompany == true then
                vehicleInformationMessage = _U('company_vehicle')
            end
            if options.fine ~= nil then
                if options.fine.amount ~= nil then
                    if options.fine.amount > 0 then
                        fineMessage = _U('notification_fine_amount', options.fine.amount)
                        if options.fine.points ~= nil then
                            if options.fine.points > 0 then
                                if hasLicense == true then
                                    if options.fine.points > 0 then
                                        fineMessage = fineMessage .. '\n' ..
                                                            _U('notification_license_point_removed',
                                                options.fine.points, options.license.newPoints)
                                    end
                                else
                                    fineMessage = fineMessage .. '\n' .. _U('no_license')
                                end
                            else
                                if hasLicense == false then
                                    fineMessage = fineMessage .. '\n' .. _U('no_license')
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    if forLSPD == true then
        notification.msg = locationMessage .. '\n' ..  _U('just_flashed_lspd') .. '\n' .. baseMessage
        if vehicleInformationMessage ~= nil and options.isVehicleStolen == false then
            notification.msg = notification.msg .. '\n' .. vehicleInformationMessage
        end
        TriggerServerEvent('esx_service:notifyAllInService', notification, 'police')
    else
        if options.isAuthorized then
            notification.msg = locationMessage .. '\n' .. _U('just_flashed_self') .. '\n' .. baseMessage
            if vehicleInformationMessage ~= nil and options.isVehicleStolen == false then
                notification.msg = notification.msg .. '\n' .. vehicleInformationMessage
            end
            if options.vehicleOwner ~= nil then
                TriggerServerEvent('lp_speedradars:showNotification', options.vehicleOwner, notification)
            end
        elseif options.isCompany == true then
            notification.msg = locationMessage .. '\n' .. _U('just_flashed_enterprise') .. '\n' .. baseMessage .. '\n' .. vehicleInformationMessage
            TriggerServerEvent('esx_service:notifyAllInService', notification, options.company)
            notification.msg = locationMessage .. '\n' .. _U('just_flashed_self') .. '\n' .. baseMessage .. '\n' .. fineMessage .. '\n' .. vehicleInformationMessage
            if options.vehicleOwner ~= nil then
                TriggerServerEvent('lp_speedradars:showNotification', options.vehicleOwner, notification)
            end
        else
            notification.msg = locationMessage .. '\n' .. _U('just_flashed_self') .. '\n'..  baseMessage
            if fineMessage ~= nil then
                notification.msg = notification.msg .. '\n' .. fineMessage
            end
            if vehicleInformationMessage ~= nil then
                notification.msg = notification.msg .. '\n' .. vehicleInformationMessage
            end
            if options.isVehicleStolen == false then
                if options.vehicleOwner ~= nil then
                    TriggerServerEvent('lp_speedradars:showNotification', options.vehicleOwner, notification)
                end
            else -- Vehicle is stolen
                ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                    notification.picture, notification.iconType)
            end
        end
    end
end

function DrawText3Ds(x, y, z, text)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local _, count = string.gsub(text, "\n", "")

    if onScreen then
        SetTextScale(0.35, 0.35)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 215)
        BeginTextCommandDisplayText("STRING")
        SetTextCentre(true)
        AddTextComponentSubstringPlayerName(text)
        SetDrawOrigin(x, y, z, 0)
        DrawText(0.0, 0.0)
        local factor = (string.len(text)) / 370
        DrawRect(0.0, 0.0125 * (count + 1), 0.017 + factor, 0.03 + (0.03 * count), 0, 0, 0, 75)
        ClearDrawOrigin()
    end
end

-- Draw markers when radars shown
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if isBlipsShown then
            for _, blip in pairs(blips) do
                DrawMarker(
                    1,
                    blip.position.x,
                    blip.position.y,
                    blip.position.z - 1.0,
                    0.0,
                    0.0,
                    0.0,
                    0,
                    0.0,
                    0.0,
                    40.0,
                    40.0,
                    10.0,
                    255,
                    0,
                    255,
                    100,
                    false,
                    true,
                    2,
                    false,
                    false,
                    false,
                    false
                )
            end
        end
    end
end)

-- Shows blips on map
RegisterNetEvent('lp_speedradars:showRadars')
AddEventHandler('lp_speedradars:showRadars', function()
    if not isBlipsShown then
        for _, radar in pairs(Radars) do
            local blip = AddBlipForCoord(radar.position)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Radar")
            EndTextCommandSetBlipName(blip)
            table.insert(blips, {
                position = radar.position,
                blip = blip
            })
        end
        isBlipsShown = true
    end
end)

-- Hides blips from map
RegisterNetEvent('lp_speedradars:hideRadars')
AddEventHandler('lp_speedradars:hideRadars', function()
    for _, v in pairs(blips) do
        RemoveBlip(v.blip)
    end
    isBlipsShown = false
    blips = {}
end)

RegisterNetEvent('lp_speedradars:radarAdded')
AddEventHandler('lp_speedradars:radarAdded', function(radarAdded)
    local radarProp = CreateObject(GetHashKey('prop_cctv_pole_03'), radarAdded.position.x, radarAdded.position.y, (radarAdded.position.z - 2.0), false, true, true)
    radarAdded.prop = radarProp
    FreezeEntityPosition(radarAdded.prop, true)
    SetEntityCanBeDamaged(radarAdded.prop, false)
    table.insert(Radars, radarAdded)
    if isBlipsShown then
        local blip = AddBlipForCoord(radarAdded.position)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString("Radar")
        EndTextCommandSetBlipName(blip)
        table.insert(blips, {
            position = radarAdded.position,
            blip = blip
        })
    end
end)

RegisterNetEvent('lp_speedradars:radarRemoved')
AddEventHandler('lp_speedradars:radarRemoved', function(radarRemoved)
    for index, radar in ipairs(Radars) do
        if #(radar.position - radarRemoved.position) == 0 then
            if radar.prop then
                DeleteObject(radar.prop)
            end
            table.remove(Radars, index)
            break
        end
    end
    if isBlipsShown == true then
        for index, blip in pairs(blips) do
            if #(blip.position - radarRemoved.position) == 0 then
                if DoesBlipExist(blip.blip) then
                    RemoveBlip(blip.blip)
                    table.remove(blips, index)
                end
            end
        end
    end
end)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job)
    ESX.PlayerData.job = job
end)
