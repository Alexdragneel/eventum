Config = {}
Config.DrawDistance         = 100
Config.Locale               = "fr"
Config.MarkerColor = {r = 242, g = 242, b = 24, a = 255}
Config.MarkerType =  1
Config.MarkerSize = {x = 2.0, y = 2.0, z = 1.0}
Config.BlipSprites = {bar = 93, liquor = 59, gasstation = 59, store = 59, pizzeria = 93}
Config.BlipColors = {bar = 25, liquor = 63, gasstation = 61, store = 25, pizzeria = 25}