ESX = nil
local shopList = {}
local isInMarker, hasAlreadyEnteredMarker = false, false
local currentShop = nil

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

-- ESX Listeners --
RegisterNetEvent("esx:playerLoaded")
AddEventHandler("esx:playerLoaded", function(xPlayer)
	DrawBlips()
end)
-- --

-- Draw blips --
function DrawBlips()
	Citizen.CreateThread(function()
		ESX.TriggerServerCallback('lp_shops:getShopList', function(_shopList)
			shopList = _shopList
			for k, shop in ipairs(_shopList) do
				if not shop.hidden and shop.active then
					local blip = AddBlipForCoord(shop.coords)
					local blipSprite = Config.BlipSprites[shop.typeName]
					local blipColor = Config.BlipColors[shop.typeName]
					if blipSprite == nil then 
						blipSprite = 59
					end
					if blipColor == nil then
						blipColor = 25
					end
					
					SetBlipSprite (blip, blipSprite)
					SetBlipDisplay(blip, 4)
					SetBlipScale  (blip, 1.0)
					SetBlipColour (blip, blipColor)
					SetBlipAsShortRange(blip, true)

					BeginTextCommandSetBlipName("STRING")
					AddTextComponentSubstringPlayerName(shop.label)
					EndTextCommandSetBlipName(blip)
				
				end
			end
		end)
	end)
end
-- --

-- Draw Markers + detection --
Citizen.CreateThread(function()
	while next(shopList) == nil do
		Citizen.Wait(300)
	end

	while true do
		Citizen.Wait(1)

		local playerCoords = GetEntityCoords(PlayerPedId())
		isInMarker = false

		for k, shop in ipairs(shopList) do
			if shop.active then
				local distance = GetDistanceBetweenCoords(playerCoords, shop.coords, true)

				if distance < Config.DrawDistance then
					DrawMarker(Config.MarkerType, shop.coords, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				end

				if distance < Config.MarkerSize.x then
					isInMarker = true
					currentShop = shop
					ESX.ShowHelpNotification(_U('access_shop'))
				end
			end
		end
			

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('lp_shops:hasExitedMarker')
		end
	end
end)

AddEventHandler('lp_shops:hasExitedMarker', function(zone)
	currentShop = nil
	ESX.UI.Menu.CloseAll()
end)
-- --

-- Key Controls --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if IsControlJustReleased(0, 38) and isInMarker then
			ESX.UI.Menu.CloseAll()
			OpenShopMenu()
		end
	end
end)
-- --

-- Shop Menu --
function OpenShopMenu()
	if currentShop ~= nil then
		ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
			local elements = {}
			for i = 1, #currentShop.items, 1 do
				local shopItem = currentShop.items[i]

				if dbItems[shopItem] then
					local dbItem = dbItems[shopItem]
					table.insert(elements, {
						label     	= ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s <span style="color:green;">%s</span></span>'):format(dbItem.label, _U('shop_item', ESX.Math.GroupDigits(dbItem.sellPrice))),
						itemLabel	= dbItem.label,
						item      	= shopItem,
						price     	= dbItem.sellPrice,
			
						-- menu properties
						value      = 1,
						type       = 'slider',
						min        = 1,
						max        = 100
					})
				end
			end

			ESX.UI.Menu.CloseAll()

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop', {
				title    = currentShop.label .. ' - ' .. currentShop.typeLabel,
				align    = 'top-left',
				elements = elements
			}, function(data, menu)
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
					title    = _U('shop_confirm', data.current.value, data.current.itemLabel, ESX.Math.GroupDigits(data.current.price * data.current.value)),
					align    = 'top-left',
					elements = {
						{label = _U('no'),  value = 'no'},
						{label = _U('yes'), value = 'yes'}
					}
				}, function(data2, menu2)
					if data2.current.value == 'yes' then
						TriggerServerEvent('lp_shops:buyItem', data.current.item, data.current.value)
					end

					menu2.close()
				end, function(data2, menu2)
					menu2.close()
				end)
			end, function(data, menu)
				menu.close()
			end)
		end)
	end
end
-- --