-- Variables --
ESX = nil
local shopList = nil
-- --

-- Init ESX stuff --
TriggerEvent(
	"esx:getSharedObject",
	function(obj)
		ESX = obj
	end
)
-- --

-- Retrieve shops in DB --
MySQL.ready(function()
	MySQL.Async.fetchAll('SELECT \
            lp_shops.name as shopName, \
            lp_shops.label as shopLabel, \
            lp_shops.coordsX as shopCoordsX, \
            lp_shops.coordsY as shopCoordsY, \
            lp_shops.coordsZ as shopCoordsZ, \
            lp_shops.hidden as shopHidden, \
            lp_shops.active as shopActive, \
            lp_shop_types.name as shopTypeName, \
            lp_shop_types.label as shopTypeLabel, \
            lp_shop_types.items as shopItems, \
            lp_shop_types.category as shopCategory, \
            lp_shop_types.grades as shopGrades \
        FROM lp_shops LEFT JOIN lp_shop_types ON lp_shop_types.name = lp_shops.type', {}, 
        function(result)
            shopList = {}
            for i=1, #result, 1 do
                local shop = result[i]
                local shopItems = json.decode(shop.shopItems)
                local shopGrades = json.decode(shop.shopGrades)

                table.insert(shopList, {
                    name = shop.shopName,
                    label = shop.shopLabel,
                    coords = vector3(shop.shopCoordsX, shop.shopCoordsY, shop.shopCoordsZ),
                    hidden = shop.shopHidden,
                    active = shop.shopActive,
                    typeName = shop.shopTypeName,
                    typeLabel = shop.shopTypeLabel,
                    items = shopItems,
                    category = shop.shopCategory,
                    grades = shopGrades
                })
            end
	    end
    )
end)

ESX.RegisterServerCallback('lp_shops:getShopList', function(source, cb)
	cb(shopList)
end)
-- --

-- Purchase logic --
RegisterServerEvent('lp_shops:buyItem')
AddEventHandler('lp_shops:buyItem', function(itemName, amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	amount = ESX.Math.Round(amount)

	if amount < 0 then
		print('lp_shops: ' .. xPlayer.identifier .. ' attempted to exploit the shop!')
		return
	end

	-- get price
    local dbItems = ESX.GetItems()
    local item = dbItems[itemName]

    if item ~= nil then
        local price = item.sellPrice * amount

        if xPlayer.getMoney() >= price then
            if xPlayer.canCarryItem(itemName, amount) then
                xPlayer.removeMoney(price)
                xPlayer.addInventoryItem(itemName, amount, false, true)
                xPlayer.showNotification(_U('bought', amount, item.label, ESX.Math.GroupDigits(price)))
            else
                xPlayer.showNotification(_U('player_cannot_hold'))
            end
        else
            local missingMoney = price - xPlayer.getMoney()
            xPlayer.showNotification(_U('not_enough', ESX.Math.GroupDigits(missingMoney)))
        end
    end
end)
-- --
