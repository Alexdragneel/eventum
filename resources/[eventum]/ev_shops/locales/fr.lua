Locales['fr'] = {
    ['access_shop']         = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au magasin',
    ['shop_item']           = '$%s',
    ['shop_confirm']        = 'Acheter %s %s pour $%s ?',
    ['bought']              = 'Vous venez d\'acheter ~y~%sx~s~ ~b~%s~s~ pour ~r~$%s~s~',
    ['not_enough']          = 'Vous n\'avez ~r~pas~s~ assez d\'argent: il vous manque ~r~$%s~s~',
    ['player_cannot_hold']  = 'Vous n\'avez ~r~pas~s~ assez ~y~de place~s~ dans votre inventaire !',
    ['no']                  = 'Non',
    ['yes']                 = 'Oui',
}
