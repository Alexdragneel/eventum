CREATE TABLE lp_shops (
	name varchar(100) NOT NULL,
	label varchar(255) NOT NULL,
	coordsX FLOAT NOT NULL,
	coordsY FLOAT NOT NULL,
	coordsZ FLOAT NOT NULL,
	`type` varchar(100) NOT NULL,
	hidden TINYINT(1) DEFAULT 0 NULL,
	active TINYINT(1) DEFAULT 1 NULL,
	CONSTRAINT lp_shops_pk PRIMARY KEY (name)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE lp_shop_types (
	name varchar(100) NOT NULL,
	label varchar(255) NOT NULL,
	items LONGTEXT NOT NULL,
	category varchar(100) DEFAULT 'shop' NULL,
	grades varchar(100) DEFAULT '[]' NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (name)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

INSERT INTO lp_shops (name,label,coordsX,coordsY,coordsZ,`type`,hidden,active) VALUES
	 ('ltdgasoline1','LTD Gasoline',-48.5,-1757.5,28.4,'gasstation',0,1),
	 ('ltdgasoline2','LTD Gasoline',1163.3,-323.8,68.2,'gasstation',0,1),
	 ('ltdgasoline3','LTD Gasoline',-707.5,-914.2,18.2,'gasstation',0,1),
	 ('ltdgasoline4','LTD Gasoline',-1820.5,792.5,137.1,'gasstation',0,1),
	 ('ltdgasoline5','LTD Gasoline',1698.3,4924.4,41.0,'gasstation',0,1),
	 ('pizzapier','Chez Dino',-1329.69,-1285.65,4.1,'pizzeria',0,1),
	 ('robsliquor1','Rob''s Liquor',1135.8,-982.2,45.4,'liquor',0,1),
	 ('robsliquor2','Rob''s Liquor',-1222.9,-906.9,11.3,'liquor',0,1),
	 ('robsliquor3','Rob''s Liquor',-1487.5,-379.1,39.1,'liquor',0,1),
	 ('robsliquor4','Rob''s Liquor',-2968.2,390.9,14.0,'liquor',0,1),
	 ('robsliquor5','Rob''s Liquor',1166.0,2708.9,37.1,'liquor',0,1),
	 ('robsliquor6','Rob''s Liquor',1392.5,3604.6,33.9,'liquor',0,1),
	 ('robsliquor7','Rob''s Liquor',127.8,-1284.7,28.2,'liquor',0,1),
	 ('robsliquor8','Rob''s Liquor',-1393.4,-606.6,29.3,'liquor',0,1),
	 ('robsliquor9','Rob''s Liquor',-559.9,287.0,81.1,'liquor',0,1),
	 ('twentyfourseven1','Twenty Four Seven',373.8,325.8,102.5,'store',0,1),
	 ('twentyfourseven2','Twenty Four Seven',2557.4,382.2,107.6,'store',0,1),
	 ('twentyfourseven3','Twenty Four Seven',-3038.9,585.9,6.9,'store',0,1),
	 ('twentyfourseven4','Twenty Four Seven',-3241.9,1001.4,11.8,'store',0,1),
	 ('twentyfourseven5','Twenty Four Seven',547.4,2671.7,41.1,'store',0,1),
	 ('twentyfourseven6','Twenty Four Seven',1961.4,3740.6,31.3,'store',0,1),
	 ('twentyfourseven7','Twenty Four Seven',2678.9,3280.6,54.2,'store',0,1),
	 ('twentyfourseven8','Twenty Four Seven',1729.2,6414.1,34.0,'store',0,1);

INSERT INTO lp_shop_types (name,label,items,category,grades) VALUES
	 ('gasstation','Magasin','["phone","water","bread","beer"]','store','[]'),
	 ('liquor','Magasin','["phone","water","bread","beer"]','store','[]'),
	 ('pizzeria','Pizzeria','["pizza","hotdog","cheeseburger","water","beer"]','food','[]'),
	 ('store','Magasin','["phone","water","bread","beer"]','store','[]');
	 