ESX = nil

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

RegisterNetEvent("lp_change_model:setModel")
AddEventHandler(
	"lp_change_model:setModel",
	function(model, isAnimal)
        ChangePedModel(model, isAnimal)
	end
)

function ChangePedModel(model, isAnimal)
	local playerPed = PlayerPedId()
	local characterModel = GetHashKey(model)

	RequestModel(characterModel)

	Citizen.CreateThread(function()
		while not HasModelLoaded(characterModel) do
			RequestModel(characterModel)
			Citizen.Wait(0)
		end

		if IsModelInCdimage(characterModel) and IsModelValid(characterModel) then
			SetPlayerModel(PlayerId(), characterModel)
			SetPedDefaultComponentVariation(playerPed)

            if isAnimal then
                SetPedComponentVariation(playerPed, 0, 0, 0, 0)
                Citizen.Wait(200)
                SetPedComponentVariation(playerPed, 0, 0, 1, 0)
                Citizen.Wait(200)
            end
		end

		SetModelAsNoLongerNeeded(characterModel)
	end)
end