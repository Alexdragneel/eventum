ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterCommand('setmodel', 'admin', function(xPlayer, args, showError)
	xPlayer.triggerEvent('lp_change_model:setModel', args.model, args.isAnimal ~= nil and args.isAnimal == '1')
end, false, {help = _U('command_setmodel'), validate = false, arguments = {
	{name = 'model', help = _U('command_setmodel_model'), type = 'any'},
	{name = 'isAnimal', help = _U('command_setmodel_isAnimal'), type = 'any'}
}})
