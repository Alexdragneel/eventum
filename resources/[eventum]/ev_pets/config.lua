Config = {}

Config.Locale = 'fr'

Config.DrawDistance = 50.0

Config.PetShop = {
	{
		pet = 'cat',
		label = _U('cat'),
		price = 1900
	},

    {
		pet = 'westy',
		label = _U('westy'),
		price = 5100
	},

    {
		pet = 'retriever',
		label = _U('retriever'),
		price = 6850
	},

    {
		pet = 'rottweiler',
		label = _U('rottweiler'),
		price = 7500
	},

    {
		pet = 'pug',
		label = _U('pug'),
		price = 7950
	},

	{
		pet = 'sheperd',
		label = _U('sheperd'),
		price = 8250
	},

	{
		pet = 'husky',
		label = _U('husky'),
		price = 9550
	},

    {
		pet = 'poodle',
		label = _U('poodle'),
		price = 10000
	},
}

Config.FarmShop = {
    {
		pet = 'rat',
		label = _U('rat'),
		price = 150
	},
    {
		pet = 'rabbit',
		label = _U('rabbit'),
		price = 500
	},
    {
		pet = 'chicken',
		label = _U('chicken'),
		price = 500
	},
    {
		pet = 'pig',
		label = _U('pig'),
		price = 1000
	},
    {
		pet = 'boar',
		label = _U('boar'),
		price = 2000
	},
	{
		pet = 'cow',
		label = _U('cow'),
		price = 5000
	},
}

Config.Zones = {
	{
		Category = 'pet',
		Pos = {x = 562.19, y = 2741.30, z = 41.86 },
		Sprite = 463,
		Display = 4,
		Scale = 1.0,
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
        Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 171, g = 60, b = 230, a = 100, rotate = false}
	},
	{
		Category = 'pet',
		Pos = {x = -269.61, y = 6283.41, z = 30.52 },
		Sprite = 463,
		Display = 4,
		Scale = 1.0,
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
        Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 171, g = 60, b = 230, a = 100, rotate = false}
	},
    {
		Category = 'farm',
		Pos = {x = 1677.79, y = 4882.09, z = 41.08 },
		Sprite = 463,
		Display = 4,
		Scale = 1.0,
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
        Marker = {type = 1, x = 1.5, y = 1.5, z = 1.5, r = 171, g = 60, b = 230, a = 100, rotate = false}
	}
}