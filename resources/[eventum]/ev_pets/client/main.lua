-- internal variables
ESX = nil

local Pet = {
	model = nil,
	name = 'Animal',
	hunger = 0,
	isDead = true,
	deathReason = ''
}

local pedPet, modelPet, object, animation = nil, {}, {}, {}, {}
local objCoords
local come = 0
local petIsSpawned = false
local petIsReturning = false
local isAttached = false
local hasBalle, currentBalle = false, nil
local currentAnim = nil

local Melee = { -1569615261, 1737195953, 1317494643, -1786099057, 1141786504, -2067956739, -868994466 }
local Knife = { -1716189206, 1223143800, -1955384325, -1833087301, 910830060, }
local Bullet = { 453432689, 1593441988, 584646201, -1716589765, 324215364, 736523883, -270015777, -1074790547, -2084633992, -1357824103, -1660422300, 2144741730, 487013001, 2017895192, -494615257, -1654528753, 100416529, 205991906, 1119849093 }
local Animal = { -100946242, 148160082 }
local FallDamage = { -842959696 }
local Explosion = { -1568386805, 1305664598, -1312131151, 375527679, 324506233, 1752584910, -1813897027, 741814745, -37975472, 539292904, 341774354, -1090665087 }
local Gas = { -1600701090 }
local Burn = { 615608432, 883325847, -544306709 }
local Drown = { -10959621, 1936677264 }
local Car = { 133987706, -1553120962 }

-----------------
-- LOAD MODELS --
-----------------

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	Citizen.Wait(5000)
	DoRequestModel(GetHashKey('a_c_cat_01')) -- chat
	DoRequestModel(GetHashKey('a_c_husky')) -- husky
	DoRequestModel(GetHashKey('a_c_poodle')) -- caniche
	DoRequestModel(GetHashKey('a_c_pug')) -- carlin
	DoRequestModel(GetHashKey('a_c_retriever')) -- retriever
    DoRequestModel(GetHashKey('a_c_rottweiler')) -- rottweiler
	DoRequestModel(GetHashKey('a_c_shepherd')) -- berger
	DoRequestModel(GetHashKey('a_c_westy')) -- westie

    DoRequestModel(GetHashKey('a_c_rat')) -- rat
    DoRequestModel(GetHashKey('a_c_rabbit_01')) -- lapin
    DoRequestModel(GetHashKey('a_c_hen')) -- poule
    DoRequestModel(GetHashKey('a_c_pig')) -- cochon
    DoRequestModel(GetHashKey('a_c_boar')) -- sanglier
	DoRequestModel(GetHashKey('a_c_cow')) -- cow
end)

function DoRequestModel(model)
	RequestModel(model)
	while not HasModelLoaded(model) do
		Citizen.Wait(1)
	end
end

-------------
-- F9 MENU --
-------------

function OpenPetMenu()
	local elements = {}
	if come == 1 then

		if not petIsSpawned then
			ESX.ShowNotification(_U('pet_incoming', Pet.name))
			return
		end

		if petIsReturning then
			ESX.ShowNotification(_U('doghouse_returning', Pet.name))
			return
		end

		table.insert(elements, {label = _U('hunger', Pet.hunger), value = nil})
		table.insert(elements, {label = _U('givefood'), value = 'give_food'})

		table.insert(elements, {label = _U('attachpet'), value = 'attached_animal'})

		if Pet.model == 'cat'  then
			table.insert(elements, {label = _U('getdown'), value = 'coucher2'})
			table.insert(elements, {label = _U('getup'), value = 'debout'})
		elseif Pet.model == 'westy' or Pet.model == 'pug' or Pet.model == 'poodle' then
			table.insert(elements, {label = _U('sitdown'), value = 'assis'})
			table.insert(elements, {label = _U('getup'), value = 'debout'})
		elseif Pet.model == 'retriever' or Pet.model == 'sheperd' or Pet.model == 'husky'then
			table.insert(elements, {label = _U('sitdown'), value = 'assis2'})
			table.insert(elements, {label = _U('getdown'), value = 'coucher'})
			table.insert(elements, {label = _U('getup'), value = 'debout'})
		elseif Pet.model == 'rottweiler' then
			table.insert(elements, {label = _U('sitdown'), value = 'assis3'})
			table.insert(elements, {label = _U('getdown'), value = 'coucher'})
			table.insert(elements, {label = _U('getup'), value = 'debout'})
		end
		table.insert(elements, {label = _U('looksamedirection'), value = 'looksamedirection'})
		table.insert(elements, {label = _U('pied'), value = 'pied'})

		table.insert(elements, {label = _U('doghouse'), value = 'return_doghouse'})

	else
		table.insert(elements, {label = _U('callpet', Pet.name), value = 'come_animal'})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pet_menu', {
		title    = _U('pet_management', Pet.name),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)

		-- Appeler l'animal
		if data.current.value == 'come_animal' and come == 0 then
			if not IsPedSittingInAnyVehicle(PlayerPedId()) then
				ESX.TriggerServerCallback('lp_pets:getPet', function(aPet)

					if aPet ~= nil and not aPet.isDead then

						Pet = aPet

						ESX.ShowNotification(_U('pet_coming', Pet.name))

						petIsSpawned = false
						-- animaux d'animalerie
						if Pet.model == 'cat' then
							modelPet = GetHashKey('a_c_cat_01')
							come = 1
							openPet()
						elseif Pet.model == 'husky' then
							modelPet = GetHashKey('a_c_husky')
							come = 1
							openPet()
						elseif Pet.model == 'poodle' then
							modelPet = GetHashKey('a_c_poodle')
							come = 1
							openPet()
						elseif Pet.model == 'pug' then
							modelPet = GetHashKey('a_c_pug')
							come = 1
							openPet()
						elseif Pet.model == 'retriever' then
							modelPet = GetHashKey('a_c_retriever')
							come = 1
							openPet()
						elseif Pet.model == 'sheperd' then
							modelPet = GetHashKey('a_c_shepherd')
							come = 1
							openPet()
						elseif Pet.model == 'westy' then
							modelPet = GetHashKey('a_c_westy')
							come = 1
							openPet()
						elseif Pet.model == 'rottweiler' then
							modelPet = GetHashKey('a_c_rottweiler')
							come = 1
							openPet()

						-- animaux de ferme
						elseif Pet.model == 'rat' then
							modelPet = GetHashKey('a_c_rat')
							come = 1
							openPet()
						elseif Pet.model == 'rabbit' then
							modelPet = GetHashKey('a_c_rabbit_01')
							come = 1
							openPet()
						elseif Pet.model == 'chicken' then
							modelPet = GetHashKey('a_c_hen')
							come = 1
							openPet()
						elseif Pet.model == 'pig' then
							modelPet = GetHashKey('a_c_pig')
							come = 1
							openPet()
						elseif Pet.model == 'boar' then
							modelPet = GetHashKey('a_c_boar')
							come = 1
							openPet()
						elseif Pet.model == 'cow' then
							modelPet = GetHashKey('a_c_cow')
							come = 1
							openPet()
						end
					end
				end)
			else
				ESX.ShowNotification(_U('callpet_incar', Pet.name))
			end
			menu.close()

		-- Attacher l'animal
		elseif data.current.value == 'attached_animal' then
			if not IsPedSittingInAnyVehicle(pedPet) then
				attached()
			else
				ESX.ShowNotification(_U('dontattachhiminacar'))
			end

		-- Nourrir l'animal
		elseif data.current.value == 'give_food' then
			local inventory = ESX.GetPlayerData().inventory
			local coords1   = GetEntityCoords(PlayerPedId())
			local coords2   = GetEntityCoords(pedPet)
			local distance  = GetDistanceBetweenCoords(coords1, coords2, true)

			local count = 0
			for i=1, #inventory, 1 do
				if inventory[i].name == 'croquettes' then
					count = inventory[i].count
				end
			end
			if distance < 5 then
				if count >= 1 then
					if Pet.hunger < 100 then
						Pet.hunger = Pet.hunger + 20
						ESX.ShowNotification(_U('gavepetfood', Pet.name))
						TriggerServerEvent('lp_pets:consumePetFood')
						if Pet.hunger > 100 then
							Pet.hunger = 100
						end
						menu.close()
					else
						ESX.ShowNotification(_U('nomorehunger', Pet.name))
					end
				else
					ESX.ShowNotification(_U('donthavefood'))
				end
			else
				ESX.ShowNotification(_U('hestoofar', Pet.name))
			end

		-- Renvoyer l'animal
		elseif data.current.value == 'return_doghouse' then
			if isAttached then
				detached()
			end

			local coords      = GetEntityCoords(PlayerPedId())

			petIsReturning = true
			ESX.ShowNotification(_U('doghouse_returning', Pet.name))

			local anInt = GetRandomIntInRange(1,5)
			if anInt == 1 then
				TaskGoToCoordAnyMeans(pedPet, coords.x + 40, coords.y, coords.z, 5.0, 0, 0, 786603, 0xbf800000)
			elseif anInt == 2 then
				TaskGoToCoordAnyMeans(pedPet, coords.x - 40, coords.y, coords.z, 5.0, 0, 0, 786603, 0xbf800000)
			elseif anInt == 3 then
				TaskGoToCoordAnyMeans(pedPet, coords.x, coords.y + 40, coords.z, 5.0, 0, 0, 786603, 0xbf800000)
			else
				TaskGoToCoordAnyMeans(pedPet, coords.x, coords.y - 40, coords.z, 5.0, 0, 0, 786603, 0xbf800000)
			end
			SetEntityInvincible(pedPet, true)

			menu.close()

			Citizen.Wait(5000)
			DeleteEntity(pedPet)
			petIsReturning = false
			petIsSpawned = false
			come = 0

		-- Appeler l'animal au pied
		elseif data.current.value == 'pied' then
			if not IsPedSittingInAnyVehicle(PlayerPedId()) then
				TaskPlayAnim(PlayerPedId(), 'rcmnigel1c', 'hailing_whistle_waive_a' ,8.0, -8, -1, 120, 0, false, false, false)
				Citizen.Wait(500)
				if isAttached then
					detached()
				end
				local coords = GetEntityCoords(PlayerPedId())
				TaskGoToCoordAnyMeans(pedPet, coords, 5.0, 0, 0, 786603, 0xbf800000)
			else
				ESX.ShowNotification(_U('callpet_incar', Pet.name))
			end

		-- Coucher l'animal
		elseif data.current.value == 'coucher' then -- [chien, retriever, sheperd, husky]
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = 'coucher'
				attached()
				--menu.close()
			end
		elseif data.current.value == 'coucher2' then -- [chat]
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = 'coucher2'
				attached()
				--menu.close()
			end

		-- Faire assoir l'animal
		elseif data.current.value == 'assis' then -- [carlin, pug]
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = 'assis'
				attached()
				--menu.close()
			end
		elseif data.current.value == 'assis2' then -- [retriever, sheperd, husky]
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = 'assis2'
				attached()
				--menu.close()
			end
		elseif data.current.value == 'assis3' then -- [rottweiler ]
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = 'assis3'
				attached()
				--menu.close()
			end

		-- Faire lever l'animal
		elseif data.current.value == 'debout' then
			if not IsPedSittingInAnyVehicle(pedPet) then
				currentAnim = nil
				ClearPedTasks(pedPet)
				--menu.close()
			end

		-- Faire regarder dans la même direction
		elseif data.current.value == 'looksamedirection' then
			if not IsPedSittingInAnyVehicle(pedPet) then
				attached()
				TaskAchieveHeading(pedPet, GetEntityHeading(PlayerPedId()), 2000)
			end
		end
	end, function(data, menu)
		menu.close()
	end)
end

function openPet()
	local playerPed = PlayerPedId()
	--local LastPosition = GetEntityCoords(playerPed)

	DoRequestAnimSet('rcmnigel1c')

	TaskPlayAnim(playerPed, 'rcmnigel1c', 'hailing_whistle_waive_a', 8.0, -8, -1, 120, 0, false, false, false)

	Citizen.SetTimeout(2000, function()
		local LastPosition = GetEntityCoords(playerPed)
		pedPet = CreatePed(28, modelPet, LastPosition.x, LastPosition.y, LastPosition.z -1, 1, 1)
		SetEntityNoCollisionEntity(pedPet, PlayerPedId(), true)
		
		SetPedDefaultComponentVariation(pedPet)

		petIsSpawned = true

		Citizen.Wait(5)
		attached()
		Citizen.Wait(5)
		detached()
	end)
end

function attached()
	isAttached = true
	ClearPedTasks(pedPet)
end

function detached()
	isAttached = false
end

-------------------------
-- DIFFERENTES BOUCLES --
-------------------------

-- pour que le pet suive
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)

		if come == 1 and petIsSpawned and not isAttached and not petIsReturning and GetEntitySpeed(pedPet) < 0.05 then

			local coords   = GetEntityCoords(PlayerPedId())
			local coords2  = GetEntityCoords(pedPet)
			local distance = GetDistanceBetweenCoords(coords, coords2, true)
			if IsPedInAnyVehicle(PlayerPedId(), false) and not IsPedInAnyVehicle(pedPet, true) and distance < 10.0 and CurrentPetCanSeatInVehicle() then
				local vehicle  = GetVehiclePedIsUsing(PlayerPedId())

				local vc = GetVehicleClass(vehicle)
				local model = GetEntityModel(vehicle)
				local isBike = vc == 8 or vc == 13 or model == GetHashKey('policeb') or model == GetHashKey('policeb1') or model == GetHashKey('policeb2')

				if not isBike then
					local petIsInVehicle = true
					if IsVehicleSeatFree(vehicle, 0) then
						SetPedIntoVehicle(pedPet, vehicle, 0)
					elseif IsVehicleSeatFree(vehicle, 1) then
						SetPedIntoVehicle(pedPet, vehicle, 1)
					elseif IsVehicleSeatFree(vehicle, 2) then
						SetPedIntoVehicle(pedPet, vehicle, 2)
					else
						petIsInVehicle = false
					end
					if petIsInVehicle then
						if Pet.model == 'cat'  then
							DoRequestAnimSet('creatures@cat@amb@world_cat_sleeping_ground@idle_a')
							TaskPlayAnim(pedPet, 'creatures@cat@amb@world_cat_sleeping_ground@idle_a', 'idle_a' ,8.0, -8.0, -1, 1, 0, false, false, false)
						elseif Pet.model == 'westy' or Pet.model == 'pug' or Pet.model == 'poodle' then
							DoRequestAnimSet('creatures@pug@amb@world_dog_sitting@idle_a')
							TaskPlayAnim(pedPet, 'creatures@pug@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8.0, -1, 1, 0, false, false, false)
						elseif Pet.model == 'retriever' or Pet.model == 'sheperd' or Pet.model == 'husky'then
							DoRequestAnimSet('creatures@retriever@amb@world_dog_sitting@idle_a')
							TaskPlayAnim(pedPet, 'creatures@retriever@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8.0, -1, 1, 0, false, false, false)
						elseif Pet.model == 'rottweiler' then
							DoRequestAnimSet('creatures@rottweiler@amb@world_dog_sitting@idle_a')
							TaskPlayAnim(pedPet, 'creatures@rottweiler@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8.0, -1, 1, 0, false, false, false)
						end
					end
				end
			end
			if not IsPedInAnyVehicle(PlayerPedId(), false) and IsPedInAnyVehicle(pedPet, true) then
				SetEntityNoCollisionEntity(pedPet, PlayerPedId(), true)
				SetEntityCoords(pedPet, coords.x, coords.y, coords.z - 0.5)
			end
			if not IsPedInAnyVehicle(pedPet, true) then
				TaskGoToEntity(pedPet, PlayerPedId(), 60000, GetDistanceForTaskGoToWithCurrentPet(), 100.0, 1073741824, 0)
			end
			--if distance <= GetDistanceForTaskGoToWithCurrentPet() and hasBalle then
			--	PetDropBall()
			--end
		end
	end
end)

function CurrentPetCanSeatInVehicle()
	if Pet.model == 'boar' or Pet.model == 'cow' then
		return false
	else
		return true
	end
end

function GetDistanceForTaskGoToWithCurrentPet()
	if Pet.model == 'boar' or Pet.model == 'pig' then
		return 2.0
	elseif Pet.model == 'cow' then
		return 3.0
	elseif Pet.model == 'rottweiler' then
		return 1.2
	else
		return 0.9
	end
end

-- check si player devient invisible (concess, garage achat/location)
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(100)

		if come == 1 and petIsSpawned and not petIsReturning and not IsEntityVisible(PlayerPedId()) then

			petIsReturning = true
			ESX.ShowNotification(_U('doghouse_returning', Pet.name))

			DeleteEntity(pedPet)
			petIsReturning = false
			petIsSpawned = false
			come = 0

		end
	end
end)

-- check si pet trop loin
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(2000)

		if come == 1 and petIsSpawned and not petIsReturning then

			local coords   = GetEntityCoords(PlayerPedId())
			local coords2  = GetEntityCoords(pedPet)
			local distance = GetDistanceBetweenCoords(coords, coords2, true)

			if distance > 640.0 then
				ESX.ShowNotification(_U('doghouse_returning', Pet.name))
				DeleteEntity(pedPet)
				petIsReturning = false
				petIsSpawned = false
				come = 0
			end
		end
	end
end)

-- Balle
--[[Citizen.CreateThread(function()
	while true do
		Citizen.Wait(50)

		if not hasBalle then 

			local coordsPet = GetEntityCoords(pedPet)

			local closestBall = GetClosestObjectOfType(coordsPet, 50.0, GetHashKey('w_am_baseball'))
			if DoesEntityExist(closestBall) and not IsEntityAttached(closestBall) then
				local coordsBall =  GetEntityCoords(closestBall)

				if come == 1 and petIsSpawned and not isAttached and not petIsReturning and GetEntitySpeed(pedPet) < 0.05 then
					if closestBall ~= nil then
						TaskGoToCoordAnyMeans(pedPet, coordsBall, 5.0, 0, 0, 786603, 0xbf800000)
					end
				end

				local coordsPlayer = GetEntityCoords(PlayerPedId())
				if GetDistanceBetweenCoords(coordsPet, coordsBall, true) < 1.0 and GetDistanceBetweenCoords(coordsPlayer, coordsBall, true) > 3.0 then
					PetTakeBall(closestBall)
				end
			end
		
		end

	end
end)

function CurrentPetCanLookForBall()
	if Pet.model == 'boar' or Pet.model == 'cow' then
		return false
	else
		return true
	end
end]]--

-- boucle assis coucher
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(100)
		if currentAnim == 'coucher' then -- [chien, retriever, sheperd, husky]
			if not IsEntityPlayingAnim(pedPet, 'creatures@rottweiler@amb@sleep_in_kennel@', 'sleep_in_kennel', 3) then
				DoRequestAnimSet('creatures@rottweiler@amb@sleep_in_kennel@')
				TaskPlayAnim(pedPet, 'creatures@rottweiler@amb@sleep_in_kennel@', 'sleep_in_kennel' ,8.0, -8, -1, 1, 0, false, false, false)
			end
		elseif currentAnim == 'coucher2' then -- [chat]
			if not IsEntityPlayingAnim(pedPet, 'creatures@cat@amb@world_cat_sleeping_ground@idle_a', 'idle_a', 3) then
				DoRequestAnimSet('creatures@cat@amb@world_cat_sleeping_ground@idle_a')
				TaskPlayAnim(pedPet, 'creatures@cat@amb@world_cat_sleeping_ground@idle_a', 'idle_a' ,8.0, -8, -1, 1, 0, false, false, false)
			end
		-- Faire assoir l'animal
		elseif currentAnim == 'assis' then -- [carlin, pug]
			if not IsEntityPlayingAnim(pedPet, 'creatures@pug@amb@world_dog_sitting@idle_a', 'idle_b', 3) then
				DoRequestAnimSet('creatures@pug@amb@world_dog_sitting@idle_a')
				TaskPlayAnim(pedPet, 'creatures@pug@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8, -1, 1, 0, false, false, false)
			end
		elseif currentAnim == 'assis2' then -- [retriever, sheperd, husky]
			if not IsEntityPlayingAnim(pedPet, 'creatures@retriever@amb@world_dog_sitting@idle_a', 'idle_b', 3) then
				DoRequestAnimSet('creatures@retriever@amb@world_dog_sitting@idle_a')
				TaskPlayAnim(pedPet, 'creatures@retriever@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8, -1, 1, 0, false, false, false)
			end
		elseif currentAnim == 'assis3' then -- [rottweiler ]
			if not IsEntityPlayingAnim(pedPet, 'creatures@rottweiler@amb@world_dog_sitting@idle_a', 'idle_b', 3) then
				DoRequestAnimSet('creatures@rottweiler@amb@world_dog_sitting@idle_a')
				TaskPlayAnim(pedPet, 'creatures@rottweiler@amb@world_dog_sitting@idle_a', 'idle_b' ,8.0, -8, -1, 1, 0, false, false, false)
			end
		end
	end
end)

-- boucle de faim
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(72000)

		if come == 1 and not Pet.isDead then
			Pet.hunger = Pet.hunger - 1
		end

		if Pet.hunger == 0 and not Pet.isDead then
			Pet.isDead = true
			Pet.deathReason = 'Mort de faim'
			TriggerServerEvent('lp_pets:savePet', Pet)
			SetEntityHealth(pedPet, 0)

			ESX.ShowNotification(_U('pet_dead_hunger', Pet.name))
			come = 0
		end

		if Pet.model ~= nil and come == 1 then
			TriggerServerEvent('lp_pets:savePet', Pet)
		end
	end
end)

-- check si pet meurt
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)

		if come == 1 and petIsSpawned then

			if IsPedDeadOrDying(pedPet, true) then
				
				local isRealDeath = true
				local d = GetPedCauseOfDeath(pedPet)
				if checkArray(Melee, d) then
					Pet.deathReason = 'Bagarre'
				elseif checkArray(Bullet, d) then
					Pet.deathReason = 'Arme à feu'
				elseif checkArray(Knife, d) then
					Pet.deathReason = 'Couteau'
				elseif checkArray(Animal, d) then
					Pet.deathReason = 'Mordu par animal'
				elseif checkArray(FallDamage, d) then
					Pet.deathReason = 'Chute'
				elseif checkArray(Explosion, d) then
					Pet.deathReason = 'Explosion'
				elseif checkArray(Gas, d) then
					Pet.deathReason = 'Gaz'
				elseif checkArray(Burn, d) then
					Pet.deathReason = 'Feu'
				elseif checkArray(Drown, d) then
					Pet.deathReason = 'Noyé'
				elseif checkArray(Car, d) then
					Pet.deathReason = 'Ecrasé par véhicule'
				else
					Pet.deathReason = 'Mort inconnu'
					isRealDeath = false
				end

				if isRealDeath then
					Pet.isDead = true
					TriggerServerEvent('lp_pets:savePet', Pet)
					ESX.ShowNotification(_U('pet_dead', Pet.name))
					come = 0
				else
					ESX.ShowNotification(_U('doghouse_returning', Pet.name))
					petIsReturning = false
					petIsSpawned = false
					come = 0
				end
			end
		end
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if IsControlJustPressed(0, 56) and GetLastInputMethod(2) and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'pet_menu') then
            ESX.TriggerServerCallback('lp_pets:getPet', function(aPet)
                if aPet == nil or aPet.isDead then
                    ESX.ShowNotification(_U('no_pet'))
                else
                    Pet = aPet
			        OpenPetMenu()
                end
            end)
		end
	end
end)


------------------
-- BALL MANAGER --
------------------

function PetTakeBall(balle)
	hasBalle = true
	currentBalle = balle

	AttachEntityToEntity(balle, pedPet, 12844, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, false, false, false, false, 2, true)
	SetEntityCollision(currentBalle, false, false)
end

function PetDropBall()
	hasBalle = false

	DetachEntity(currentBalle, true, true)
	SetEntityCollision(currentBalle, true, true)
end

---------------------
-- BLIPS & MARKERS --
---------------------

-- Create Blips
Citizen.CreateThread(function()
	for i=1, #Config.Zones, 1 do
		local blip = AddBlipForCoord(Config.Zones[i].Pos.x, Config.Zones[i].Pos.y, Config.Zones[i].Pos.z)

		SetBlipSprite (blip, Config.Zones[i].Sprite)
		SetBlipDisplay(blip, Config.Zones[i].Display)
		SetBlipScale  (blip, Config.Zones[i].Scale)
		SetBlipAsShortRange(blip, true)
	
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(_U('pet_shop'))
		EndTextCommandSetBlipName(blip)
	end
end)

-- Draw Marker
Citizen.CreateThread(function()

    while true do
        Citizen.Wait(0)

        local coord = GetEntityCoords(PlayerPedId())

		for i=1, #Config.Zones, 1 do
			local distance = #(coord - vector3(Config.Zones[i].Pos.x, Config.Zones[i].Pos.y, Config.Zones[i].Pos.z))
			if distance < Config.DrawDistance then
				DrawMarker(
					Config.Zones[i].Marker.type,
					Config.Zones[i].Pos.x,
					Config.Zones[i].Pos.y,
					Config.Zones[i].Pos.z,
					0.0,
					0.0,
					0.0,
					0.0,
					0.0,
					0.0,
					Config.Zones[i].Marker.x,
					Config.Zones[i].Marker.y,
					Config.Zones[i].Marker.z,
					Config.Zones[i].Marker.r,
					Config.Zones[i].Marker.g,
					Config.Zones[i].Marker.b,
					Config.Zones[i].Marker.a,
					false,
					false,
					2,
					Config.Zones[i].Marker.rotate,
					nil,
					nil,
					false
				)
			end
		end
    end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local coord = GetEntityCoords(PlayerPedId())
		local showOpenShop = false

		for i=1, #Config.Zones, 1 do
			if GetDistanceBetweenCoords(coord, Config.Zones[i].Pos.x, Config.Zones[i].Pos.y, Config.Zones[i].Pos.z, true) < 2 then
				ESX.ShowHelpNotification(_U('enterkey'))
				showOpenShop = true
				if IsControlJustReleased(0, 38) then
					if Config.Zones[i].Category == 'pet' then
						OpenPetShop()
					else
						OpenFarmShop()
					end
				end
			end
		end

		if not showOpenShop then
			Citizen.Wait(500)
		end
	end
end)


----------------------
-- MAGASINS DE PETS --
----------------------

function OpenPetShop()
	local elements = {}

	for i=1, #Config.PetShop, 1 do
		table.insert(elements, {
			label = ('%s<span style="color:green;">%s</span>'):format(Config.PetShop[i].label, _U('shop_item', ESX.Math.GroupDigits(Config.PetShop[i].price))),
			pet = Config.PetShop[i].pet,
			price = Config.PetShop[i].price
		})
	end

	OpenShop(elements)
end

function OpenFarmShop()
	local elements = {}

	for i=1, #Config.FarmShop, 1 do
		table.insert(elements, {
			label = ('%s<span style="color:green;">%s</span>'):format(Config.FarmShop[i].label, _U('shop_item', ESX.Math.GroupDigits(Config.FarmShop[i].price))),
			pet = Config.FarmShop[i].pet,
			price = Config.FarmShop[i].price
		})
	end

	OpenShop(elements)
end

function OpenShop(elements)

	ESX.ShowNotification(_U('warning_buy_pet'))

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pet_shop', {
		title    = _U('pet_shop'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'pet_shop_name', {
			title = _U('pet_shop_name')
		}, function(data2, menu2)
			ESX.TriggerServerCallback('lp_pets:buyPet', function(boughtPed)
				if boughtPed then

					if come == 1 then
						DeleteEntity(pedPet)
						come = 0
					end

					menu2.close()
					menu.close()
				else
					menu2.close()
				end
			end, data.current.pet, data2.value, data.current.price)
		end, function(data2, menu2)
			menu2.close()
		end)
	end, function(data, menu)
		menu.close()
	end)
end

function DoRequestAnimSet(anim)
	RequestAnimDict(anim)
	while not HasAnimDictLoaded(anim) do
		Citizen.Wait(1)
	end
end

function checkArray (array, val)
	for name, value in ipairs(array) do
		if value == val then
			return true
		end
	end

	return false
end