ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('lp_pets:getPet', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT pet FROM users WHERE identifier = @identifier', {
		['@identifier'] = xPlayer.identifier
	}, function(result)
		if result[1].pet ~= nil and result[1].pet ~= '' then
			cb(json.decode(result[1].pet))
		else
			cb(nil)
		end
	end)
end)

RegisterServerEvent('lp_pets:consumePetFood')
AddEventHandler('lp_pets:consumePetFood', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('croquettes', 1)
end)

ESX.RegisterServerCallback('lp_pets:buyPet', function(source, cb, aModel, aName, price)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.getMoney() >= price then
		xPlayer.removeMoney(price)

		local pet = {
			model = aModel,
			name = aName,
			hunger = 100,
			isDead = false,
			deathReason = ''
		}

		MySQL.Async.execute('UPDATE users SET pet = @pet WHERE identifier = @identifier', {
			['@identifier'] = xPlayer.identifier,
			['@pet'] = json.encode(pet)
		}, function(rowsChanged)
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_bought', aName, ESX.Math.GroupDigits(price)))
			cb(true)
		end)
	else
		TriggerClientEvent('esx:showNotification', source, _U('not_bought'))
		cb(false)
	end
end)

RegisterServerEvent('lp_pets:savePet')
RegisterServerEvent('lp_pets:savePet', function(pet)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.execute('UPDATE users SET pet = @pet WHERE identifier = @identifier', {
        ['@identifier'] = xPlayer.identifier,
        ['@pet'] = json.encode(pet)
    })
end)