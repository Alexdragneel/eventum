ALTER TABLE `users` CHANGE `pet` `pet` LONGTEXT NULL; 

UPDATE users
SET pet = (
	SELECT
		CONCAT (
			CONCAT (
				CONCAT (
					CONCAT (
						'{"model":"',
						SUBSTRING(
							pet,
							1,
							INSTR(
								pet,
								':'
							) - 1
						)
					),
					'","name":"'
				),
				SUBSTRING(
					REPLACE(
						pet,
						SUBSTRING(
							pet,
							1,
							INSTR(
								pet,
								':'
							)
						),
						''
					),
					1,
					INSTR(
						REPLACE(
							pet,
							SUBSTRING(
								pet,
								1,
								INSTR(
									pet,
									':'
								)
							),
							''
						),
						':'
					) - 1
				)
			),
			'","hunger":100,"isDead":false,"deathReason":""}'
		)
)
WHERE pet IS NOT NULL AND pet <> ''