Config = {}
Config.Locale = 'fr'

Config.Debug = false
Config.JailBlip = vector3(1854.0, 2622.0, 45.0)
Config.JailLocation = vector3(1720.0, 2566.3, 44.56)
Config.JailCenter = vector3(1700.595, 2576.151,44.56)
Config.Infirmary = vector3(1682.73, 2476.52, 44.81)
Config.JailTimeSyncInterval = 60000 * 5
Config.DrawDistance = 5
Config.TaskWaitTime = 3
Config.TaskReduceTime = 1 * 60
Config.EscapeTimeMultiplier = 2

Config.Blips = {
	Penitentiary = {
		xyz = vector3(1854.0, 2622.0, 45.0),
		Sprite = 188,
		Scale = 1.9,
		Color = 6,
		Name = 'penitentiary_name',
		Restrictions = {
			prisoner = false
		}
	},
	Main = {
		xyz = vector3(1762.42, 2489.47, 45.8),
		Sprite = 685,
		Scale = 0.9,
		Color = 6,
		Name = 'penitentiary_main_block',
		Restrictions = {
			prisoner = true
		}
	},
	Canteen = {
		xyz = vector3(1782.22, 2551.60, 45.6),
		Sprite = 59,
		Scale = 0.9,
		Color = 6,
		Name = 'penitentiary_canteen',
		Restrictions = {
			prisoner = true
		}
	},
	Meeting = {
		xyz = vector3(1841.3854, 2586.783, 45.8),
		Sprite = 787,
		Scale = 0.9,
		Color = 6,
		Name = 'penitentiary_meeting',
		Restrictions = {
			prisoner = true
		}
	},

}

Config.Uniforms = {
	prison_wear = {
		male = {
			['tshirt_1'] = 15,  ['tshirt_2'] = 0,
			['torso_1']  = 146, ['torso_2']  = 0,
			['decals_1'] = -1,
			['arms'] = 0,
			['pants_1']  = 3, 	['pants_2']  = 7,
			['shoes_1']  = 12,	['shoes_2']  = 12,
			['chain_1']  = -1,
			['helmet_1'] = -1,
			['mask_1'] = -1,
			['ears_1'] = -1,
			['watches_1'] = -1,
			['bracelets_1'] = -1,
			['bag_1'] = 0
		},
		female = {
			['tshirt_1'] = 3,   ['tshirt_2'] = 0,
			['torso_1']  = 38,  ['torso_2']  = 3,
			['decals_1'] = -1,
			['arms'] = 2,   
			['pants_1']  = 3,	['pants_2']  = 15,
			['shoes_1']  = 66,	['shoes_2']  = 5,   
			['chain_1']  = -1,
			['helmet_1'] = -1,
			['mask_1'] = -1,
			['ears_1'] = -1,
			['watches_1'] = -1,
			['bracelets_1'] = -1,
			['bag_1'] = 0
		}
	}
}

Config.FastTravels = {
	{
		-- New inmate G3 to inside
		From = vector3(1690.7, 2591.6, 44.91),
		To = {coords = vector3(1691.6, 2565.5, 44.56), heading = 180.0},
		Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 255, g = 0, b = 165, a = 100, rotate = false},
		Prompt = "use_door",
		Restrictions = {
			job = 'police',
			inService = true,
			prisoner = false
		}
	},
	{
		-- Inside to New inmate G3
		From = vector3(1691.6, 2565.5, 44.56),
		To = {coords = vector3(1690.7, 2591.6, 44.91), heading = 0.0},
		Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 255, g = 0, b = 165, a = 100, rotate = false},
		Prompt = "use_door",
		Restrictions = {
			job = 'police',
			inService = true,
			prisoner = false
		}
	},
	{
		-- Meeting room oustide to jail inside
		From = vector3(1792.05, 2593.68, 44.8),
		To = {coords = vector3(1765.64, 2565.6, 44.56), heading = 180.0},
		Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 255, g = 0, b = 165, a = 100, rotate = false},
		Prompt = "use_door"
	},
	{
		-- jail inside to Meeting room oustide
		From = vector3(1765.64, 2565.6, 44.56),
		To = {coords = vector3(1792.05, 2593.68, 44.8), heading = 270.0},
		Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 255, g = 0, b = 165, a = 100, rotate = false},
		Prompt = "use_door"
	},
	{
		-- Corridor to Meeting room
		From = vector3(1836.2, 2585.33, 44.89),
		To = {coords = vector3(1837.5, 2585.42, 44.89), heading = 270.0},
		Marker = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 255, g = 0, b = 165, a = 100, rotate = false},
		Prompt = "exit_jail",
		Restrictions = {
			prisoner = false
		},
		Action = {
			type = 'server',
			event = 'lp_jail:exit'
		}
	},
}

Config.Tasks = {
	Barbells = {
		Type = 'Sport',
		Duration = 30000,
		MaxSteps = 2,
		Anim = {
			Base = {
				lib = 'amb@world_human_muscle_free_weights@male@barbell@base',
				anim = 'base',
				props = {
					{
						name = 'barbell_r', bone = 18905 --[[57005--]], x = 0.1, y = 0.0, z = 0.015, xRot = 0.0, yRot = 25.0, zRot = 80.0, model = 'prop_barbell_01'
					},
					{
						name = 'barbell_l', bone = 57005 --[[18905--]], x = 0.1, y = 0.0, z = -0.015, xRot = 0.0, yRot = -25.0, zRot = 80.0, model = 'prop_barbell_01'
					}
				}
			},
			Idle = {
				{
					lib = 'amb@world_human_muscle_free_weights@male@barbell@idle_a',
					anim = 'idle_a'
				},
				{
					lib = 'amb@world_human_muscle_free_weights@male@barbell@idle_b',
					anim = 'idle_b'
				},
				{
					lib = 'amb@world_human_muscle_free_weights@male@barbell@idle_c',
					anim = 'idle_c'
				}
			},
		},
		Locations = {
			{
				xyz = vector3(1646.3, 2535.0, 44.56),
				showBlip = true
			},
			{
				xyz = vector3(1645.0, 2523.5, 44.56),
				showBlip = true
			},
			{
				xyz = vector3(1733.5, 2489.0, 44.83),
				showBlip = true
			}
		},
		Prompt = 'lift_weights',
		Visibility = 'always'
	},
	Chinups = {
		Type = 'Sport',
		Duration = 30000,
		MaxSteps = 2,
		Anim = {
			Enter = {
				lib = 'amb@prop_human_muscle_chin_ups@male@enter',
				anim = 'enter'
			},
			Base = {
				lib = 'amb@prop_human_muscle_chin_ups@male@base',
				anim = 'base'
			},
			Idle = {
				{
					lib = 'amb@prop_human_muscle_chin_ups@male@idle_a',
					anim = 'idle_a'
				}
			},
			Exit = {
				lib = 'amb@prop_human_muscle_chin_ups@male@exit',
				anim = 'exit'
			}
		},
		Locations = {
			{
				xyz = vector3(1643.22, 2528.0, 44.56),
				handling = 227.0,
				showBlip = true
			},
			{
				xyz = vector3(1648.96, 2529.8, 44.56),
				handling = 227.0,
				showBlip = true
			}
		},
		Prompt = 'do_pulls',
		Visibility = 'always'
	},
	Showers = {
		Type = 'Showers',
		Duration = 60000,
		Anim = {
			Enter = {
				lib = 'amb@prop_human_muscle_chin_ups@male@enter',
				anim = 'enter'
			},
			Base = {
				lib = 'anim@mp_yacht@shower@male@',
				anim = 'male_shower_idle_d'
			},
			Idle = {
				{
					lib = 'amb@prop_human_muscle_chin_ups@male@idle_a',
					anim = 'idle_a'
				}
			},
			Exit = {
				lib = 'amb@prop_human_muscle_chin_ups@male@exit',
				anim = 'exit'
			}
		},
		Locations = {
			{
				xyz = vector3(1764.35, 2512.1, 48.21),
				handling = 33.0,
				showBlip = true
			},
			{
				xyz = vector3(1765.8, 2513.0, 48.21),
				handling = 33.0,
				showBlip = true
			},
			{
				xyz = vector3(1767.5, 2514.0, 48.21),
				handling = 33.0,
				showBlip = true
			},
			{
				xyz = vector3(1769.25, 2515.0, 48.21),
				handling = 33.0,
				showBlip = true
			},
			{
				xyz = vector3(1766.2, 2509.0, 48.21),
				handling = 210.0,
				showBlip = true
			},
			{
				xyz = vector3(1767.6, 2510.0, 48.21),
				handling = 210.0,
				showBlip = true
			},
			{
				xyz = vector3(1769.4, 2511.0, 48.21),
				handling = 210.0,
				showBlip = true
			},
			{
				xyz = vector3(1771.1, 2512.0, 48.21),
				handling = 210.0,
				showBlip = true
			},
		},
		Prompt = 'take_shower',
		Visibility = 'always'
	},
	Cleaning = {
		Type = 'Cleaning',
		Duration = 30000,
		MaxSteps = 2,
		Anim = {
			Base = {
				lib = 'amb@world_human_janitor@male@idle_a',
				anim = 'idle_a',
				props = {
					{
						name = 'broom', bone = 28422, x = 0.0, y = 0.0, z = 0.0, xRot = 0.0, yRot = 0.0, zRot = 0.0, model = 'prop_tool_broom'
					},
					{
						name = 'cleaning_trolly', bone = nil, x = -1.0, y = -1.0, z = 0.0, xRot = 0.0, yRot = 0.0, zRot = 0.0, model = 'prop_cleaning_trolly'	
					}
				}
			},
		},
		Locations = {
			{
				xyz = vector3(1778.5, 2482.5, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1768.0, 2474.5, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1762.0, 2472.5, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1773.5, 2497.0, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1761.5, 2490.0, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1746.5, 2481.0, 44.85),
				showBlip = true
			},
			{
				xyz = vector3(1733.5, 2496.0, 44.85),
				showBlip = true
			},
		},
		Marker = {
			size = vector3(5.0, 5.0, 0.5)
		},
		Prompt = 'clean',
		Visibility = 'task'
	},
	Canteen = {
		Type = 'Canteen',
		Duration = 12500,
		MaxSteps = 4,
		Anim = {
			Base = {
				lib = 'timetable@floyd@clean_kitchen@base',
				anim = 'base',
				props = {
					{
						name = 'sponge', bone = 28422, x = 0.0, y = 0.00, z = 0.0, xRot = 0.0, yRot = 0.0, zRot = 0.0, model = 'v_res_fa_sponge01'
					}
				}
			}
		},
		Locations = {
			{
				xyz = vector3(1787.1, 2567.73, 44.62),
				handling = 0.0,
				showBlip = true
			},
			{
				xyz = vector3(1781.0, 2567.58, 44.62),
				handling = 0.0,
				showBlip = true
			},
			{
				xyz = vector3(1776.9, 2564.35, 44.62),
				handling = 90.0,
				showBlip = true
			},
			{
				xyz = vector3(1788.2, 2551.34, 44.62),
				handling = 230.0,
				showBlip = true
			},
			{
				xyz = vector3(1783.0, 2544.0, 44.62),
				handling = 230.0,
				showBlip = true
			},
			{
				xyz = vector3(1777.38, 2550.83, 44.62),
				handling = 230.0,
				showBlip = true
			}
		},
		Prompt = 'clean',
		Visibility = 'task'
	},
	--[[
	Service = {
		Type = 'Service',
		Duration = 60000,
		Anim = {
			Base = {
				lib = 'mp_common',
				anim = 'givetake1_b',
				props = {
					{
						name = 'plate', bone = 28422, x = 0.25, y = 0.04, z = 0.0, xRot = 270.0, yRot = 00.0, zRot = 00.0, model = 'v_ret_fh_plate3'
					}
				}
			}
		},
		Locations = {
			{
				xyz = vector3(1780.0, 2561.8, 44.62),
				showBlip = true
			},
			{
				xyz = vector3(1782.7, 2561.8, 44.62),
				showBlip = true
			},
			{
				xyz = vector3(1785.0, 2561.8, 44.62),
				showBlip = true
			},
			{
				xyz = vector3(1787.2, 2561.8, 44.62),
				showBlip = true
			}
		},
		Prompt = 'serve',
		Visibility = 'always'
	}, --]]
	Race = {
		Type = 'Race',
		Locations = {
			{
				xyz = vector3(1763.35, 2536.0, 45.56), -- Step 1 : Start
				handling = 116.0,
				showBlip = true
			},
			{
				xyz = vector3(1713.7, 2503.0, 45.46), -- Step 2
				handling = 0.0
			},
			{
				xyz = vector3(1663.5, 2494.0, 45.56), -- Step 3
				handling = 0.0
			},
			{
				xyz = vector3(1624.5, 2526.5, 45.56), -- Step 4
				handling = 0.0
			},
			{
				xyz = vector3(1656.5, 2561.0, 45.56), -- Step 5
				handling = 0.0
			},
			{
				xyz = vector3(1723.45, 2561.0, 45.56), -- Step 6
				handling = 0.0
			},
			--[[{
				xyz = vector3(0.0, 0.0, 0.0), -- Step 7
				handling = 0.0
			},
			{
				xyz = vector3(0.0, 0.0, 0.0), --Step 8
				handling = 0.0
			},
			--]]
			{
				xyz = vector3(1763.35, 2536.0, 45.56), -- Finish
				handling = 116.0
			}
		},
		Prompt = 'start_running',
		Visibility = 'task'
	},
	Check = {
		Type = 'Check',
		Locations = {
			{
				xyz = vector3(1758.5, 2540.75, 44.56),
				handling = 120.0,
				showBlip = true
			}
		},
		Visibility = 'task'
	},
}

Config.Places = {
	Canteen = {
		Type = 'Canteen',
		Locations = {
			{
				xyz = vector3(1781.5, 2557.75, 44.62)
			},
			{
				xyz = vector3(1786.3, 2557.8, 44.62)
			}
		},
		Prompt = 'take_meal',
		Visibility = 'always',
		Action = {
			type = 'server',
			event = 'lp_jail:getMealTray'
		}
	},
}

Config.GuardLocations = {
	From = {
		xyz = vector3(1765.64, 2565.6, 44.56), 
		handling = 180.0
	},
	To = {
		xyz = vector3(1752.0, 2536.0, 44.57), 
		handling = 300.0
	}
}