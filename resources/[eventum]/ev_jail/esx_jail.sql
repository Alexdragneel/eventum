START TRANSACTION;

CREATE TABLE `lp_jail` (
  `identifier` varchar(40) NOT NULL,
  `isPrisoner` tinyint(4) NOT NULL DEFAULT '0',
  `jailTime` int(11) NOT NULL DEFAULT '0',
  `remainingJailTime` int(11) NOT NULL DEFAULT '0',
  `remainingEscapeTime` int(11) NOT NULL DEFAULT '0'
);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `lp_jail`
--
ALTER TABLE `lp_jail`
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `lp_jail`
--
ALTER TABLE `lp_jail`
  ADD CONSTRAINT `lp_jail_user_identifier` FOREIGN KEY (`identifier`) REFERENCES `users` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `lp_items` (`name`, `label`, `sellPrice`, `farmPrice`, `weight`, `actions`, `canRemove`, `rarity`, `icon`, `stackable`) VALUES
('meal_tray', 'Plateau repas', 0, 0, 200, '{\"anim\":{\"prop\":\"prop_food_tray_02\",\"dict\":\"mp_player_inteat@burger\",\"name\":\"mp_player_int_eat_burger_fp\"},\"effects\":[{\"type\":\"add\",\"attribute\":\"hunger\",\"amount\":1000000},{\"type\":\"add\",\"attribute\":\"thirst\",\"amount\":1000000}]}', 1, 'common', 'plateau_repas.png', 1);

COMMIT;
