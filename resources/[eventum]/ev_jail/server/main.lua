ESX = nil
local playersInJail = {}
local currentTask = nil
local lastTask = nil
local prisonnierPedSpawned = false
local prisonersPed = {}
local guardPed = nil
local checkPrisonerList = {}
local checkListIndex = 0
local startCheck = false
local hasTaskTimer = false
local policeInServiceCount = 0

TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)

RegisterNetEvent('lp_jail:disableService')
AddEventHandler('lp_jail:disableService', function(name)
    if name == 'police' then
        GetPoliceInServiceCount()
    end
end)

RegisterNetEvent('lp_jail:enableService')
AddEventHandler('lp_jail:enableService', function(name)
    if name == 'police' then
        GetPoliceInServiceCount()
    end
end)

function GetPoliceInServiceCount()
    TriggerEvent('esx_service:InServiceCount', 'police', function(count)
        policeInServiceCount = count
    end)
end

RegisterNetEvent('lp_jail:prisonerEscaped')
AddEventHandler('lp_jail:prisonerEscaped', function()
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

    if playersInJail[_source] ~= nil then
        local notification = GetDefaultNotification()
        playersInJail[_source].escaped = true
        notification.subject = _U('escape')
        notification.msg = _U('prisoner_escape_in_progress', xPlayer.get('lastName'), xPlayer.get('firstName'))
        TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
        GetJailedTime(xPlayer.identifier, function(jailedTime)
            playersInJail[_source].remainingEscapeTime = jailedTime * Config.EscapeTimeMultiplier
            playersInJail[_source].escaped = true
            playersInJail[_source].jailed = false
            playersInJail[_source].remainingJailTime = 0
            SaveJailTime(_source)
            TriggerClientEvent('lp_jail:setPrisonerData', _source, playersInJail[_source])
        end)
    end
end)

RegisterNetEvent('lp_jail:areaPenetration')
AddEventHandler('lp_jail:areaPenetration', function()
    local _source = source
    local notification = GetDefaultNotification()
    notification.subject = _U('area_violation')
    notification.msg = _U('penitentiary_area_violation')
    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
    notification.msg = _U('warning_penitentiary_area_violation')
    TriggerClientEvent('esx:showAdvancedNotification', _source, notification.title, notification.subject,
        notification.msg, notification.picture, notification.iconType, false, true, 6)
end)

RegisterNetEvent('lp_jail:exit')
AddEventHandler('lp_jail:exit', function()
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local playerInJail = playersInJail[_source]
    xPlayer.setInventoryItem('meal_tray', 0)
    xPlayer.setInventoryItem('phone', 1)
    playersInJail[_source] = {
        identifier = playerInJail.identifier,
        isPrisoner = false,
        remainingJailTime = 0,
        remainingEscapeTime = 0
    }
    SaveJailTime(_source)
    playersInJail[_source] = nil
    TriggerClientEvent('lp_jail:exit', _source)
    TriggerClientEvent('lp_jail:setPlayerIsPrisoner', _source, false)
    TriggerEvent('lp_jail:setPlayerIsPrisoner', _source, false)
end)

AddEventHandler('esx:playerDropped', function(playerId, reason)
    if playersInJail[playerId] then
        if not playersInJail[playerId].escaped then
            local xPlayer = ESX.GetPlayerFromId(playerId)
            local notification = GetDefaultNotification()
            notification.subject = _U('prisoner_status')
            notification.msg = _U('prisoner_go_to_sleep', xPlayer.get('lastName'), xPlayer.get('firstName'))
            TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
        end
        SaveJailTime(playerId)
        playersInJail[playerId] = nil
    end
    TriggerClientEvent('lp_jail:setPlayerIsPrisoner', playerId, false)
    TriggerEvent('lp_jail:setPlayerIsPrisoner', playerId, false)
end)

AddEventHandler('esx:playerLoaded', function(playerId, xPlayer)
    MySQL.Async.fetchAll(
        'SELECT * FROM lp_jail WHERE identifier = @identifier AND (isPrisoner = 1 OR remainingJailTime > 0 OR remainingEscapeTime > 0)',
        {
            ['@identifier'] = xPlayer.identifier
        }, function(result)
            local isPrisoner = false
            if result then
                if #result > 0 then
                    playersInJail[playerId] = {
                        remainingJailTime = result[1].remainingJailTime,
                        remainingEscapeTime = result[1].remainingEscapeTime,
                        identifier = xPlayer.identifier,
                        isPrisoner = result[1].isPrisoner == 1,
                        escaped = result[1].remainingEscapeTime > 0,
                        jailed = result[1].remainingJailTime > 0
                    }
                    xPlayer.triggerEvent('lp_jail:setPrisonerData', playersInJail[playerId])
                    isPrisoner = result[1].isPrisoner == 1
                end
            end
            TriggerClientEvent('lp_jail:setPlayerIsPrisoner', playerId, isPrisoner)
            TriggerEvent('lp_jail:setPlayerIsPrisoner', playerId, isPrisoner)
        end)
end)

RegisterNetEvent('lp_jail:prisonerWakeUp')
AddEventHandler('lp_jail:prisonerWakeUp', function()
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local notification = GetDefaultNotification()
    notification.subject = _U('prisoner_status')
    notification.msg = _U('prisoner_wake_up', xPlayer.get('lastName'), xPlayer.get('firstName'))
    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
end)

ESX.RegisterCommand('jail', 'admin', function(xPlayer, args, showError)
    TriggerEvent('lp_jail:sendToJail', args.playerId, args.time * 60)
end, true, {
    help = 'Jail a player',
    validate = true,
    arguments = {{
        name = 'playerId',
        help = 'player id',
        type = 'playerId'
    }, {
        name = 'time',
        help = 'jail time in minutes',
        type = 'number'
    }}
})

ESX.RegisterCommand('unjail', 'admin', function(xPlayer, args, showError)
    unjailPlayer(args.playerId)
end, true, {
    help = 'Unjail a player',
    validate = true,
    arguments = {{
        name = 'playerId',
        help = 'player id',
        type = 'playerId'
    }}
})

RegisterNetEvent('lp_jail:sendToJail')
AddEventHandler('lp_jail:sendToJail', function(playerId, jailTime)
    local xPlayer = ESX.GetPlayerFromId(playerId)

    if xPlayer then
        MySQL.Async.fetchAll('SELECT identifier FROM lp_jail WHERE identifier = @identifier', {
            ['@identifier'] = xPlayer.identifier
        }, function(result)
            if #result > 0 then
                MySQL.Async.execute(
                    'UPDATE lp_jail SET isPrisoner = 1, jailTime = @jailTime, remainingJailTime = @jailTime, remainingEscapeTime = 0 WHERE identifier = @identifier',
                    {
                        ['@identifier'] = xPlayer.identifier,
                        ['@jailTime'] = jailTime
                    }, function(rowsChanged)
                        xPlayer.triggerEvent('lp_jail:jailPlayer', jailTime, true)
                        playersInJail[playerId] = {
                            remainingJailTime = jailTime,
                            remainingEscapeTime = 0,
                            identifier = xPlayer.identifier,
                            isPrisoner = true,
                            escaped = false,
                            jailed = true
                        }
                        TriggerClientEvent('lp_jail:setPlayerIsPrisoner', playerId, true)
                        TriggerEvent('lp_jail:setPlayerIsPrisoner', playerId, true)
                    end)
            else
                MySQL.Async.execute(
                    'INSERT INTO lp_jail (identifier, isPrisoner, jailTime, remainingJailTime, remainingEscapeTime) VALUES (@identifier, 1, @jailTime, @jailTime, 0)',
                    {
                        ['@identifier'] = xPlayer.identifier,
                        ['@jailTime'] = jailTime
                    }, function(rowsChanged)
                        xPlayer.triggerEvent('lp_jail:jailPlayer', jailTime, true)
                        playersInJail[playerId] = {
                            remainingJailTime = jailTime,
                            remainingEscapeTime = 0,
                            identifier = xPlayer.identifier,
                            isPrisoner = true,
                            escaped = false,
                            jailed = true
                        }
                        TriggerClientEvent('lp_jail:setPlayerIsPrisoner', playerId, true)
                        TriggerEvent('lp_jail:setPlayerIsPrisoner', playerId, true)
                    end)
            end
        end)
    end
end)

ESX.RegisterServerCallback('lp_jail:isPlayerInJail', function(source, cb)
    if playersInJail then
        if playersInJail[source] then
            cb(playersInJail[source].escaped == false)
        else
            cb(false)
        end
    else
        cb(false)
    end
end)

RegisterNetEvent('lp_jail:removePrisonerClothes')
AddEventHandler('lp_jail:removePrisonerClothes', function()
    local _source = source
    if playersInJail[_source] ~= nil then
        local xPlayer = ESX.GetPlayerFromId(source)
        MySQL.Async.execute(
        'UPDATE lp_jail SET isPrisoner = 0 WHERE identifier = @identifier',
        {
            ['@identifier'] = xPlayer.identifier
        }, function(rowsChanged)
            playersInJail[_source].isPrisoner = false
            TriggerClientEvent('lp_jail:setPlayerIsPrisoner', _source, false)
            TriggerEvent('lp_jail:setPlayerIsPrisoner', _source, false)
        end)
    end
end)

RegisterNetEvent('lp_jail:reduceTime')
AddEventHandler('lp_jail:reduceTime', function()
    ReduceTime(source)
end)

function ReduceTime(playerId)
    local notification = GetDefaultNotification()
    if playersInJail[playerId].remainingJailTime < Config.TaskReduceTime then
        playersInJail[playerId].remainingJailTime = 0
    else
        playersInJail[playerId].remainingJailTime = playersInJail[playerId].remainingJailTime - Config.TaskReduceTime
    end
    notification.subject = _U('task')
    notification.msg = _U('task_done')
    TriggerClientEvent('esx:showAdvancedNotification', playerId, notification.title, notification.subject,
        notification.msg, notification.picture, notification.iconType, false, true, 20)
    TriggerClientEvent('lp_jail:taskDone', playerId, currentTask)
end

function unjailPlayer(playerId)
    local xPlayer = ESX.GetPlayerFromId(playerId)

    if xPlayer then
        if playersInJail[playerId] then
            MySQL.Async.execute(
                'UPDATE lp_jail SET isPrisoner = 0, jailTime = 0, remainingJailTime = 0, remainingEscapeTime = 0 WHERE identifier = @identifier',
                {
                    ['@identifier'] = xPlayer.identifier
                }, function(rowsChanged)
                    playersInJail[playerId] = nil
                    xPlayer.triggerEvent('lp_jail:unjailPlayer')
                    TriggerClientEvent('lp_jail:setPlayerIsPrisoner', playerId, false)
                    TriggerEvent('lp_jail:setPlayerIsPrisoner', playerId, false)
                end)
        end
    end
end

RegisterNetEvent('lp_jail:startTask')
AddEventHandler('lp_jail:startTask', function(task)
    local _source = source
    if currentTask == task.Type then
        if task.Type == 'Check' then
            local xPlayer = ESX.GetPlayerFromId(_source)
            table.insert(checkPrisonerList, {
                fullName = string.format("%s %s", xPlayer.get('lastName'), xPlayer.get('firstName')),
                playerId = _source
            })
            local notification = GetDefaultNotification()
            notification.subject = _U('task')
            notification.picture = 'CHAR_STEVE'
            notification.msg = _U('check_joined')
            TriggerClientEvent('esx:showAdvancedNotification', _source, notification.title, notification.subject,
                notification.msg, notification.picture, notification.iconType, false, true)
            task.step = 2
            TriggerClientEvent('lp_jail:updateTask', _source, task)
        else
            if task.Duration then
                hasTaskTimer = true
                TriggerClientEvent('rprogress:start_lpjobs', _source, _U(task.Prompt), task.Duration,
                    'lp_jail:updateTaskStep', _source, task)
            end
        end
    end
end)

RegisterNetEvent('lp_jail:updateTaskStep')
AddEventHandler('lp_jail:updateTaskStep', function(playerId, task)
    if task.MaxSteps ~= nil then
        local notification = GetDefaultNotification()
        notification.subject = _U('task')
        notification.msg = _U('task_' .. string.lower(task.Type) .. '_progress', task.step, task.MaxSteps)
        TriggerClientEvent('esx:showAdvancedNotification', playerId, notification.title, notification.subject,
            notification.msg, notification.picture, notification.iconType, false, true)
        task.step = task.step + 1
        TriggerClientEvent('lp_jail:updateTask', playerId, task)
        if task.step > task.MaxSteps then
            ReduceTime(playerId)
        else
            TriggerClientEvent('lp_jail:taskStop', playerId, task)
        end
    else
        ReduceTime(playerId)
    end
end)

RegisterNetEvent('lp_jail:getMealTray')
AddEventHandler('lp_jail:getMealTray', function()
    local _source = source
    local object = 'meal_tray'
    local xPlayer = ESX.GetPlayerFromId(_source)
    local mealTrayCount = 0
    local toAdd = 0
    if xPlayer.getInventoryItem(object) ~= nil then
        mealTrayCount = xPlayer.getInventoryItem(object).count
    end
    if mealTrayCount < 5 then
        toAdd = 5 - mealTrayCount
        if xPlayer.canCarryItem(object, toAdd) then
            xPlayer.addInventoryItem(object, toAdd)
            xPlayer.showNotification(_U('meal_received', toAdd), false, true)
        else
            xPlayer.showNotification(_U('can_not_carry_more'), false, true)
        end
    else
        xPlayer.showNotification(_U('can_get_more_meal'), false, true)
    end
end)

RegisterNetEvent('lp_jail:updateTask')
AddEventHandler('lp_jail:updateTask', function(task)
    local _source = source
    if task.Type == currentTask then
        task.lock = false
        if currentTask == 'Race' then
            if task.step == #Config.Tasks.Race.Locations then
                TriggerClientEvent('lp_jail:updateTask', _source, task)
                ReduceTime(_source)
            else
                task.step = task.step + 1
                TriggerClientEvent('lp_jail:updateTask', _source, task)
            end
        else
            task.step = task.step + 1
            TriggerClientEvent('lp_jail:updateTask', _source, task)
        end
    end
end)

RegisterNetEvent('lp_jail:stopTask')
AddEventHandler('lp_jail:stopTask', function(task)
    if task == currentTask then
        if hasTaskTimer then
            hasTaskTimer = false
            TriggerClientEvent('rprogress:stop', source)
        end
    end
end)

RegisterNetEvent('lp_jail:checkTaskHandsUp')
AddEventHandler('lp_jail:checkTaskHandsUp', function()
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local fullName = string.format("%s %s", xPlayer.get('lastName'), xPlayer.get('firstName'))
    local checkListItem = nil

    if checkListIndex > 0 then
        checkListItem = checkPrisonerList[checkListIndex]

        if checkListItem.fullName == fullName then
            table.remove(checkPrisonerList, checkListIndex)
            ESX.SetTimeout(1000, function()
                ReduceTime(_source)
            end)
        else
            local notification = GetDefaultNotification()
            notification.subject = _U('tasks')
            notification.msg = _U('check_task_wrong_name', checkListItem.fullName)
            notification.picture = 'CHAR_STEVE'
            TriggerClientEvent('esx:showAdvancedNotification', _source, notification.title, notification.subject,
                notification.msg, notification.picture, notification.iconType, false, true, 6)
        end
    end
end)

RegisterNetEvent('lp_jail:setPrisonersPed')
AddEventHandler('lp_jail:setPrisonersPed', function(peds)
    prisonersPed = peds
    for i, ped in ipairs(prisonersPed) do
        table.insert(checkPrisonerList, {
            fullName = GetRandomFullName(),
            ped = ped
        })
    end
end)

RegisterNetEvent('lp_jail:setGuard')
AddEventHandler('lp_jail:setGuard', function(ped)
    guardPed = ped
end)

function GetNextTask()
    local foundTask = false
    local nextTaskIndex = 1
    local currentTaskIndex = 1
    local max = GetTableSize(Config.Tasks)
    local newTask = nil
    while not foundTask do
        currentTaskIndex = 0
        Citizen.Wait(0)
        nextTaskIndex = math.random(1, max)
        for i, task in pairs(Config.Tasks) do
            if currentTaskIndex == nextTaskIndex then
                if task.Type ~= lastTask then
                    newTask = task.Type
                    foundTask = true
                end
                break
            end
            currentTaskIndex = currentTaskIndex + 1
        end
    end
    return newTask
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1000)
        Citizen.CreateThread(function()
            for playerId, data in pairs(playersInJail) do
                if data.jailed then
                    if playersInJail[playerId].remainingJailTime > 0 then
                        playersInJail[playerId].remainingJailTime = data.remainingJailTime - 1
                    end
                    TriggerClientEvent('lp_jail:syncJailTime', playerId, data.remainingJailTime)
                elseif data.escaped then
                    if policeInServiceCount > 0 then
                        if data.remainingEscapeTime < 1 then
                            unjailPlayer(playerId)
                        else
                            playersInJail[playerId].remainingEscapeTime = data.remainingEscapeTime - 1
                            TriggerClientEvent('lp_jail:syncEscapeTime', playerId, data.remainingEscapeTime)
                        end
                    end
                end
            end
        end)
    end
end)

function SaveJailTime(playerId, cb)
    local data = playersInJail[playerId]
    MySQL.Async.execute(
        'UPDATE lp_jail SET isPrisoner = @isPrisoner, remainingJailTime = @jailTime, remainingEscapeTime = @escapeTime WHERE identifier = @identifier',
        {
            ['@identifier'] = data.identifier,
            ['isPrisoner'] = data.isPrisoner,
            ['@jailTime'] = data.remainingJailTime,
            ['@escapeTime'] = data.remainingEscapeTime
        }, function(rowsChanged)
            if cb ~= nil then
                cb(rowsChanged)
            end
        end)
end

Citizen.CreateThread(function()
    local tasks = {}
    while true do
        Citizen.Wait(Config.JailTimeSyncInterval)
        tasks = {}
        for playerId, data in pairs(playersInJail) do
            local task = function(cb)
                SaveJailTime(playerId, cb)
            end
            table.insert(tasks, task)
        end
        Async.parallelLimit(tasks, 4, function(results)
        end)
    end
end)

ESX.RegisterServerCallback('lp_jail:getNotification', function(source, cb)
    cb(GetDefaultNotification())
end)

Citizen.CreateThread(function()
    local minutes = 0
    local taskNotification = nil
    local hasPlayerInJail = false
    Citizen.Wait(10000)
    while true do
        hasPlayerInJail = false
        currentTask = GetNextTask()
        lastTask = currentTask
        taskNotification = GetNewTaskNotification(currentTask)
        for playerId, data in pairs(playersInJail) do
            if playerId and data.jailed then
                hasPlayerInJail = true
                TriggerClientEvent('lp_jail:setTask', playerId, currentTask)
                TriggerClientEvent('esx:showAdvancedNotification', playerId, taskNotification.title,
                    taskNotification.subject, taskNotification.msg, taskNotification.picture, taskNotification.iconType,
                    false, true)
            end
        end
        if hasPlayerInJail then
            if currentTask == 'Check' then
                ESX.SetTimeout(60000, function()
                    local notification = GetDefaultNotification()
                    notification.subject = _U('task')
                    notification.msg = _U('check_task_start')
                    notification.picture = 'CHAR_STEVE'
                    ESX.SetTimeout(10000, function()
                        startCheck = true
                    end)
                    for i, prisoner in ipairs(checkPrisonerList) do
                        if prisoner.playerId ~= nil then
                            TriggerClientEvent('esx:showAdvancedNotification', prisoner.playerId, notification.title,
                                notification.subject, notification.msg, notification.picture, notification.iconType,
                                false, true)
                        end
                    end
                end)
                if not prisonnierPedSpawned then
                    for playerId, data in pairs(playersInJail) do
                        if playerId and data.jailed then
                            TriggerClientEvent('lp_jail:spawnPrisoners', playerId, Config.Tasks.Check.Locations[1])
                            TriggerClientEvent('lp_jail:spawnGuard', playerId)
                            prisonnierPedSpawned = true
                        end
                        break
                    end
                end
            end
        end
        while minutes < Config.TaskWaitTime do
            Citizen.Wait(60000)
            minutes = minutes + 1
        end
        minutes = 0
        if currentTask ~= nil then
            currentTask = nil
            for playerId, data in pairs(playersInJail) do
                if playerId and data.jailed then
                    TriggerClientEvent('lp_jail:cancelTask', playerId, nil)
                end
            end
        end
    end
end)

function PedHandsUp(ped)
    if #playersInJail > 0 then
        for playerId, data in pairs(playersInJail) do
            if playerId and data.jailed then
                TriggerClientEvent('lp_jail:pedHandsUp', playerId, ped)
                break
            end
        end
    else
        TriggerClientEvent('lp_jail:pedHandsUp', -1, ped) -- If none in jail, trigger for everyone ?
    end
end

function FreePed(ped)
    if #playersInJail > 0 then
        for playerId, data in pairs(playersInJail) do
            if playerId and data.jailed then
                TriggerClientEvent('lp_jail:freePed', playerId, ped)
                break
            end
        end
    else
        TriggerClientEvent('lp_jail:freePed', -1, ped) -- If none in jail, trigger for everyone ?
    end
end

function GetNextPrisoner()
    if #checkPrisonerList > 1 then
        checkListIndex = math.random(1, #checkPrisonerList)
    else
        checkListIndex = 1
    end
    local prisoner = checkPrisonerList[checkListIndex]
    if prisoner ~= nil then
        return prisoner.ped, prisoner.fullName
    else
        return nil, nil
    end
end

Citizen.CreateThread(function()
    local ped, fullName = nil, nil
    local notification = nil
    while true do
        if startCheck then
            ped, fullName = GetNextPrisoner()
            if fullName ~= nil then
                notification = GetDefaultNotification()
                notification.subject = _U('task')
                notification.msg = _U('check_task_name', fullName)
                notification.picture = 'CHAR_STEVE'
                for i, prisoner in ipairs(checkPrisonerList) do
                    if prisoner.playerId ~= nil then
                        TriggerClientEvent('esx:showAdvancedNotification', prisoner.playerId, notification.title,
                            notification.subject, notification.msg, notification.picture, notification.iconType, false,
                            true)
                    end
                end
                if ped ~= nil then
                    Citizen.Wait(1000)
                    PedHandsUp(ped)
                    Citizen.Wait(2000)
                    FreePed(ped)
                    Citizen.Wait(1000)
                    table.remove(checkPrisonerList, checkListIndex)
                else
                    Citizen.Wait(4000)
                end
            else
                if #checkPrisonerList == 0 then
                    prisonnierPedSpawned = false
                    FreePed(guardPed)
                    guardPed = nil
                    for playerId, data in pairs(playersInJail) do
                        if playerId and data.jailed then
                            TriggerClientEvent('lp_jail:cancelTask', playerId)
                        end
                    end
                    startCheck = false
                    currentTask = nil
                end
                Citizen.Wait(100)
            end
        else
            Citizen.Wait(100)
        end
    end
end)
