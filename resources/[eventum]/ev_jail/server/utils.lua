local lastFullName = nil

RegisterNetEvent('lp_jail:getPrisonerSkin')
AddEventHandler('lp_jail:getPrisonerSkin', function(sex, cb)
    if sex == 0 then
        cb(Config.Uniforms.prison_wear.male)
    else
        cb(Config.Uniforms.prison_wear.female)
    end
end)

function GetRandomFullName()
    local firstNames = {
        'Loïc',
        'Dorian',
        'Lukas',
        'Kevin',
        'William',
        'Sacha',
        'Morgan',
        'Bryan',
        'Killian',
        'Mateo',
        'Thibault',
        'Vincent',
        'Oscar',
        'Etienne',
        'Samuel',
        'Simon',
        'Gael',
        'Tiago',
        'Matthieu',
        'Robin',
        'Benjamin',
        'Arnaud',
        'Jordan',
        'Damien',
        'Edward',
        'Brandon',
        'Karim',
        'Kenneth',
        'John',
        'Jeffrey',
        'Robert',
        'Nathaniel',
        'Michael',
        'Gary',
        'Edward',
        'Jack',
        'William'
    }
    local lastNames = {
        'McDaniel',
        'White',
        'Mills',
        'Carter',
        'Racine',
        'Landry',
        'Rochon',
        'Rivard',
        'Oliveira',
        'Carvalho',
        'Gomes',
        'Laureano',
        'Arce',
        'Quiroz',
        'Galindo',
        'Quartararo',
        'Barrios',
        'Wright',
        'Tarr',
        'Arellano',
        'Garcia',
        'Howard',
        'Hopkins',
        'Maguire',
        'Jackson',
        'Mitchell',
        'Bernhard',
        'Cline',
        'Vogt',
        'Mishler',
        'Glaze',
        'Clark',
        'Keeton',
        'Reed',
        'O\'Neil',
        'Austin',
        'Bustamante',
        'Wallin',
        'Gentile',
        'Jenkins',
        'Bennett',
        'Simmons'
    }
    local fullName = nil
    while true do
        local lastName = lastNames[math.random(1, #lastNames)]
        local firstName = firstNames[math.random(1, #firstNames)]
        fullName = string.format("%s %s", lastName, firstName)
        Citizen.Wait(0)
        if fullName ~= lastFullName then
            break
        end
    end
    lastFullName = fullName
    return fullName
end

function GetTableSize(table)
    local count = 0
    for n in pairs(table) do
        count = count + 1
    end
    return count
end

function GetDefaultNotification()
    return {
        title = _U('penitentiary_name'),
        subject = nil,
        msg = nil,
        picture = 'CHAR_CALL911',
        iconType = 1
    }
end

function GetNewTaskNotification(task)
    local notification = GetDefaultNotification()
    notification.subject = _U('task')
    notification.msg = _U('task_' .. string.lower(task))
    return notification
end

function GetJailedTime(identifier, cb)
    MySQL.Async.fetchAll('SELECT jailTime FROM lp_jail WHERE identifier = @identifier', {
        ['@identifier'] = identifier
    }, function(result)
        if result[1] then
            cb(result[1].jailTime)
        end
    end)
end