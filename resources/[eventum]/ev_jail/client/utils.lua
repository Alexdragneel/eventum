local Blips = {}
local ActivityBlips = {}
local props = {}

RegisterNetEvent('lp_jail:freePed')
AddEventHandler('lp_jail:freePed', function(ped)
    SetEntityInvincible(ped, false)
    ClearPedTasks(ped)
    ClearPedSecondaryTask(ped)
    SetEntityAsNoLongerNeeded(ped)
    SetBlockingOfNonTemporaryEvents(ped, false)
end)

RegisterNetEvent('lp_jail:spawnPrisoners')
AddEventHandler('lp_jail:spawnPrisoners', function(location)
    SpawnPrisoners(location)
end)

RegisterNetEvent('lp_jail:pedHandsUp')
AddEventHandler('lp_jail:pedHandsUp', function(ped)
    ESX.Streaming.RequestAnimDict('random@mugging3', function()
        TaskPlayAnim(ped, 'random@mugging3', 'handsup_standing_base', 8.0, -8, -1, 49, 0, 0, 0, 0)
        RemoveAnimDict('random@mugging3')
    end)
end)

RegisterNetEvent('lp_jail:spawnGuard')
AddEventHandler('lp_jail:spawnGuard', function()
    SpawnGuard()
end)

RegisterNetEvent('lp_jail:revive')
AddEventHandler('lp_jail:revive', function()
    local infirmary = Config.Infirmary
    SetEntityCoordsNoOffset(PlayerPedId(), infirmary.x, infirmary.y, infirmary.z, false, false, false)
end)

RegisterNetEvent('lp_jail:getPrisonerSkin')
AddEventHandler('lp_jail:getPrisonerSkin', function(sex, cb)
    if sex == 0 then
        cb(Config.Uniforms.prison_wear.male)
    else
        cb(Config.Uniforms.prison_wear.female)
    end
end)

function drawMarker(location, size, type, rot, dir, color)
    if color == nil then
        color = {
            r = 255,
            g = 0,
            b = 168,
            a = 100
        }
    end
    if rot == nil then
        rot = vector3(0.0, 0.0, 0.0)
    end
    if dir == nil then
        dir = vector3(0.0, 0.0, 0.0)
    end
    DrawMarker(type or 1, location, rot.x, rot.y, rot.z, dir.x, dir.y, dir.z, size or vector3(1.5, 1.5, 0.5), color.r,
        color.g, color.b, color.a, false, true, 2, false, nil, nil, false)
end

function drawActivityBlip(taskType)
    removeActivityBlips()
    local blip = nil
    for i, task in pairs(Config.Tasks) do
        if task.Type == taskType then
            for j, location in ipairs(task.Locations) do
                if location.showBlip then
                    blip = AddBlipForCoord(location.xyz)
                    SetBlipDisplay(blip, 4)
                    SetBlipColour(blip, 3)
                    SetBlipCategory(blip, 3)
                    SetBlipAsShortRange(blip, true)
                    BeginTextCommandSetBlipName('STRING')
                    AddTextComponentSubstringPlayerName(_U('blip_' .. string.lower(taskType)))
                    EndTextCommandSetBlipName(blip)
                    table.insert(ActivityBlips, blip)
                end
            end
        end
    end
end

function removeActivityBlips()
    for index, blip in pairs(ActivityBlips) do
        while DoesBlipExist(blip) do
            RemoveBlip(blip)
            Citizen.Wait(0)
        end
    end
    ActivityBlips = {}
end

function DrawBlips()
    local blip = nil
    for k, v in pairs(Config.Blips) do
        if CheckRestrictions(v.Restrictions) then
            blip = AddBlipForCoord(v.xyz)
            SetBlipSprite(blip, v.Sprite)
            SetBlipDisplay(blip, 4)
            SetBlipScale(blip, v.Scale or 1.0)
            SetBlipColour(blip, v.Color or 6)
            SetBlipCategory(blip, 3)
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName('STRING')
            AddTextComponentSubstringPlayerName(_U(v.Name))
            EndTextCommandSetBlipName(blip)
            table.insert(Blips, blip)
        end
    end
end

function RemoveBlips()
    for index, blip in pairs(Blips) do
        while DoesBlipExist(blip) do
            RemoveBlip(blip)
            Citizen.Wait(0)
        end
    end
    Blips = {}
end

function FastTravel(coords, heading)
    local playerPed = PlayerPedId()

    DoScreenFadeOut(800)

    while not IsScreenFadedOut() do
        Citizen.Wait(500)
    end

    ESX.Game.Teleport(playerPed, coords, function()
        DoScreenFadeIn(800)
        if heading then
            SetEntityHeading(playerPed, heading)
        end
    end)
end

function PlayerInJailArea(playerPed)
    local coords = GetEntityCoords(playerPed)
    local v = math.abs(coords - Config.JailCenter)
    return #v.xy < 151.0 and v.z < 701.0
end

function IsPoliceJob(job)
    if job.name == 'police' then
        return true
    end
    return false
end

function PlayAnim(anim)
    RequestAnimDict(anim.lib)
    while not HasAnimDictLoaded(anim.lib) do
        Citizen.Wait(0)
    end
    TaskPlayAnim(PlayerPedId(), anim.lib, anim.anim, 8.0, 8.0, -1, 1, 1000, false, false, false)
    if anim.props ~= nil then
        local playerPed = PlayerPedId()
        for i, prop in ipairs(anim.props) do
            local coords = GetOffsetFromEntityInWorldCoords(playerPed, prop.x, prop.y, prop.z)
            ESX.Game.SpawnObject(prop.model, {
                x = 0,
                y = 0,
                z = 0
            }, function(object)
                local netId = ObjToNet(object)
                SetNetworkIdExistsOnAllMachines(netId, true)
                NetworkSetNetworkIdDynamic(netId, true)
                SetNetworkIdCanMigrate(netId, false)
                if prop.bone ~= nil then
                    local boneIndex = GetPedBoneIndex(playerPed, prop.bone)
                    AttachEntityToEntity(object, playerPed, boneIndex, prop.x, prop.y, prop.z, prop.xRot, prop.yRot,
                        prop.zRot, false, false, false, false, 2, true)
                else
                    SetEntityCoords(object, coords.x, coords.y, coords.z, false, false, false, false)
                    PlaceObjectOnGroundProperly(object)
                end
                table.insert(props, {
                    object = object,
                    model = prop.model,
                    netId = netId
                })
            end)
        end
    end
end

function isPedPlayingAnim(anim)
    RequestAnimDict(anim.lib)
    while not HasAnimDictLoaded(anim.lib) do
        Citizen.Wait(0)
    end
    return IsEntityPlayingAnim(PlayerPedId(), anim.lib, anim.anim, 3)
end

function GetRandomIdleAnim(task)
    local idleTasksLen = #task.Anim.Idle
    local randomNumber = 1
    if idleTasksLen > 1 then
        math.randomseed(GetGameTimer())
        randomNumber = math.random(1, idleTasksLen)
    end
    return task.Anim.Idle[randomNumber]
end

function RemovePropsFromWorld()
    local position = GetEntityCoords(PlayerPedId())
    for i, prop in pairs(props) do
        DetachEntity(NetToObj(prop.netId), 1, 1)
        DeleteEntity(NetToObj(prop.netId))
    end
    props = {}
end

function GetPeds(models)
    local peds = ESX.Game.GetPeds(true)
    local playerPed = PlayerPedId()
    local coords = GetEntityCoords(playerPed)
    local filteredEntities = {}

    for k, entity in pairs(peds) do
        local pedModel = GetEntityModel(entity)
        local distance = #(coords - GetEntityCoords(entity))
        if distance < 300.0 then
            for i, model in ipairs(models) do
                if pedModel == model then
                    table.insert(filteredEntities, entity)
                    break
                end
            end
        end
    end
    return filteredEntities
end

function SpawnPrisoners(location)
    local models = {'s_m_y_prismuscl_01', 's_m_y_prisoner_01'}
    local newLocation = location
    local prisoners = {}
    local pedsInArea = GetPeds(models)
    if #pedsInArea >= 5 then
        for i = 1, 5, 1 do
            local ped = pedsInArea[i]
            table.insert(prisoners, ped)
        end
    else
        for i, model in ipairs(models) do
            local hash = GetHashKey(model)
            RequestModel(hash)
            while not HasModelLoaded(hash) do
                Citizen.Wait(1)
            end
        end
        for i = 1, 5, 1 do
            local spawnLocation = vector3(location.xyz.x + (math.random(3, 5) + 0.0),
                location.xyz.y + (math.random(3, 5) + 0.0), location.xyz.z)
            local npc = CreatePed(4, models[math.random(1, 2)], spawnLocation.xyz.x, spawnLocation.xyz.y,
                spawnLocation.xyz.z, location.handling, true, true)
            while DoesEntityExist(npc) == false do
                Citizen.Wait(0)
            end
            SetEntityAsMissionEntity(npc, true, false)
            table.insert(prisoners, npc)
            Citizen.Wait(100)
        end
    end
    TriggerServerEvent('lp_jail:setPrisonersPed', prisoners)
    for i, prisoner in ipairs(prisoners) do
        newLocation = GetLocationDeviation(location.xyz)
        TaskGoStraightToCoord(prisoner, newLocation.x, newLocation.y, newLocation.z, -1, -1, location.handling)
        SetBlockingOfNonTemporaryEvents(prisoner, true)
        Citizen.Wait(100)
    end
end

function SpawnGuard()
    local models = {'s_m_m_prisguard_01'}
    local from = Config.GuardLocations.From
    local to = Config.GuardLocations.To
    local guard = nil
    local pedsInArea = GetPeds(models)
    if #pedsInArea >= 1 then
        guard = pedsInArea[1]
    else
        for i, model in ipairs(models) do
            local hash = GetHashKey(model)
            RequestModel(hash)
            while not HasModelLoaded(hash) do
                Citizen.Wait(0)
            end
        end
        guard = CreatePed(4, models[1], from.xyz.x, from.xyz.y, from.xyz.z, from.handling, true, true)
        while DoesEntityExist(guard) == false do
            Citizen.Wait(0)
        end
        SetEntityAsMissionEntity(guard, true, false)
    end
    SetEntityInvincible(guard, true)
    TaskGoStraightToCoord(guard, to.xyz.x, to.xyz.y, to.xyz.z, -1, -1, to.handling)
    TriggerServerEvent('lp_jail:setGuard', guard)
    while true do
        if #(GetEntityCoords(guard) - to.xyz) < 1.0 then
            break
        end
        Citizen.Wait(1000)
    end
    Citizen.Wait(1000)
    TaskStartScenarioInPlace(guard, 'WORLD_HUMAN_CLIPBOARD', 0, false)
    SetBlockingOfNonTemporaryEvents(guard, true)
end

function GetLocationDeviation(location)
    return vector3(location.xyz.x + ((math.random(-25, 25) / 10) + 0.0),
        location.xyz.y + ((math.random(-25, 25) / 10) + 0.0), location.xyz.z)
end
