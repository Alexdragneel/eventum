local prisonerData = {
    isPrisoner = false,
    remainingJailTime = 0,
    remainingEscapeTime = 0,
    jailed = false,
    escaped = false
}
local isAreaViolated = false -- Violation de périmètre
local isInService = false
local Task = {
    current = {
        type = nil,
        step = 1,
        lock = false
    },
    next = nil,
    anim = nil
}
local hasHandsUp, wasHandsUp = false, false
ESX = nil

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end
    while ESX.GetPlayerData().job == nil do
        Citizen.Wait(10)
    end
    ESX.PlayerData = ESX.GetPlayerData()
    DrawBlips()
end)

AddEventHandler('skinchanger:modelLoaded', function()
    Citizen.CreateThread(function()
        CheckEscape()
    end)
    if prisonerData.isPrisoner then
        SetPrisonerSkin()
    end
    if prisonerData.remainingJailTime > 0 then
        if PlayerInJailArea(PlayerPedId()) then
            TriggerServerEvent('lp_jail:prisonerWakeUp')
        end
    end
end)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job)
    if ESX.PlayerData.job.name ~= job.name then
        ESX.TriggerServerCallback('esx_service:isInService', function(inService)
            ESX.PlayerData.job.name = job.name
            isInService = inService
            RemoveBlips()
            DrawBlips()
        end, job.name)
    end
end)

RegisterNetEvent('esx_service:disableService')
AddEventHandler('esx_service:disableService', function()
    TriggerServerEvent('lp_jail:disableService', ESX.PlayerData.job.name)
    isInService = false
    RemoveBlips()
    DrawBlips()
end)

RegisterNetEvent('esx_service:enableService')
AddEventHandler('esx_service:enableService', function()
    TriggerServerEvent('lp_jail:enableService', ESX.PlayerData.job.name)
    isInService = true
    RemoveBlips()
    DrawBlips()
end)

RegisterNetEvent('lp_jail:setPrisonerData')
AddEventHandler('lp_jail:setPrisonerData', function(playerData)
    prisonerData = playerData
end)

RegisterNetEvent('lp_jail:syncJailTime')
AddEventHandler('lp_jail:syncJailTime', function(timeRemaining)
    prisonerData.remainingJailTime = timeRemaining
end)

RegisterNetEvent('lp_jail:syncEscapeTime')
AddEventHandler('lp_jail:syncEscapeTime', function(timeRemaining)
    prisonerData.remainingEscapeTime = timeRemaining
end)

RegisterNetEvent('lp_jail:jailPlayer')
AddEventHandler('lp_jail:jailPlayer', function(jailTime, needTeleport)
    prisonerData.remainingJailTime = jailTime

    SetPrisonerSkin()

    if needTeleport == true then
        ESX.Game.Teleport(PlayerPedId(), Config.JailLocation)
    end
    prisonerData = {
        isPrisoner = true,
        remainingJailTime = jailTime,
        remainingEscapeTime = 0,
        jailed = true,
        escaped = false
    }
    RemoveBlips()
    DrawBlips()
    TriggerServerEvent('esx_service:disableService', ESX.PlayerData.job.name)
end)

function SetPrisonerSkin()
    ESX.TriggerServerCallback("esx_skin:getPlayerSkin", function(skin, jobSkin)
        if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, Config.Uniforms.prison_wear.male)
        else
            TriggerEvent('skinchanger:loadClothes', skin, Config.Uniforms.prison_wear.female)
        end
    end)
    SetPedArmour(PlayerPedId(), 0)
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local playerCoords, canFastTravel = GetEntityCoords(PlayerPedId()), false
        -- Fast Travels
        for k, v in ipairs(Config.FastTravels) do
            local distance = #(playerCoords - v.From)
            if CheckRestrictions(v.Restrictions) then
                if distance < Config.DrawDistance then
                    drawMarker(v.From)
                    if distance < v.Marker.x then
                        if v.Prompt ~= nil then
                            ESX.ShowHelpNotification(_U(v.Prompt))
                            if IsControlJustReleased(0, 38) then
                                canFastTravel = true
                            end
                        else
                            canFastTravel = true
                        end
                        if canFastTravel then
                            FastTravel(v.To.coords, v.To.heading)
                            canFastTravel = false
                            if v.Action ~= nil then
                                if v.Action.type == 'client' then
                                    TriggerEvent(v.Action.event)
                                else
                                    TriggerServerEvent(v.Action.event, GetPlayerServerId(PlayerId()))
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end)

function TimedTask(task, distance, renderDistance, promptDistance, location)
    local step = Task.current.step or 1
    if distance <= renderDistance then
        if task.Marker then
            drawMarker(location.xyz, task.Marker.size)
        else
            drawMarker(location.xyz)
        end
        if distance <= promptDistance then
            if task.Prompt ~= nil then
                ESX.ShowHelpNotification('~INPUT_CONTEXT~ ' .. _U(task.Prompt))
                if IsControlJustReleased(0, 38) then
                    if location.handling ~= nil then
                        TaskGoStraightToCoord(PlayerPedId(), location.xyz.x, location.xyz.y, location.xyz.z, -1, -1,
                            location.handling)
                        Citizen.Wait(2500)
                    end
                    if task.Anim ~= nil then
                        PlayAnim(task.Anim.Base)
                        Citizen.Wait(100)
                        Task.anim = task.Anim.Base
                    end
                    Task.current = task
                    Task.current.step = step
                    if Task.next ~= nil then
                        if Task.current.Type == Task.next then
                            TriggerServerEvent('lp_jail:startTask', Task.current)
                        end
                    end
                end
            end
        end
    end
end

Citizen.CreateThread(function()
    local playerCoords = nil
    local distance = 500
    local playerPed = PlayerPedId()
    local renderDistance = nil
    local promptDistance = nil
    while true do
        playerPed = PlayerPedId()
        playerCoords = GetEntityCoords(playerPed)
        Citizen.Wait(0)
        if Task.anim == nil and (prisonerData.jailed and not prisonerData.escaped) then
            for i, task in pairs(Config.Tasks) do
                for j, location in pairs(task.Locations) do
                    distance = #(playerCoords - location.xyz)
                    if task.Marker ~= nil then
                        renderDistance = 10 * task.Marker.size.x
                        promptDistance = task.Marker.size.x / 2
                    else
                        renderDistance = 100
                        promptDistance = 1.5
                    end
                    if task.Type == Task.next then
                        if task.Duration then
                            TimedTask(task, distance, renderDistance, promptDistance, location)
                        end
                    else
                        if task.Visibility == 'always' then
                            TimedTask(task, distance, renderDistance, promptDistance, location)
                        end
                    end
                end
            end
            for i, place in pairs(Config.Places) do
                for j, location in pairs(place.Locations) do
                    if place.Visibility == 'always' then
                        distance = #(playerCoords - location.xyz)
                        if distance < 10 then
                            drawMarker(location.xyz)
                            if distance < 1.5 then
                                if place.Prompt ~= nil then
                                    ESX.ShowHelpNotification('~INPUT_CONTEXT~ ' .. _U(place.Prompt))
                                    if IsControlJustReleased(0, 38) then
                                        if location.handling ~= nil then
                                            TaskGoStraightToCoord(playerPed, location.xyz.x, location.xyz.y,
                                                location.xyz.z, -1, -1, location.handling)
                                            Citizen.Wait(2500)
                                        end
                                        if place.Action ~= nil then
                                            if place.Action.type == 'client' then
                                                TriggerEvent(place.Action.event)
                                            else
                                                TriggerServerEvent(place.Action.event, GetPlayerServerId(PlayerId()))
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        else
            Citizen.Wait(100)
        end
    end
end)

function CheckEscape()
    local mugshot = nil
    local mugshotStr = nil
    local playerPed = nil
    while true do
        playerPed = PlayerPedId()
        if PlayerInJailArea(playerPed) then
            if not isAreaViolated and not prisonerData.jailed and IsEntityVisible(playerPed) then
                isAreaViolated = true
                TriggerServerEvent('lp_jail:areaPenetration')
            end
        else
            isAreaViolated = false
            if prisonerData.jailed and not prisonerData.escaped then
                prisonerData.escaped = true
                TriggerServerEvent('lp_jail:prisonerEscaped')
                ClearNextTask()
                StopCurrentTask()
                Task.current = {
                    Type = nil,
                    step = 1,
                    lock = false
                }
                removeActivityBlips()
                RemoveBlips()
                DrawBlips()
            end
        end
        Citizen.Wait(5000)
    end
end

Citizen.CreateThread(function()
    while true do
        Tasks()
        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local jailTime = prisonerData.remainingJailTime
        local escapeTime = prisonerData.remainingEscapeTime
        local text = ""
        local time = jailTime
        if prisonerData.escaped then
            time = escapeTime
        end
        local hours = math.floor(time / 3600)
        local minutes = math.floor((time - (3600 * hours)) / 60)
        local seconds = ESX.Math.Round(time % 60)
        if hours > 0 then
            text = text .. hours .. 'h '
        end
        if minutes < 10 then
            text = text .. '0' .. minutes .. 'min '
        else
            text = text .. minutes .. 'min '
        end
        if seconds < 10 then
            text = text .. '0' .. seconds .. 'sec'
        else
            text = text .. seconds .. 'sec'
        end
        if prisonerData.escaped then
            if time > 0 then
                TriggerEvent('lp_core:drawTextCenter', _U('remaining_escape_time', text), 0.5, 0.05)
            end
        else
            if time > 0 then
                TriggerEvent('lp_core:drawTextCenter', _U('remaining_msg', text), 0.5, 0.05)
            else
                if prisonerData.jailed then
                    TriggerEvent('lp_core:drawTextCenter', _U('can_exit_jail'), 0.5, 0.05)
                end
            end
        end
    end
end)

RegisterNetEvent('lp_jail:unjailPlayer')
AddEventHandler('lp_jail:unjailPlayer', function()
    local wasJailed = prisonerData.jailed
    prisonerData = {
        isPrisoner = false,
        remainingJailTime = 0,
        remainingEscapeTime = 0,
        jailed = wasJailed,
        escaped = false
    }
    RemoveBlips()
    DrawBlips()
end)

RegisterNetEvent('lp_jail:exit')
AddEventHandler('lp_jail:exit', function()
    ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
        TriggerEvent('skinchanger:loadSkin', skin)
        prisonerData = {
            isPrisoner = false,
            remainingJailTime = 0,
            remainingEscapeTime = 0,
            jailed = false,
            escaped = false
        }
        ClearNextTask()
        StopCurrentTask()
        Task.current = {
            Type = nil,
            step = 1,
            lock = false
        }
        removeActivityBlips()
        RemoveBlips()
        DrawBlips()
    end)
end)

Citizen.CreateThread(function()
    while Config.Debug do
        Citizen.Wait(0)
        drawMarker(Config.JailCenter, vector3(300.0, 300.0, 700.0))
    end
end)

function Tasks()
    if Task.next == 'Race' then
        RaceTask()
    elseif Task.next == 'Check' then
        CheckTask()
    end
    if Task.anim ~= nil then
        if isPedPlayingAnim(Task.anim) == false then
            RemoveAnimDict(Task.anim.lib)
            RemovePropsFromWorld()
            StopCurrentTask()
            Task.anim = nil
        end
    end
end

function RaceTask()
    local currentStep = Task.current.step or 1
    local currentStepLocation = Config.Tasks.Race.Locations[currentStep]
    local markerType = 2
    local playerPed = PlayerPedId()
    local distance = #(GetEntityCoords(playerPed) - currentStepLocation.xyz)
    local totalSteps = #Config.Tasks.Race.Locations
    if currentStep == 1 then
        markerType = 4
    elseif currentStep == totalSteps then
        markerType = 4
    end
    if distance < 2.0 and Task.current.lock == false then
        if currentStep == 1 then
            ESX.ShowHelpNotification('~INPUT_CONTEXT~ ' .. _U('start_running'))
            if IsControlJustReleased(0, 38) then
                Task.current = Config.Tasks.Race
                Task.current.step = currentStep
                Task.current.lock = true
                TriggerServerEvent('lp_jail:updateTask', Task.current)
                if currentStepLocation.handling ~= nil then
                    TaskGoStraightToCoord(playerPed, currentStepLocation.xyz.x, currentStepLocation.xyz.y,
                        currentStepLocation.xyz.z, -1, -1, currentStepLocation.handling)
                    Citizen.Wait(2500)
                end
                ESX.TriggerServerCallback('lp_jail:getNotification', function(notification)
                    notification.subject = _U('task')
                    notification.picture = 'CHAR_STEVE'
                    notification.msg = _U('run_task_guard_1')
                    ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                        notification.picture, notification.iconType, false, true)
                end)
            end
        else
            Task.current.lock = true
            TriggerServerEvent('lp_jail:updateTask', Task.current)
            if currentStep == totalSteps then
                ESX.TriggerServerCallback('lp_jail:getNotification', function(notification)
                    notification.subject = _U('task')
                    notification.picture = 'CHAR_STEVE'
                    notification.msg = _U('run_task_guard_9')
                    ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                        notification.picture, notification.iconType, false, true)
                end)
            elseif currentStep == (totalSteps - 1) then
                ESX.TriggerServerCallback('lp_jail:getNotification', function(notification)
                    notification.subject = _U('task')
                    notification.picture = 'CHAR_STEVE'
                    notification.msg = _U('run_task_guard_8')
                    ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                        notification.picture, notification.iconType, false, true)
                end)
            else
                ESX.TriggerServerCallback('lp_jail:getNotification', function(notification)
                    notification.subject = _U('task')
                    notification.picture = 'CHAR_STEVE'
                    notification.msg = _U('run_task_guard_' .. math.random(2, 7))
                    ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                        notification.picture, notification.iconType, false, true)
                end)
            end
        end
    end
    drawMarker(currentStepLocation.xyz, vector3(2.0, 1.0, 2.0), markerType)
end

function CheckTask()
    local currentStep = Task.current.step
    local playerPed = PlayerPedId()
    local distance = #(GetEntityCoords(playerPed) - Config.Tasks.Check.Locations[1].xyz)
    if distance < 2.5 then
        if currentStep == 1 then
            ESX.ShowHelpNotification('~INPUT_CONTEXT~ ' .. _U('join_check'))
            if IsControlJustReleased(0, 38) then
                Task.current = Config.Tasks.Check
                Task.current.step = currentStep
                Task.current.lock = true
                TriggerServerEvent('lp_jail:startTask', Task.current)
            end
        else
            if not wasHandsUp and hasHandsUp then
                TriggerServerEvent('lp_jail:checkTaskHandsUp')
            end
        end
    end
    drawMarker(Config.Tasks.Check.Locations[1].xyz, vector3(5.0, 5.0, 0.5))
    wasHandsUp = hasHandsUp
end

function StopCurrentTask()
    if Task.current.Type == Task.next then
        TriggerServerEvent('lp_jail:stopTask', Task.current.Type)
    end
    local step = Task.current.step
    Task.current = {
        Type = nil,
        lock = false,
        step = step
    }
    ClearPedTasks(PlayerPedId())
end

function ClearNextTask()
    Task.next = nil
end

RegisterNetEvent('lp_jail:setTask')
AddEventHandler('lp_jail:setTask', function(taskType)
    Task.next = taskType
    if taskType ~= nil then
        drawActivityBlip(taskType)
    else
        removeActivityBlips()
    end
end)

RegisterNetEvent('lp_jail:taskStop')
AddEventHandler('lp_jail:taskStop', function(task)
    if task.Type == Task.current.Type then
        StopCurrentTask()
    end
end)

RegisterNetEvent('lp_jail:updateTask')
AddEventHandler('lp_jail:updateTask', function(task)
    if task.Type == Task.current.Type then
        Task.current = task
    end
end)

RegisterNetEvent('lp_jail:taskDone')
AddEventHandler('lp_jail:taskDone', function(task)
    if Task.current.Type == task then
        ClearNextTask()
        StopCurrentTask()
        Task.current = {
            Type = nil,
            step = 1,
            lock = false
        }
        removeActivityBlips()
    end
end)

RegisterNetEvent('lp_jail:cancelTask')
AddEventHandler('lp_jail:cancelTask', function()
    if Task.next ~= nil then
        ClearNextTask()
        StopCurrentTask()
        Task.current = {
            Type = nil,
            step = 1,
            lock = false
        }
        ESX.TriggerServerCallback('lp_jail:getNotification', function(notification)
            notification.subject = _U('task')
            notification.msg = _U('end_task')
            ESX.ShowAdvancedNotification(notification.title, notification.subject, notification.msg,
                notification.picture, notification.iconType, false, true, 6)
        end)
        removeActivityBlips()
    end
end)

AddEventHandler('lp_core:handsUp', function(handsUp)
    hasHandsUp = handsUp
end)

AddEventHandler('onResourceStop', function(resourceName)
    if GetCurrentResourceName() == resourceName then
        RemoveBlips()
    end
end)

function CheckRestrictions(restrictions)
    local isValid = false
    if restrictions ~= nil then
        if restrictions.job ~= nil then
            if ESX.PlayerData.job ~= nil then
                if ESX.PlayerData.job.name == restrictions.job then
                    if restrictions.inService ~= nil then
                        isValid = isInService == restrictions.inService
                    end
                end
            end
        elseif restrictions.prisoner ~= nil then
            if not prisonerData.escaped then
                if restrictions.prisoner then
                    isValid = (prisonerData.remainingJailTime > 0)
                else
                    isValid = (prisonerData.remainingJailTime == 0)
                end
            end
        end
    else
        isValid = true
    end
    return isValid
end
