local mode = "whitelist"
if Config.method == "SQL" then
    MySQL.ready(function()
        AddEventHandler('playerConnecting', function(name, setReason, deferrals)
            local _source = source
            local discordId = nil
            local steamId = nil
            local found = false
            local whitelisted = {}

            local identifiers = GetPlayerIdentifiers(_source)
            deferrals.defer()
            Citizen.Wait(0)
            deferrals.update(string.format("Hello %s. Your IDs are being checked.", name))

            for k, v in pairs(identifiers) do
                if string.sub(v, 1, string.len("discord:")) == "discord:" then
                    discordId = v
                end
            end

            if discordId ~= nil then
                whitelisted = MySQL.Sync.fetchAll('SELECT * FROM `user_whitelist` WHERE `identifiers`  LIKE "%' ..
                                                      discordId .. '%"')
            end

            if Config.logging == 1 then
                for i in ipairs(identifiers) do
                    print('Player: ' .. name .. ', Identifier #' .. i .. ': ' .. identifiers[i])
                end
            end

            if #whitelisted > 0 then
                for k, id in pairs(identifiers) do
                    if Config.logging == 1 then
                        print('Checking ' .. mode .. ' entry for ' .. id)
                    end
                    local isFound, isWhitelisted = inArray(id, whitelisted)
                    if isFound == true then
                        if isWhitelisted == true then
                            found = true
                            deferrals.done()
                            if Config.logging == 1 or Config.logging == 2 then
                                print(id .. ' found in ' .. mode .. ' entry, accepting...')
                            end
                        end
                        break
                    end
                end

                if found == false then
                    setReason(Config.message)
                    deferrals.done(Config.message)
                    CancelEvent()
                    if Config.logging == 1 or Config.logging == 2 then
                        print('IDs not found in ' .. mode .. ' entry, cancelling...')
                    end
                end
            else
                if discordId == nil then
                    setReason("Not connected to discord!")
                    deferrals.done("Not connected to discord!")
                    print(name .. ' not connected to discord, cancelling...')
                    CancelEvent()
                else
                    setReason(Config.message)
                    deferrals.done(Config.message)
                    CancelEvent()
                    if Config.logging == 1 or Config.logging == 2 then
                        print('IDs not found in ' .. mode .. ' entry, cancelling...')
                    end
                end
            end
        end)

        -- Returns TRUE if value is in array, FALSE otherwise
        function inArray(value, table)
            for _, wl in pairs(table) do
                local ids = json.decode(wl.identifiers)
                if ids ~= nil then
                    for _, id in pairs(ids) do
                        if id == value then
                            return true, wl.whitelisted == 1
                        end
                    end
                end
            end
        end
    end)

elseif method == "Standard" then

    AddEventHandler('playerConnecting', function(name, setReason)
        local _source = source
        local steamId = GetPlayerIdentifiers(_source)[1]
        if Config.logging == 1 then
            for i in ipairs(GetPlayerIdentifiers(_source)) do
                print('Player: ' .. name .. ', Identifier #' .. i .. ': ' .. GetPlayerIdentifiers(_source)[i])
            end
        end
        if Config.logging == 1 or Config.logging == 2 then
            print('Checking ' .. mode .. ' entry for ' .. steamId)
        end
        if string.find(GetPlayerIdentifiers(_source)[1], "steam") then
            local check = inArray(steamId, Config.auths)

            if mode ~= 'whitelist' then
                check = not check
            end

            if not check then
                setReason(config.message)
                CancelEvent()
                if Config.logging == 1 or Config.logging == 2 then
                    print(steamId .. " not found in database. Kicking...")
                end
            end
        else
            setReason('You must have steam open!')
            CancelEvent()
            if Config.logging == 1 or Config.logging == 2 then
                print('Steam not activated for ' .. name)
            end
        end
    end)

    -- Returns TRUE if value is in array, FALSE otherwise
    function inArray(value, array)
        for _, v in pairs(array) do
            v = getSteamId(v)
            if v == value then
                return true
            end
        end
        return false
    end

end

-- Returns TRUE if steamId start with "discord:", FALSE otherwise
function isNativeDiscordId(discordId)
    if string.sub(discordId, 0, string.len("discord:")) == "discord:" then
        return true
    end
    return false
end

function getDiscordId(discordId)
    print("getDiscordId discordId", discordId)
    if not isNativeDiscordId(discordId) then -- FiveM discordId conversion
        discordId = "discord:" .. string.format("%x", tonumber(discordId))
    else
        discordId = string.lower(discordId) -- Lowercase conversion
    end
    return discordId
end

-- Returns TRUE if steamId start with "steam:", FALSE otherwise
function isNativeSteamId(steamId)
    if string.sub(steamId, 0, 6) == "steam:" then
        return true
    end
    return false
end

function getSteamId(steamId)
    if not isNativeSteamId(steamId) then -- FiveM SteamID conversion
        steamId = "steam:" .. string.format("%x", tonumber(steamId))
    else
        steamId = string.lower(steamId) -- Lowercase conversion
    end
    return steamId
end
