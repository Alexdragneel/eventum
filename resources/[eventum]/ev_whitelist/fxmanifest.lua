fx_version 'adamant'

game 'gta5'

description 'Whitelist System'

version '1.0'

server_scripts {
    '@mysql-async/lib/MySQL.lua', 
    'versioncheck.lua', 
    'config.lua', 
    'server.lua'
}
