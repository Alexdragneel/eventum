ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end
	ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
end)

RegisterNetEvent('lp_core:newOwnedVehicle')
AddEventHandler('lp_core:newOwnedVehicle', function(vehicleName, plate)
    local model = (type(vehicleName) == 'number' and vehicleName or GetHashKey(vehicleName))

    if plate == nil then
        plate = exports["esx_vehicleshop"]:GeneratePlate()
    end

    ESX.TriggerServerCallback('lp_core:spawnOwnedVehicle', function(success)
        if success then
            if IsModelInCdimage(model) then
                local playerPed = PlayerPedId()
                local playerCoords, playerHeading = GetEntityCoords(playerPed), GetEntityHeading(playerPed)
        
                ESX.Game.SpawnVehicle(model, playerCoords, playerHeading, function(vehicle)
                    local vehicleClass = GetVehicleClass(vehicle)
					if vehicleClass == 15 then
						vehType = "helicopter"
					elseif vehicleClass == 14 then
						vehType = "boat"
					else
						vehType = "car"
					end
                    TriggerServerEvent('lp_core:updateOwnedVehicle', plate, vehType)
                    TriggerServerEvent('esx_fuel:SetFuelLevel', plate, -1)
                    TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
                    SetVehicleNumberPlateText(vehicle, plate)
                end)
            end
        end
    end, model, plate)
end)

-- Suppression de la regen auto, message en dessous de 20% de la vie et suppression de la capacité de courir
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		SetPlayerHealthRechargeMultiplier(PlayerId(), 0.0)
		local health = GetEntityHealth(GetPlayerPed(-1))-100
		if health <=20 then
			ESX.ShowNotification('Vous avez beau être matinal, vous avez mal.')
			TriggerEvent('lp_personalmenu:setCurrentAttitude', 'move_m@injured')
        	DisableControlAction(2, 21, true)
		end
	end
end)

RegisterNetEvent('lp_core:newBlipWithRadius')
AddEventHandler('lp_core:newBlipWithRadius', function (targetCoords, options)
	local alpha = 250
	local newBlip = AddBlipForRadius(targetCoords.x, targetCoords.y, targetCoords.z, options.radius or 64.0)

	SetBlipHighDetail(newBlip, true)
	SetBlipColour(newBlip, options.color or 1)
	SetBlipAlpha(newBlip, alpha)
	SetBlipAsShortRange(newBlip, true)

	while alpha ~= 0 do
		Citizen.Wait((options.duration or 30) * 4)
		alpha = alpha - 1
		SetBlipAlpha(newBlip, alpha)

		if alpha == 0 then
			RemoveBlip(newBlip)
			return
		end
	end
end)

function BillingLabelMenu()
	local target, distance = ESX.Game.GetClosestPlayer()
	if target ~= -1 and distance <= 3.0 then
		ESX.UI.Menu.Open(
			'dialog', GetCurrentResourceName(), 'billing_label',
			{
				title = _U('billing_label'),
				value = _U('billing')
			},
			function(data, menu)
				local label = tostring(data.value) or ''

				if label ~= '' then
					menu.close()
					BillingAmountMenu(target, label)
				else
					ESX.ShowNotification(_U('label_billing_invalid'))
				end
			end,
			function(data, menu)
				menu.close()
			end
		)
	else
		ESX.ShowNotification(_U('no_players_nearby'))
	end
end
RegisterNetEvent('lp_core:openBillingMenu')
AddEventHandler('lp_core:openBillingMenu', BillingLabelMenu)

function BillingAmountMenu(target, label)
	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'billing_amount',
		{
			title = _U('billing_amount')
		},
		function(data, menu)
			local amount = tonumber(data.value)
			menu.close()
			if amount == nil then
				ESX.ShowNotification(_U('amount_billing_invalid'))
			else
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(target), 'society_' .. ESX.PlayerData.job.name, label, amount)
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

local printText = false

function PrepareText()
	SetTextFont(4)
	SetTextProportional(0)
	SetTextScale(0.65,0.65)
	SetTextJustification(0)
	SetTextDropshadow(0, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
end
function DrawTextCenter(msg, x, y, time)
	if time then
		printText = true
		Citizen.CreateThread(function ()
			Citizen.Wait(time)
			printText = false
		end)
		while printText do
			Citizen.Wait(0)
			PrepareText()
			BeginTextCommandDisplayText('STRING')
			AddTextComponentSubstringPlayerName(msg)
			EndTextCommandDisplayText(x, y)
		end
	else
		PrepareText()
		BeginTextCommandDisplayText('STRING')
		AddTextComponentSubstringPlayerName(msg)
		EndTextCommandDisplayText(x, y)
	end
end
RegisterNetEvent('lp_core:drawTextCenter')
AddEventHandler('lp_core:drawTextCenter', DrawTextCenter)

function ClearTextCenter()
	printText = false
end
RegisterNetEvent('lp_core:clearTextCenter')
AddEventHandler('lp_core:clearTextCenter', ClearTextCenter)