Locales['fr'] = {
    ['command_mycar'] = 'spawn a vehicle that will belong to you',
    ['command_mycar_car'] = 'vehicle spawn name or hash',
    ['command_mycar_plate'] = 'vehicle custom plate (optional)',
    ['billing'] = 'facture',
    ['billing_label'] = 'intitulé de la facture',
    ['label_billing_invalid'] = 'intitulé invalide',
    ['billing_amount'] = 'montant de la facture',
    ['amount_billing_invalid'] = 'montant invalide',
    ['no_players_nearby'] = 'aucun joueur à proximité'
}
