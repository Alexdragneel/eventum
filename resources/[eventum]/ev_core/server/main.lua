-- Variables --
ESX = nil
-- --

-- Init ESX stuff --
TriggerEvent("esx:getSharedObject", function(obj)
    ESX = obj
end)
-- --

ESX.RegisterServerCallback('lp_core:getPlayerInventory', function(source, cb)
    local xPlayer = ESX.GetPlayerFromId(source)
    local items = xPlayer.inventory

    cb(items, ESX.GetItems())
end)

ESX.RegisterServerCallback('lp_core:getPlayerAccounts', function(source, cb)
    local xPlayer = ESX.GetPlayerFromId(source)
    local accounts = xPlayer.accounts

    cb(accounts)
end)

ESX.RegisterServerCallback('lp_core:getPlayerMaxWeight', function(source, cb)
    local xPlayer = ESX.GetPlayerFromId(source)
    local maxWeight = xPlayer.maxWeight

    cb(maxWeight)
end)

RegisterNetEvent('lp_core:GetVehicleByPlate')
AddEventHandler('lp_core:GetVehicleByPlate', function(plate, cb)
    GetVehicleByPlate(plate, cb)
end)

ESX.RegisterServerCallback('lp_core:GetVehicleByPlate', function(source, cb, plate)
    GetVehicleByPlate(plate, cb)
end)

function GetVehicleByPlate(plate, cb)
    MySQL.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate = @plate LIMIT 1', {
        ['@plate'] = plate
    }, function(results)
        if #results > 0 then
            local vehicle = results[1]
            local owner = ESX.GetPlayerFromIdentifier(vehicle.owner)
            cb(vehicle, owner)
        else
            cb()
        end
    end)
end

RegisterServerEvent('lp_core:removeSocietyMoney')
AddEventHandler('lp_core:removeSocietyMoney', function(societyName, amount)
    TriggerEvent('esx_society:getSociety', societyName, function(society)
        amount = ESX.Math.Round(tonumber(amount))
        TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
            if amount > 0 then
                account.removeMoney(amount)
            end
        end)
    end)
end)

RegisterServerEvent('lp_core:addSocietyMoney')
AddEventHandler('lp_core:addSocietyMoney', function(societyName, amount)
    TriggerEvent('esx_society:getSociety', societyName, function(society)
        amount = ESX.Math.Round(tonumber(amount))
        TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
            if amount > 0 then
                account.addMoney(amount)
            end
        end)
    end)
end)

RegisterNetEvent('lp_core:removeFromAccount')
AddEventHandler('lp_core:removeFromAccount', function(account, playerId, amount, isOnline)
    if isOnline == true then
        local xPlayer = ESX.GetPlayerFromId(playerId)
        xPlayer.removeAccountMoney(account, amount)
    else
        MySQL.Async.fetchScalar("SELECT JSON_EXTRACT(accounts, @account) AS balance FROM users WHERE identifier = @identifier", {
            ['@identifier'] = playerId,
            ['@account'] = '$.' .. account
        }, function(result)
            local PlayerCurrentBalance = tonumber(result)
            PlayerCurrentBalance = PlayerCurrentBalance - amount
            MySQL.Sync.execute('UPDATE `users` SET `accounts` = JSON_SET(accounts, @account, @balance) WHERE `identifier` = @identifier', {
                ['@account'] = '$.' .. account, ['@identifier'] = playerId, ['@balance'] = PlayerCurrentBalance
            })
        end)
    end
end)

ESX.RegisterCommand('mycar', 'admin', function(xPlayer, args, showError)
	xPlayer.triggerEvent('lp_core:newOwnedVehicle', args.car, args.plate)
end, false, {help = _U('command_mycar'), validate = false, arguments = {
	{name = 'car', help = _U('command_mycar_car'), type = 'any'},
	{name = 'plate', help = _U('command_mycar_plate'), type = 'any'},
}})

ESX.RegisterServerCallback('lp_core:spawnOwnedVehicle', function(source, cb, model, plate)
	local xPlayer = ESX.GetPlayerFromId(source)

    MySQL.Async.fetchAll('SELECT 1 FROM owned_vehicles WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)
        if result[1] == nil then
            MySQL.Async.execute('INSERT INTO owned_vehicles (owner, plate, vehicle) VALUES (@owner, @plate, @vehicle)', {
                ['@owner']   = xPlayer.identifier,
                ['@plate']   = plate,
                ['@vehicle'] = json.encode({model = GetHashKey(model), plate = plate})
            }, function(rowsChanged)
                print('Véhicule créé avec la plaque : ' .. plate)
                cb(true)
            end)
        else
            cb(false)
        end
	end)
end)

RegisterNetEvent('lp_core:updateOwnedVehicle')
AddEventHandler('lp_core:updateOwnedVehicle', function(plate, type)
	local xPlayer = ESX.GetPlayerFromId(source)

    MySQL.Async.fetchAll('SELECT 1 FROM owned_vehicles WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)
        if result[1] ~= nil then
            MySQL.Async.execute('UPDATE owned_vehicles SET type = @type WHERE plate = @plate', {
                ['@type']   = type,
                ['@plate']   = plate
            })
        end
	end)
end)
