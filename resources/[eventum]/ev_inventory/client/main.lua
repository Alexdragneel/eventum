ESX = nil
local isAnimated, isDead, isRich = false, false, false, false
local playerData = nil

-- Animation utilisé pour la destruction
local dictDestruction, animDestruction = 'weapons@first_person@aim_rng@generic@projectile@sticky_bomb@', 'plant_floor'
local isDestroyingObject = false

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

-- ESX Listeners --
AddEventHandler('esx:onPlayerSpawn', function() isDead = false end)
AddEventHandler('esx:onPlayerDeath', function() isDead = true end)
-- --

-- Key Controls --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if IsControlJustReleased(0, 289) then
			if IsInputDisabled(0) and not isDead and not ESX.UI.Menu.IsOpen('default', 'lp_inventory', 'inventory') then
				ShowInventory()
			end
		end
	end
end)
-- --

-- Detecter si l'animation de destruction a été annulée --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(5)			
		if isDestroyingObject then
			if IsControlJustReleased(0, 73) or isDead or not IsPedOnFoot(PlayerPedId()) or IsPedFalling(PlayerPedId()) then
				TriggerEvent('rprogress:stop')
				isDestroyingObject = false
			elseif not IsEntityPlayingAnim(PlayerPedId(), dictDestruction, animDestruction, 3) then
				TaskPlayAnim(PlayerPedId(), dictDestruction, animDestruction, 8.0, 1.0, 2000, 1, 0.0, false, false, false)
			end
		end
	end
end)
-- --

-- Big Money = Big Malette --
RegisterNetEvent('esx:setAccountMoney')
AddEventHandler('esx:setAccountMoney', function(account)
	if account.name ~= 'bank' then
		if account.money > Config.MaxMoneyBeforeMalette then
			IsTooRich(true)
		else
			IsTooRich(false)
		end
	end
end)

RegisterNetEvent("lp_inventory:restoreMalette")
AddEventHandler("lp_inventory:restoreMalette", function()
	local _isRich = isRich
	isRich = not isRich
	IsTooRich(_isRich)
end)

RegisterNetEvent("esx_skin:playerRegistered")
AddEventHandler("esx_skin:playerRegistered", function()
	TriggerEvent('esx:restoreLoadout')
	Citizen.CreateThread(function()
		while playerData == nil do
			Citizen.Wait(100)
		end

		local accounts = playerData.accounts or {}
		for index, account in ipairs(accounts) do 
			if account.name ~= 'bank' then
				if account.money > Config.MaxMoneyBeforeMalette then
					IsTooRich(true)
					break
				end
			end
		end

		ESX.TriggerServerCallback('lp_personalmenu:Admin_getUsergroup', function(plyGroup)
			local group = plyGroup

			while true do
				if isRich and group == 'user' then
					Citizen.Wait(1)
					HudWeaponWheelIgnoreSelection()
					HideHudComponentThisFrame(19)
				else
					Citizen.Wait(1000)
				end
			end
		end)
	end)
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(_playerData)
	playerData = _playerData
end)

function IsTooRich(tooRich)
	if tooRich and not isRich then
		if not HasWeaponAssetLoaded('weapon_briefcase') then
			RequestWeaponAsset('weapon_briefcase')
	
			while not HasWeaponAssetLoaded('weapon_briefcase') do
				Citizen.Wait(1)
			end
		end
		isRich = true
		GiveWeaponToPed(GetPlayerPed(-1), GetHashKey('weapon_briefcase'), 1, false, true)
	elseif not tooRich and isRich then
		GiveWeaponToPed(GetPlayerPed(-1), GetHashKey('weapon_unarmed'), 1, false, true)
		isRich = false
	end
end
-- --

-- Functions --
function GetInventoryItemMenu(data)
	local elements = {}
	local closestPlayer, closestPlayerDistance = ESX.Game.GetClosestPlayer()

	if data.usable then
		table.insert(elements, {
			label = _U('use'),
			action = 'use',
			type = data.type,
			value = data.value,
			name = data.name
		})
	end

	if data.canRemove then
		if closestPlayer ~= -1 and closestPlayerDistance <= 3.0 then
			table.insert(elements, {
				label = _U('give'),
				action = 'give',
				type = data.type,
				value = data.value
			})
		end

		table.insert(elements, {
			label = _U('remove'),
			action = 'remove',
			type = data.type,
			value = data.value
		})
	end

	table.insert(elements, {label = _U('return'), action = 'return'})

	return elements
end

function GiveItem(playerId, item, type, count, menu1)
	local closestPlayer, closestPlayerDistance = ESX.Game.GetClosestPlayer()

	if closestPlayer ~= -1 and closestPlayerDistance <= 3.0 then
		local selectedPlayerPed = GetPlayerPed(closestPlayer)
		local playerServerId = GetPlayerServerId(closestPlayer)

		if IsPedOnFoot(selectedPlayerPed) and not IsPedFalling(selectedPlayerPed) then
			ESX.UI.Menu.Open('dialog', 'lp_inventory', 'inventory_item_count_give', {
				title = _U('amount'),
				type = 'number'
			}, function(data2, menu2)
				local quantity = tonumber(data2.value)

				if quantity and quantity > 0 and count >= quantity then
					TriggerServerEvent('esx:giveInventoryItem', playerServerId, type, item, quantity)
					menu2.close()
					menu1.close()
				else
					ESX.ShowNotification(_U('amount_invalid'))
				end
			end, function(data2, menu2)
				menu2.close()
			end)
		else
			ESX.ShowNotification(_U('action_impossible'))
		end
	else
		ESX.ShowNotification(_U('no_players_nearby'))
	end
end

function RemoveItem(playerId, item, type, count, menu1)
	if IsPedOnFoot(playerId) and not IsPedFalling(playerId) then
		ESX.Streaming.RequestAnimDict(dictDestruction)

		ESX.UI.Menu.Open('dialog', 'lp_inventory', 'inventory_item_count_remove', {
			title = _U('amount'),
			type = 'number'
		}, function(data2, menu2)
			local quantity = tonumber(data2.value)

			if quantity and quantity > 0 and count >= quantity then
				menu2.close()
				menu1.close()

				local destructionDuration = 2000

				TaskPlayAnim(playerId, dictDestruction, animDestruction, 8.0, 1.0, 2000, 1, 0.0, false, false, false)
				isDestroyingObject = true

				for i = 1, quantity do
					TriggerEvent('rprogress:start', _U("destruction_inprogress"), destructionDuration-200)
					Citizen.Wait(destructionDuration)
					if isDestroyingObject then
						TriggerServerEvent('esx:removeInventoryItem', type, item, 1)
					else
						break
					end
				end
				ClearPedSecondaryTask(playerId)
				isDestroyingObject = false
			else
				ESX.ShowNotification(_U('amount_invalid'))
			end
		end, function(data2, menu2)
			menu2.close()
		end)
	end
end

function GiveAmmo(playerId, item, ammo, menu1)
	local closestPlayer, closestPlayerDistance = ESX.Game.GetClosestPlayer()

	if closestPlayer ~= -1 and closestPlayerDistance <= 3.0 then
		local selectedPlayerPed = GetPlayerPed(closestPlayer)
		local playerServerId = GetPlayerServerId(closestPlayer)

		if IsPedOnFoot(selectedPlayerPed) and not IsPedFalling(selectedPlayerPed) then
			if ammo > 0 then
				ESX.UI.Menu.Open('dialog', 'lp_inventory', 'inventory_item_count_give', {
					title = _U('amount_ammo'),
					type = 'number'
				}, function(data2, menu2)
					local quantity = tonumber(data2.value)

					if quantity and quantity > 0 then
						if ammo >= quantity then
							TriggerServerEvent('esx:giveInventoryItem', playerServerId, 'item_ammo', item, quantity)
							menu2.close()
							menu1.close()
						else
							ESX.ShowNotification(_U('no_ammo'))
						end
					else
						ESX.ShowNotification(_U('amount_invalid'))
					end
				end, function(data2, menu2)
					menu2.close()
				end)
			else
				ESX.ShowNotification(_U('no_ammo'))
			end
		else
			ESX.ShowNotification(_U('action_impossible'))
		end
	else
		ESX.ShowNotification(_U('no_players_nearby'))
	end
end

function RefreshItemMenu(data, menu1)
	ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
		local count = 0
		for k,v in ipairs(items) do
			if v.itemId == data.value then
				count = v.count
				if v.count > 0 then
					local dbItem = dbItems[v.name]
					if dbItem then
						local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(v.label, ESX.Math.GroupDigits(v.count))
						if v.useCount > 0 and not v.stackable then
							
							if dbItem.actions ~= nil and dbItem.actions.useCount ~= nil and dbItem.actions.useCount > 0 then
								label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s %s</span>'):format(v.label, v.useCount, dbItem.actions.countUnit)
							end
						end
						data.label = label
						data.count = v.count
					end
				end
				break
			end
		end

		if count == 0 then
			ESX.UI.Menu.CloseAll()
			ShowInventory()
		else
			ShowItemMenu(data)
		end
	end)
end

function ShowItemMenu(data)
	local playerId = PlayerPedId()
	local elements = GetInventoryItemMenu(data)
		
	ESX.UI.Menu.Open('default', 'lp_inventory', 'inventory_item', {
		title    = data.labelRaw .. ' x' .. data.count,
		align    = 'top-left',
		elements = elements,
	}, function (data1, menu1)
		local menuAction = data1.current.action
		if menuAction == 'return' then
			ESX.UI.Menu.CloseAll()
			ShowInventory()
		else
			local item, type, itemName = data1.current.value, data1.current.type, data1.current.name
			local count = data.count

			if menuAction == 'use' then
				TriggerServerEvent('esx:useItem', item)
				menu1.close()
				if itemName ~= 'radio' and itemName ~= 'phone' and itemName ~= 'binoculars' and itemName ~= 'wheelchair' then
					RefreshItemMenu(data)
				end
			elseif menuAction == 'give' then
				GiveItem(playerId, item, type, count, menu1)
			elseif menuAction == 'remove' then
				RemoveItem(playerId, item, type, count, menu1)
			elseif menuAction == 'give_ammo' then
				GiveAmmo(playerId, item, data.ammo, menu1)
			end
		end
	end, function (data1, menu1)
		ESX.UI.Menu.CloseAll()
		ShowInventory()
	end)
end

function ShowInventory()
    local playerId = PlayerPedId()
    local elements = {}
    local currentWeight = 0

	ESX.UI.Menu.CloseAll()

	ESX.TriggerServerCallback('lp_core:getPlayerAccounts', function(accounts)
		for k,v in pairs(accounts) do
			if v.money > 0 and v.name ~= 'bank' then
				local formattedMoney = _U('locale_currency', ESX.Math.GroupDigits(v.money))

				table.insert(elements, {
					label = ('%s: <span style="color:green;">%s</span>'):format(v.label, formattedMoney),
					labelRaw = v.label,
					count = v.money,
					type = 'item_account',
					value = v.name,
					usable = false,
					rarity = 'common',
					canRemove = true
				})
			end
		end

		ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
			table.sort(items, function(a, b)
				return a.label < b.label
			end)
			for k,v in ipairs(items) do
				if v.count > 0 then
					currentWeight = currentWeight + (v.weight * v.count)
					
					local dbItem = dbItems[v.name]
					
					if dbItem then
						local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(v.label, ESX.Math.GroupDigits(v.count))
						if v.useCount > 0 and not v.stackable then
							if dbItem.actions ~= nil and dbItem.actions.useCount ~= nil and dbItem.actions.useCount > 0 then
								label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s %s</span>'):format(v.label, v.useCount, dbItem.actions.countUnit)
							end
						end
						table.insert(elements, {
							name = v.name,
							label = label,
							labelRaw = v.label,
							count = v.count,
							type = 'item_standard',
							value = v.itemId,
							usable = v.usable,
							rarity = v.rarity,
							canRemove = v.canRemove
						})
					end
				end
			end
		
			ESX.TriggerServerCallback('lp_core:getPlayerMaxWeight', function(maxWeight)
				ESX.UI.Menu.Open('default', 'lp_inventory', 'inventory', {
					title    = _U('inventory', currentWeight, maxWeight),
					align    = 'top-left',
					elements = elements
				}, function (data, menu)
					menu.close()
					
					ShowItemMenu(data.current)
				end, function (data, menu)
					menu.close()
				end)
			end)
		end)
	end)
end
-- --

-- Events logic --
RegisterNetEvent('lp_inventory:showInventory')
AddEventHandler('lp_inventory:showInventory', function ()
	ShowInventory()
end)

RegisterNetEvent('lp_inventory:playAnim')
AddEventHandler('lp_inventory:playAnim', function (anim)
	if not isAnimated then
		isAnimated = true

		Citizen.CreateThread(function()
			local playerPed = PlayerPedId()
			local prop = nil

			if anim.prop ~= nil then
				local x,y,z = table.unpack(GetEntityCoords(playerPed))
				prop = CreateObject(GetHashKey(anim.prop), x, y, z + 0.2, true, true, true)
				local boneIndex = GetPedBoneIndex(playerPed, 18905)
				AttachEntityToEntity(prop, playerPed, boneIndex, 0.12, 0.028, 0.001, 10.0, 175.0, 0.0, true, true, false, true, 1, true)
			end

			if anim.dict ~= nil then
				ESX.Streaming.RequestAnimDict(anim.dict, function()
					TaskPlayAnim(playerPed, anim.dict, anim.name, 1.0, -1.0, 2000, 50, 1, false, false, false)
	
					Citizen.Wait(3000)
					isAnimated = false
					ClearPedSecondaryTask(playerPed)
					TriggerEvent('lp_inventory:stopAnim')
					
					if prop ~= nil then
						DeleteObject(prop)
					end
				end)
			end
		end)
	end
end)

RegisterNetEvent('lp_inventory:notifAction')
AddEventHandler('lp_inventory:notifAction', function (msg)
	ESX.ShowNotification(msg)
end)
-- --
