CREATE TABLE lp_items (
	id INT auto_increment NOT NULL,
	name varchar(100) NOT NULL,
	label varchar(255) NOT NULL,
	sellPrice INT DEFAULT 0 NULL,
	farmPrice INT DEFAULT 0 NULL,
	weight INT DEFAULT 0 NULL,
	actions LONGTEXT NOT NULL,
	canRemove TINYINT(1) DEFAULT 1 NULL,
	rarity varchar(100) DEFAULT 'common' NULL,
	icon varchar(255) DEFAULT 'no_icon.png' NULL,
	stackable TINYINT(1) DEFAULT 1 NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (id),
    CONSTRAINT `lp_items_un` UNIQUE KEY (name)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

INSERT INTO lp_items (name,label,sellPrice,weight,actions,farmPrice,canRemove,rarity,icon,stackable) VALUES
	 ('bandage','Bandage',50,100,'{}',0,1,'common','bandage.png',1),
	 ('bread','Pain',20,200,'{
  "anim": {
    "prop": "v_res_fa_bread03",
    "dict": "mp_player_inteat@burger",
    "name": "mp_player_int_eat_burger_fp",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "hunger",
	  	"amount": 20000
	  },
	  {
	  	"type": "remove",
	  	"attribute": "thirst",
	  	"amount": 1000
	  }
  ]
}',0,1,'common','bread.png',1),
	 ('chicken','Poulet vivant',0,1500,'{}',0,1,'common','chicken.png',1),
	 ('clothing','Vêtement',40,150,'{}',40,1,'common','clothing.png',1),
	 ('copper','Cuivre',5,200,'{}',5,1,'common','copper.png',1),
	 ('cutted_wood','Bois coupé',0,1000,'{}',0,1,'common','cutted_wood.png',1),
	 ('diamond','Diamant',250,200,'{}',250,1,'common','diamond.png',1),
	 ('fabric','Tissu',0,150,'{}',0,1,'common','fabric.png',1),
	 ('fish','Poisson',11,1000,'{}',11,1,'common','fish.png',1),
	 ('gas','Litre d''essence',61,1000,'{}',61,1,'common','gas.png',1),
	 ('gold','Lingot d''or',25,300,'{}',25,1,'common','gold.png',1),
	 ('iron','Barre de fer',9,150,'{}',9,1,'common','iron.png',1),
	 ('medikit','Medikit',50,500,'{}',0,1,'common','medikit.png',1),
	 ('oil','Litre de pétrole',0,1000,'{}',0,1,'common','oil.png',1),
	 ('packaged_chicken','Poulet en barquette',23,500,'{}',23,1,'common','packaged_chicken.png',1),
	 ('packaged_plank','Lot de planches',13,300,'{}',13,1,'common','packaged_plank.png',1),
	 ('phone','Téléphone',200,220,'{}',0,0,'common','phone.png',1),
	 ('refined_oil','Litre de pétrole raffiné',0,1000,'{}',0,1,'common','refined_oil.png',1),
	 ('slaughtered_chicken','Poulet abattu',0,1400,'{}',0,1,'common','slaughtered_chicken.png',1),
	 ('stone','Roche',0,6500,'{}',0,1,'common','stone.png',1),
	 ('washed_stone','Métal brut',0,6000,'{}',0,1,'common','washed_stone.png',1),
	 ('water','Bouteille d''eau',20,1500,'{
	"becomes": "used_water"
}',0,1,'common','water.png',1),
	 ('wood','Bûche',0,1500,'{}',0,1,'common','wood.png',1),
	 ('wool','Laine',0,300,'{}',0,1,'common','wool.png',1),
	 ('used_water','Bouteille d''eau entamée',15,1500,'{
  "useCount": 5,
  "countUnit": "verres",
  "becomes": "plastic_bottle",
  "anim": {
    "prop": "prop_ld_flow_bottle",
    "dict": "mp_player_intdrink",
    "name": "loop_bottle",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "thirst",
	  	"amount": 200000
	  }
  ],
  "notification": "HYDRATEZ VOUS!"
}',0,1,'common','used_water.png',0),
	 ('plastic_bottle','Bouteille en plastique',10,35,'{}',0,1,'common','plastic_bottle.png',1),
	 ('pizza','Pizza',40,400,'{
  "anim": {
    "prop": "v_res_tt_pizzaplate",
    "dict": "mp_player_inteat@burger",
    "name": "mp_player_int_eat_burger_fp",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "hunger",
	  	"amount": 200000
	  },
	  {
	  	"type": "remove",
	  	"attribute": "thirst",
	  	"amount": 10000
	  }
  ]
}',0,1,'common','pizza.png',1),
	 ('cheeseburger','Cheeseburger',40,400,'{
  "anim": {
    "prop": "prop_cs_burger_01",
    "dict": "mp_player_inteat@pnq",
    "name": "loop_fp",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "hunger",
	  	"amount": 200000
	  },
	  {
	  	"type": "remove",
	  	"attribute": "thirst",
	  	"amount": 10000
	  }
  ]
}',0,1,'common','cheeseburger.png',1),
	 ('hotdog','Hot Dog',30,200,'{
  "anim": {
    "prop": "prop_cs_hotdog_01",
    "dict": "mp_player_inteat@burger",
    "name": "mp_player_int_eat_burger_fp",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "hunger",
	  	"amount": 150000
	  },
	  {
	  	"type": "remove",
	  	"attribute": "thirst",
	  	"amount": 5000
	  }
  ]
}',0,1,'common','hotdog.png',1),
	 ('beer','Bière',30,500,'{
  "anim": {
    "prop": "prop_cs_beer_bot_02",
    "dict": "mp_player_intdrink",
    "name": "loop_bottle",
  },
  "effects": [
	  {
	  	"type": "add",
	  	"attribute": "drunk",
	  	"amount": 50000
	  }
  ],
  "notification": "L''alcool c''est de l''eau"
}',0,1,'common','beer.png',1);
