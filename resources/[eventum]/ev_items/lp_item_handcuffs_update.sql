START TRANSACTION;


INSERT INTO `lp_items` (`name`, `label`, `sellPrice`, `farmPrice`, `weight`, `actions`, `canRemove`, `rarity`, `icon`, `stackable`) VALUES
('handcuffs', 'Menottes', 60, 0, 200, '{\"trigger\":\"type\":\"client\",\"event\":\"lp_items:handcuffs\"}}', 1, 'common', 'handcuffs.png', 1),
('handcuffs_keys', 'Clés de menottes', 60, 0, 200, '{\"trigger\":\"type\":\"client\",\"event\":\"lp_items:handcuffs_keys\"}}', 1, 'common', 'handcuffs_keys.png', 1);

COMMIT;