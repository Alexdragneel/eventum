START TRANSACTION;

INSERT INTO `lp_items` (`name`, `label`, `sellPrice`, `farmPrice`, `weight`, `actions`, `canRemove`, `rarity`, `icon`, `stackable`) VALUES
('umbrella', 'Parapluie', 60, 0, 200, '{\"trigger\":\"type\":\"client\",\"event\":\"lp_items:umbrella\"}}', 1, 'common', 'umbrella.png', 1),
('club_sandwich', 'Club sandwich', 20, 0, 200, '{\"anim\":\"prop\":\"v_res_fa_bread03\",\"dict\":\"mp_player_inteat@burger\",\"name\":\"mp_player_int_eat_burger_fp\"},\"effects\":[{\"type\":\"add\",\"attribute\":\"hunger\",\"amount\":200000},\"type\":\"remove\",\"attribute\":\"thirst\",\"amount\":1000}]}', 1, 'common', 'club-sandwich.png', 1);

UPDATE `lp_shop_types` SET `items`= "[\"phone\",\"umbrella\",\"water\",\"bread\",\"beer\",\"compass\",\"gps\"]" WHERE `lp_shop_types`.`name` = 'gasstation';
UPDATE `lp_shop_types` SET `items`= "[\"phone\",\"umbrella\",\"water\",\"bread\",\"pizza\",\"hotdog\",\"cheeseburger\",\"beer\",\"compass\",\"gps\"]" WHERE `lp_shop_types`.`name` = 'store';

UPDATE `lp_shop_types` SET `items`= REPLACE(`lp_shop_types`.`items`, "bread", "club_sandwich");

COMMIT;