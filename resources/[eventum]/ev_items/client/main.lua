ESX = nil
local hasUmbrella = false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent('lp_items:umbrella')
AddEventHandler('lp_items:umbrella', function()
    local playerPed = GetPlayerPed(-1)
    local position = GetEntityCoords(playerPed, false)
    local coords = GetEntityCoords(playerPed)
    local boneIndex = GetPedBoneIndex(playerPed, 57005)

    RequestAnimDict('amb@code_human_wander_drinking@male@base')
    while not HasAnimDictLoaded('amb@code_human_wander_drinking@male@base') do
        Citizen.Wait(1)
    end

    if hasUmbrella == false then
        ESX.Game.SpawnObject('p_amb_brolly_01', {
            x = coords.x,
            y = coords.y,
            z = coords.z + 2
        }, function(object)
            hasUmbrella = true
            AttachEntityToEntity(object, playerPed, boneIndex, 0.10, 0, -0.001, 80.0, 150.0, 200.0, true, true, false,
                true, 1, true)
            TaskPlayAnim(playerPed, "amb@code_human_wander_drinking@male@base", "static", 3.5, -8, -1, 49, 0, 0, 0, 0)
        end)
    else
        hasUmbrella = false
        local object = 0
        repeat
            object = GetClosestObjectOfType(position.x, position.y, position.z, 2.0, GetHashKey("p_amb_brolly_01"),
                false, false, false)
            if object ~= 0 then
                for x = 1, 5 do
                    DeleteObject(object)
                end
            end
            Citizen.Wait(0)
        until object == 0
        ClearPedSecondaryTask(playerPed)
    end
end)

RegisterNetEvent('lp_items:binoculars')
AddEventHandler('lp_items:binoculars', function()
    local ped = PlayerPedId()
    if not IsPedInAnyVehicle(ped) then        
        Citizen.CreateThread(function()
            ClearPedTasks(ped)
            TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_BINOCULARS", 0, 1)
            Citizen.Wait(2000)
            TriggerEvent("lp_specialcameras:activateCamera", 'BINOCULARS', propUsed)
        end)
    end
end)


RegisterNetEvent('lp_items:handcuffs')
AddEventHandler('lp_items:handcuffs', function()

    local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('esx_policejob:handcuff', GetPlayerServerId(closestPlayer))
    end
end)

RegisterNetEvent('lp_items:handcuffs_keys')
AddEventHandler('lp_items:handcuffs_keys', function()

end)