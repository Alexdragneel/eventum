fx_version 'bodacious'
game 'gta5'

description 'Los Plantos Loading Screen'

loadscreen 'html/index.html'
loadscreen_manual_shutdown 'yes'

client_scripts {
    'client/client.lua'
}

files {
    'html/index.html',
    'html/css/stylesheet.css',
    'html/css/config.css',
    'html/js/config.js',
    'html/js/app.js',
    'html/img/*',
    'html/mp3/losplantos.mp3',
    'html/mp3/LP_Xmas.mp3'
}
