const isXmas = false;
function shuffle(array) {
    var currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}

$(function () {
    const audio = document.getElementById("introMusic");

    if (isXmas) {
        document.querySelector('html').setAttribute('class', 'xmas');
        audio.src = 'mp3/LP_Xmas.mp3';
        document.getElementById("bg").innerHTML = `<img width="100%"height="100%" src="img/bgXmas.png">`;
        document.querySelector('.logo').setAttribute('src', 'img/logoXmas.png');
    }

    audio.volume = window.localStorage['loadingscreen_volume'] || 0.20;

    document.body.addEventListener("mousemove", (event) => {
        const cursor = document.getElementById("cursor");
        const x = event.pageX - 5;
        const y = event.pageY;
        cursor.style.left = x + "px";
        cursor.style.top = y + "px";
    });

    const slider = document.getElementById("slider");

    slider.value = `${audio.volume * 100}`;

    slider.oninput = function () {
        audio.volume = this.value / 100;
        window.localStorage['loadingscreen_volume'] = audio.volume
    };

    if (isXmas === false) {

        let images = config.images;
        shuffle(images);

        images.forEach(i => {
            document.getElementById("bg").innerHTML = document.getElementById("bg").innerHTML + `<img width="100%"height="100%" src=img/${i}>`;
        });

        let img = $('div#bg img');
        let len = img.length;
        let idx = 0;
        img.hide();
        img.eq(idx).show();

        setInterval(function () {
            img.eq(idx).fadeOut(config.transitionInterval, function () {
                idx++;
                if (idx == len) idx = 0;
                img.eq(idx).fadeIn(config.transitionInterval);
            });
        }, 2 * config.transitionInterval + config.imgInterval);
    }
})

let count = 0;
let thisCount = 0;

const handlers = {
    startInitFunctionOrder(data) {
        count = data.count;

        document.querySelector('.loadbar .text .type').innerHTML = data.type;
    },

    initFunctionInvoking(data) {
        document.querySelector('.loadbar .bar').style.left = '0%';
        document.querySelector('.loadbar .bar').style.width = ((data.idx / count) * 100) + '%';
    },

    startDataFileEntries(data) {
        count = data.count;
    },

    performMapLoadFunction(data) {
        ++thisCount;

        document.querySelector('.loadbar .bar').style.left = '0%';
        document.querySelector('.loadbar .bar').style.width = ((thisCount / count) * 100) + '%';
    },

    onLogLine(data) {
        document.querySelector('.loadbar .text .message').innerHTML = data.message + '...';
    }
};

window.addEventListener('message', function (e) {
    (handlers[e.data.eventName] || function () { })(e.data);
});

/////////////////////////////////////////////
