local isRegistered = nil
local isModelLoaded = false

function EnterCityAnimation()
	if not IsScreenFadedOut() then
		DoScreenFadeOut(0)
	end

	while not isModelLoaded do
		Citizen.Wait(0)
	end

	playerPed = PlayerPedId()
	SwitchOutPlayer(playerPed, 0, 1)

	while GetPlayerSwitchState() ~= 5 do
		Citizen.Wait(0)
	end

	DoScreenFadeIn(500)
	while not IsScreenFadedIn() do
		Citizen.Wait(0)
	end

	SwitchInPlayer(playerPed)

	if not isRegistered then
		local fading = false
		while GetPlayerSwitchState() ~= 12 do
			if GetPlayerSwitchState() == 8 and not fading then
				fading = true
				DoScreenFadeOut(2000)
				while not IsScreenFadedOut() do
					Citizen.Wait(0)
				end
			end
			Citizen.Wait(0)
		end
		-- on first connection we want the enter city animation to be over the airport
		-- we now need to TP the player to the hidden secret room for the skin creation scene
	
		local hiddenChangingRoom = {x = 402.9, y = -996.8, z = -99.0, heading = 170.0}
		SetEntityCoords(playerPed, hiddenChangingRoom.x, hiddenChangingRoom.y, hiddenChangingRoom.z, false, false, false, false)
		SetEntityHeading(playerPed, hiddenChangingRoom.heading)

		Citizen.Wait(600)

		DoScreenFadeIn(800)
		while not IsScreenFadedIn() do
			Citizen.Wait(0)
		end
	end

	TriggerEvent('lp_instance:enterCityEnded')
end

AddEventHandler('esx:loadingScreenOff', function()
	while isRegistered == nil do
		Citizen.Wait(100)
	end

	EnterCityAnimation()
end)

RegisterNetEvent('esx_identity:alreadyRegistered')
AddEventHandler('esx_identity:alreadyRegistered', function()
	isRegistered = true
end)

RegisterNetEvent('esx_identity:showRegisterIdentity')
AddEventHandler('esx_identity:showRegisterIdentity', function()
	isRegistered = false
end)

AddEventHandler('skinchanger:modelLoaded', function()
	isModelLoaded = true
end)
