-- --------------------------------------------
-- Settings
-- --------------------------------------------
local defaultOptions = {
    type = 'topRight',
    timeout = 1500,
    center = false,
    -- Text
    scale = 0.3,
    offsetLine = 0.014,
    -- Warp
    offsetX = 0.005,
    offsetY = 0.004,
    -- Sprite
    dict = 'commonmenu',
    sprite = 'gradient_bgd',
    width = 0.14,
    height = 0.012,
    heading = -90.0,
    -- Betwenn != notifications
    gap = 0.002
}

RequestStreamedTextureDict(defaultOptions.dict) -- Load the sprite dict. properly
AddTextEntry('NOTIF_LABEL', '~a~')

function clone(object)
    local lookup_table = {}
    local function copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for key, value in pairs(object) do
            new_table[copy(key)] = copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return copy(object)
end

function MergeConfig(t1, t2)
    local copy = clone(t1)
    for k, v in pairs(t2) do
        if type(v) == "table" then
            if type(copy[k] or false) == "table" then
                MergeConfig(copy[k] or {}, t2[k] or {})
            else
                copy[k] = v
            end
        else
            copy[k] = v
        end
    end
    return copy
end

-- --------------------------------------------
-- Calculus functions
-- --------------------------------------------

local function goDown(v, id) -- Notifications will go under the previous notifications
    for i, notif in ipairs(v) do
        if notif.draw and i ~= id then
            notif.y = notif.y + (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 2 +
                          notif.options.gap
        end
    end
end

local function goUp(v, id) -- Notifications will go above the previous notifications
    for i, notif in ipairs(v) do
        if notif.draw and i ~= id then
            notif.y = notif.y - (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 2 -
                          notif.options.gap
        end
    end
end

local function centeredDown(v, id) -- Notification will stay centered from the default position and new notification will go at the bottom
    for i, notif in ipairs(v) do
        if notif.draw and i ~= id then
            notif.y = notif.y - (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 4 -
                          notif.options.gap / 2
            v[id].y = notif.y + (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 2 +
                          notif.options.gap
        end
    end
end

local function centeredUp(v, id) -- Notification will stay centered from the default position and new notification will go at the top
    for i, notif in ipairs(v) do
        if notif.draw and i ~= id then
            notif.y = notif.y.y + (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 4 +
                          notif.options.gap / 2
            v[id].y = notif.y - (notif.options.height + (v[id].lines * 2 + 1) * notif.options.offsetLine) / 2 -
                          notif.options.gap
        end
    end
end

local function CountLines(v, text, options)
    BeginTextCommandLineCount("NOTIF_LABEL")
    SetTextScale(options.scale, options.scale)
    SetTextWrap(v.x, v.x + options.width - options.offsetX)
    AddTextComponentSubstringPlayerName(text)
    local nbrLines = GetTextScreenLineCount(v.x + options.offsetX, v.y + options.offsetY)
    return nbrLines
end

local function DrawText(v, text)
    SetTextOutline()
    SetTextScale(v.options.scale, v.options.scale)
    SetTextWrap(v.x, v.x + v.options.width - v.options.offsetX)
    if v.options.center == true then
        -- SetTextCentre(true)
    end
    BeginTextCommandDisplayText("NOTIF_LABEL")
    AddTextComponentSubstringPlayerName(text)
    EndTextCommandDisplayText(v.x + v.options.offsetX, v.y + v.options.offsetY)
end

local function DrawBackground(v)
    local body = v.options
    DrawSprite(body.dict, body.sprite, v.x + body.width / 2, v.y + (body.height + v.lines * body.offsetLine) / 2,
        body.width, body.height + v.lines * body.offsetLine, body.heading, 255, 255, 255, 255)
end

-- --------------------------------------------
-- Different options
-- --------------------------------------------

local positions = {
    ['centerRight'] = {
        x = 0.85,
        y = 0.5,
        notif = {},
        offset = centeredUp
    },
    ['centerLeft'] = {
        x = 0.01,
        y = 0.5,
        notif = {},
        offset = centeredUp
    },
    ['topRight'] = {
        x = 0.85,
        y = 0.015,
        notif = {},
        offset = goDown
    },
    ['topLeft'] = {
        x = 0.01,
        y = 0.015,
        notif = {},
        offset = goDown
    },
    ['bottomRight'] = {
        x = 0.85,
        y = 0.955,
        notif = {},
        offset = goUp
    },
    ['bottomLeft'] = {
        x = 0.015,
        y = 0.75,
        notif = {},
        offset = goUp
    },
    ['center'] = {
        x = 0.5 - 0.05,
        y = 0.56,
        notif = {},
        offset = goDown
    },
    ['custom'] = {
        x = 0.54,
        y = 0.610,
        notif = {},
        offset = goDown
    }
    -- ['position name'] = { starting x, starting y, notif = {} (nothing to put here it's juste the handle), offset = the way multiple notifications will stack up}
}

-- --------------------------------------------
-- Main
-- --------------------------------------------

function SendNotification(options)
    local currentOptions = MergeConfig(defaultOptions, options)
    local text = currentOptions.text or ""
    local type = currentOptions.type
    local timeout = currentOptions.timeout
    local hasBackground = currentOptions.background or false

    local p = positions[type]
    local id = #p.notif + 1
    local nbrLines = CountLines(p, text, currentOptions)

    p.notif[id] = {
        x = p.x,
        y = p.y,
        lines = nbrLines,
        draw = true,
        options = currentOptions
    }

    if id > 1 then
        p.offset(p.notif, id)
    end

    Citizen.CreateThread(function()
        Citizen.Wait(timeout)
        p.notif[id].draw = false
    end)

    Citizen.CreateThread(function()
        while p.notif[id].draw do
            Citizen.Wait(0)
            if hasBackground then
                DrawBackground(p.notif[id])
            end
            DrawText(p.notif[id], text)
        end
    end)
end

RegisterNetEvent('lp_notif:send')
AddEventHandler('lp_notif:send', function(options)
    SendNotification(options)
end)
