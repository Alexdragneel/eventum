ESX = nil

-- Init ESX stuff --
TriggerEvent("esx:getSharedObject", function(obj)
    ESX = obj
end)
-- --

RegisterNetEvent('lp_notif:send')
AddEventHandler('lp_notif:send', function(target, options)
    local xPlayer = ESX.GetPlayerFromId(target)
    xPlayer.TriggerEvent('lp_notif:send', options)
end)
