Config                      = {}
Config.Locale               = "fr"
Config.Desks                = {
    POLICE_STATION = {
        coords = vector3(441.01, -981.25, 29.6),
        message = 'desk_message_llpd',
        number = 'police'
    },
    HOSPITAL_LEVEL_1 = {
        coords = vector3(351.38, -588.24, 27.7),
        message = 'desk_message_ems_1',
        number = 'ambulance'
    },
    HOSPITAL_LEVEL_3 = {
        coords = vector3(308.47, -592.13, 42.2),
        message = 'desk_message_ems_3',
        number = 'ambulance'
    }
}
Config.DrawDistance         = 100.0
Config.MarkerColor          = {r = 0, g = 90, b = 210, a = 255}
Config.MarkerType           =  1
Config.MarkerSize           = {x = 1.0, y = 1.0, z = 0.5}
Config.Countdown            = 60
