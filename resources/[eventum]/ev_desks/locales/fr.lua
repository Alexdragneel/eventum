Locales['fr'] = {
    ['call_for_help']       = 'Appuyez sur ~INPUT_PICKUP~ pour demander une assistance',
    ['please_wait']         = 'Merci de patienter, quelqu\'un va venir s\'occuper de vous !',
    ['desk_message_llpd']   = 'Besoin d\'assistance à l\'accueil du commissariat',
    ['desk_message_ems_1']  = 'Besoin d\'assistance à l\'accueil de l\'hôpital (Niveau 1)',
    ['desk_message_ems_3']  = 'Besoin d\'assistance à l\'accueil de l\'hôpital (Niveau 3)',
}
