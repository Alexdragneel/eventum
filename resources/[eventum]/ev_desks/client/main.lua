ESX = nil
local isInMarker, hasAlreadyEnteredMarker = false, false
local currentDesk = nil
local countDown = 0

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

-- Draw Markers + detection --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)

		local playerCoords = GetEntityCoords(PlayerPedId())
		isInMarker = false

		for k, desk in pairs(Config.Desks) do
			
			local distance = GetDistanceBetweenCoords(playerCoords, desk.coords, true)
		
			if distance < Config.DrawDistance and countDown == 0 then
				DrawMarker(Config.MarkerType, desk.coords, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
			end

			if distance < Config.MarkerSize.x * 1.5 then
				isInMarker = true
				currentDesk = desk
				if countDown == 0 then
					ESX.ShowHelpNotification(_U('call_for_help'))
				else
					ESX.ShowHelpNotification(_U('please_wait'))
				end
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			currentDesk = nil
		end
	end
end)
-- --

-- Key Controls --
Citizen.CreateThread(function()
	while true do
		if currentDesk ~= nil and countDown == 0 then
			Citizen.Wait(0)
			if IsControlJustReleased(0, 38) and isInMarker and currentDesk ~= nil then
				countDown = Config.Countdown
				PlaySoundFrontend(-1, "Hack_Success", "DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS", 1)
				TriggerServerEvent('gcPhone:sendMessage', currentDesk.number, _U(currentDesk.message))
			end
		else
			Citizen.Wait(1000)
			if countDown > 0 then
				countDown = countDown - 1
			end
		end
	end
end)
-- --
