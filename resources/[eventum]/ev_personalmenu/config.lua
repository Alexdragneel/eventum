Config = {}

-- LANGUAGE --
Config.Locale = 'fr'

-- GENERAL --
Config.MenuTitle = 'Los Plantos'
Config.NoclipSpeed = 0.5 --if shift doubler

-- CONTROLS --
Config.Controls = {
	OpenMenu = {keyboard = 166},
	HandsUP = {keyboard = 243},
	Pointing = {keyboard = 29},
	Crouch = {keyboard = 36},
	StopTasks = {keyboard = 73},
	TPMarker = {keyboard1 = 19, keyboard2 = 38},
	ReviveMe = {keyboard1 = 19, keyboard2 = 45}
}

-- ANIMATIONS --

Config.AnimationShortcutsCount = 9

Config.Animations = {
	{
		name = 'party',
		label = _U('animation_party_title'),
		items = {
			{label = _U('animation_party_smoke'), type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING"}},
			--{label = _U('animation_party_guitar'), type = "anim", data = {lib = "amb@world_human_musician@guitar@male@idle_a", anim = "idle_a", isLoop = 1}},
			--{label = _U('animation_party_bongos'), type = "anim", data = {lib = "amb@world_human_musician@bongos@male@idle_a", anim = "idle_a", isLoop = 1}},
			{label = _U('animation_party_playsong'), type = "scenario", data = {anim = "WORLD_HUMAN_MUSICIAN"}},
			{label = _U('animation_party_dj'), type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@dj", anim = "dj", isLoop = 0}},
			{label = _U('animation_party_beer'), type = "scenario", data = {anim = "WORLD_HUMAN_DRINKING"}},
			{label = _U('animation_party_dancing'), type = "scenario", data = {anim = "WORLD_HUMAN_PARTYING"}},
			{label = _U('animation_party_airguitar'), type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@air_guitar", anim = "air_guitar"}},
			{label = _U('animation_party_shagging'), type = "anim", data = {lib = "anim@mp_player_intcelebrationfemale@air_shagging", anim = "air_shagging"}},
			{label = _U('animation_party_rock'), type = "anim", data = {lib = "mp_player_int_upperrock", anim = "mp_player_int_rock", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_party_drunk'), type = "anim", data = {lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a"}},
			{label = _U('animation_party_vomit'), type = "anim", data = {lib = "oddjobs@taxi@tie", anim = "vomit_outside"}}
		}
	},
	{
		name = 'salute',
		label = _U('animation_salute_title'),
		items = {
			{label = _U('animation_salute_saluate'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_hello", isTopOnly = 1}},
			{label = _U('animation_salute_serrer'), type = "anim", data = {lib = "mp_common", anim = "givetake1_a"}},
			{label = _U('animation_salute_tchek'), type = "anim", data = {lib = "mp_ped_interaction", anim = "handshake_guy_a"}},
			{label = _U('animation_salute_bandit'), type = "anim", data = {lib = "mp_ped_interaction", anim = "hugs_guy_a"}},
			{label = _U('animation_salute_military'), type = "anim", data = {lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute", isLoop = 1, isTopOnly = 1}}
		}
	},
	{
		name = 'work',
		label = _U('animation_work_title'),
		items = {
			{label = _U('animation_work_fisherman'), type = "scenario", data = {anim = "world_human_stand_fishing"}},
			{label = _U('animation_work_inspect'), type = "anim", data = {lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f", isLoop = 0}},
			--{label = _U('animation_work_radio'), type = "anim", data = {lib = "random@arrests", anim = "generic_radio_chatter", isTopOnly = 1}},
			{label = _U('animation_work_circulation'), type = "scenario", data = {anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT"}},
			--{label = _U('animation_work_binoculars'), type = "scenario", data = {anim = "WORLD_HUMAN_BINOCULARS"}},
			{label = _U('animation_work_harvest'), type = "scenario", data = {anim = "world_human_gardener_plant"}},
			{label = _U('animation_work_repair'), type = "anim", data = {lib = "mini@repair", anim = "fixing_a_ped", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_work_observe'), type = "scenario", data = {anim = "CODE_HUMAN_MEDIC_KNEEL"}},
			{label = _U('animation_work_talk'), type = "anim", data = {lib = "oddjobs@taxi@driver", anim = "leanover_idle"}},
			{label = _U('animation_work_bill'), type = "anim", data = {lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger"}},
			{label = _U('animation_work_buy'), type = "anim", data = {lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper"}},
			{label = _U('animation_work_shot'), type = "anim", data = {lib = "mini@drinking", anim = "shots_barman_b"}},
			{label = _U('animation_work_picture'), type = "scenario", data = {anim = "WORLD_HUMAN_PAPARAZZI"}},
			{label = _U('animation_work_notes'), type = "scenario", data = {anim = "WORLD_HUMAN_CLIPBOARD"}},
			{label = _U('animation_work_hammer'), type = "scenario", data = {anim = "WORLD_HUMAN_HAMMERING"}},
			{label = _U('animation_work_beg'), type = "scenario", data = {anim = "WORLD_HUMAN_BUM_FREEWAY"}},
			{label = _U('animation_work_statue'), type = "scenario", data = {anim = "WORLD_HUMAN_HUMAN_STATUE"}}
		}
	},
	{
		name = 'applaud',
		label = _U('animation_applaud_title'),
		items = {
			--{label = _U('animation_mood_felicitate'), type = "scenario", data = {anim = "WORLD_HUMAN_CHEERING"}},
			{label = _U('animation_applaud_applaud8'), type = "anim", data = {lib = "amb@world_human_cheering@male_e", anim = "base", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_applaud_applaud4'), type = "anim", data = {lib = "amb@world_human_cheering@female_d", anim = "base", isLoop = 1}},
			{label = _U('animation_applaud_applaud5'), type = "anim", data = {lib = "amb@world_human_cheering@male_a", anim = "base", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_applaud_applaud1'), type = "anim", data = {lib = "amb@world_human_cheering@female_a", anim = "base", isLoop = 1}},
			{label = _U('animation_applaud_applaud3'), type = "anim", data = {lib = "amb@world_human_cheering@female_c", anim = "base", isLoop = 1}},
			{label = _U('animation_applaud_applaud6'), type = "anim", data = {lib = "amb@world_human_cheering@male_b", anim = "base", isLoop = 1}},
		}
	},
	{
		name = 'mood',
		label = _U('animation_mood_title'),
		items = {
			--{label = _U('animation_mood_felicitate'), type = "scenario", data = {anim = "WORLD_HUMAN_CHEERING"}},
			{label = _U('animation_mood_nice'), type = "anim", data = {lib = "mp_action", anim = "thanks_male_06", isTopOnly = 1}},
			{label = _U('animation_mood_you'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_point", isTopOnly = 1}},
			{label = _U('animation_mood_come'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft", isTopOnly = 1}}, 
			{label = _U('animation_mood_what'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on", isTopOnly = 1}},
			{label = _U('animation_mood_me'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_me", isTopOnly = 1}},
			{label = _U('animation_mood_seriously'), type = "anim", data = {lib = "anim@am_hold_up@male", anim = "shoplift_high", isTopOnly = 1}},
			{label = _U('animation_mood_tired'), type = "anim", data = {lib = "amb@world_human_jog_standing@male@idle_b", anim = "idle_d"}},
			{label = _U('animation_mood_shit'), type = "anim", data = {lib = "amb@world_human_bum_standing@depressed@idle_a", anim = "idle_a", isTopOnly = 1}},
			{label = _U('animation_mood_facepalm'), type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm", isTopOnly = 1}},
			{label = _U('animation_mood_calm'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_easy_now", isTopOnly = 1}},
			{label = _U('animation_mood_why'), type = "anim", data = {lib = "oddjobs@assassinate@multi@", anim = "react_big_variations_a"}},
			{label = _U('animation_mood_fear'), type = "anim", data = {lib = "amb@code_human_cower_stand@male@react_cowering", anim = "base_right", isLoop = 1}},
			{label = _U('animation_mood_fight'), type = "anim", data = {lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e"}},
			{label = _U('animation_mood_notpossible'), type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_damn", isTopOnly = 1}},
			{label = _U('animation_mood_embrace'), type = "anim", data = {lib = "mp_ped_interaction", anim = "kisses_guy_a", isTopOnly = 1}},
			{label = _U('animation_mood_fuckyou'), type = "anim", data = {lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01", isTopOnly = 1}},
			{label = _U('animation_mood_wanker'), type = "anim", data = {lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_mood_suicide'), type = "anim", data = {lib = "mp_suicide", anim = "pistol" }}
		}
	},
	{
		name = 'sports',
		label = _U('animation_sports_title'),
		items = {
			{label = _U('animation_sports_muscle'), type = "anim", data = {lib = "amb@world_human_muscle_flex@arms_at_side@base", anim = "base", isTopOnly = 1}},
			{label = _U('animation_sports_weightbar'), type = "anim", data = {lib = "amb@world_human_muscle_free_weights@male@barbell@base", anim = "base", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_sports_pushup'), type = "anim", data = {lib = "amb@world_human_push_ups@male@base", anim = "base", isLoop = 1}},
			{label = _U('animation_sports_abs'), type = "anim", data = {lib = "amb@world_human_sit_ups@male@base", anim = "base", isLoop = 1}},
			{label = _U('animation_sports_abs2'), type = "anim", data = {lib = "amb@world_human_sit_ups@male@idle_a", anim = "idle_c", isLoop = 1}},
			{label = _U('animation_sports_plank'), type = "anim", data = {lib = "missfam5_yoga", anim = "c3_pose", isLoop = 1}},

			{label = _U('animation_sports_meditating'), type = "anim", data = {lib = "rcmcollect_paperleadinout@", anim = "meditiate_idle", isLoop = 1}},
			{label = _U('animation_sports_yoga_circuit'), type = "anim", data = {lib = "amb@world_human_yoga@male@base", anim = "base_a", isLoop = 0}},
			{label = _U('animation_sports_yoga_circuit2'), type = "anim", data = {lib = "amb@world_human_yoga@female@base", anim = "base_c", isLoop = 0}},
			--{label = _U('animation_sports_yoga_pose'), type = "anim", data = {lib = "missfam5_yoga", anim = "a1_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose2'), type = "anim", data = {lib = "missfam5_yoga", anim = "a2_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose3'), type = "anim", data = {lib = "missfam5_yoga", anim = "a3_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose4'), type = "anim", data = {lib = "missfam5_yoga", anim = "b4_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose6'), type = "anim", data = {lib = "missfam5_yoga", anim = "c2_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose8'), type = "anim", data = {lib = "missfam5_yoga", anim = "c4_pose", isLoop = 1}},
			{label = _U('animation_sports_yoga_pose9'), type = "anim", data = {lib = "missfam5_yoga", anim = "c5_pose", isLoop = 1}},
		}
	},
	{
		name = 'waitwall',
		label = _U('animation_waitwall_title'),
		items = {
			{label = _U('animation_waitwall_handsdown'), type = "anim", data = {lib = "amb@world_human_leaning@male@wall@back@hands_together@base", anim = "base", isLoop = 1}},
			{label = _U('animation_waitwall_elbow'), type = "anim", data = {lib = "amb@world_human_leaning@female@wall@back@holding_elbow@base", anim = "base", isLoop = 1}},
			{label = _U('animation_waitwall_handup'), type = "anim", data = {lib = "amb@world_human_leaning@female@wall@back@hand_up@base", anim = "base", isLoop = 1}},
			{label = _U('animation_waitwall_crosslegs'), type = "anim", data = {lib = "amb@world_human_leaning@male@wall@back@legs_crossed@base", anim = "base", isLoop = 1}},
			{label = _U('animation_waitwall_footup'), type = "anim", data = {lib = "amb@world_human_leaning@male@wall@back@foot_up@base", anim = "base", isLoop = 1}},
			--{label = _U('animation_waitwall_smokef'), type = "anim", data = {lib = "amb@world_human_leaning@female@smoke@idle_a", anim = "idle_b", isLoop = 1}},
			--{label = _U('animation_waitwall_smokem'), type = "anim", data = {lib = "amb@world_human_leaning@male@wall@back@smoking@idle_a", anim = "idle_b", isLoop = 1}},
			--{label = _U('animation_waitwall_coffeef'), type = "anim", data = {lib = "amb@world_human_leaning@female@coffee@idle_a", anim = "idle_c", isLoop = 1}},
			--{label = _U('animation_waitwall_coffeem'), type = "anim", data = {lib = "amb@world_human_leaning@male@coffee@idle_a", anim = "idle_b", isLoop = 1}},
			{label = _U('animation_waitwall_title'), type = "scenario", data = {anim = "world_human_leaning"}},
		}
	},
	{
		name = 'other',
		label = _U('animation_other_title'),
		items = {
			--{label = _U('animation_other_sit'), type = "anim", data = {lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle"}},
			{label = _U('animation_other_sit'), type = "scenario", data = {anim = "PROP_HUMAN_SEAT_CHAIR_MP_PLAYER"}},
			{label = _U('animation_other_sitfloor'), type = "anim", data = {lib = "anim@amb@business@bgen@bgen_no_work@", anim = "sit_phone_idle_02_nowork", isLoop = 1}},
			{label = _U('animation_other_sitfloor2'), type = "anim", data = {lib = "amb@world_human_picnic@female@base", anim = "base", isLoop = 1}},
			{label = _U('animation_other_sitfloor3'), type = "anim", data = {lib = "amb@world_human_picnic@male@base", anim = "base", isLoop = 1}},
			--{label = _U('animation_other_crossarms'), type = "anim", data = {lib = "oddjobs@assassinate@construction@", anim = "unarmed_fold_arms"}},
			{label = _U('animation_other_crossarms'), type = "anim", data = {lib = "missfbi_s4mop", anim = "guard_idle_a", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_other_guard'), type = "scenario", data = {anim = "WORLD_HUMAN_GUARD_STAND"}},
			{label = _U('animation_other_holdoutarms'), type = "anim", data = {lib = "missfam5_yoga", anim = "c1_pose", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_other_holdoutarms2'), type = "anim", data = {lib = "missfam5_yoga", anim = "c8_pose", isLoop = 1, isTopOnly = 1}},
			{label = _U('animation_other_surrender'), type = "anim", data = {lib = "random@arrests@busted", anim = "idle_c", isLoop = 1, isTopOnly = 1}},
			--{label = _U('animation_other_waitwall'), type = "scenario", data = {anim = "world_human_leaning"}},
			{label = _U('animation_other_ontheback'), type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE_BACK"}},
			{label = _U('animation_other_stomach'), type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE"}},
			{label = _U('animation_other_clean'), type = "scenario", data = {anim = "world_human_maid_clean"}},
			{label = _U('animation_other_cooking'), type = "scenario", data = {anim = "PROP_HUMAN_BBQ"}},
			{label = _U('animation_other_search'), type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_bj_to_prop_female"}},
			{label = _U('animation_other_selfie'), type = "scenario", data = {anim = "world_human_tourist_mobile"}},
			{label = _U('animation_other_door'), type = "anim", data = {lib = "mini@safe_cracking", anim = "idle_base"}}
		}
	},
	{
		name = 'dance',
		label = _U('animation_dance_title'),
		items = {
			{label = _U('animation_dance_female1'), type = "anim", data = {lib = "anim@amb@nightclub@mini@dance@dance_solo@female@var_a@", anim = "high_center", isLoop = 1}},
			{label = _U('animation_dance_female2'), type = "anim", data = {lib = "anim@amb@nightclub@mini@dance@dance_solo@female@var_a@", anim = "high_center_down", isLoop = 1}},
			{label = _U('animation_dance_male1'), type = "anim", data = {lib = "anim@amb@nightclub@mini@dance@dance_solo@male@var_b@", anim = "high_right_down", isLoop = 1}},
			{label = _U('animation_dance_male2'), type = "anim", data = {lib = "anim@amb@nightclub@mini@dance@dance_solo@male@var_b@", anim = "low_left", isLoop = 1}},
			{label = _U('animation_dance_nightclub3'), type = "anim", data = {lib = "anim@amb@nightclub@djs@black_madonna@", anim = "dance_a_loop_blamadon", isLoop = 1}},--Danse applaudir
			{label = _U('animation_dance_nightclub1'), type = "anim", data = {lib = "anim@amb@nightclub@dancers@black_madonna_entourage@", anim = "hi_dance_facedj_09_v2_male^5", isLoop = 1}}, -- Danse dehanche
			{label = _U('animation_dance_nightclub2'), type = "anim", data = {lib = "anim@amb@nightclub@dancers@crowddance_groups@", anim = "hi_dance_crowd_09_v2_male^5", isLoop = 1}},-- Danse du singe
			{label = _U('animation_dance_nightclub4'), type = "anim", data = {lib = "anim@amb@nightclub@dancers@crowddance_groups@", anim = "hi_dance_crowd_11_v1_female^5", isLoop = 1}},--n3
			{label = _U('animation_dance_strip1'), type = "anim", data = {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f", isLoop = 1}},
			{label = _U('animation_dance_strip2'), type = "anim", data = {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2", isLoop = 1}},
			{label = _U('animation_dance_stripfloor'), type = "anim", data = {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3", isLoop = 1}},
			{label = _U('animation_dance_lap'), type = "anim", data = {lib = "mp_safehouse", anim = "lap_dance_girl", isLoop = 1}},--LapDance
		}
	},
	--[[{
		name = 'pegi',
		label = _U('animation_pegi_title'),
		items = {
			{label = _U('animation_pegi_hsuck'), type = "anim", data = {lib = "oddjobs@towing", anim = "m_blow_job_loop"}},
			{label = _U('animation_pegi_fsuck'), type = "anim", data = {lib = "oddjobs@towing", anim = "f_blow_job_loop"}},
			{label = _U('animation_pegi_hfuck'), type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_player"}},
			{label = _U('animation_pegi_ffuck'), type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_female"}},
			{label = _U('animation_pegi_scratch'), type = "anim", data = {lib = "mp_player_int_uppergrab_crotch", anim = "mp_player_int_grab_crotch"}},
			{label = _U('animation_pegi_charm'), type = "anim", data = {lib = "mini@strip_club@idles@stripper", anim = "stripper_idle_02"}},
			{label = _U('animation_pegi_golddigger'), type = "scenario", data = {anim = "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"}},
			{label = _U('animation_pegi_breast'), type = "anim", data = {lib = "mini@strip_club@backroom@", anim = "stripper_b_backroom_idle_b"}},
			{label = _U('animation_pegi_strip1'), type = "anim", data = {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f"}},
			{label = _U('animation_pegi_strip2'), type = "anim", data = {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2"}},
			{label = _U('animation_pegi_stripfloor'), type = "anim", data = {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3"}}
		}
	},]]--
	{
		name = 'duo',
		label = _U('animation_duo_title'),
		items = {
			{label = _U('animation_duo_handshake'), type = "duo",
				data = {lib = "mp_ped_interaction", anim = "handshake_guy_a", 
						lib2 = "mp_ped_interaction", anim2 = "handshake_guy_b"}},

			{label = _U('animation_duo_hug'), type = "duo",
				data = {lib = "mp_ped_interaction", anim = "kisses_guy_a", 
						lib2 = "mp_ped_interaction", anim2 = "kisses_guy_b"}},

			{label = _U('animation_duo_bro'), type = "duo",
				data = {lib = "mp_ped_interaction", anim = "hugs_guy_a", 
						lib2 = "mp_ped_interaction", anim2 = "hugs_guy_b"}},

			{label = _U('animation_duo_give'), type = "duo",
				data = {lib = "mp_common", anim = "givetake1_a", 
						lib2 = "mp_common", anim2 = "givetake1_b"}},

			{label = _U('animation_duo_baseball'), type = "duo",
				data = {lib = "anim@arena@celeb@flat@paired@no_props@", anim = "baseball_a_player_a", 
						lib2 = "anim@arena@celeb@flat@paired@no_props@", anim2 = "baseball_a_player_b"}},
			{label = _U('animation_duo_baseball2'), type = "duo",
				data = {lib = "anim@arena@celeb@flat@paired@no_props@", anim = "baseball_a_player_b", 
						lib2 = "anim@arena@celeb@flat@paired@no_props@", anim2 = "baseball_a_player_a"}},
		}
	},
	{
		name = 'attitudes',
		label = _U('animation_attitudes_title'),
		items = {
			{label = "Par défaut", type = "attitude", data = {anim = nil}},
			{label = "Normal M", type = "attitude", data = {anim = "move_m@confident"}},
			{label = "Normal F", type = "attitude", data = {anim = "move_f@heels@c"}},
			{label = "Depressif", type = "attitude", data = {anim = "move_m@depressed@a"}},
			{label = "Depressif F", type = "attitude", data = {anim = "move_f@depressed@a"}},
			{label = "Business", type = "attitude", data = {anim = "move_m@business@a"}},
			{label = "Determine", type = "attitude", data = {anim = "move_m@brave@a"}},
			{label = "Casual", type = "attitude", data = {anim = "move_m@casual@a"}},
			{label = "Trop mange", type = "attitude", data = {anim = "move_m@fat@a"}},
			{label = "Hipster", type = "attitude", data = {anim = "move_m@hipster@a"}},
			{label = "Blesse", type = "attitude", data = {anim = "move_m@injured"}},
			{label = "Intimide", type = "attitude", data = {anim = "move_m@hurry@a"}},
			{label = "Hobo", type = "attitude", data = {anim = "move_m@hobo@a"}},
			{label = "Malheureux", type = "attitude", data = {anim = "move_m@sad@a"}},
			{label = "Muscle", type = "attitude", data = {anim = "move_m@muscle@a"}},
			{label = "Choc", type = "attitude", data = {anim = "move_m@shocked@a"}},
			{label = "Sombre", type = "attitude", data = {anim = "move_m@shadyped@a"}},
			{label = "Fatigue", type = "attitude", data = {anim = "move_m@buzzed"}},
			{label = "Pressee", type = "attitude", data = {anim = "move_m@hurry_butch@a"}},
			{label = "Fier", type = "attitude", data = {anim = "move_m@money"}},
			{label = "Petite course", type = "attitude", data = {anim = "move_m@quick"}},
			{label = "Mangeuse d'homme", type = "attitude", data = {anim = "move_f@maneater"}},
			{label = "Impertinent", type = "attitude", data = {anim = "move_f@sassy"}},
			{label = "Arrogante", type = "attitude", data = {anim = "move_f@arrogant@a"}},
			{label = "Personne âgée", type = "attitude", data = {anim = "move_heist_lester"}},
			{label = "Shooté", type = "attitude", data = {anim = "MOVE_M@BAIL_BOND_TAZERED"}},
			{label = "Jogging", type = "attitude", data = {anim = "move_m@JOG@"}},
		}
	}
}

-- ADMIN --
Config.Admin = {
	{	
		categoryName = 'tp',
		categoryLabel = _U('admin_cat_tp'),
		elements = {
			{
				name = 'goto',
				label = _U('admin_goto_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin', 'mod'},
				additionalInfos = 'Pour se téléporter à un joueur',
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectPlayer', function(targetId)
						TriggerServerEvent('lp_personalmenu:Admin_BringS', GetPlayerServerId(PlayerId()), targetId)
						ESX.UI.Menu.CloseAll()
					end)
				end
			},
			{
				name = 'bring',
				label = _U('admin_bring_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin', 'mod'},
				additionalInfos = 'Pour téléporter un joueur sur moi',
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectPlayer', function(targetId)
						TriggerServerEvent('lp_personalmenu:Admin_BringS', targetId, GetPlayerServerId(PlayerId()))
						ESX.UI.Menu.CloseAll()
					end)
				end
			},
			{
				name = 'tpmarker',
				label = _U('admin_tpmarker_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				additionalInfos = 'Pour se téléporter au marqueur',
				command = function()
					local waypointHandle = GetFirstBlipInfoId(8)

					if DoesBlipExist(waypointHandle) then
						Citizen.CreateThread(function()
							local waypointCoords = GetBlipInfoIdCoord(waypointHandle)
							local foundGround, zCoords, zPos = false, -500.0, 0.0

							while not foundGround do
								zCoords = zCoords + 10.0
								RequestCollisionAtCoord(waypointCoords.x, waypointCoords.y, zCoords)
								Citizen.Wait(0)
								foundGround, zPos = GetGroundZFor_3dCoord(waypointCoords.x, waypointCoords.y, zCoords)

								if not foundGround and zCoords >= 2000.0 then
									foundGround = true
								end
							end

							SetPedCoordsKeepVehicle(GetPlayerPed(-1), waypointCoords.x, waypointCoords.y, zPos)
							ESX.ShowNotification(_U('admin_tpmarker'))
						end)
					else
						ESX.ShowNotification(_U('admin_nomarker'))
					end
				end
			},
			{
				name = 'tpxyz',
				label = _U('admin_tpxyz_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local pos = KeyboardInput('BOX_XYZ', _U('dialogbox_xyz'), '', 50)

					if pos ~= nil and pos ~= '' then
						local _, _, x, y, z = string.find(pos, '([%d%.]+) ([%d%.]+) ([%d%.]+)')
								
						if x ~= nil and y ~= nil and z ~= nil then
							SetEntityCoords(GetPlayerPed(-1), x + .0, y + .0, z + .0)
						else
							_, _, x, y, z = string.find(pos, '([%d%.]+),([%d%.]+),([%d%.]+)')
							if x ~= nil and y ~= nil and z ~= nil then
								SetEntityCoords(GetPlayerPed(-1), x + .0, y + .0, z + .0)
							else
								_, _, x, y, z = string.find(pos, '([%d%.]+);([%d%.]+);([%d%.]+)')
								if x ~= nil and y ~= nil and z ~= nil then
									SetEntityCoords(GetPlayerPed(-1), x + .0, y + .0, z + .0)
								end
							end
						end
					end

					ESX.UI.Menu.CloseAll()
				end
			}
		}
	},
	{	
		categoryName = 'show',
		categoryLabel = _U('admin_cat_show'),
		elements = {
			{
				name = 'showxyz',
				label = _U('admin_showxyz_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin', 'mod'},
				command = function()
					Player.showCoords = not Player.showCoords
				end
			},
			{
				name = 'showname',
				label = _U('admin_showname_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin', 'mod'},
				command = function()
					Player.showName = not Player.showName

					if not showname then
						for targetPlayer, gamerTag in pairs(Player.gamerTags) do
							RemoveMpGamerTag(gamerTag)
							Player.gamerTags[targetPlayer] = nil
						end
					end
				end
			}
		}
	},
	{	
		categoryName = 'perso',
		categoryLabel = _U('admin_cat_perso'),
		elements = {
			{
				name = 'noclip',
				label = _U('admin_noclip_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin', 'mod'},
				boolean = function()
					return Player.noclip
				end,
				command = function()
					Player.noclip = not Player.noclip

					Player.ghostmode = false

					if Player.noclip then
						FreezeEntityPosition(GetPlayerPed(-1), true)
						SetEntityInvincible(GetPlayerPed(-1), true)
						SetEntityCollision(GetPlayerPed(-1), false, false)

						SetEntityVisible(GetPlayerPed(-1), false, false)

						SetEveryoneIgnorePlayer(PlayerId(), true)
						SetPoliceIgnorePlayer(PlayerId(), true)
						--ESX.ShowNotification(_U('admin_noclipon'))
					else
						FreezeEntityPosition(GetPlayerPed(-1), false)
						SetEntityInvincible(GetPlayerPed(-1), false)
						SetEntityCollision(GetPlayerPed(-1), true, true)

						SetEntityVisible(GetPlayerPed(-1), true, false)

						SetEveryoneIgnorePlayer(PlayerId(), false)
						SetPoliceIgnorePlayer(PlayerId(), false)
						--ESX.ShowNotification(_U('admin_noclipoff'))
					end

					RefreshAdminSubMenu()
				end
			},
			{
				name = 'godmode',
				label = _U('admin_godmode_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				boolean = function()
					return Player.godmode
				end,
				command = function()
					Player.godmode = not Player.godmode

					if Player.godmode then
						SetEntityInvincible(GetPlayerPed(-1), true)
						--ESX.ShowNotification(_U('admin_godmodeon'))
					else
						SetEntityInvincible(GetPlayerPed(-1), false)
						--ESX.ShowNotification(_U('admin_godmodeoff'))
					end

					RefreshAdminSubMenu()
				end
			},
			{
				name = 'ghostmode',
				label = _U('admin_ghostmode_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				boolean = function()
					return Player.ghostmode
				end,
				command = function()
					Player.ghostmode = not Player.ghostmode

					if Player.ghostmode then
						SetEntityVisible(GetPlayerPed(-1), false, false)
						--ESX.ShowNotification(_U('admin_ghoston'))
					else
						if not Player.noclip then
							SetEntityVisible(GetPlayerPed(-1), true, false)
							--ESX.ShowNotification(_U('admin_ghostoff'))
						end
					end

					RefreshAdminSubMenu()
				end
			},
			{
				name = 'revive',
				label = _U('admin_revive_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectPlayer', function(targetId)
						TriggerServerEvent('lp_personalmenu:Admin_revive', targetId)
					end)
				end
			},
			{
				name = 'heal',
				label = _U('admin_heal_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectPlayer', function(targetId)
						TriggerServerEvent('lp_personalmenu:Admin_heal', targetId)
					end)
				end
			},
			{
				name = 'setjob',
				label = _U('admin_setjob_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectJob', function(job, grade)
						TriggerServerEvent('lp_personalmenu:Admin_setJob', job, grade)
						RefreshAdminSubMenu()
					end)
				end
			},
			{
				name = 'toggleservice',
				label = _U('admin_toggleservice_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				boolean = function()
					return isInService
				end,
				command = function()
					TriggerEvent('lp_jobs:toggleService')
					isInService = not isInService
					RefreshAdminSubMenu()
				end
			}
		}
	},
	{	
		categoryName = 'vehicle',
		categoryLabel = _U('admin_cat_vehicle'),
		elements = {
			{
				name = 'spawnveh',
				label = _U('admin_spawnveh_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local modelName = KeyboardInput('BOX_VEHICLE_NAME', _U('dialogbox_vehiclespawner'), '', 50)

					if modelName ~= nil then
						modelName = tostring(modelName)
						if type(modelName) == 'string' then
							if IsModelInCdimage(GetHashKey(modelName)) then
								ESX.Game.SpawnVehicle(modelName, GetEntityCoords(GetPlayerPed(-1)), GetEntityHeading(GetPlayerPed(-1)), function(vehicle)
									TaskWarpPedIntoVehicle(GetPlayerPed(-1), vehicle, -1)
								end)
							end
						end
					end

					ESX.UI.Menu.CloseAll()
				end
			},
			{
				name = 'repairveh',
				label = _U('admin_repairveh_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local plyVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
					SetVehicleFixed(plyVeh)
					SetVehicleDirtLevel(plyVeh, 0.0)
				end
			},
			{
				name = 'fuelveh',
				label = _U('admin_fuelveh_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1))
					if vehicle > 0 then
						local plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))
						local fuelLevel = GetVehicleHandlingFloat(vehicle, "CHandlingData", "fPetrolTankVolume")
						TriggerServerEvent('esx_fuel:SetFuelLevel', plate, fuelLevel)
					end
				end
			},
			{
				name = 'flipveh',
				label = _U('admin_flipveh_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local plyCoords = GetEntityCoords(GetPlayerPed(-1))
					local newCoords = plyCoords + vector3(0.0, 2.0, 0.0)
					local closestVeh = GetClosestVehicle(plyCoords, 10.0, 0, 70)

					SetEntityCoords(closestVeh, newCoords)
					ESX.ShowNotification(_U('admin_vehicleflip'))
				end
			},
		}
	},
	{	
		categoryName = 'inventory',
		categoryLabel = _U('admin_cat_inventory'),
		elements = {
			{
				name = 'giveweapon',
				label = _U('admin_giveweapon_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()
					TriggerEvent('lp_personalmenu:Admin_SelectWeapon', function(weaponName)
						TriggerServerEvent('lp_personalmenu:Admin_giveWeapon', weaponName, 1000)
						ESX.UI.Menu.CloseAll()
					end)
				end
			},
			{
				name = 'giveammo',
				label = _U('admin_giveammo_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local _,weaponHash = GetCurrentPedWeapon(GetPlayerPed(-1), true)
					SetPedAmmo(GetPlayerPed(-1), weaponHash, 9999)
					TriggerServerEvent('lp_personalmenu:Admin_giveAmmo', weaponName, 1000)
				end
			},
			{
				name = 'clearweapon',
				label = _U('admin_clearweapon_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					ESX.UI.Menu.Open('confirm', GetCurrentResourceName(), 'confirm', {
						title    = _U('dialogbox_confirm_clearweapon'),
						align    = 'center',
					}, function(data, menu)
						ESX.SetPlayerData("loadout", {})
						TriggerServerEvent('lp_personalmenu:Admin_clearWeapon')
						menu.close()
					end, function(data, menu)
						menu.close()
					end)
				end
			},
			{
				name = 'giveitem',
				label = _U('admin_giveitem_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()

					TriggerEvent('lp_personalmenu:Admin_SelectItem', function(item, count)
						TriggerServerEvent('lp_personalmenu:Admin_giveItem', item, count)
						ESX.UI.Menu.CloseAll()
					end)
				end
			},
			{
				name = 'clearinventory',
				label = _U('admin_clearinventory_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					ESX.UI.Menu.Open('confirm', GetCurrentResourceName(), 'confirm', {
						title    = _U('dialogbox_confirm_clearinventory'),
						align    = 'center',
					}, function(data, menu)
						TriggerServerEvent('lp_personalmenu:Admin_clearInventory')
						menu.close()
					end, function(data, menu)
						menu.close()
					end)
				end
			}
		}
	},
	{	
		categoryName = 'money',
		categoryLabel = _U('admin_cat_money'),
		elements = {
			{
				name = 'givemoney',
				label = _U('admin_givemoney_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local amount = KeyboardInput('BOX_AMOUNT', _U('dialogbox_amount'), '', 8)

					if amount ~= nil then
						amount = tonumber(amount)

						if type(amount) == 'number' then
							TriggerServerEvent('lp_personalmenu:Admin_giveCash', amount)
						end
					end

					ESX.UI.Menu.CloseAll()
				end
			},
			{
				name = 'givebank',
				label = _U('admin_givebank_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local amount = KeyboardInput('BOX_AMOUNT', _U('dialogbox_amount'), '', 8)

					if amount ~= nil then
						amount = tonumber(amount)

						if type(amount) == 'number' then
							TriggerServerEvent('lp_personalmenu:Admin_giveBank', amount)
						end
					end

					ESX.UI.Menu.CloseAll()
				end
			},
			{
				name = 'givedirtymoney',
				label = _U('admin_givedirtymoney_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local amount = KeyboardInput('BOX_AMOUNT', _U('dialogbox_amount'), '', 8)

					if amount ~= nil then
						amount = tonumber(amount)

						if type(amount) == 'number' then
							TriggerServerEvent('lp_personalmenu:Admin_giveDirtyMoney', amount)
						end
					end

					ESX.UI.Menu.CloseAll()
				end
			}
		}
	},
	{	
		categoryName = 'apparence',
		categoryLabel = _U('admin_cat_apparence'),
		elements = {
			{
				name = 'changeskin',
				label = _U('admin_changeskin_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				rightArrow = true,
				command = function()
					ESX.UI.Menu.CloseAll()
					Citizen.Wait(100)
					TriggerEvent('esx_skin:openSaveableMenu')
				end
			},
			{
				name = 'saveskin',
				label = _U('admin_saveskin_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					TriggerEvent('esx_skin:requestSaveSkin')
				end
			},
			{
				name = 'changemodel',
				label = _U('admin_changemodel_button'),
				groups = {'_dev', 'owner', 'superadmin', 'admin'},
				command = function()
					local modelName = KeyboardInput('BOX_VEHICLE_NAME', _U('dialogbox_modelname'), '', 50)

					if modelName ~= nil then
						modelName = tostring(modelName)

						if type(modelName) == 'string' then
							TriggerEvent('lp_change_model:setModel', modelName)
							ESX.UI.Menu.CloseAll()
						end
					end
				end
			}
		}
	}
}

Config.AdminWeaponList = {
	{
		label = "Mêlée",
		weapons = {
			{ label = "Dague", name = "dagger"},
			{ label = "Batte de Baseball", name = "bat"},
			{ label = "Bouteille", name = "bottle"},
			{ label = "Pied de Biche", name = "crowbar"},
			{ label = "Lampe Torche", name = "flashlight"},
			{ label = "Club de Golf", name = "golfclub"},
			{ label = "Marteau", name = "hammer"},
			{ label = "Hache", name = "hatchet"},
			{ label = "knuckle", name = "knuckle"},
			{ label = "Couteau", name = "knife"},
			{ label = "Machete", name = "machete"},
			{ label = "switchblade", name = "switchblade"},
			{ label = "nightstick", name = "nightstick"},
			{ label = "wrench", name = "wrench"},
			{ label = "battleaxe", name = "battleaxe"},
			{ label = "poolcue", name = "poolcue"},
			{ label = "Hache en Pierre", name = "stone_hatchet"}
		}
	},
	{
		label = "Pistolets",
		weapons = {
			{ label = "Pistolet", name = "pistol"},
			{ label = "Pistolet Mk2", name = "pistol_mk2"},
			{ label = "Pistolet de Combat", name = "combatpistol"},
			{ label = "appistol", name = "appistol"},
			{ label = "Tazer", name = "stungun"},
			{ label = "pistol50", name = "pistol50"},
			{ label = "snspistol", name = "snspistol"},
			{ label = "snspistol_mk2", name = "snspistol_mk2"},
			{ label = "heavypistol", name = "heavypistol"},
			{ label = "vintagepistol", name = "vintagepistol"},
			{ label = "flaregun", name = "flaregun"},
			{ label = "marksmanpistol", name = "marksmanpistol"},
			{ label = "Revolver", name = "revolver"},
			{ label = "Revoler Mk2", name = "revolver_mk2"},
			{ label = "doubleaction", name = "doubleaction"},
			{ label = "raypistol", name = "raypistol"},
			{ label = "ceramicpistol", name = "ceramicpistol"},
			{ label = "navyrevolver", name = "navyrevolver"}
		}
	},
	{
		label = "Mitraillettes",
		weapons = {
			{ label = "microsmg", name = "microsmg"},
			{ label = "smg", name = "smg"},
			{ label = "smg_mk2", name = "smg_mk2"},
			{ label = "assaultsmg", name = "assaultsmg"},
			{ label = "combatpdw", name = "combatpdw"},
			{ label = "machinepistol", name = "machinepistol"},
			{ label = "minismg", name = "minismg"},
			{ label = "raycarbine", name = "raycarbine"}
		}
	},
	{
		label = "Pompes",
		weapons = {
			{ label = "pumpshotgun", name = "pumpshotgun"},
			{ label = "pumpshotgun_mk2", name = "pumpshotgun_mk2"},
			{ label = "sawnoffshotgun", name = "sawnoffshotgun"},
			{ label = "assaultshotgun", name = "assaultshotgun"},
			{ label = "bullpupshotgun", name = "bullpupshotgun"},
			{ label = "musket", name = "musket"},
			{ label = "heavyshotgun", name = "heavyshotgun"},
			{ label = "dbshotgun", name = "dbshotgun"},
			{ label = "autoshotgun", name = "autoshotgun"}
		}
	},
	{
		label = "Fusils d'Assauts",
		weapons = {
			{ label = "assaultrifle", name = "assaultrifle"},
			{ label = "assaultrifle_mk2", name = "assaultrifle_mk2"},
			{ label = "carbinerifle", name = "carbinerifle"},
			{ label = "carbinerifle_mk2", name = "carbinerifle_mk2"},
			{ label = "advancedrifle", name = "advancedrifle"},
			{ label = "specialcarbine", name = "specialcarbine"},
			{ label = "specialcarbine_mk2", name = "specialcarbine_mk2"},
			{ label = "bullpuprifle", name = "bullpuprifle"},
			{ label = "bullpuprifle_mk2", name = "bullpuprifle_mk2"},
			{ label = "compactrifle", name = "compactrifle"}
		}
	},
	{
		label = "Mitrailleuses Lourdes",
		weapons = {
			{ label = "mg", name = "mg"},
			{ label = "combatmg", name = "combatmg"},
			{ label = "combatmg_mk2", name = "combatmg_mk2"},
			{ label = "Gusenberg", name = "gusenberg"}
		}
	},
	{
		label = "Snipers",
		weapons = {
			{ label = "Sniper", name = "sniperrifle"},
			{ label = "Sniper Lourd", name = "heavysniper"},
			{ label = "Sniper Lourd Mk2", name = "heavysniper_mk2"},
			{ label = "Fusil Tactique", name = "marksmanrifle"},
			{ label = "Fusil Tactique Mk2", name = "marksmanrifle_mk2"}
		}
	},
	{
		label = "Armes Lourdes",
		weapons = {
			{ label = "RPG", name = "rpg"},
			{ label = "Lance-Grenades", name = "grenadelauncher"},
			{ label = "Lance-Grenades de fumée", name = "grenadelauncher_smoke"},
			{ label = "Minigun", name = "minigun"},
			{ label = "Lance-Feu d'artifice", name = "firework"},
			{ label = "Railgun", name = "railgun"},
			{ label = "hominglauncher", name = "hominglauncher"},
			{ label = "compactlauncher", name = "compactlauncher"},
			{ label = "Minigun Lazer", name = "rayminigun"}
		}
	},
	{
		label = "Projectiles",
		weapons = {
			{ label = "Grenade", name = "grenade"},
			{ label = "Grenade à Gaz", name = "bzgas"},
			{ label = "Grenade de Fumée", name = "smokegrenade"},
			{ label = "flare", name = "flare"},
			{ label = "Cocktail Molotof", name = "molotov"},
			{ label = "Bombe Collante", name = "stickybomb"},
			{ label = "Mine de Proximité", name = "proxmine"},
			{ label = "Boule de Neige", name = "snowball"},
			{ label = "pipebomb", name = "pipebomb"},
			{ label = "Ball de Baseball", name = "ball"}
		}
	},
	{
		label = "Divers",
		weapons = {
			{ label = "Bidon d'Essence", name = "petrolcan"},
			{ label = "Extincteur", name = "fireextinguisher"},
			{ label = "Parachute", name = "parachute"},
			{ label = "hazardcan", name = "hazardcan"}
		}
	}
}
