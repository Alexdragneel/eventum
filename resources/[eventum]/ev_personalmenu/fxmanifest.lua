fx_version "adamant"

game "gta5"

description "Los Plantos Personal Menu"

version "1.0.0"

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'locales/fr.lua',
	'config.lua',
	'server/main.lua'
}

client_scripts {
	'@es_extended/locale.lua',
	'locales/fr.lua',
	'config.lua',
	'client/main.lua',
	'client/other.lua'
}

dependencies {
    'es_extended',
	'esx_service',
    'teb_speed_control'
}