ESX = nil

local PersonalMenu = {
	ItemSelected = {},
	ItemIndex = {},
	WeaponData = {},
	WalletIndex = {},
	WalletList = {_U('wallet_option_give'), _U('wallet_option_drop')},
	ClothesButtons = {'torso', 'pants', 'shoes', 'bag', 'bproof'},
	AccessoriesButtons = {'ears', 'glasses', 'helmet', 'mask', 'chain', 'bracelets', 'watches'},
	DoorState = {
		FrontLeft = false,
		FrontRight = false,
		BackLeft = false,
		BackRight = false,
		Hood = false,
		Trunk = false
	},
	DoorIndex = 1,
	VoiceIndex = 2,
	VoiceList = {},
	ExtraIndex = 1,
	ExtraList = {
		extraLabels = {},
		extraIds = {},
		states = {}
	},
	AdminMenuSelected = nil,
}

Player = {
	isDead = false,
	inAnim = false,
	crouched = false,
	handsup = false,
	pointing = false,
	noclip = false,
	godmode = false,
	ghostmode = false,
	showCoords = false,
	showName = false,
	gamerTags = {},
	group = 'user'
}

local societymoney, societymoney2 = nil, nil

isInService = false

local animationMenuForShortcut = false
local animationShortcutToEdit = -1
local animationShortcutsList = {}
local animationShortcutsAreDisabled = false

currentAttitude = nil

local plyPed = nil

local isLimiterOn = false

local rightArrow = '&#10095;&#10095;'
local itemIsSelected = '<span style="color:#FF00A5; font-weight:bolder;">&#9673;</span>'
local itemIsNotSelected = '<span style="color:gray; font-weight:bolder;">&#9678;</span>'

------------------------
-- Keymapping Commands--
------------------------

for i = 1, Config.AnimationShortcutsCount, 1 do
	RegisterCommand('animation_shortcut_' .. i, function()
		local anim = animationShortcutsList['anim' .. i]
		if anim ~= nil and not IsPedInAnyVehicle(PlayerPedId(), false) and not ESX.UI.Menu.MenuIsOpen() and not animationShortcutsAreDisabled and not Player.isDead then
			PlayAnimationWithTypeAndData(anim.type, anim.data, anim.name)
		end
	end, false)

	RegisterKeyMapping('animation_shortcut_' .. i, _U('animation_shortcut_i', i), 'keyboard', 'NUMPAD'..i)
end

RegisterCommand("set_speed_control", function()
	if isLimiterOn == false and IsPedInAnyVehicle(PlayerPedId(), false) then
		if IsPedDriving() then
			OpenCustomSpeedLimiterDialog()
		else
			ShowNotDrivingNotification()
		end	
	elseif isLimiterOn == true and IsPedInAnyVehicle(PlayerPedId(), false) then
		if IsPedDriving() then
			TurnSpeedLimiterOff()
		else
			ShowNotDrivingNotification()
		end	
	end
end)

RegisterKeyMapping('set_speed_control', _U('vehicle_speed_control'), 'keyboard', 'J')

-------------------------
-------------------------


Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(10)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	ESX.PlayerData = ESX.GetPlayerData()

	while actualSkin == nil do
		TriggerEvent('skinchanger:getSkin', function(skin) actualSkin = skin end)
		Citizen.Wait(100)
	end

	RefreshMoney()

	PersonalMenu.WeaponData = ESX.GetWeaponList()

	for i = 1, #PersonalMenu.WeaponData, 1 do
		if PersonalMenu.WeaponData[i].name == 'WEAPON_UNARMED' then
			PersonalMenu.WeaponData[i] = nil
		else
			PersonalMenu.WeaponData[i].hash = GetHashKey(PersonalMenu.WeaponData[i].name)
		end
	end

	ESX.TriggerServerCallback('lp_personalmenu:requestPlayerAnimationShortcuts', function(anAnimationShortcutsList)
		if anAnimationShortcutsList then
			animationShortcutsList = anAnimationShortcutsList
		end
	end)
end)

----------------
-- ESX Events --
----------------

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	ESX.PlayerData = xPlayer
end)

AddEventHandler('esx:onPlayerDeath', function()
	Player.isDead = true
	ESX.UI.Menu.CloseAll()
	ESX.UI.Menu.CloseAll()
end)

AddEventHandler('playerSpawned', function()
	Player.isDead = false
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
	RefreshMoney()
end)

RegisterNetEvent('esx:setJob2')
AddEventHandler('esx:setJob2', function(job2)
	ESX.PlayerData.job2 = job2
	RefreshMoney2()
end)

RegisterNetEvent("esx_service:disableService")
AddEventHandler("esx_service:disableService", function()
	isInService = false
end)

RegisterNetEvent("esx_service:enableService")
AddEventHandler("esx_service:enableService", function()
	isInService = true
end)

RegisterNetEvent('esx_addonaccount:setMoney')
AddEventHandler('esx_addonaccount:setMoney', function(society, money)
	if ESX.PlayerData.job ~= nil and (ESX.PlayerData.job.grade_name == 'boss' or ESX.PlayerData.job.grade_name == 'captain') and 'society_' .. ESX.PlayerData.job.name == society then
		societymoney = ESX.Math.GroupDigits(money)
	end

	if ESX.PlayerData.job2 ~= nil and ESX.PlayerData.job2.grade_name == 'boss' and 'society_' .. ESX.PlayerData.job2.name == society then
		societymoney2 = ESX.Math.GroupDigits(money)
	end
end)

------------------
-- LOOP THREADS --
------------------

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if IsControlJustReleased(0, Config.Controls.OpenMenu.keyboard) and not Player.isDead then
			ESX.TriggerServerCallback('lp_personalmenu:Admin_getUsergroup', function(plyGroup)
				Player.group = plyGroup
				PersonalMenu.ExtraList = {
					extraLabels = {},
					extraIds = {},
					states = {}
				}
				PersonalMenu.ExtraIndex = 1
				ESX.PlayerData = ESX.GetPlayerData()
				OpenMainMenu()
			end)
        end
    end
end)

Citizen.CreateThread(function()
	while true do
		plyPed = PlayerPedId()

		if IsControlJustReleased(0, Config.Controls.StopTasks.keyboard) and IsInputDisabled(2) and not Player.isDead then
			Player.handsup, Player.pointing = false, false
			SetCurrentPedWeapon(plyPed, GetHashKey("WEAPON_UNARMED"), true)
			ClearPedTasks(plyPed)
		end

		if IsControlPressed(1, Config.Controls.TPMarker.keyboard1) and IsControlJustReleased(1, Config.Controls.TPMarker.keyboard2) and IsInputDisabled(2) and not Player.isDead then
			ESX.TriggerServerCallback('lp_personalmenu:Admin_getUsergroup', function(plyGroup)
				if plyGroup ~= nil and (plyGroup == 'mod' or plyGroup == 'admin' or plyGroup == 'superadmin' or plyGroup == 'owner' or plyGroup == '_dev') then
					local waypointHandle = GetFirstBlipInfoId(8)

					if DoesBlipExist(waypointHandle) then
						Citizen.CreateThread(function()
							local waypointCoords = GetBlipInfoIdCoord(waypointHandle)
							local foundGround, zCoords, zPos = false, -500.0, 0.0

							while not foundGround do
								zCoords = zCoords + 10.0
								RequestCollisionAtCoord(waypointCoords.x, waypointCoords.y, zCoords)
								Citizen.Wait(0)
								foundGround, zPos = GetGroundZFor_3dCoord(waypointCoords.x, waypointCoords.y, zCoords)

								if not foundGround and zCoords >= 2000.0 then
									foundGround = true
								end
							end

							SetPedCoordsKeepVehicle(plyPed, waypointCoords.x, waypointCoords.y, zPos)
							ESX.ShowNotification(_U('admin_tpmarker'))
						end)
					else
						ESX.ShowNotification(_U('admin_nomarker'))
					end
				end
			end)
		end

		if IsControlPressed(1, Config.Controls.ReviveMe.keyboard1) and IsControlJustReleased(1, Config.Controls.ReviveMe.keyboard2) and IsInputDisabled(2) and Player.isDead then
			ESX.TriggerServerCallback('lp_personalmenu:Admin_getUsergroup', function(plyGroup)
				if plyGroup ~= nil and (plyGroup == 'mod' or plyGroup == 'admin' or plyGroup == 'superadmin' or plyGroup == 'owner' or plyGroup == '_dev') then
					TriggerServerEvent('lp_personalmenu:Admin_reviveMyself', plyPed)
				end
			end)
		end

		if Player.showCoords then
			local plyCoords = GetEntityCoords(plyPed, false)
			Text('~r~X~s~: ' .. ESX.Math.Round(plyCoords.x, 2) .. '\n~b~Y~s~: ' .. ESX.Math.Round(plyCoords.y, 2) .. '\n~g~Z~s~: ' .. ESX.Math.Round(plyCoords.z, 2) .. '\n~y~Angle~s~: ' .. ESX.Math.Round(GetEntityPhysicsHeading(plyPed), 2))
		end

		if Player.noclip then
			local plyCoords = GetEntityCoords(plyPed, false)
			local camCoords = GetCamDirection()
			local camCoords2 = GetCamHorizontalDirection()
			SetEntityVelocity(plyPed, 0.01, 0.01, 0.01)

            local noClipSpeed = Config.NoclipSpeed
            if IsControlPressed(0, 21) then
                noClipSpeed = noClipSpeed * 2
            end

			if IsControlPressed(0, 32) then
				plyCoords = plyCoords + (noClipSpeed * camCoords)
			end

			if IsControlPressed(0, 33) then
				plyCoords = plyCoords - (noClipSpeed * camCoords)
			end

			if IsControlPressed(0, 34) then
				plyCoords = plyCoords + (noClipSpeed/2 * camCoords2)
			end

			if IsControlPressed(0, 35) then
				plyCoords = plyCoords - (noClipSpeed/2 * camCoords2)
			end

			SetEntityCoordsNoOffset(plyPed, plyCoords, true, true, true)
		end

		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	while true do
		if Player.showName then
			for k, v in ipairs(ESX.Game.GetPlayers()) do
				local otherPed = GetPlayerPed(v)

				if otherPed ~= plyPed then
					if #(GetEntityCoords(plyPed, false) - GetEntityCoords(otherPed, false)) < 5000.0 then
						Player.gamerTags[v] = CreateFakeMpGamerTag(otherPed, ('[%s] %s'):format(GetPlayerServerId(v), GetPlayerName(v)), false, false, '', 0)
					else
						RemoveMpGamerTag(Player.gamerTags[v])
						Player.gamerTags[v] = nil
					end
				end
			end
		end

		Citizen.Wait(100)
	end
end)

Citizen.CreateThread( function()
	while true do
	  	Citizen.Wait(50)		
	  	local playerPed = GetPlayerPed(-1)
	  	local playerVeh = GetVehiclePedIsUsing(playerPed)
  
	  	if playerVeh ~= 0 then
			SetPedHelmet(playerPed, false)
			RemovePedHelmet(playerPed, true)
		end
	end	
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)
		local playerPed = GetPlayerPed(-1)
		if isLimiterOn == true and (not IsPedInAnyVehicle(playerPed, false) or not IsPedDriving()) then
			isLimiterOn = false
		end
	end
end)

-----------------------
-- VARIOUS FUNCTIONS --
-----------------------

function HasAccessToAdminMenu()
    return Player.group ~= nil and (Player.group == 'mod' or Player.group == 'admin' or Player.group == 'superadmin' or Player.group == 'owner' or Player.group == '_dev')
end

function RefreshMoney()
	if ESX.PlayerData.job ~= nil and (ESX.PlayerData.job.grade_name == 'boss' or ESX.PlayerData.job.grade_name == 'captain') then
		ESX.TriggerServerCallback('esx_society:getSocietyMoney', function(money)
			societymoney = ESX.Math.GroupDigits(money)
		end, ESX.PlayerData.job.name)
	end
end

function CheckQuantity(number)
	number = tonumber(number)

	if type(number) == 'number' then
		number = ESX.Math.Round(number)

		if number > 0 then
			return true, number
		end
	end

	return false, number
end

function KeyboardInput(entryTitle, textEntry, inputText, maxLength)
	AddTextEntry(entryTitle, textEntry)
	DisplayOnscreenKeyboard(1, entryTitle, '', inputText, '', '', '', maxLength)

	while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
		Citizen.Wait(0)
	end

	if UpdateOnscreenKeyboard() ~= 2 then
		local result = GetOnscreenKeyboardResult()
		Citizen.Wait(500)
		return result
	else
		Citizen.Wait(500)
		return nil
	end
end

function startAnimAction(lib, anim)
	ESX.Streaming.RequestAnimDict(lib, function()
		TaskPlayAnim(plyPed, lib, anim, 8.0, 1.0, -1, 49, 0, false, false, false)
		RemoveAnimDict(lib)
	end)
end

function IsPedDriving()
	local playerPed = GetPlayerPed(-1)
	local currentVehicle = GetVehiclePedIsIn(playerPed, false)

	return IsPedConductor() and GetIsVehicleEngineRunning(currentVehicle)

end

function IsPedConductor()
	local playerPed = GetPlayerPed(-1)
	local currentVehicle = GetVehiclePedIsIn(playerPed, false)
	local driver = GetPedInVehicleSeat(currentVehicle, -1)

	return driver == playerPed

end

function ShowNotDrivingNotification()
	if IsPedConductor() then
		ESX.ShowNotification(_U('engine_off'))
	else
		ESX.ShowNotification(_U('not_driving'))
	end	
end

RegisterNetEvent('lp_personalmenu:enableShortcutAnimation')
AddEventHandler('lp_personalmenu:enableShortcutAnimation', function()
	
	animationShortcutsAreDisabled = false
	
end)

RegisterNetEvent('lp_personalmenu:disableShortcutAnimation')
AddEventHandler('lp_personalmenu:disableShortcutAnimation', function()
	
	animationShortcutsAreDisabled = true

end)

---------------
-- MAIN MENU --
---------------

function OpenMainMenu()
    local elements = {}
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('loadout_title')), value = 'loadout'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('wallet_title')), value = 'wallet'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('bills_title')), value = 'billing'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('clothes_title')), value = 'clothes'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('accessories_title')), value = 'accessories'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('animation_title')), value = 'animation'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('animation_shortcut_title')), value = 'animation_shortcut'})
    table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('vehicle_title')), value = 'vehicle'})

    if HasAccessToAdminMenu() then
        table.insert(elements, {label = ('%s <span> ' .. rightArrow .. ' </span>'):format(_U('admin_title')), value = 'admin'})
    end

    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'personal_menu', {
        title    = _U('menu_title'),
        align    = 'top-left',
        elements = elements
    }, function(data, menu)
        local menuToOpen = data.current.value
        if menuToOpen == 'loadout' then
            OpenLoadoutMenu()
        elseif menuToOpen == 'wallet' then
            OpenWalletMenu()
        elseif menuToOpen == 'billing' then
            OpenBillingMenu()
        elseif menuToOpen == 'clothes' then
            OpenClothesMenu()
        elseif menuToOpen == 'accessories' then
			OpenAccessoriesMenu()
        elseif menuToOpen == 'animation' then
			OpenAnimationsMenu()
        elseif menuToOpen == 'animation_shortcut' then
			OpenAnimationShortcutsMenu()
        elseif menuToOpen == 'vehicle' then
			OpenVehicleMenu()
        elseif menuToOpen == 'admin' then
			OpenAdminMenu()
        end
    end, function(data, menu)
        menu.close()
    end)
end

------------------
-- LOADOUT MENU --
------------------

function OpenLoadoutMenu()
    local elements = {}
    for i = 1, #PersonalMenu.WeaponData, 1 do
        if HasPedGotWeapon(plyPed, PersonalMenu.WeaponData[i].hash, false) then
            local ammo = GetAmmoInPedWeapon(plyPed, PersonalMenu.WeaponData[i].hash)
            table.insert(elements, {label = PersonalMenu.WeaponData[i].label .. ' [' .. ammo .. '] <span> ' .. rightArrow .. ' </span>', value = PersonalMenu.WeaponData[i]})
        end
    end

    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'loadout_menu', {
        title    = _U('loadout_title'),
        align    = 'top-left',
        elements = elements
    }, function(data, menu)
        OpenWeaponMenu(data.current.value)
    end, function(data, menu)
        menu.close()
    end)
end

function OpenWeaponMenu(weapon)
    if HasPedGotWeapon(plyPed, weapon.hash, false) then
        local elements = {}
        table.insert(elements, {label = _U('loadout_give_button') .. '<span> ' .. rightArrow .. ' </span>', value = 'give_weapon'})
        table.insert(elements, {label = _U('loadout_givemun_button') .. '<span> ' .. rightArrow .. ' </span>', value = 'give_ammo'})
        table.insert(elements, {label = _U('loadout_drop_button') .. '<span> ' .. rightArrow .. ' </span>', value = 'drop_weapon'})

		local ammo = GetAmmoInPedWeapon(plyPed, weapon.hash)
        ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'weapon_menu', {
            title    = weapon.label .. ' [' .. ammo .. ']',
            align    = 'top-left',
            elements = elements
        }, function(data, menu)
            menu.close()
            local option = data.current.value
            if option == 'give_weapon' then

                local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

                if closestDistance ~= -1 and closestDistance <= 3 then
                    local closestPed = GetPlayerPed(closestPlayer)

                    if IsPedOnFoot(closestPed) then
                        local ammo = GetAmmoInPedWeapon(plyPed, weapon.hash)
                        TriggerServerEvent('esx:giveInventoryItem', GetPlayerServerId(closestPlayer), 'item_weapon', weapon.name, ammo)
                        ESX.UI.Menu.CloseAll()
                    else
                        ESX.ShowNotification(_U('in_vehicle_give', weapon.label))
                    end
                else
                    ESX.ShowNotification(_U('players_nearby'))
                end

            elseif option == 'give_ammo' then

                local post, quantity = CheckQuantity(KeyboardInput('BOX_AMMO_AMOUNT', _U('dialogbox_amount_ammo'), '', 8))

                if post then
                    local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

                    if closestDistance ~= -1 and closestDistance <= 3 then
                        local closestPed = GetPlayerPed(closestPlayer)

                        if IsPedOnFoot(closestPed) then
                            local ammo = GetAmmoInPedWeapon(plyPed, weapon.hash)

                            if ammo > 0 then
                                if quantity <= ammo and quantity >= 0 then
                                    local finalAmmo = math.floor(ammo - quantity)
                                    SetPedAmmo(plyPed, weapon.name, finalAmmo)

                                    TriggerServerEvent('lp_personalmenu:Weapon_addAmmoToPedS', GetPlayerServerId(closestPlayer), weapon.name, quantity)
                                    ESX.ShowNotification(_U('gave_ammo', quantity))
                                    ESX.UI.Menu.CloseAll()
                                else
                                    ESX.ShowNotification(_U('not_enough_ammo'))
                                end
                            else
                                ESX.ShowNotification(_U('no_ammo'))
                            end
                        else
                            ESX.ShowNotification(_U('in_vehicle_give', weapon.label))
                        end
                    else
                        ESX.ShowNotification(_U('players_nearby'))
                    end
                else
                    ESX.ShowNotification(_U('amount_invalid'))
                end

            elseif option == 'drop_weapon' then
                if IsPedOnFoot(plyPed) then
                    TriggerServerEvent('esx:removeInventoryItem', 'item_weapon', weapon.name)
                    ESX.UI.Menu.CloseAll()
                else
                    ESX.ShowNotification(_U('in_vehicle_drop', weapon.label))
                end
            end
        end, function(data, menu)
            menu.close()
        end)
    end
end

RegisterNetEvent('lp_personalmenu:Weapon_addAmmoToPedC')
AddEventHandler('lp_personalmenu:Weapon_addAmmoToPedC', function(value, quantity)
	local weaponHash = GetHashKey(value)

	if HasPedGotWeapon(plyPed, weaponHash, false) and value ~= 'WEAPON_UNARMED' then
		AddAmmoToPed(plyPed, value, quantity)
		ESX.ShowNotification(_U('receive_ammo', quantity))
	end
end)

-----------------
-- WALLET MENU --
-----------------

function OpenWalletMenu()

    local elements = {}
    table.insert(elements, {label = _U('wallet_job_button', ESX.PlayerData.job.label, ESX.PlayerData.job.grade_label), option = ''})

    for i = 1, #ESX.PlayerData.accounts, 1 do
        if ESX.PlayerData.accounts[i].name == 'money' then
            if PersonalMenu.WalletIndex[ESX.PlayerData.accounts[i].name] == nil then
                PersonalMenu.WalletIndex[ESX.PlayerData.accounts[i].name] = 1
            end

            table.insert(elements, {
                label = _U('wallet_money_button', ESX.Math.GroupDigits(ESX.PlayerData.accounts[i].money)),
                option = 'money',
                value = 0,
                type       = 'slider',
                options = {
                    {label = _U('wallet_option_give')},
                    {label = _U('wallet_option_drop')}
                }
            })
        end

        if ESX.PlayerData.accounts[i].name == 'black_money' then
            if PersonalMenu.WalletIndex[ESX.PlayerData.accounts[i].name] == nil then
                PersonalMenu.WalletIndex[ESX.PlayerData.accounts[i].name] = 1
            end

            table.insert(elements, {
                label = _U('wallet_blackmoney_button', ESX.Math.GroupDigits(ESX.PlayerData.accounts[i].money)),
                option = 'black_money',
                value = 0,
                type       = 'slider',
                options = {
                    {label = _U('wallet_option_give')},
                    {label = _U('wallet_option_drop')}
                }
            })
        end
    end

    table.insert(elements, {label = _U('wallet_show_idcard_button'), option = 'show_idcard'})
    table.insert(elements, {label = _U('wallet_check_idcard_button'), option = 'check_idcard'})
    table.insert(elements, {label = _U('wallet_show_driver_button'), option = 'show_driver'})
    table.insert(elements, {label = _U('wallet_check_driver_button'), option = 'check_driver'})
    table.insert(elements, {label = _U('wallet_show_firearms_button'), option = 'show_weapon'})
    table.insert(elements, {label = _U('wallet_check_firearms_button'), option = 'check_weapon'})

    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'wallet_menu', {
        title    = _U('wallet_title'),
        align    = 'top-left',
        elements = elements
    }, function(data, menu)
        --menu.close()
        local option = data.current.option
        if option == 'money' then
            if data.current.value == 0 then
                GiveMoneyToClosestPlayer('money')
            else
                DropMoney('money')
            end
        elseif option == 'black_money' then
            if data.current.value == 0 then
                GiveMoneyToClosestPlayer('black_money')
            else
                DropMoney('black_money')
            end
        elseif option == 'show_idcard' then
            ShowIdCard(nil)
        elseif option == 'show_driver' then
            ShowIdCard('driver')
        elseif option == 'show_weapon' then
            ShowIdCard('weapon')
        elseif option == 'check_idcard' then
            TriggerServerEvent('jsfour-idcard:open', GetPlayerServerId(PlayerId()), GetPlayerServerId(PlayerId()))
        elseif option == 'check_driver' then
            TriggerServerEvent('jsfour-idcard:open', GetPlayerServerId(PlayerId()), GetPlayerServerId(PlayerId()), 'driver')
        elseif option == 'check_weapon' then
            TriggerServerEvent('jsfour-idcard:open', GetPlayerServerId(PlayerId()), GetPlayerServerId(PlayerId()), 'weapon')
        end
    end, function(data, menu)
        menu.close()
    end)
end

function GiveMoneyToClosestPlayer(account)
    local post, quantity = CheckQuantity(KeyboardInput('BOX_AMOUNT', _U('dialogbox_amount'), '', 8))

    if post then
        local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

        if closestDistance ~= -1 and closestDistance <= 3 then
            local closestPed = GetPlayerPed(closestPlayer)

            if not IsPedSittingInAnyVehicle(closestPed) then
                TriggerServerEvent('esx:giveInventoryItem', GetPlayerServerId(closestPlayer), 'item_account', account, quantity)
                ESX.UI.Menu.CloseAll()
            else
                ESX.ShowNotification(_U('in_vehicle_give', 'de l\'argent'))
            end
        else
            ESX.ShowNotification(_U('players_nearby'))
        end
    else
        ESX.ShowNotification(_U('amount_invalid'))
    end
end

function DropMoney(account)
    local post, quantity = CheckQuantity(KeyboardInput('BOX_AMOUNT', _U('dialogbox_amount'), '', 8))

    if post then
        if not IsPedSittingInAnyVehicle(plyPed) then
            TriggerServerEvent('esx:removeInventoryItem', 'item_account', account, quantity)
            ESX.UI.Menu.CloseAll()
        else
            ESX.ShowNotification(_U('in_vehicle_drop', 'de l\'argent'))
        end
    else
        ESX.ShowNotification(_U('amount_invalid'))
    end
end

function ShowIdCard(type)
    local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

    if closestDistance ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('jsfour-idcard:open', GetPlayerServerId(PlayerId()), GetPlayerServerId(closestPlayer), type)
    else
        ESX.ShowNotification(_U('players_nearby'))
    end
end

------------------
-- BILLING MENU --
------------------

function OpenBillingMenu()

    ESX.TriggerServerCallback('lp_personalmenu:Bill_getBills', function(bills)
        local elements = {}

		if #bills > 0 then
			for i = 1, #bills, 1 do
				table.insert(elements, {label = bills[i].label .. '<span style="color: darkred;">$' .. ESX.Math.GroupDigits(bills[i].amount) .. '</span>', value = bills[i].id})
			end
		else
			table.insert(elements, {label = '<span style="color: gray;">Aucune facture</span>', value = nil})
		end

        ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'billing_menu', {
            title    = _U('bills_title'),
            align    = 'top-left',
            elements = elements
        }, function(data, menu)
			if data.current.value ~= nil then
				ESX.TriggerServerCallback('esx_billing:payBill', function()
					OpenBillingMenu()
				end, data.current.value)
			end
        end, function(data, menu)
            menu.close()
        end)
    end)
end

------------------
-- CLOTHES MENU --
------------------

local currentClotheMenuOption = nil
local isSwitchingUniform = false
local clothesMenuClosed = false
function OpenClothesMenu(fromToggle)

	if fromToggle and clothesMenuClosed then
		return
	end

	clothesMenuClosed = false

    ESX.TriggerServerCallback("esx_skin:getPlayerSkinAndCompleteJobSkin", function(aSkin, jobSkin)
        local skin = nil
        if isInService then
            skin = jobSkin
        else
            skin = aSkin
        end
        TriggerEvent('skinchanger:getSkin', function(skin2)

			isSwitchingUniform = false

            local elements = {}
            for i = 1, #PersonalMenu.ClothesButtons, 1 do
                local clothe = PersonalMenu.ClothesButtons[i]
                local isSet = true
				local hasClothe = true
                if clothe == 'torso' then    
                    if skin.torso_1 ~= skin2.torso_1 then
                        isSet = false
					elseif HasNoTorso(skin, skin2) then
						hasClothe = false
                    end
                elseif clothe == 'pants' then
                    if skin.pants_1 ~= skin2.pants_1 then
                        isSet = false
                    elseif HasNoPants(skin, skin2) then
						hasClothe = false
                    end
                elseif clothe == 'shoes' then
                    if skin.shoes_1 ~= skin2.shoes_1 then
                        isSet = false
                    elseif HasNoShoes(skin, skin2) then
						hasClothe = false
                    end
                elseif clothe == 'bag' then
                    if skin.bags_1 ~= skin2.bags_1 then
                        isSet = false
                    elseif HasNoBag(skin, skin2) then
						hasClothe = false
                    end
                elseif clothe == 'bproof' then
                    if skin.bproof_1 ~= skin2.bproof_1 then
                        isSet = false
                    elseif HasNoBProof(skin, skin2) then
						hasClothe = false
                    end
                end
                local txt = ''
				if hasClothe then
					if isSet then
						txt = itemIsSelected
					else
						txt = itemIsNotSelected
					end
				else
					txt = '<span style="color:gray;">Aucun</span>'
				end

                local isSelected = currentClotheMenuOption == PersonalMenu.ClothesButtons[i]
                table.insert(elements, {label = _U(('clothes_%s'):format(clothe)) .. txt, value = PersonalMenu.ClothesButtons[i], selected = isSelected})
            end
            ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'clothes_menu', {
                title    = _U('clothes_title'),
                align    = 'top-left',
                elements = elements
            }, function(data, menu)
				if not isSwitchingUniform then
					isSwitchingUniform = true
                	setUniform(data.current.value, plyPed)
				end
            end, function(data, menu)
				clothesMenuClosed = true
				menu.close()
            end, function(data, menu)
                currentClotheMenuOption = data.current.value
            end)
        end)
    end)
end

function setUniform(value, plyPed)
	ESX.TriggerServerCallback("esx_skin:getPlayerSkinAndCompleteJobSkin", function(aSkin, jobSkin)
		local skin = nil
		if isInService then
			skin = jobSkin
		else
			skin = aSkin
		end
		TriggerEvent('skinchanger:getSkin', function(skin2)
			if value == 'torso' then
				if not HasNoTorso(skin, skin2) then
					startAnimAction('clothingtie', 'try_tie_neutral_a')
					Citizen.Wait(1000)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)

					if skin.torso_1 ~= skin2.torso_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['torso_1'] = skin.torso_1, ['torso_2'] = skin.torso_2, ['tshirt_1'] = skin.tshirt_1, ['tshirt_2'] = skin.tshirt_2, ['arms'] = skin.arms, ['arms_2'] = skin.arms_2, ['decals_1'] = skin.decals_1, ['decals_2'] = skin.decals_2})
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['torso_1'] = 15, ['torso_2'] = 0, ['tshirt_1'] = 15, ['tshirt_2'] = 0, ['arms'] = 15, ['arms_2'] = 0, ['decals_1'] = 0, ['decals_2'] = 0})
					end
					OpenClothesMenu(true)
				else
					OpenClothesMenu(true)
				end
			elseif value == 'pants' then
				if skin.pants_1 ~= skin2.pants_1 then
					TriggerEvent('skinchanger:loadClothes', skin2, {['pants_1'] = skin.pants_1, ['pants_2'] = skin.pants_2})
				else
					if skin.sex == 0 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['pants_1'] = 61, ['pants_2'] = 1})
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['pants_1'] = 15, ['pants_2'] = 0})
					end
				end
                OpenClothesMenu(true)
			elseif value == 'shoes' then
				if skin.shoes_1 ~= skin2.shoes_1 then
					TriggerEvent('skinchanger:loadClothes', skin2, {['shoes_1'] = skin.shoes_1, ['shoes_2'] = skin.shoes_2})
				else
					if skin.sex == 0 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['shoes_1'] = 34, ['shoes_2'] = 0})
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['shoes_1'] = 35, ['shoes_2'] = 0})
					end
				end
                OpenClothesMenu(true)
			elseif value == 'bag' then
				if skin.bags_1 ~= skin2.bags_1 then
					TriggerEvent('skinchanger:loadClothes', skin2, {['bags_1'] = skin.bags_1, ['bags_2'] = skin.bags_2})
				else
					TriggerEvent('skinchanger:loadClothes', skin2, {['bags_1'] = 0, ['bags_2'] = 0})
				end
                OpenClothesMenu(true)
			elseif value == 'bproof' then
				if not HasNoBProof(skin, skin2) then
					startAnimAction('clothingtie', 'try_tie_neutral_a')
					Citizen.Wait(1000)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)

					if skin.bproof_1 ~= skin2.bproof_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['bproof_1'] = skin.bproof_1, ['bproof_2'] = skin.bproof_2})
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['bproof_1'] = 0, ['bproof_2'] = 0})
					end
					OpenClothesMenu(true)
				else
					OpenClothesMenu(true)
				end
			end
		end)
	end)
end

function HasNoTorso(skin, skin2)
	return  skin.torso_1  == skin2.torso_1  and skin.torso_1  == 15
		and skin.torso_2  == skin2.torso_2  and skin.torso_2  == 0
		and skin.tshirt_1 == skin2.tshirt_1 and skin.tshirt_1 == 15
		and skin.tshirt_2 == skin2.tshirt_2 and skin.tshirt_2 == 0
		and skin.arms     == skin2.arms     and skin.arms     == 15
		and skin.arms_2   == skin2.arms_2   and skin.arms_2   == 0
		and skin.decals_1 == skin2.decals_1 and skin.decals_1 == 0
		and skin.decals_2 == skin2.decals_2 and skin.decals_2 == 0
end

function HasNoPants(skin, skin2)
	if skin.sex == 0 then
		return  skin.pants_1  == skin2.pants_1  and skin.pants_1  == 61
		   	and skin.pants_2  == skin2.pants_2  and skin.pants_2  == 1
	else
		return  skin.pants_1  == skin2.pants_1  and skin.pants_1  == 15
		   	and skin.pants_2  == skin2.pants_2  and skin.pants_2  == 0
	end
end

function HasNoShoes(skin, skin2)
	if skin.sex == 0 then
		return  skin.shoes_1  == skin2.shoes_1  and skin.shoes_1  == 34
		   	and skin.shoes_2  == skin2.shoes_2  and skin.shoes_2  == 0
	else
		return  skin.shoes_1  == skin2.shoes_1  and skin.shoes_1  == 35
		   	and skin.shoes_2  == skin2.shoes_2  and skin.shoes_2  == 0
	end
end

function HasNoBag(skin, skin2)
	return  skin.bags_1  == skin2.bags_1  and skin.bags_1  == 0
	   	and skin.bags_2  == skin2.bags_2  and skin.bags_2  == 0
end

function HasNoBProof(skin, skin2)
	return  skin.bproof_1  == skin2.bproof_1  and skin.bproof_1  == 0
	   	and skin.bproof_2  == skin2.bproof_2  and skin.bproof_2  == 0
end

----------------------
-- ACCESSORIES MENU --
----------------------

local currentAccessoriesMenuOption = nil
local isSwitchingAccessory = false
local accessoriesMenuClosed = false
function OpenAccessoriesMenu(fromToggle)

	if fromToggle and accessoriesMenuClosed then
		return
	end

	accessoriesMenuClosed = false

    ESX.TriggerServerCallback("esx_skin:getPlayerSkinAndCompleteJobSkin", function(aSkin, jobSkin)
        local skin = nil
        if isInService then
            skin = jobSkin
        else
            skin = aSkin
        end
        TriggerEvent('skinchanger:getSkin', function(skin2)

			isSwitchingAccessory = false

            local elements = {}
            for i = 1, #PersonalMenu.AccessoriesButtons, 1 do
                local accessory = PersonalMenu.AccessoriesButtons[i]
                local isSet = true
				local hasAccessory = true
                if accessory == 'chain' then    
                    if skin.chain_1 ~= skin2.chain_1 then
                        isSet = false
					elseif HasNoChain(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'watches' then
                    if skin.watches_1 ~= skin2.watches_1 then
                        isSet = false
                    elseif HasNoWatches(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'bracelets' then
                    if skin.bracelets_1 ~= skin2.bracelets_1 then
                        isSet = false
                    elseif HasNoBracelets(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'ears' then
                    if skin.ears_1 ~= skin2.ears_1 then
                        isSet = false
                    elseif HasNoEars(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'glasses' then
                    if skin.glasses_1 ~= skin2.glasses_1 then
                        isSet = false
                    elseif HasNoGlasses(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'helmet' then
                    if skin.helmet_1 ~= skin2.helmet_1 then
                        isSet = false
                    elseif HasNoHelmet(skin, skin2) then
						hasAccessory = false
                    end
                elseif accessory == 'mask' then
                    if skin.mask_1 ~= skin2.mask_1 then
                        isSet = false
                    elseif HasNoMask(skin, skin2) then
						hasAccessory = false
                    end
                end
                local txt = ''
				if hasAccessory then
					if isSet then
						txt = itemIsSelected
					else
						txt = itemIsNotSelected
					end
				else
					txt = '<span style="color:gray;">Aucun</span>'
				end

                local isSelected = currentAccessoriesMenuOption == PersonalMenu.AccessoriesButtons[i]
                table.insert(elements, {label = _U(('accessories_%s'):format(accessory)) .. txt, value = PersonalMenu.AccessoriesButtons[i], selected = isSelected})
            end
            ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'accessories_menu', {
                title    = _U('accessories_title'),
                align    = 'top-left',
                elements = elements
            }, function(data, menu)
				if not isSwitchingAccessory then
					isSwitchingAccessory = true
                	setAccessory(data.current.value, plyPed)
				end
            end, function(data, menu)
				accessoriesMenuClosed = true
				menu.close()
            end, function(data, menu)
                currentAccessoriesMenuOption = data.current.value
            end)
        end)
    end)
end

function setAccessory(accessory)
	local _accessory = (accessory):lower()
	ESX.TriggerServerCallback("esx_skin:getPlayerSkinAndCompleteJobSkin", function(aSkin, jobSkin)
		local skin = nil
		if isInService then
			skin = jobSkin
		else
			skin = aSkin
		end
		TriggerEvent('skinchanger:getSkin', function(skin2)
			if _accessory == 'chain' then
				if not HasNoChain(skin, skin2) then
					startAnimAction('clothingtie', 'try_tie_neutral_a')
					Citizen.Wait(1000)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)
					if skin.chain_1 ~= skin2.chain_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['chain_1'] = skin.chain_1, ['chain_2'] = skin.chain_2})
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['chain_1'] = 0})
					end
					OpenAccessoriesMenu(true)
				else
					OpenAccessoriesMenu(true)
				end
			elseif _accessory == 'watches' then
				if skin.watches_1 ~= skin2.watches_1 then
					TriggerEvent('skinchanger:loadClothes', skin2, {['watches_1'] = skin.watches_1, ['watches_2'] = skin.watches_2})
				else
					TriggerEvent('skinchanger:loadClothes', skin2, {['watches_1'] = -1})
				end
				OpenAccessoriesMenu(true)
			elseif _accessory == 'bracelets' then
				if skin.bracelets_1 ~= skin2.bracelets_1 then
					TriggerEvent('skinchanger:loadClothes', skin2, {['bracelets_1'] = skin.bracelets_1, ['bracelets_2'] = skin.bracelets_2})
				else
					TriggerEvent('skinchanger:loadClothes', skin2, {['bracelets_1'] = -1})
				end
				OpenAccessoriesMenu(true)
			elseif _accessory == 'ears' then
				if not HasNoEars(skin, skin2) then
					startAnimAction('mini@ears_defenders', 'takeoff_earsdefenders_idle')
					Citizen.Wait(250)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)
					if skin.ears_1 ~= skin2.ears_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['ears_1'] = skin.ears_1, ['ears_2'] = skin.ears_2})
						TriggerEvent('skinchanger:setWearEars', true)
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['ears_1'] = -1})
						TriggerEvent('skinchanger:setWearEars', false)
					end
					OpenAccessoriesMenu(true)
				else
					OpenAccessoriesMenu(true)
				end
			elseif _accessory == 'glasses' then
				if not HasNoGlasses(skin, skin2) then
					startAnimAction('clothingspecs', 'try_glasses_positive_a')
					Citizen.Wait(1000)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)
					if skin.glasses_1 ~= skin2.glasses_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['glasses_1'] = skin.glasses_1, ['glasses_2'] = skin.glasses_2})
						TriggerEvent('skinchanger:setWearGlasses', true)
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['glasses_1'] = -1})
						TriggerEvent('skinchanger:setWearGlasses', false)
					end
					OpenAccessoriesMenu(true)
				else
					OpenAccessoriesMenu(true)
				end
			elseif _accessory == 'helmet' then
				if not HasNoHelmet(skin, skin2) then
					startAnimAction('missfbi4', 'takeoff_mask')
					Citizen.Wait(1000)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed)
					if skin.helmet_1 ~= skin2.helmet_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['helmet_1'] = skin.helmet_1, ['helmet_2'] = skin.helmet_2})
						TriggerEvent('skinchanger:setWearHelmet', true)
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['helmet_1'] = -1})
						TriggerEvent('skinchanger:setWearHelmet', false)
					end
					OpenAccessoriesMenu(true)
				else
					OpenAccessoriesMenu(true)
				end
			elseif _accessory == 'mask' then
				if not HasNoMask(skin, skin2) then
					startAnimAction('missfbi4', 'takeoff_mask')
					Citizen.Wait(850)
					Player.handsup, Player.pointing = false, false
					ClearPedTasks(plyPed) 
					if skin.mask_1 ~= skin2.mask_1 then
						TriggerEvent('skinchanger:loadClothes', skin2, {['mask_1'] = skin.mask_1, ['mask_2'] = skin.mask_2})
						TriggerEvent('skinchanger:setWearMask', true)
					else
						TriggerEvent('skinchanger:loadClothes', skin2, {['mask_1'] = -1})
						TriggerEvent('skinchanger:setWearMask', false)
					end
					OpenAccessoriesMenu(true)
				else
					OpenAccessoriesMenu(true)
				end
			end
		end)
	end)
end

function HasNoChain(skin, skin2)
	return skin.chain_1 < 1 and skin2.chain_1 < 1
end

function HasNoWatches(skin, skin2)
	return  skin.watches_1 == skin2.watches_1  and skin.watches_1  == -1
end

function HasNoBracelets(skin, skin2)
	return  skin.bracelets_1 == skin2.bracelets_1  and skin.bracelets_1  == -1
end

function HasNoEars(skin, skin2)
	return  skin.ears_1 == skin2.ears_1  and skin.ears_1  == -1
end

function HasNoGlasses(skin, skin2)
	return (skin.glasses_1  == -1 or (skin.sex == 0 and skin.glasses_1  == 0) or (skin.sex == 1 and skin.glasses_1  == 5))
	   and (skin2.glasses_1 == -1 or (skin.sex == 0 and skin2.glasses_1 == 0) or (skin.sex == 1 and skin2.glasses_1 == 5))
end

function HasNoHelmet(skin, skin2)
	return skin.helmet_1 < 0 and skin2.helmet_1 < 0
end

function HasNoMask(skin, skin2)
	return skin.mask_1 < 1 and skin2.mask_1 < 1
end

---------------------
-- ANIMATIONS MENU --
---------------------

function OpenAnimationsMenu(forShortcut)
	animationMenuForShortcut = forShortcut
	local elements = {}
	if forShortcut then
		table.insert(elements, {label = 'Retirer l\'Animation', value = nil})
	end
	for i = 1, #Config.Animations, 1 do
		table.insert(elements, {label = Config.Animations[i].label .. '<span> ' .. rightArrow .. ' </span>', value = i})
	end
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'animations_menu', {
		title    = _U('animation_title'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if data.current.value ~= nil then
			OpenAnimationsSubMenu(data.current.value, menu)
		else
			animationShortcutsList['anim'..animationShortcutToEdit] = nil
			TriggerServerEvent('lp_personalmenu:updateAnimationShortcuts', animationShortcutsList)
			menu.close()
			OpenAnimationShortcutsMenu()
		end
	end, function(data, menu)
		menu.close()
	end)
end

function table.removekey(table, key)
    local element = table[key]
    table[key] = nil
    return element
end

function OpenAnimationsSubMenu(index, parentMenu)
	local elements = {}
	for i = 1, #Config.Animations[index].items, 1 do
		table.insert(elements, {label = Config.Animations[index].items[i].label, value = Config.Animations[index].items[i]})
	end
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'animations_sub_menu', {
		title    = Config.Animations[index].label,
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if not animationMenuForShortcut then
			PlayAnimationWithTypeAndData(data.current.value.type, data.current.value.data, data.current.value.label)
		else
			animationShortcutsList['anim'..animationShortcutToEdit] = {name = data.current.value.label, type = data.current.value.type, data = data.current.value.data}
			TriggerServerEvent('lp_personalmenu:updateAnimationShortcuts', animationShortcutsList)
			menu.close()
			parentMenu.close()
			OpenAnimationShortcutsMenu()
		end
	end, function(data, menu)
		menu.close()
	end)
end

function PlayAnimationWithTypeAndData(type, data, label)
	ClearPedSecondaryTask(plyPed)
	if type == 'anim' then
		startAnim(data.lib, data.anim, data.isLoop, data.isTopOnly)
	elseif type == 'duo' then
			startAnimDuo(data, label)
	elseif type == 'scenario' then
		if data.anim == 'PROP_HUMAN_SEAT_CHAIR_MP_PLAYER' then
			local ped = plyPed
			local PedPosition = GetEntityCoords(ped)
			local heading = GetEntityPhysicsHeading(ped)
			local pos = PedPosition - GetEntityForwardVector(ped) * 0.55

			TaskStartScenarioAtPosition(ped, data.anim, pos.x, pos.y, pos.z - 0.5, heading, -1, 1, false)
		else
			TaskStartScenarioInPlace(plyPed, data.anim, 0, false)
		end
	elseif type == 'attitude' then
		startAttitude(data.anim)
	end
end

function startAnim(lib, anim, isLoop, isTopOnly)
	ESX.Streaming.RequestAnimDict(lib, function()
		local n = 0
		if (isTopOnly or false) then
			n = 50
		end
		TaskPlayAnim(plyPed, lib, anim, 8.0, -8.0, -1, (isLoop or 0) + n, 0, false, false, false)
		RemoveAnimDict(lib)
	end)
end

function startAttitude(anim)
	currentAttitude = anim
	if anim then
		ESX.Streaming.RequestAnimSet(anim, function()
			SetPedMotionBlur(plyPed, false)
			SetPedMovementClipset(plyPed, anim, true)
			RemoveAnimSet(anim)
		end)
	else
		ResetPedMovementClipset(plyPed, 0)
	end
end

RegisterNetEvent('lp_personalmenu:setCurrentAttitude')
AddEventHandler('lp_personalmenu:setCurrentAttitude', function(attitude)
	startAttitude(attitude)
end)

-- Animation Duo
function startAnimDuo(data, label)
	local closestPlayer, distance = ESX.Game.GetClosestPlayer()
	if closestPlayer ~= -1 and distance < 1.5 then
		TriggerServerEvent('lp_personalmenu:requestAnimationToTarget', GetPlayerServerId(closestPlayer), data, label)
	else
		ESX.ShowNotification(_U('players_nearby'))
	end
end

RegisterNetEvent('lp_personalmenu:requestAnimationReceived')
AddEventHandler('lp_personalmenu:requestAnimationReceived', function(source, data, label)
	Citizen.CreateThread(function()
		local hasAccepted = false
		local waitUntil = GetGameTimer() + 5000
		while waitUntil > GetGameTimer() do
			Citizen.Wait(5)
			ESX.ShowHelpNotification(_U('anim_accept_duo', label))
			if IsControlJustReleased(0, 246) then
				local distance =  #(GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(source)), true) - GetEntityCoords(plyPed, true))
				if distance < 1.5 then
					hasAccepted = true
					break
				else
					ESX.ShowNotification(_U('anim_toofar_duo'))
				end
			end
		end
		if hasAccepted then
			TriggerServerEvent('lp_personalmenu:requestAccepted', source, data, label)
		end
	end)
end)


RegisterNetEvent('lp_personalmenu:startAnimDuo')
AddEventHandler('lp_personalmenu:startAnimDuo', function(target, dict, anim)
	ESX.Streaming.RequestAnimDict(dict, function()
		local p1 = GetEntityCoords(plyPed, true)
		local p2 = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(target)), true)
		local dx = p2.x - p1.x
		local dy = p2.y - p1.y
		local heading = GetHeadingFromVector_2d(dx, dy)
		SetEntityHeading(plyPed, heading)
		TaskPlayAnim(plyPed, dict, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
		RemoveAnimDict(dict)
	end)
end)

-------------------------------
-- ANIMATIONS SHORTCUTS MENU --
-------------------------------

function OpenAnimationShortcutsMenu()
	local elements = {}
	for i = 1, Config.AnimationShortcutsCount, 1 do
		local anim = animationShortcutsList['anim' .. i]
		if anim ~= nil then
			table.insert(elements, {label = _U('animation_shortcut_i_label', i, anim.name)  .. '<span> ' .. rightArrow .. ' </span>', value = i})
		else
			table.insert(elements, {label = _U('animation_shortcut_i_nolabel', i)  .. '<span> ' .. rightArrow .. ' </span>', value = i})
		end
	end
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'animation_shortcuts_menu', {
		title    = _U('animation_shortcut_title'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		animationShortcutToEdit = data.current.value
		OpenAnimationsMenu(true)
	end, function(data, menu)
		menu.close()
	end)
end

-------------------
-- VEHICULE MENU --
-------------------

function OpenVehicleMenu()

	local elements = {}

	if IsPedSittingInAnyVehicle(plyPed) then

		table.insert(elements, {
			label = '<span style="margin-right:1vw;">' .. _U('vehicle_cruise_control_on') .. '</span>',
			option = 'cruise_control_on',
			value = 0,
			type       = 'slider',
			options = {
				{label = '90 km/h'},
				{label = '110 km/h'},
				{label = '160 km/h'},
				{label = 'Perso.'}
			}
		})

		table.insert(elements, {label = _U('vehicle_cruise_control_off'), option = 'cruise_control_off'})

		table.insert(elements, {
			label = '<span style="margin-right:1vw;">' .. _U('vehicle_speed_control_on') .. '</span>',
			option = 'speed_control_on',
			value = 0,
			type       = 'slider',
			options = {
				{label = '90 km/h'},
				{label = '110 km/h'},
				{label = '160 km/h'},
				{label = 'Perso.'}
			}
		})

		table.insert(elements, {label = _U('vehicle_speed_control_off'), option = 'speed_control_off'})
		table.insert(elements, {label = _U('vehicle_engine_button'), option = 'engine_button'})

		table.insert(elements, {
			label = '<span style="margin-right:1vw;">' .. _U('vehicle_hud_scale') .. '</span>',
			option = 'hud_scale',
			value = 0,
			type       = 'slider',
			options = {
				{label = _U('vehicle_hud_small')},
				{label = _U('vehicle_hud_medium')},
				{label = _U('vehicle_hud_large')}
			}
		})

		table.insert(elements, {
			label = '<span style="margin-right:1vw;">' .. _U('vehicle_door_button') .. '</span>',
			option = 'door_button',
			value = 0,
			type       = 'slider',
			options = {
				{label = _U('vehicle_door_frontleft')},
				{label = _U('vehicle_door_frontright')},
				{label = _U('vehicle_door_backleft')},
				{label = _U('vehicle_door_backright')}
			}
		})

		table.insert(elements, {label = _U('vehicle_hood_button'), option = 'hood_button'})
		table.insert(elements, {label = _U('vehicle_trunk_button'), option = 'trunk_button'})

		if next(PersonalMenu.ExtraList.extraIds) == nil then
			local plyVeh = GetVehiclePedIsIn(plyPed, false)
			local i = 1
			for extraId=0, 14 do
				if DoesExtraExist(plyVeh, extraId) then
					local state = IsVehicleExtraTurnedOn(plyVeh, extraId) == 1
					table.insert(PersonalMenu.ExtraList.extraIds, extraId)
					table.insert(PersonalMenu.ExtraList.extraLabels, 'Option ' .. i .. '')
					table.insert(PersonalMenu.ExtraList.states, state)
					i = i + 1
				end
			end
		end

		if next(PersonalMenu.ExtraList.extraIds) ~= nil then

			local options = {}
			for i = 1, #PersonalMenu.ExtraList.extraIds, 1 do
				table.insert(options, {label = PersonalMenu.ExtraList.extraLabels[i]})
			end

			table.insert(elements, {
				label = _U('vehicle_extras_button'),
				option = 'extras_button',
				value = 0,
				type       = 'slider',
				options = options
			})
		end
	end

	if not IsPedSittingInAnyVehicle(plyPed) then
		table.insert(elements, {label = _U('vehicle_attach_button'), option = 'attach_button'})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_menu', {
        title    = _U('vehicle_title'),
        align    = 'top-left',
        elements = elements
    }, function(data, menu)
        local option = data.current.option
        if option == 'cruise_control_on' then
			
			-- Activate cruise control
			if IsPedSittingInAnyVehicle(plyPed) and IsPedDriving() then
				if data.current.value == 0 then
					SetCruiseControl('90')
				elseif data.current.value == 1 then
					SetCruiseControl('110')
				elseif data.current.value == 2 then
					SetCruiseControl('160')
				else
					ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'menuDiag',{
						title = _U('vehicle_speed_control_dialog'),
						value = text
					},
					function(data, menu)
						menu.close()
						local text = tonumber(data.value)
						if text == nil then 
							ESX.ShowNotification(_U('invalid_speed'))
						else
							SetCruiseControl(text)
						end
					end,
					function(data, menu)
						menu.close()
					end)
				end
			elseif IsPedSittingInAnyVehicle(plyPed) and not IsPedDriving() then
				ShowNotDrivingNotification()
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'cruise_control_off' then

			-- Deactivate cruise control
			if IsPedSittingInAnyVehicle(plyPed) and IsPedDriving() then
				TriggerEvent('teb_speed_control:stop')
				ESX.ShowNotification(_U('vehicle_cruise_control_stop'))
			elseif IsPedSittingInAnyVehicle(plyPed) and not IsPedDriving() then
				ShowNotDrivingNotification()	
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'speed_control_on' then

			-- Activate speed control
			if IsPedSittingInAnyVehicle(plyPed) and IsPedDriving() then

				if data.current.value == 0 then
					SetSpeedLimiter('90')
				elseif data.current.value == 1 then
					SetSpeedLimiter('110')
				elseif data.current.value == 2 then
					SetSpeedLimiter('160')
				else
					OpenCustomSpeedLimiterDialog ()	
				end
			elseif IsPedSittingInAnyVehicle(plyPed) and not IsPedDriving() then
				ShowNotDrivingNotification()
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'speed_control_off'  then
			
			-- Deactivate speed control
			if IsPedSittingInAnyVehicle(plyPed) and IsPedDriving() then
				TurnSpeedLimiterOff()
			elseif IsPedSittingInAnyVehicle(plyPed) and not IsPedDriving() then
				ShowNotDrivingNotification()
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'engine_button' then

			if IsPedSittingInAnyVehicle(plyPed) and IsPedConductor() then
				local plyVeh = GetVehiclePedIsIn(plyPed, false)

				if GetIsVehicleEngineRunning(plyVeh) then
					SetVehicleEngineOn(plyVeh, false, false, true)
					SetVehicleUndriveable(plyVeh, true)
					TurnSpeedLimiterOff()
				elseif not GetIsVehicleEngineRunning(plyVeh) then
					SetVehicleEngineOn(plyVeh, true, false, true)
					SetVehicleUndriveable(plyVeh, false)
				end
			elseif IsPedSittingInAnyVehicle(plyPed) and not IsPedConductor() then
				ESX.ShowNotification(_U('not_driving'))
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'hud_scale' then
			local hudScale
			if data.current.value == 0 then
				hudScale = 'small'
			elseif data.current.value == 1 then
				hudScale = 'medium'
			else
				hudScale = 'big'
			end
			TriggerEvent('lp_hud:sendNui', {
				setHudScale = true,
				scale = hudScale
			})
        elseif option == 'door_button' then

			if IsPedSittingInAnyVehicle(plyPed) then
				local plyVeh = GetVehiclePedIsIn(plyPed, false)

				if not PersonalMenu.DoorState[data.current.value+1] then
					PersonalMenu.DoorState[data.current.value+1] = true
					SetVehicleDoorOpen(plyVeh, data.current.value, false, false)
				elseif PersonalMenu.DoorState[data.current.value+1] then
					PersonalMenu.DoorState[data.current.value+1] = false
					SetVehicleDoorShut(plyVeh, data.current.value, false, false)
				end
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'hood_button' then

			if IsPedSittingInAnyVehicle(plyPed) then
				local plyVeh = GetVehiclePedIsIn(plyPed, false)

				if not PersonalMenu.DoorState.Hood then
					PersonalMenu.DoorState.Hood = true
					SetVehicleDoorOpen(plyVeh, 4, false, false)
				elseif PersonalMenu.DoorState.Hood then
					PersonalMenu.DoorState.Hood = false
					SetVehicleDoorShut(plyVeh, 4, false, false)
				end
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'trunk_button' then

			if IsPedSittingInAnyVehicle(plyPed) then
				local plyVeh = GetVehiclePedIsIn(plyPed, false)

				if not PersonalMenu.DoorState.Trunk then
					PersonalMenu.DoorState.Trunk = true
					SetVehicleDoorOpen(plyVeh, 5, false, false)
				elseif PersonalMenu.DoorState.Trunk then
					PersonalMenu.DoorState.Trunk = false
					SetVehicleDoorShut(plyVeh, 5, false, false)
				end
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

        elseif option == 'extras_button' then

			if IsPedSittingInAnyVehicle(plyPed) then
				local plyVeh = GetVehiclePedIsIn(plyPed, false)
				local elementExtraId = PersonalMenu.ExtraList.extraIds[data.current.value+1]
				local elementExtraState = PersonalMenu.ExtraList.states[data.current.value+1]
				local savedFuelLevel = GetVehicleFuelLevel(plyVeh)

				SetVehicleAutoRepairDisabled(plyVeh,true)
				if elementExtraState then
					SetVehicleExtra(plyVeh, tonumber(elementExtraId), 1)
				else
					SetVehicleExtra(plyVeh, tonumber(elementExtraId), 0)
				end
				SetVehicleAutoRepairDisabled(plyVeh,false)

				PersonalMenu.ExtraList.states[data.current.value+1] = not elementExtraState
			else
				ESX.ShowNotification(_U('no_vehicle'))
			end

		elseif option == 'attach_button' then

			ESX.UI.Menu.CloseAll()
			TriggerEvent('lp_vehicle_attach:openMenu')

        end
    end, function(data, menu)
        menu.close()
    end)
end

function SetCruiseControl(speed)
	TriggerEvent('teb_speed_control:setCruiseControl', speed)
	ESX.ShowNotification(_U('vehicle_cruise_control_start', speed))
end

function SetSpeedLimiter(speed)
	isLimiterOn = true
	TriggerEvent('teb_speed_control:setSpeedLimiter', speed)
	ESX.ShowNotification(_U('vehicle_speed_control_start', speed))
end

function TurnSpeedLimiterOff()
	isLimiterOn = false
	if IsPedSittingInAnyVehicle(plyPed) then
		TriggerEvent('teb_speed_control:stop')
		ESX.ShowNotification(_U('vehicle_speed_control_stop'))
	else
		ESX.ShowNotification(_U('no_vehicle'))
	end
end

function OpenCustomSpeedLimiterDialog()
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'menuDiag',{
		title = _U('vehicle_speed_control_dialog'),
		value = text
	},
	function(data, menu)
		menu.close()
		local text = tonumber(data.value)
		if text == nil or text == 0 then 
			ESX.ShowNotification(_U('invalid_speed'))
		else
			SetSpeedLimiter(text)
		end
		menu.close() 
	end,
	function(data, menu)
		menu.close()
	end)
end	





----------------
-- ADMIN MENU --
----------------

function OpenAdminMenu()

	local elements = {}

	for i = 1, #Config.Admin, 1 do
		table.insert(elements, {label = Config.Admin[i].categoryLabel .. '<span> ' .. rightArrow .. ' </span>', value = i})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'admin_menu', {
		title    = _U('admin_title'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		OpenAdminSubMenu(data.current.value, menu)
	end, function(data, menu)
		menu.close()
	end)
end

local currentAdminSubMenuIndex, currentAdminSubMenuParentMenu, currentAdminSubMenuSelected = nil, nil, -1
function OpenAdminSubMenu(index, parentMenu)
	currentAdminSubMenuIndex, currentAdminSubMenuParentMenu = index, parentMenu
	RefreshAdminSubMenu()
end

function RefreshAdminSubMenu()

	local index, parentMenu = currentAdminSubMenuIndex, currentAdminSubMenuParentMenu

	local elements = {}
	
	for i = 1, #Config.Admin[index].elements, 1 do
		local authorized = false
		for j = 1, #Config.Admin[index].elements[i].groups, 1 do
			if Config.Admin[index].elements[i].groups[j] == Player.group then
				authorized = true
			end
		end
		if authorized then
			local txt = ''
			if Config.Admin[index].elements[i].boolean ~= nil then
				if Config.Admin[index].elements[i].boolean() then
					txt = itemIsSelected
				else
					txt = itemIsNotSelected
				end
			end
			if Config.Admin[index].elements[i].rightArrow then
				txt = '<span> ' .. rightArrow .. ' </span>'
			end
			local isSelected = i == currentAdminSubMenuSelected
			table.insert(elements, {label = Config.Admin[index].elements[i].label .. txt, value = i, selected = isSelected, additionalInfos = Config.Admin[index].elements[i].additionalInfos})
		else
			table.insert(elements, {label = '<span style="color:gray;">' .. Config.Admin[index].elements[i].label .. '</span>', value = nil})
		end
	end
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'admin_sub_menu', {
		title    = Config.Admin[index].categoryLabel,
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if data.current.value ~= nil then
			Config.Admin[index].elements[data.current.value].command()
			TriggerServerEvent('lp_personalmenu:Admin_logCommand', GetPlayerServerId(PlayerId()), Config.Admin[index].elements[data.current.value].label)
		end
	end, function(data, menu)
		menu.close()
	end, function(data, menu)
		if data.current.value ~= nil then
			currentAdminSubMenuSelected = data.current.value
		end
	end)
end

function Text(text)
	SetTextColour(186, 186, 186, 255)
	SetTextFont(0)
	SetTextScale(0.378, 0.378)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(false)
	SetTextDropshadow(0, 0, 0, 0, 255)
	SetTextEdge(1, 0, 0, 0, 205)
	SetTextOutline()
	BeginTextCommandDisplayText('STRING')
	AddTextComponentSubstringPlayerName(text)
	EndTextCommandDisplayText(0.5, 0.03)
end

function GetCamDirection()
	local heading = GetGameplayCamRelativeHeading() + GetEntityPhysicsHeading(plyPed)
	local pitch = GetGameplayCamRelativePitch()
	local coords = vector3(-math.sin(heading * math.pi / 180.0), math.cos(heading * math.pi / 180.0), math.sin(pitch * math.pi / 180.0))
	local len = math.sqrt((coords.x * coords.x) + (coords.y * coords.y) + (coords.z * coords.z))

	if len ~= 0 then
		coords = coords / len
	end

	return coords
end

function GetCamHorizontalDirection()
	local heading = GetGameplayCamRelativeHeading() + GetEntityPhysicsHeading(plyPed) + 90
	local coords = vector3(-math.sin(heading * math.pi / 180.0), math.cos(heading * math.pi / 180.0), 0)
	local len = math.sqrt((coords.x * coords.x) + (coords.y * coords.y) + (coords.z * coords.z))

	if len ~= 0 then
		coords = coords / len
	end

	return coords
end

RegisterNetEvent('lp_personalmenu:Admin_BringC')
AddEventHandler('lp_personalmenu:Admin_BringC', function(plyCoords)
	SetEntityCoords(plyPed, plyCoords)
end)

RegisterNetEvent('lp_personalmenu:Admin_SelectPlayer')
AddEventHandler('lp_personalmenu:Admin_SelectPlayer', function(cb)
	local elements = {}

	ESX.TriggerServerCallback('lp_personalmenu:GetPlayerIdentifiers', function(playerIdentifiers)

		for i = 1, #playerIdentifiers, 1 do
			table.insert(elements, {label = playerIdentifiers[i].label, value = playerIdentifiers[i].value})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_player_menu', {
			title    = _U('menu_title_select_player'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			cb(data.current.value)
		end, function(data, menu)
			menu.close()
		end)

	end)
end)

RegisterNetEvent('lp_personalmenu:Admin_SelectJob')
AddEventHandler('lp_personalmenu:Admin_SelectJob', function(cb)
	local elements = {}

	ESX.TriggerServerCallback('lp_personalmenu:GetListJobs', function(jobs)

		local elements = {}

		for i = 1, #jobs, 1 do
			table.insert(elements, {label = jobs[i].label .. '<span> ' .. rightArrow .. ' </span>', value = i})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_job_menu', {
			title    = _U('menu_title_select_job'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)

			local index = data.current.value

			local elements2 = {}
			for j = 1, #jobs[index].grades, 1 do
				table.insert(elements2, {label = jobs[index].grades[j].label, value = jobs[index].grades[j].grade})
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_job_menu2', {
				title    = jobs[index].label,
				align    = 'top-left',
				elements = elements2
			}, function(data2, menu2)
				cb(jobs[index].name, data2.current.value)
			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)

	end)
end)

RegisterNetEvent('lp_personalmenu:Admin_SelectWeapon')
AddEventHandler('lp_personalmenu:Admin_SelectWeapon', function(cb)

	local elements = {}

	for i = 1, #Config.AdminWeaponList, 1 do
		table.insert(elements, {label = Config.AdminWeaponList[i].label .. '<span> ' .. rightArrow .. ' </span>', value = i})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_weapon_menu', {
		title    = _U('menu_title_select_weapon'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)

		local index = data.current.value

		local elements2 = {}
		for j = 1, #Config.AdminWeaponList[index].weapons, 1 do
			table.insert(elements2, {label = Config.AdminWeaponList[index].weapons[j].label, value = Config.AdminWeaponList[index].weapons[j].name})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_weapon_menu2', {
			title    = Config.AdminWeaponList[index].label,
			align    = 'top-left',
			elements = elements2
		}, function(data2, menu2)
			if data2.current.value == 'parachute' then
				cb('GADGET_PARACHUTE')
			else
				cb('WEAPON_'.. string.upper(data2.current.value))
			end
		end, function(data2, menu2)
			menu2.close()
		end)

	end, function(data, menu)
		menu.close()
	end)
end)

RegisterNetEvent('lp_personalmenu:Admin_SelectItem')
AddEventHandler('lp_personalmenu:Admin_SelectItem', function(cb)
	local elements = {}

	ESX.TriggerServerCallback('lp_personalmenu:GetListItems', function(items)

		for i = 1, #items, 1 do
			table.insert(elements, {label = items[i].label, name = items[i].name, value = 1, type = 'slider', min = 1, max = 100})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'select_item_menu', {
			title    = _U('menu_title_select_item'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			cb(data.current.name, data.current.value)
		end, function(data, menu)
			menu.close()
		end)

	end)
end)
