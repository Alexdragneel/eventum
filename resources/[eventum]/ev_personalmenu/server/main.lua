ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function getMaximumGrade(jobname)
	local queryDone, queryResult = false, nil

	MySQL.Async.fetchAll('SELECT * FROM job_grades WHERE job_name = @jobname ORDER BY `grade` DESC ;', {
		['@jobname'] = jobname
	}, function(result)
		queryDone, queryResult = true, result
	end)

	while not queryDone do
		Citizen.Wait(10)
	end

	if queryResult[1] then
		return queryResult[1].grade
	end

	return nil
end

function getAdminCommand(name)
	for i = 1, #Config.Admin, 1 do
		for j = 1, #Config.Admin[i].elements, 1 do
			if Config.Admin[i].elements[j].name == name then
				return {i, j}
			end
		end
	end

	return false
end

function isAuthorized(index, group)
	if index then
		for i = 1, #Config.Admin[index[1]].elements[index[2]].groups, 1 do
			if Config.Admin[index[1]].elements[index[2]].groups[i] == group then
				return true
			end
		end
	end

	return false
end

---------------
-- CALLBACKS --
---------------

ESX.RegisterServerCallback('lp_personalmenu:GetCharacterName', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer then
		cb(xPlayer.name)
	else
		cb(nil)
	end
end)

ESX.RegisterServerCallback('lp_personalmenu:GetPlayerIdentifiers', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	
	local playerIdentifiers = {}

	local ids = GetPlayers()
	for i = 1, #ids, 1 do
		if tonumber(ids[i]) == tonumber(source) then
			table.insert(playerIdentifiers, 1, { label = 'Moi : [' .. source .. '] ' .. xPlayer.name, value = source })
		else
			table.insert(playerIdentifiers, {label = '['..ids[i] .. '] ' .. GetPlayerName(ids[i]), value = ids[i]})
		end
	end

	cb(playerIdentifiers)
end)

ESX.RegisterServerCallback('lp_personalmenu:GetListJobs', function(source, cb)

	local jobs = {}

	MySQL.Async.fetchAll('SELECT jobs.name as jobname, jobs.label as joblabel, job_grades.grade as grade, job_grades.label as gradelabel FROM jobs, job_grades WHERE job_grades.job_name = jobs.name ORDER BY joblabel, grade', {}, function(result)
		for i=1, #result, 1 do
			local job = result[i].jobname
			if jobs[job] == nil then
				jobs[job] = {
					name = job,
					label = result[i].joblabel,
					grades = {}
				}
				table.insert(jobs, jobs[job])
			end
			table.insert(jobs[job].grades, {
				grade = result[i].grade,
				label = result[i].gradelabel
			})
		end
		cb(jobs)
	end)
end)

ESX.RegisterServerCallback('lp_personalmenu:GetListItems', function(source, cb)

	local items = {}

	MySQL.Async.fetchAll('SELECT name, label FROM lp_items ORDER BY label', {}, function(result)
		for i=1, #result, 1 do
			table.insert(items, {
				name = result[i].name,
				label = result[i].label,
			})
		end
		cb(items)
	end)
end)

ESX.RegisterServerCallback('lp_personalmenu:Bill_getBills', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local bills = {}

	MySQL.Async.fetchAll('SELECT * FROM billing WHERE identifier = @identifier and paid_at is null and is_deleted != 1', {
		['@identifier'] = xPlayer.identifier
	}, function(result)
		for i = 1, #result, 1 do
			table.insert(bills, {
				id = result[i].id,
				label = result[i].label,
				amount = result[i].amount
			})
		end

		cb(bills)
	end)
end)

ESX.RegisterServerCallback('lp_personalmenu:Admin_getUsergroup', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if plyGroup ~= nil then 
		cb(plyGroup)
	else
		cb('user')
	end
end)


----------------
-- ADMIN MENU --
----------------

RegisterServerEvent('lp_personalmenu:Admin_logCommand')
AddEventHandler('lp_personalmenu:Admin_logCommand', function(source, label)
	local xPlayer = ESX.GetPlayerFromId(source)

	print(('[ADMIN CMD] %s : "%s" a utilisé la commande "%s"'):format(os.date('%H:%M'), xPlayer.getName(), label))
end)

-- Weapon Menu --
RegisterServerEvent('lp_personalmenu:Weapon_addAmmoToPedS')
AddEventHandler('lp_personalmenu:Weapon_addAmmoToPedS', function(plyId, value, quantity)
	if #(GetEntityCoords(GetPlayerPed(source)) - GetEntityCoords(GetPlayerPed(plyId))) <= 3.0 then
		TriggerClientEvent('lp_personalmenu:Weapon_addAmmoToPedC', plyId, value, quantity)
	end
end)

-- Admin Menu --
RegisterServerEvent('lp_personalmenu:Admin_BringS')
AddEventHandler('lp_personalmenu:Admin_BringS', function(plyId, targetId)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('bring'), plyGroup) or isAuthorized(getAdminCommand('goto'), plyGroup) then
		local targetCoords = GetEntityCoords(GetPlayerPed(targetId))
		TriggerClientEvent('lp_personalmenu:Admin_BringC', plyId, targetCoords)
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_revive')
AddEventHandler('lp_personalmenu:Admin_revive', function(targetId)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xTarget = ESX.GetPlayerFromId(targetId)

	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('revive'), plyGroup) then
		print(('[ADMIN CMD] "%s" a utilisé la commande "revive" sur "%s" (%d)'):format(xPlayer.getName(), xTarget.getName(), os.time()))
		xTarget.triggerEvent("esx_ambulancejob:revive")
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_reviveMyself')
AddEventHandler('lp_personalmenu:Admin_reviveMyself', function(targetId)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('revive'), plyGroup) then
		print(('[ADMIN CMD] "%s" a utilisé la commande "revive" sur "%s" (%d)'):format(xPlayer.getName(), xPlayer.getName(), os.time()))
		xPlayer.triggerEvent("esx_ambulancejob:revive")
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_heal')
AddEventHandler('lp_personalmenu:Admin_heal', function(targetId)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xTarget = ESX.GetPlayerFromId(targetId)

	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('heal'), plyGroup) then
		print(('[ADMIN CMD] "%s" a utilisé la commande "heal" sur "%s" (%d)'):format(xPlayer.getName(), xTarget.getName(), os.time()))
		xTarget.triggerEvent('esx_basicneeds:healPlayer')
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_setJob')
AddEventHandler('lp_personalmenu:Admin_setJob', function(job, grade)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('setjob'), plyGroup) then
		if ESX.DoesJobExist(job, grade) then
			xPlayer.setJob(job, grade)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('command_setjob_invalid'))
		end
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveWeapon')
AddEventHandler('lp_personalmenu:Admin_giveWeapon', function(weaponName, ammoCount)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('giveweapon'), plyGroup) then
		xPlayer.addWeapon(weaponName, ammoCount)
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveAmmo')
AddEventHandler('lp_personalmenu:Admin_giveAmmo', function(weaponName, ammoCount)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('giveammo'), plyGroup) then
		xPlayer.addWeaponAmmo(weaponName, ammoCount)
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_clearWeapon')
AddEventHandler('lp_personalmenu:Admin_clearWeapon', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('clearweapon'), plyGroup) then
		for i = 1, #xPlayer.loadout, 1 do
			xPlayer.removeWeapon(xPlayer.loadout[i].name)
		end
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveItem')
AddEventHandler('lp_personalmenu:Admin_giveItem', function(item, count)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('giveitem'), plyGroup) then
		xPlayer.addInventoryItem(item, count, false, true)
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_clearInventory')
AddEventHandler('lp_personalmenu:Admin_clearInventory', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('clearinventory'), plyGroup) then
		for k,v in ipairs(xPlayer.inventory) do
			if v.count > 0 then
				xPlayer.setInventoryItem(v.name, 0)
			end
		end
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveCash')
AddEventHandler('lp_personalmenu:Admin_giveCash', function(money)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('givemoney'), plyGroup) then
		xPlayer.addAccountMoney('money', money)
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Don de ' .. money .. '$')
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveBank')
AddEventHandler('lp_personalmenu:Admin_giveBank', function(money)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('givebank'), plyGroup) then
		xPlayer.addAccountMoney('bank', money)
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Don de ' .. money .. '$ en banque')
	end
end)

RegisterServerEvent('lp_personalmenu:Admin_giveDirtyMoney')
AddEventHandler('lp_personalmenu:Admin_giveDirtyMoney', function(money)
	local xPlayer = ESX.GetPlayerFromId(source)
	local plyGroup = xPlayer.getGroup()

	if isAuthorized(getAdminCommand('givedirtymoney'), plyGroup) then
		xPlayer.addAccountMoney('black_money', money)
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Don de ' .. money .. '$ sale')
	end
end)

-- Animations Duo

RegisterServerEvent('lp_personalmenu:requestAnimationToTarget')
AddEventHandler('lp_personalmenu:requestAnimationToTarget', function(target, data, label)
	TriggerClientEvent('lp_personalmenu:requestAnimationReceived', target, source, data, label)
end)

RegisterServerEvent('lp_personalmenu:requestAccepted')
AddEventHandler('lp_personalmenu:requestAccepted', function(target, data, label)
	local xPlayer = ESX.GetPlayerFromId(source)
	TriggerClientEvent('lp_personalmenu:startAnimDuo', target, source, data.lib, data.anim)
	TriggerClientEvent('lp_personalmenu:startAnimDuo', xPlayer.source, target, data.lib2, data.anim2)
end)

-- Animations Shortcuts

ESX.RegisterServerCallback('lp_personalmenu:requestPlayerAnimationShortcuts', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer then
		MySQL.Async.fetchAll('SELECT animationshortcuts FROM users WHERE identifier = @identifier', {
			['@identifier'] = xPlayer.identifier
		}, function(result)
			if result[1].animationshortcuts then
				cb(json.decode(result[1].animationshortcuts))
			else
				cb()
			end
		end)
	else
		cb()
	end
end)

RegisterServerEvent('lp_personalmenu:updateAnimationShortcuts')
AddEventHandler('lp_personalmenu:updateAnimationShortcuts', function(animationshortcutsList)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.execute('UPDATE users SET animationshortcuts = @animationshortcuts WHERE identifier = @identifier', {
		['@animationshortcuts'] = json.encode(animationshortcutsList),
		['@identifier'] = xPlayer.identifier
	})
end)
