fx_version "adamant"

game "gta5"

description "Los Plantos Vehicle Attach"

version "1.0.0"

client_scripts {
	"@es_extended/locale.lua",
	"locales/fr.lua",

	"config.lua",

    "client/main.lua",
}

dependencies {
	"es_extended",
}
