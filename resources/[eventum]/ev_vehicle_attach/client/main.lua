ESX = nil
local isInMenu = false
local selectedTowTruck = nil
local allTowedVehicles = {}
local moveStep = Config.Steps.Normal
local moveType = 1
local helpMoveMsg = _U('help_attach_towed_extra1')
local helpStepMsg = _U('steps_normal')
local isAttaching = false

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj) 
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

-- ESX Listeners --
RegisterNetEvent("esx:playerLoaded")
AddEventHandler(
	"esx:playerLoaded",
	function(xPlayer)
		ESX.PlayerData = xPlayer
	end
)

RegisterNetEvent("esx:setJob")
AddEventHandler(
	"esx:setJob",
	function(job)
		ESX.PlayerData.job = job
	end
)
-- --
RegisterNetEvent('lp_vehicle_attach:openMenu')
AddEventHandler('lp_vehicle_attach:openMenu', function()
	OpenMenu()
end)

function OpenMenu()
	local selectTowTruck = ('%s'):format(_U('select_towtruck'))
	if selectedTowTruck ~= nil then
		local selectedTowTruckLabel = GetDisplayNameFromVehicleModel(GetEntityModel(selectedTowTruck))
		selectTowTruck = ('%s <span style="color:darkgoldenrod;">%s</span>'):format(_U('select_towtruck'), selectedTowTruckLabel)
	end

	local elements = {
		{ label = selectTowTruck, value = 'select_towtruck' }
	}

	if next(allTowedVehicles) ~= nil then
		for i=1, #allTowedVehicles, 1 do
			local towedVehicle = allTowedVehicles[i]
			if towedVehicle.towTruck == selectedTowTruck and DoesEntityExist(towedVehicle.vehicle) then
				local selectTowedLabel = GetDisplayNameFromVehicleModel(GetEntityModel(towedVehicle.vehicle))
				local selectTowed = ('%s <span style="color:green;">%s</span>'):format(selectTowedLabel, _U('attach'))
				local selectTowedValue = 'attach_towed'
				if towedVehicle.attached then
					selectTowed = ('%s <span style="color:red;">%s</span>'):format(selectTowedLabel, _U('detach'))
					selectTowedValue = 'detach_towed'
				end

				table.insert(elements, { label = selectTowed, value = selectTowedValue, towedVehicle = towedVehicle })
			end
		end
	end

	table.insert(elements, { label = _U('select_new_towed'), value = "select_new_towed", disabled = selectedTowTruck == nil })
	
	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		"lp_vehicle_attach",
		{
			title = _U("lp_vehicle_attach"),
			align = "top-left",
			elements = elements
		},
		function(data, menu)
			ESX.UI.Menu.CloseAll()
			if data.current.value == 'select_towtruck' then
				selectedTowTruck = nil
				SelectTowTruck()
			elseif data.current.value == 'select_new_towed' then
				if not data.current.disabled then
					SelectTowed()
				end
			elseif data.current.value == 'attach_towed' then
				MoveTowedVehicle(data.current.towedVehicle, true)
			elseif data.current.value == 'detach_towed' then
				MoveTowedVehicle(data.current.towedVehicle, false)
			end
		end, function (data, menu)
			menu.close()
		end
	)
end

function RequestControlVehicle(vehicle)
	local vehicleToRequest = vehicle
	local entityAttachTo = GetEntityAttachedTo(vehicle)
	local entitiesAttached = {}
	while entityAttachTo ~= 0 do
		table.insert(entitiesAttached, {
			vehicleToRequest = vehicleToRequest,
			entityAttachTo = entityAttachTo
		})
		vehicleToRequest = entityAttachTo
		entityAttachTo = GetEntityAttachedTo(entityAttachTo)
	end

	if #entitiesAttached > 0 then
		for i = #entitiesAttached, 1, -1 do
			local attempt = 0
			while not NetworkHasControlOfEntity(entitiesAttached[i].entityAttachTo) and attempt < 100 and DoesEntityExist(entitiesAttached[i].entityAttachTo) do
				Citizen.Wait(100)
				NetworkRequestControlOfEntity(entitiesAttached[i].entityAttachTo)
				attempt = attempt + 1
			end
			if not DoesEntityExist(entitiesAttached[i].entityAttachTo) then
				ESX.ShowNotification('Ce vehicule est introuvable')
				return false
			end
			if not NetworkHasControlOfEntity(entitiesAttached[i].entityAttachTo) then
				ESX.ShowNotification('Vous n\'avez pas le controle de cette entité')
				return false
			end
		end
	else
		local attempt = 0
		while not NetworkHasControlOfEntity(vehicle) and attempt < 100 and DoesEntityExist(vehicle) do
			Citizen.Wait(100)
			NetworkRequestControlOfEntity(vehicle)
			attempt = attempt + 1
		end
		if not DoesEntityExist(vehicle) then
			ESX.ShowNotification('Ce vehicule est introuvable')
			return false
		end
		if not NetworkHasControlOfEntity(vehicle) then
			ESX.ShowNotification('Vous n\'avez pas le controle de cette entité')
			return false
		end
	end
	
	return true
end

function SelectTowTruck()
	Citizen.CreateThread(function()
		local keepGoing = true
		while keepGoing do
			Citizen.Wait(0)
			ESX.ShowHelpNotification(_U('help_select_towtruck'))

			if IsControlJustReleased(0, 38) then
				local vehicleFront = GetVehicleInFront()
				if vehicleFront > 0 then
					local hasControl = RequestControlVehicle(vehicleFront)
					if not hasControl then
						return
					end
					selectedTowTruck = vehicleFront
					keepGoing = false
					OpenMenu()
				else
					ESX.ShowNotification(_U('no_vehicle'))
				end
			end

			if IsControlJustReleased(0, 73) then
				keepGoing = false
				OpenMenu()
			end
		end
	end)
end

function SelectTowed()
	Citizen.CreateThread(function()
		local keepGoing = true
		while keepGoing do
			Citizen.Wait(0)
			ESX.ShowHelpNotification(_U('help_select_towed'))

			if IsControlJustReleased(0, 38) then
				local vehicleFront = GetVehicleInFront()
				if vehicleFront > 0 then
					if vehicleFront == selectedTowTruck then
						ESX.ShowNotification(_U('cant_tow_towtruck'))
						return
					end

					local hasControl = RequestControlVehicle(vehicleFront)
					if not hasControl then
						return
					end

					local vehicleCoords = GetEntityCoords(vehicleFront)
					local towTruckCoords = GetEntityCoords(selectedTowTruck)
					local distTowTruck = #(vehicleCoords - towTruckCoords)

					if distTowTruck > 20.0 then
						ESX.ShowNotification(_U('vehicle_too_far'))
						return
					end

					local networkId = NetworkGetNetworkIdFromEntity(vehicleFront)
					local lockStatus = GetVehicleDoorLockStatus(vehicleFront)

					table.insert(allTowedVehicles, {
						towTruck = selectedTowTruck,
						vehicle = vehicleFront,
						networkId = networkId,
						lockStatus = lockStatus,
						attached = false,
						position = vector3(0.0, -2.0, 1.5),
						rotation = vector3(0.0, 0.0, 0.0)
					})
					keepGoing = false
					OpenMenu()
				else
					ESX.ShowNotification(_U('no_vehicle'))
				end
			end

			if IsControlJustReleased(0, 73) then
				keepGoing = false
				OpenMenu()
			end
		end
	end)
end

Citizen.CreateThread(function()
	while true do
		if isAttaching then
			Citizen.Wait(0)
			ESX.ShowHelpNotification(_U('help_attach_towed', helpMoveMsg, helpStepMsg))
			DisableControlAction(0, 24,  true) -- disable attack
			DisableControlAction(0, 25,  true) -- disable aim
		else
			Citizen.Wait(1000)
		end
	end
end)

function MoveTowedVehicle(towedVehicle, attach)
	Citizen.CreateThread(function()
		local hasControl = RequestControlVehicle(towedVehicle.vehicle)
		if not hasControl then
			return
		end

		local vehProperties = ESX.Game.GetVehicleProperties(towedVehicle.vehicle)
		
		local vehCoords = GetEntityCoords(towedVehicle.vehicle)
		local x, y, z = table.unpack(vehCoords)
		local newCoords = vector3(x, y, z - 500.0)
		local vehHeading = GetEntityHeading(towedVehicle.vehicle)

		ESX.Game.SpawnLocalVehicle(vehProperties.model, newCoords, vehHeading, function(spawnedVehicle)		
			ESX.Game.SetVehicleProperties(spawnedVehicle, vehProperties)
			SetEntityCollision(spawnedVehicle, false, false)
			FreezeEntityPosition(spawnedVehicle, true)
			SetEntityAsMissionEntity(spawnedVehicle, true, false)
			SetEntityAlpha(spawnedVehicle, 200, false)
			SetVehicleDoorsLocked(spawnedVehicle, 10)
			Citizen.Wait(200)
			
			local moved = true
			isAttaching = true
			while isAttaching do
				Citizen.Wait(100)
				local savedPositionX = towedVehicle.position.x
				local savedPositionY = towedVehicle.position.y
				local savedPositionZ = towedVehicle.position.z
				local savedRotationX = towedVehicle.rotation.x
				local savedRotationY = towedVehicle.rotation.y
				local savedRotationZ = towedVehicle.rotation.z

				if IsControlPressed(0, 172) then -- UP
					moved = true
					if moveType == 1 then
						towedVehicle.position.y = towedVehicle.position.y + moveStep
					elseif moveType == 2 then
						towedVehicle.position.z = towedVehicle.position.z + moveStep
					else
						towedVehicle.rotation.x = towedVehicle.rotation.x + (moveStep * 10)
					end
				elseif IsControlPressed(0, 173) then -- DOWN
					moved = true
					if moveType == 1 then
						towedVehicle.position.y = towedVehicle.position.y - moveStep
					elseif moveType == 2 then
						towedVehicle.position.z = towedVehicle.position.z - moveStep
					else
						towedVehicle.rotation.x = towedVehicle.rotation.x - (moveStep * 10)
					end
				elseif IsControlPressed(0, 174) then -- LEFT
					moved = true
					if moveType == 1 then
						towedVehicle.position.x = towedVehicle.position.x + moveStep
					elseif moveType == 2 then
						towedVehicle.rotation.y = towedVehicle.rotation.y + (moveStep * 10)
					else
						towedVehicle.rotation.z = towedVehicle.rotation.z + (moveStep * 10)
					end
				elseif IsControlPressed(0, 175) then -- RIGHT
					moved = true
					if moveType == 1 then
						towedVehicle.position.x = towedVehicle.position.x - moveStep
					elseif moveType == 2 then
						towedVehicle.rotation.y = towedVehicle.rotation.y - (moveStep * 10)
					else
						towedVehicle.rotation.z = towedVehicle.rotation.z - (moveStep * 10)
					end
				elseif IsDisabledControlPressed(0, 24) then
					if moveType == 1 then
						moveType = 2
						helpMoveMsg = _U('help_attach_towed_extra2')
					elseif moveType == 2 then
						moveType = 3
						helpMoveMsg = _U('help_attach_towed_extra3')
					elseif moveType == 3 then
						moveType = 1
						helpMoveMsg = _U('help_attach_towed_extra1')
					end
					PlaySoundFrontend(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", true)
				elseif IsDisabledControlPressed(0, 25) then
					if moveStep == Config.Steps.Normal then
						moveStep = Config.Steps.Big
						helpStepMsg = _U('steps_big')
					elseif moveStep == Config.Steps.Big then
						moveStep = Config.Steps.Small
						helpStepMsg = _U('steps_small')
					elseif moveStep == Config.Steps.Small then
						moveStep = Config.Steps.Normal
						helpStepMsg = _U('steps_normal')
					end
					PlaySoundFrontend(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", true)
				elseif IsControlPressed(0, 18) then -- Confirm
					isAttaching = false
					moved = false

					local spawnVehCoords = GetEntityCoords(spawnedVehicle)
					local spawnVehHeading = GetEntityHeading(spawnedVehicle)

					while DoesEntityExist(spawnedVehicle) do
						ESX.Game.DeleteVehicle(spawnedVehicle)
						Citizen.Wait(100)
					end

					if attach then
						for i=1, #allTowedVehicles, 1 do
							if allTowedVehicles[i].networkId == towedVehicle.networkId then
								allTowedVehicles[i].attached = true
								allTowedVehicles[i].position = towedVehicle.position
								allTowedVehicles[i].rotation = towedVehicle.rotation
								break
							end
						end

						local hasControl = RequestControlVehicle(towedVehicle.vehicle)
						if not hasControl then
							return
						end

						SetEntityCollision(towedVehicle.vehicle, false, false)
						FreezeEntityPosition(towedVehicle.vehicle, true)
						SetVehicleDoorsLocked(towedVehicle.vehicle, 10)
						AttachEntityToEntity(towedVehicle.vehicle, selectedTowTruck, -1, towedVehicle.position.x + 0.0, towedVehicle.position.y + 0.0, towedVehicle.position.z + 0.0, towedVehicle.rotation.x + 0.0, towedVehicle.rotation.y + 0.0, towedVehicle.rotation.z + 0.0, false, false, false, false, 2, true);
						
						ESX.ShowNotification(_U('vehicle_attached'))
					else
						local idToDelete = -1
						for i=1, #allTowedVehicles, 1 do
							if allTowedVehicles[i].networkId == towedVehicle.networkId then
								idToDelete = i
								break
							end
						end
	
						if idToDelete ~= -1 then
							table.remove(allTowedVehicles, idToDelete)
						end

						local hasControl = RequestControlVehicle(towedVehicle.vehicle)
						if not hasControl then
							return
						end

						DetachEntity(towedVehicle.vehicle, true, true)
						SetEntityCoords(towedVehicle.vehicle, spawnVehCoords)
						SetEntityHeading(towedVehicle.vehicle, spawnVehHeading)
						SetEntityCollision(towedVehicle.vehicle, true, true)
						FreezeEntityPosition(towedVehicle.vehicle, false)
						SetVehicleDoorsLocked(towedVehicle.vehicle, towedVehicle.lockStatus)
						SetVehicleOnGroundProperly(towedVehicle.vehicle)
						ApplyForceToEntity(towedVehicle.vehicle, 3, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0, false, true, true, false, true)
						ESX.ShowNotification(_U('vehicle_detached'))
					end

					OpenMenu()
				elseif IsControlPressed(0, 73) then -- Cancel
					isAttaching = false

					ESX.Game.DeleteVehicle(spawnedVehicle)
					Citizen.Wait(200)

					OpenMenu()
				end
				
				if moved then
					moved = false
					AttachEntityToEntity(spawnedVehicle, selectedTowTruck, -1, towedVehicle.position.x + 0.0, towedVehicle.position.y + 0.0, towedVehicle.position.z + 0.0, towedVehicle.rotation.x + 0.0, towedVehicle.rotation.y + 0.0, towedVehicle.rotation.z + 0.0, false, false, false, false, 2, true)
					
					local spawnedCoords = GetEntityCoords(spawnedVehicle)
					local towTruckCoords = GetEntityCoords(selectedTowTruck)
					local distTowTruck = #(towTruckCoords - spawnedCoords)

					if distTowTruck > 20.0 then
						towedVehicle.position.x = savedPositionX
						towedVehicle.position.y = savedPositionY
						towedVehicle.position.z = savedPositionZ
						towedVehicle.rotation.x = savedRotationX
						towedVehicle.rotation.y = savedRotationY
						towedVehicle.rotation.x = savedRotationY
						AttachEntityToEntity(spawnedVehicle, selectedTowTruck, -1, towedVehicle.position.x + 0.0, towedVehicle.position.y + 0.0, towedVehicle.position.z + 0.0, towedVehicle.rotation.x + 0.0, towedVehicle.rotation.y + 0.0, towedVehicle.rotation.z + 0.0, false, false, false, false, 2, true)
					end
				end
			end
		end)
	end)
end

function GetVehicleInFront()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 4.0, 0.0)
    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z - 0.5, 10, GetPlayerPed(-1), 0)
    local a, b, c, d, result = GetRaycastResult(rayHandle)
    return result
end