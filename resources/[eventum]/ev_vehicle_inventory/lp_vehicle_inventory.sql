USE `fivem_server`;

INSERT INTO `items` (`name`, `label`, `weight`, `rare`, `can_remove`) VALUES
('bandage', 'Bandage', 0.1, 0, 1),
('bread', 'Pain', 0.2, 0, 1),
('chicken', 'Poulet vivant', 1.2, 0, 1),
('clothing', 'Vêtement', 0.15, 0, 1),
('copper', 'Cuivre', 0.2, 0, 1),
('cutted_wood', 'Bois coupé', 0.7, 0, 1),
('diamond', 'Diamant', 0.2, 0, 1),
('fabric', 'Tissu', 0.2, 0, 1),
('fish', 'Poisson', 1, 0, 1),
('gas', 'Essence', 1, 0, 1),
('gold', 'Or', 0.3, 0, 1),
('iron', 'Fer', 0.15, 0, 1),
('medikit', 'Medikit', 0.5, 0, 1),
('oil', 'Pétrole', 1, 0, 1),
('packaged_chicken', 'Poulet en barquette', 0.7, 0, 1),
('packaged_plank', 'Lot de planches', 0.3, 0, 1),
('phone', 'Phone', 0.22, 0, 1),
('refined_oil', 'Pétrole raffiné', 1, 0, 1),
('slaughtered_chicken', 'Poulet abattu', 0.65, 0, 1),
('stone', 'Roche', 1, 0, 1),
('washed_stone', 'Roche lavée', 0.8, 0, 1),
('water', 'Eau', 0.045, 0, 1),
('wood', 'Bois', 1, 0, 1),
('wool', 'Laine', 0.3, 0, 1);


CREATE TABLE `vehicle_inventory` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(100) NOT NULL,
	`label` varchar(255) NOT NULL,
	`plate` varchar(8) NOT NULL,
	`count` int NOT NULL DEFAULT 0

	PRIMARY KEY (`id`)
);
