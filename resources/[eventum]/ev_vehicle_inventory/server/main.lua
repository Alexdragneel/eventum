ESX = nil

-- Init ESX stuff --
TriggerEvent(
	"esx:getSharedObject",
	function(obj)
		ESX = obj
	end
)
-- --

-- Inventory logic --
RegisterServerEvent('lp_vehicle_inventory:getInventoryCb')
AddEventHandler('lp_vehicle_inventory:getInventoryCb', function(plate, cb)
    local items = {}
    local _source = source
    MySQL.Async.fetchAll('SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate', {
        ['@plate'] = plate
    }, function(inventory)
        local allItems = ESX.GetItems()
        if inventory ~= nil and #inventory > 0 then
            for i=1, #inventory, 1 do
                local thisWeight = 0
                if allItems[inventory[i].name] then
                    thisWeight = allItems[inventory[i].name].weight
                end
                table.insert(items, {
                    label = inventory[i].label,
                    name = inventory[i].name,
                    count = inventory[i].count,
                    weight = thisWeight
                })
            end
        end
        
        cb(items, allItems)
    end)
end)

ESX.RegisterServerCallback('lp_vehicle_inventory:getInventory', function(source, cb, plate)
    TriggerEvent('lp_vehicle_inventory:getInventoryCb', plate, function(items, dbItems)
        cb(items, dbItems)
    end)
end)

ESX.RegisterServerCallback('lp_vehicle_inventory:putTrunkItems', function(source, cb, itemName, weight, maxTrunkWeight, count, vehiclePlate)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('lp_vehicle_inventory:getInventoryCb', vehiclePlate, function(items)
        local sourceItem = xPlayer.getInventoryItem(itemName)
        if sourceItem and sourceItem.count >= count and count >= 0 then
            if TrunkCanCarryItem(items, maxTrunkWeight, weight, count) then
                addTrunkItem(source, vehiclePlate, itemName, sourceItem.label, count, function()
                    xPlayer.removeInventoryItem(itemName, count, true)
                    xPlayer.showNotification(_U('have_deposited', count, sourceItem.label))
                    cb()
                end)
            else
                xPlayer.showNotification(_U('trunk_full'))
                cb()
            end
        else
            xPlayer.showNotification(_U('invalid_quantity'))
            cb()
        end
    end)
end)

ESX.RegisterServerCallback('lp_vehicle_inventory:putTrunkWeapon', function(source, cb, weaponId, weaponName, ammo, weight, maxTrunkWeight, count, vehiclePlate)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('lp_vehicle_inventory:getInventoryCb', vehiclePlate, function(items)
        
        if TrunkCanCarryItem(items, maxTrunkWeight, weight, count) then
            -- l'ID de l'arme dans le coffre est l'id de l'arme combiné par le nombre de munitions séparé par "#"
            -- Pour enregistrer le nombre de munitions, et pour "empiler" les armes avec le même nombre de munition ensemble, pour que dans la liste, ça donne "Pistolet avec 32 balles x3"
            local weaponIdInTrunk = weaponId .. "#" .. ammo
            -- On renomme ici aussi au cas où le joueur aurait tiré entre le moment d'afficher la liste des objets à déposer et le moment où il dépose l'arme
            if ammo > 0 then
                weaponName = ('%s%s'):format(weaponName, _U("with_ammo", ESX.Math.GroupDigits(ammo)))
            end
            addTrunkItem(source, vehiclePlate, weaponIdInTrunk, weaponName, count, function()
                xPlayer.removeWeapon(weaponId)
                xPlayer.showNotification(_U('have_deposited', count, weaponName))
                cb()
            end)
        else
            xPlayer.showNotification(_U('trunk_full'))
            cb()
        end
    end)
end)

ESX.RegisterServerCallback('lp_vehicle_inventory:getTrunkItems', function(source, cb, itemName, count, vehiclePlate)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('lp_vehicle_inventory:getInventoryCb', vehiclePlate, function(items)
        local item = nil

        for i=1, #items, 1 do
            if items[i].name == itemName then
                item = items[i]
                break
            end
        end

        if item ~= nil then
            if item.count >= count then
                if xPlayer.canCarryItem(itemName, count) then
                    removeTrunkItem(source, vehiclePlate, itemName, count, function ()
                        xPlayer.addInventoryItem(itemName, count, false, true)
                        xPlayer.showNotification(_U('have_withdrawn', count, item.label))
                        cb()
                    end)
                else
                    xPlayer.showNotification(_U('inventory_full'))
                    cb()
                end
            else
                xPlayer.showNotification(_U('invalid_quantity'))
                cb()
            end
        else
            cb()
        end
    end)
end)

function SplitString(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={}
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		table.insert(t, str)
	end
	return t
end

ESX.RegisterServerCallback('lp_vehicle_inventory:getTrunkWeapons', function(source, cb, itemName, count, vehiclePlate)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('lp_vehicle_inventory:getInventoryCb', vehiclePlate, function(items)
        local item = nil

        for i=1, #items, 1 do
            if items[i].name == itemName then
                item = items[i]
                break
            end
        end

        if item ~= nil then
            if item.count >= count then
                removeTrunkItem(source, vehiclePlate, itemName, count, function ()
                    -- l'itemName possède un # qui sépare le nom de l'arme et le nombre de munition (0 si arme à vide ou CàC ou tazer)
                    local t = SplitString(itemName, "#")
                    local weaponName = t[1]
                    local ammo = tonumber(t[2])
                    xPlayer.addWeapon(weaponName, ammo)
                    xPlayer.showNotification(_U('have_withdrawn', count, item.label))
                    cb()
                end)
            else
                xPlayer.showNotification(_U('invalid_quantity'))
                cb()
            end
        else
            cb()
        end
    end)
end)

function TrunkCanCarryItem(trunkInventory, maxTrunkWeight, itemWeight, count)
    local currentVaultWeight = 0

    for k,v in ipairs(trunkInventory) do
        if v ~= nil then
            currentVaultWeight = currentVaultWeight + (v.weight * v.count)
        end
    end

    local totalItemWeight = itemWeight * count
    local futureVaultWeight = currentVaultWeight + totalItemWeight

    return futureVaultWeight <= maxTrunkWeight
end

function removeTrunkItem(source, plate, item, count, cb)
    local _source = source
    MySQL.Async.execute('UPDATE `vehicle_inventory` SET `count`= `count` - @qty WHERE `plate` = @plate AND `name`= @name', {
        ['@plate'] = plate,
        ['@qty'] = count,
        ['@name'] = item
    },
    function(rowsChanged)
        cb()
    end)
end

function addTrunkItem(source, plate, item, name, count, cb)
    local _source = source
    MySQL.Async.execute('INSERT INTO `vehicle_inventory` (name,label,plate,count) VALUES (@name,@label,@plate,@qty) ON DUPLICATE KEY UPDATE count=count+ @qty', {
        ['@plate'] = plate,
        ['@qty'] = count,
        ['@name'] = item,
        ['@label'] = name,
    },
    function(rowsChanged)
        cb()
    end)
end
-- --