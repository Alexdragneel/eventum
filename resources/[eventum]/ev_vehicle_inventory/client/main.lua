ESX = nil
local vehicleFrontSaved, vehicleTrunkOpen = nil, false

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

-- Inventory logic --
function GetMaxTrunkWeight(vehicle)
	local model = GetEntityModel(vehicle)
	if model == -730904777 or model == 2016027501 then -- refiner or lumberjack trailer
		return Config.VehicleLimit[10] -- Industrials [150kg]
	elseif model == -2137348917 then -- phantom truck
		return 0
	else
		return Config.VehicleLimit[GetVehicleClass(vehicle)]
	end
end

function OpenTrunk(vehicleFront)
    ESX.UI.Menu.CloseAll()

	ESX.TriggerServerCallback('lp_vehicle_inventory:getInventory', function(items, dbItems)
		vehicleTrunkOpen = true
		local totalTrunkWeight = 0
        local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)

		for i=1, #items, 1 do
			if items[i].count > 0 then
				totalTrunkWeight = totalTrunkWeight + (items[i].weight * items[i].count)
			end
		end   
		
		local elements = {
			{label = _U('get_object'), value = 'get_object'},
			{label = _U('put_object'), value = 'put_object'}
		}

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'trunk', {
			title    = _U('trunk_content', totalTrunkWeight, maxTrunkWeight),
			align    = 'top-left',
			elements = elements,
		}, function(data, menu)
			menu.close()
			if data.current.value == 'put_object' then
				OpenPutTrunkMenu(vehicleFront)
			elseif data.current.value == 'get_object' then
				OpenGetTrunkMenu(vehicleFront)
			end
		end, function(data, menu)
			menu.close()
			CloseTrunk()
		end)
	end, GetVehicleNumberPlateText(vehicleFront))
end

function OpenPutTrunkMenu(vehicleFront)
    ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
		local elements = {}
		local currentWeight = 0
		local ped = PlayerPedId()

		-- On liste les objets qui ne sont pas des armes
		for i=1, #items, 1 do
			local item = items[i]

			if item.count > 0 then
				local dbItem = dbItems[item.name]
				if dbItem ~= nil then
					currentWeight = currentWeight + (item.weight * item.count)
					local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(item.label, ESX.Math.GroupDigits(item.count))
					if item.useCount > 0 and not item.stackable and dbItem.actions ~= nil and dbItem.actions.useCount ~= nil and dbItem.actions.useCount > 0 then
						label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s %s</span>'):format(item.label, item.useCount, dbItem.actions.countUnit)
					end
					table.insert(elements, {label = label, type = 'item_standard', value = item.name, weight = item.weight, count = item.count })
				end
			end
		end

		-- On liste ensuite les armes
		local weaponsList = ESX.GetWeaponList()
		for i = 1, #weaponsList, 1 do
			if HasPedGotWeapon(ped, weaponsList[i].name, false) then
				local ammo = GetAmmoInPedWeapon(ped, weaponsList[i].name)
				local label = ""
				-- Affiche le nombre de munitions à l'instant t
				-- Mais si le joueur tire avant de valider le choix, le label sera quand même mis à jour côté serveur 
				if ammo > 0 then
					label = ('%s%s'):format(weaponsList[i].label, _U("with_ammo", ESX.Math.GroupDigits(ammo)))
				else
					-- Si l'arme est à vide ou si c'est une arme sans munition (CaC, tazer)
					label = weaponsList[i].label
				end
				table.insert(elements, {label = label, label2 = weaponsList[i].label, type = 'item_standard', value = weaponsList[i].name, weight = 0, count = 1})
			end
		end

		ESX.TriggerServerCallback('lp_core:getPlayerMaxWeight', function(maxWeight)
			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'inventory_player',
				{
					title    = _U('inventory', currentWeight, maxWeight),
					align    = 'top-left',
					elements = elements
				},
				function(data, menu)

					local itemName = data.current.value
					local weight = data.current.weight
					local count = data.current.count
					
					if dbItems[itemName] ~= nil and dbItems[itemName].stackable then

						ESX.TriggerServerCallback('lp_vehicle_inventory:getInventory', function(itemsInTrunk, dbItemsTrunk)
							vehicleTrunkOpen = true
							local totalTrunkWeight = 0
							local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)
					
							for i=1, #itemsInTrunk, 1 do
								if itemsInTrunk[i].count > 0 then
									totalTrunkWeight = totalTrunkWeight + (itemsInTrunk[i].weight * itemsInTrunk[i].count)
								end
							end

							local remainingTrunkWeight = maxTrunkWeight - totalTrunkWeight
							local maxQuantity = math.max(math.floor(math.min(remainingTrunkWeight / weight, count)))

							ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'trunk_menu_put_item_count',
							{
								title = _U('quantity_with_max', maxQuantity),
								type = 'number'
							},
							function(data2, menu2)
								local count = tonumber(data2.value)

								if count == nil then
									ESX.ShowNotification(_U('invalid_quantity'))
								else
									menu2.close()
									menu.close()

									local plate = GetVehicleNumberPlateText(vehicleFront)
									local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)

									ESX.TriggerServerCallback('lp_vehicle_inventory:putTrunkItems', function()
										OpenPutTrunkMenu(vehicleFront)
									end, itemName, weight, maxTrunkWeight, count, plate)
								end

							end,
							function(data2, menu2)
								menu2.close()
								menu.close()
								OpenPutTrunkMenu(vehicleFront)
							end)
						end, GetVehicleNumberPlateText(vehicleFront))
					elseif HasPedGotWeapon(ped, itemName, false) then
						menu.close()
						local plate = GetVehicleNumberPlateText(vehicleFront)
						local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)
						local ammo = GetAmmoInPedWeapon(ped, itemName)
						ESX.TriggerServerCallback('lp_vehicle_inventory:putTrunkWeapon', function()
							SetCurrentPedWeapon(ped, GetHashKey("WEAPON_UNARMED"), true)
							OpenPutTrunkMenu(vehicleFront)
						end, itemName, data.current.label2, ammo, weight, maxTrunkWeight, 1, plate)
					else
						ESX.ShowNotification(_U('non_stackable_not_allowed'))
					end
				end,
				function(data, menu)
					menu.close()
					OpenTrunk(vehicleFront)
				end
			)
		end)
	end)
end

function SplitString(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={}
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		table.insert(t, str)
	end
	return t
end

function OpenGetTrunkMenu(vehicleFront)
	local plate = GetVehicleNumberPlateText(vehicleFront)

	ESX.TriggerServerCallback('lp_vehicle_inventory:getInventory', function(items, dbItems)
		local elements = {}
		local totalTrunkWeight = 0
        local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)

		for i=1, #items, 1 do
			local item = items[i]

			if item.count > 0 then
				local dbItem = dbItems[item.name]
				if dbItem ~= nil then
					totalTrunkWeight = totalTrunkWeight + (item.weight * item.count)
					local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(item.label, ESX.Math.GroupDigits(item.count))
					table.insert(elements, {label = label, value = item.name, count = item.count, weight = item.weight})
				elseif string.find(item.name, "#", 2) then
					-- si l'objet ne fait pas parti de la db des objets, et qu'il y a un "#" dedans alors c'est une arme
					-- le # sépare le nom de l'arme et le nombre de munition (0 si arme à vide ou CàC ou tazer)
					totalTrunkWeight = totalTrunkWeight + (item.weight * item.count)
					local t = SplitString(item.name, "#")
					local ammo = t[2]
					local label = ('<span>%s</span> <span>x%s</span>'):format(item.label, ESX.Math.GroupDigits(item.count))
					table.insert(elements, {label = label, value = item.name, count = 1, weight = 0})
				end
			end
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'trunk_get',
			{
				title    = _U('trunk_content', totalTrunkWeight, maxTrunkWeight),
				align    = 'top-left',
				elements = elements
			},
			function(data, menu)
				local itemName = data.current.value
				local itemWeight = data.current.weight
				local itemCount = data.current.count

				if dbItems[itemName] then

					ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
						ESX.TriggerServerCallback('lp_core:getPlayerMaxWeight', function(maxWeight)

							local currentWeight = 0
							for i=1, #items, 1 do
								local item = items[i]
					
								if item.count > 0 then
									local dbItem = dbItems[item.name]
									if dbItem ~= nil then
										currentWeight = currentWeight + (item.weight * item.count)
									end
								end
							end

							local remainingWeight = maxWeight - currentWeight
							local maxQuantity = math.max(math.floor(math.min(remainingWeight / itemWeight, itemCount)), 0)

							ESX.UI.Menu.Open(
								'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
								{
									title = _U('quantity_with_max', maxQuantity),
									type = 'number'
								},
								function(data2, menu2)

									local count = tonumber(data2.value)

									if count == nil then
										ESX.ShowNotification(_U('invalid_quantity'))
									else
										menu2.close()
										menu.close()

										ESX.TriggerServerCallback('lp_vehicle_inventory:getTrunkItems', function()
											OpenGetTrunkMenu(vehicleFront)
										end, itemName, count, plate)
									end

								end,
								function(data2, menu2)
									menu2.close()
									menu.close()
									OpenGetTrunkMenu(vehicleFront)
								end
							)
						end)
					end)

				else
					local t = SplitString(itemName, "#")
					local weaponName = t[1]
					if not HasPedGotWeapon(PlayerPedId(), weaponName, false) then
						menu.close()
						ESX.TriggerServerCallback('lp_vehicle_inventory:getTrunkWeapons', function()
							OpenGetTrunkMenu(vehicleFront)
						end, itemName, 1, plate)
					else
						ESX.ShowNotification(_U('weapon_already_owned'))
					end
				end
			end,
			function(data, menu)
				menu.close()
				OpenTrunk(vehicleFront)
			end
		)
	end, plate)
end

function CloseTrunk()
	ESX.UI.Menu.CloseAll()
	SetVehicleDoorShut(vehicleFrontSaved, 5, false)
	vehicleTrunkOpen = false
	vehicleFrontSaved = nil
end
-- --

-- Key controls --
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

		if vehicleTrunkOpen and vehicleFrontSaved ~= nil then
			if GetVehicleClass(vehicleFrontSaved) ~= 14 and GetEntitySpeed(vehicleFrontSaved) > 0.1 then
				CloseTrunk()
			else
				local playerId = GetPlayerPed(-1)
				local vehicleFront = VehicleInFront()
				local x, y, z = table.unpack(GetEntityCoords(playerId, true))
				local closestVehicle = GetClosestVehicle(x, y, z, 4.0, 0, 71)

				if vehicleFront <= 0 or closestVehicle == nil then
					CloseTrunk()
				end
			end
		elseif IsControlJustReleased(0, 74) then
            local playerId = GetPlayerPed(-1)
            local vehicleFront = VehicleInFront()
            local x, y, z = table.unpack(GetEntityCoords(playerId, true))
            local closestVehicle = GetClosestVehicle(x, y, z, 4.0, 0, 71)

			if IsPedInAnyVehicle(playerId) == false then
				if vehicleFront > 0 and closestVehicle ~= nil and GetPedInVehicleSeat(closestVehicle, -1) ~= playerId and (GetEntitySpeed(closestVehicle) < 0.1 or GetVehicleClass(closestVehicle) == 14) then
					vehicleFrontSaved = vehicleFront
					local vehicleDoorStatus = GetVehicleDoorLockStatus(vehicleFront)
					local isVehicleOpen = vehicleDoorStatus == 1 or vehicleDoorStatus == 0
					local class = GetVehicleClass(vehicleFront)
					local maxTrunkWeight = GetMaxTrunkWeight(vehicleFront)
					if maxTrunkWeight > 0 then
						if isVehicleOpen or class == 15 or class == 16 or class == 14 then
							SetVehicleDoorOpen(vehicleFront, 5, false, false)
							OpenTrunk(vehicleFront)
						else
							ESX.ShowNotification(_U('trunk_locked'))
						end
					end
				else
					vehicleFrontSaved = nil
					ESX.ShowNotification(_U('no_vehicle'))
				end
			end
        end
    end
end)
-- --

-- misc function helpers --
function VehicleInFront()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 4.0, 0.0)
    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
    local a, b, c, d, result = GetRaycastResult(rayHandle)
    return result
end
-- --
