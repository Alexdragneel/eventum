Locales['fr'] = {       
    ['access_storage']                          = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au ~y~coffre~s~',
    ['access_armory']                           = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder à l\'~y~armurerie~s~',
    ['access_food']                             = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au ~y~réfrigérateur~s~',
    ['storage_title']                           = 'Actions casier',
    ['remove_object']                           = 'prendre objet',
    ['deposit_object']                          = 'déposer objet',
    ['get_weapon']                              = 'prendre arme',
    ['put_weapon']                              = 'déposer arme',
    ['buy_weapons']                             = 'acheter armes',
    ['armory_owned']                            = 'possédé',
    ['armory_free']                             = 'gratuit',
    ['armory_item']                             = '$%s',
    ['armory_weapontitle']                      = 'armurerie - Acheter une arme',
    ['armory_componenttitle']                   = 'armurerie - Accessoires d\'armes',
    ['armory_bought']                           = 'vous achetez un ~y~%s~s~ pour ~g~$%s~s~',
    ['armory_money']                            = 'vous ne pouvez pas acheter cette arme',
    ['armory_hascomponent']                     = 'vous avez cet accessoire équipé!',
    ['get_weapon_menu']                         = 'armurerie - Retirer arme',
    ['put_weapon_menu']                         = 'armurerie - Stocker arme',
    ['weapon_already_owned']                    = 'Vous possédez déjà cette arme.',
    ['shop_item']                               = '$%s',
    ['free_item']                               = 'Offert',
    ['storage_stock']                           = 'Coffre',
    ['quantity_invalid']                        = 'quantité invalide',
    ['have_withdrawn']                          = 'vous avez retiré ~y~%sx~s~ ~b~%s~s~',
    ['have_deposited']                          = 'vous avez déposé ~y~%sx~s~ ~b~%s~s~',
    ['quantity']                                = 'quantité',
    ['quantity_with_max']                       = 'quantité (max:%s)',
    ['inventory']                               = 'inventaire',
    ['confirm_no']                              = 'Non',
    ['confirm_yes']                             = 'Oui',
    ['weapon_label']                            = 'libellé'
}
