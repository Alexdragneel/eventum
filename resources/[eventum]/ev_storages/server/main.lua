-- Variables --
ESX = nil
local storagesList = {}
-- --

-- Init ESX stuff --
TriggerEvent(
	"esx:getSharedObject",
	function(obj)
		ESX = obj
	end
)
-- --

AddEventHandler('onResourceStop', function(resourceName)
    if (GetCurrentResourceName() == resourceName) then
        storagesList = {}
    end
end)

AddEventHandler('onResourceStart', function(resourceName)
    if (GetCurrentResourceName() == resourceName) then
		MySQL.Async.fetchAll('SELECT \
				lp_storages.id, \
				lp_storages.name, \
				lp_storages.label, \
				lp_storages.coordsX, \
				lp_storages.coordsY, \
				lp_storages.coordsZ, \
				lp_storages.type, \
				lp_storages.category, \
				lp_storages.active, \
				lp_storages.min_job_grade \
			FROM lp_storages', {},
			function(result)
				for k, storage in ipairs(result) do
					table.insert(storagesList, {
						id = storage.id,
						name = storage.name,
						label = storage.label,
						coords = vector3(storage.coordsX + 0.0, storage.coordsY + 0.0, storage.coordsZ + 0.0),
						type = storage.type, -- Proprio : police, ambulance, public
						category = storage.category, -- Contenu : weapon, all, food, object
						active = storage.active,
						min_job_grade = storage.min_job_grade
					})
				end
			end
		)
	end
end)

function sortWeaponByLabel(a ,b)
	return tostring(a.label) < tostring(b.label)
end

ESX.RegisterServerCallback('lp_storages:getStoragesList', function(source, cb)
	cb(storagesList)
end)

-- Achat d'armes

ESX.RegisterServerCallback('lp_storages:buyWeapon', function(source, cb, weaponName, type, componentNum)
	local xPlayer = ESX.GetPlayerFromId(source)

	local jobAndGrade = xPlayer.job.name .. '_' .. xPlayer.job.grade_name
	local authorizedWeapons, selectedWeapon = Config.AuthorizedWeapons[jobAndGrade]

	for k,v in ipairs(authorizedWeapons) do
		if v.weapon == weaponName then
			selectedWeapon = v
			break
		end
	end

	if not selectedWeapon then
		print(('lp_storages: %s attempted to buy an invalid weapon.'):format(xPlayer.identifier))
		cb(false)
	else
		-- Weapon
		if type == 1 then
			if xPlayer.getMoney() >= selectedWeapon.price then
				xPlayer.removeMoney(selectedWeapon.price)
				xPlayer.addWeapon(weaponName, 100)

				cb(true)
			else
				cb(false)
			end

		-- Weapon Component
		elseif type == 2 then
			local price = selectedWeapon.components[componentNum]
			local weaponNum, weapon = ESX.GetWeapon(weaponName)
			local component = weapon.components[componentNum]

			if component then
				if xPlayer.getMoney() >= price then
					xPlayer.removeMoney(price)
					xPlayer.addWeaponComponent(weaponName, component.name)

					cb(true)
				else
					cb(false)
				end
			else
				print(('lp_storages: %s attempted to buy an invalid weapon component.'):format(xPlayer.identifier))
				cb(false)
			end
		end
	end
end)

-- Stock Arme

ESX.RegisterServerCallback('lp_storages:getArmoryWeapons', function(source, cb, name, sort)
	TriggerEvent('esx_datastore:getSharedDataStore', name, function(store)
		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		if sort and #weapons > 1 then
			table.sort(weapons, sortWeaponByLabel)
		end

		cb(weapons)
	end)
end)

ESX.RegisterServerCallback('lp_storages:addArmoryWeapon', function(source, cb, weaponName, ammo, label, removeWeapon, name)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getSharedDataStore', name, function(store)
		local weapons = store.get('weapons') or {}

		table.insert(weapons, {
			name  = weaponName,
			label = label,
			ammo = ammo,
			count = 1
		})

		store.set('weapons', weapons)
		if removeWeapon then
			xPlayer.removeWeapon(weaponName)
		end
		cb()
	end)
end)

ESX.RegisterServerCallback('lp_storages:removeArmoryWeapon', function(source, cb, label, name)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getSharedDataStore', name, function(store)
		local weapons = store.get('weapons') or {}

		for i=1, #weapons, 1 do
			if weapons[i].label == label or weapons[i].name == label then
				xPlayer.addWeapon(weapons[i].name, weapons[i].ammo or 42)
				table.remove(weapons, i)
				break
			end
		end

		store.set('weapons', weapons)
		cb()
	end)
end)

-- Stock Item

ESX.RegisterServerCallback('lp_storages:getStockItems', function(source, cb, name)
	TriggerEvent('esx_addoninventory:getSharedInventory', name, function(inventory)
		cb(inventory.items)
	end)
end)

RegisterNetEvent('lp_storages:getStockItem')
AddEventHandler('lp_storages:getStockItem', function(itemName, count, name)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	TriggerEvent('esx_addoninventory:getSharedInventory', name, function(inventory)
		local inventoryItem = inventory.getItem(itemName)

		-- is there enough in the society?
		if count > 0 and inventoryItem.count >= count then

			-- can the player carry the said amount of x item?
			if xPlayer.canCarryItem(itemName, count) then
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count, false, true)
				xPlayer.showNotification(_U('have_withdrawn', count, inventoryItem.label))
			else
				xPlayer.showNotification(_U('quantity_invalid'))
			end
		else
			xPlayer.showNotification(_U('quantity_invalid'))
		end
	end)
end)

ESX.RegisterServerCallback('lp_storages:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb({items = items})
end)

RegisterNetEvent('lp_storages:putStockItems')
AddEventHandler('lp_storages:putStockItems', function(itemName, count, name)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', name, function(inventory)
		local sourceItem = xPlayer.getInventoryItem(itemName)
		local inventoryItem = inventory.getItem(itemName)

		if sourceItem and sourceItem.count >= count and count > 0 then
			xPlayer.removeInventoryItem(itemName, count, true)
			inventory.addItem(itemName, count)
			xPlayer.showNotification(_U('have_deposited', count, inventoryItem.label))
		else
			xPlayer.showNotification(_U('quantity_invalid'))
		end
	end)
end)