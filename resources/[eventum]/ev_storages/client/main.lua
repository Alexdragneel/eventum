ESX = nil
local storagesList = {}
local storagesBlips = {}
local isInMarker, hasAlreadyEnteredMarker, isInService = false, false, false
local isInShopMenu = false
local currentStorage

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
			Citizen.Wait(0)
		end
	
		while ESX.GetPlayerData().job == nil do
			Citizen.Wait(10)
		end

		ESX.PlayerData = ESX.GetPlayerData()
		ESX.TriggerServerCallback('esx_service:isInService', function(_isInService)
			if _isInService then
				isInService = _isInService
			end
			DrawBlips()
		end, ESX.PlayerData.job.name)
	end
)
-- --

-- ESX Listeners --
RegisterNetEvent("esx:playerLoaded")
AddEventHandler(
	"esx:playerLoaded",
	function(xPlayer)
		DrawBlips()
	end
)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job)
	if ESX.PlayerData.job.name ~= job.name then
		isInService = false
	end
	ESX.PlayerData.job = job
	DrawBlips()
end)

RegisterNetEvent("esx_service:disableService")
AddEventHandler(
	"esx_service:disableService",
	function()
		isInService = false
		DrawBlips()
	end
)

RegisterNetEvent("esx_service:enableService")
AddEventHandler(
	"esx_service:enableService",
	function()
		isInService = true
		DrawBlips()
	end
)
-- --

-- Draw blips --
function DeleteJobBlips()
	if storagesBlips[1] ~= nil then
		for i=1, #storagesBlips, 1 do
			RemoveBlip(storagesBlips[i])
			storagesBlips[i] = nil
		end
	end
end

function DrawBlips()
	Citizen.CreateThread(function()
		DeleteJobBlips()
		ESX.TriggerServerCallback('lp_storages:getStoragesList', function(_storagesList)
			storagesList = _storagesList
			for i, storage in pairs(storagesList) do
				if playerCanUseStorage(storage) then
					DrawBlip(storage, storage.type)
				end
			end
		end)
	end)
end

function DrawBlip(storage, storageTypeKey)
	if storage.active then
		local blip = AddBlipForCoord(storage.coords)

		SetBlipSprite (blip, Config.BlipSprite[storage.category])
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, Config.BlipSize[storageTypeKey] or 0.7)
		SetBlipColour (blip, Config.BlipColor[storageTypeKey] or 27)
		SetBlipAsShortRange(blip, true)

		BeginTextCommandSetBlipName("STRING")
		AddTextComponentSubstringPlayerName(storage.label)
		EndTextCommandSetBlipName(blip)
		table.insert(storagesBlips, blip)
	end
end
-- --

-- Draw Markers + detection --
Citizen.CreateThread(function()
	while storagesList == nil do
		Citizen.Wait(300)
	end

	while true do
		Citizen.Wait(1)
		local playerCoords = GetEntityCoords(PlayerPedId())
		isInMarker = false

		for i, storage in pairs(storagesList) do
			if playerCanUseStorage(storage) then
				DrawZone(storage, playerCoords)
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('lp_storages:hasExitedMarker')
		end
	end
end)

function DrawZone(storage, playerCoords)
	if storage.active then
		local distance = GetDistanceBetweenCoords(playerCoords, storage.coords, true)

		if distance < Config.DrawDistance then
			if Config.MarkerColor[storage.type] == nil then
				Config.MarkerColor[storage.type] = {
					r = 171, g = 60, b = 230, a = 100
				}
			end
			DrawMarker(
				Config.MarkerType[storage.category],
				storage.coords,
				0.0,
				0.0,
				0.0,
				0.0,
				0.0,
				0.0,
				Config.MarkerSize[storage.category].x,
				Config.MarkerSize[storage.category].y,
				Config.MarkerSize[storage.category].z,
				Config.MarkerColor[storage.type].r or 255,
				Config.MarkerColor[storage.type].g or 0,
				Config.MarkerColor[storage.type].b or 255,
				Config.MarkerColor[storage.type].a or 255,
				false,
				false,
				2,
				true,
				nil,
				nil,
				false
			)
		end

		if distance < 1.4 then
			isInMarker = true
			currentStorage = storage
			if currentStorage.category == 'all' then
				ESX.ShowHelpNotification(_U('access_storage'))
			elseif currentStorage.category == 'weapon' then
				ESX.ShowHelpNotification(_U('access_armory'))
			elseif currentStorage.category == 'food' then
				ESX.ShowHelpNotification(_U('access_food'))
			else
				ESX.ShowHelpNotification(_U('access_storage'))
			end
		end
	end
end

AddEventHandler('lp_storages:hasExitedMarker', function(zone)
	if not isInShopMenu then
		currentStorage = nil
		ESX.UI.Menu.CloseAll()
	end
end)
-- --

-- Key Controls --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if IsControlJustReleased(0, 38) and isInMarker then
			ESX.UI.Menu.CloseAll()
			OpenStorageMenu()
		end
	end
end)
-- --

-- Storage  Menus --
function OpenStorageMenu()
	local elements = {}

	if (currentStorage.category == 'all' or currentStorage.category == 'weapon') then
		local jobAndGrade = ESX.PlayerData.job.name .. '_' .. ESX.PlayerData.job.grade_name
		local authorizedWeapons = Config.AuthorizedWeapons[jobAndGrade]
		if authorizedWeapons and #authorizedWeapons > 0 then
			table.insert(elements, {label = _U('buy_weapons'), value = 'buy_weapons'})
		end
	end

	if (currentStorage.category == 'all' or currentStorage.category == 'weapon') then
		table.insert(elements, {label = _U('get_weapon'),     value = 'get_weapon'})
		table.insert(elements, {label = _U('put_weapon'),     value = 'put_weapon'})
	end

	if (currentStorage.category == 'all' or currentStorage.category == 'object' or currentStorage.category == 'food') then
		table.insert(elements, {label = _U('remove_object'),  value = 'get_stock'})
		table.insert(elements, {label = _U('deposit_object'), value = 'put_stock'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'storage', {
		title    = _U('storage_title'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)

		if data.current.value == 'get_weapon' then
			OpenGetWeaponMenu()
		elseif data.current.value == 'put_weapon' then
			OpenPutWeaponMenu()
		elseif data.current.value == 'buy_weapons' then
			OpenBuyWeaponsMenu()
		elseif data.current.value == 'put_stock' then
			OpenPutStocksMenu()
		elseif data.current.value == 'get_stock' then
			OpenGetStocksMenu()
		end

	end, function(data, menu)
		menu.close()

		CurrentAction     = 'menu_armory'
		CurrentActionMsg  = _U('open_armory')
		CurrentActionData = {station = station}
	end)
end

-- Buy functions --

function OpenBuyWeaponsMenu()
	local elements = {}
	local playerPed = PlayerPedId()

	local jobAndGrade = ESX.PlayerData.job.name .. '_' .. ESX.PlayerData.job.grade_name
	for k,v in ipairs(Config.AuthorizedWeapons[jobAndGrade]) do
		local weaponNum, weapon = ESX.GetWeapon(v.weapon)
		--local components, label = {}
		local hasWeapon = HasPedGotWeapon(playerPed, GetHashKey(v.weapon), false)

		--[[if v.components then
			for i=1, #v.components do
				if v.components[i] then
					local component = weapon.components[i]
					local hasComponent = HasPedGotWeaponComponent(playerPed, GetHashKey(v.weapon), component.hash)

					if hasComponent then
						label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _U('armory_owned'))
					else
						if v.components[i] > 0 then
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _U('armory_item', ESX.Math.GroupDigits(v.components[i])))
						else
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _U('armory_free'))
						end
					end

					table.insert(components, {
						label = label,
						componentLabel = component.label,
						hash = component.hash,
						name = component.name,
						price = v.components[i],
						hasComponent = hasComponent,
						componentNum = i
					})
				end
			end
		end

		if hasWeapon and v.components then
			label = ('%s: <span style="color:green;">></span>'):format(weapon.label)
		elseif hasWeapon and not v.components then
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _U('armory_owned'))
		]]--
		if hasWeapon then
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _U('armory_owned'))
		else
			if v.price > 0 then
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _U('armory_item', ESX.Math.GroupDigits(v.price)))
			else
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _U('armory_free'))
			end
		end

		table.insert(elements, {
			label = label,
			weaponLabel = weapon.label,
			name = weapon.name,
			components = components,
			price = v.price,
			hasWeapon = hasWeapon
		})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons', {
		title    = _U('armory_weapontitle'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if data.current.hasWeapon then
			if #data.current.components > 0 then
				--OpenWeaponComponentShop(data.current.components, data.current.name, menu)
			end
		else
			ESX.TriggerServerCallback('lp_storages:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_U('armory_bought', data.current.weaponLabel, ESX.Math.GroupDigits(data.current.price)))
					end

					menu.close()
					OpenBuyWeaponsMenu()
				else
					ESX.ShowNotification(_U('armory_money'))
				end
			end, data.current.name, 1)
		end
	end, function(data, menu)
		menu.close()
	end)
end

--[[function OpenWeaponComponentShop(components, weaponName, parentShop)
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons_components', {
		title    = _U('armory_componenttitle'),
		align    = 'top-left',
		elements = components
	}, function(data, menu)
		if data.current.hasComponent then
			ESX.ShowNotification(_U('armory_hascomponent'))
		else
			ESX.TriggerServerCallback('lp_storages:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_U('armory_bought', data.current.componentLabel, ESX.Math.GroupDigits(data.current.price)))
					end

					menu.close()
					parentShop.close()
					OpenBuyWeaponsMenu()
				else
					ESX.ShowNotification(_U('armory_money'))
				end
			end, weaponName, 2, data.current.componentNum)
		end
	end, function(data, menu)
		menu.close()
	end)
end]]--

-- Open functions --

function OpenGetWeaponMenu()
	local isSeize = currentStorage.name == 'police_seize'
	ESX.TriggerServerCallback('lp_storages:getArmoryWeapons', function(weapons)
		local elements = {}

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				table.insert(elements, {
					label = weapons[i].label or  ESX.GetWeaponLabel(weapons[i].name),
					value = weapons[i].weapon or weapons[i].name
				})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_get_weapon', {
			title    = _U('get_weapon_menu'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			menu.close()

			if not HasPedGotWeapon(PlayerPedId(), data.current.value, false) then
				ESX.TriggerServerCallback('lp_storages:removeArmoryWeapon', function()
					OpenGetWeaponMenu()
				end, data.current.label, currentStorage.name)
			else
				ESX.ShowNotification(_U("weapon_already_owned"))
				OpenGetWeaponMenu()
			end
		end, function(data, menu)
			menu.close()
		end)
	end, currentStorage.name, isSeize == false)
end

function OpenPutWeaponMenu()
	local elements   = {}
	local playerPed  = PlayerPedId()
	local weaponList = ESX.GetWeaponList()

	for i=1, #weaponList, 1 do
		local weaponHash = GetHashKey(weaponList[i].name)

		if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
			table.insert(elements, {
				label = weaponList[i].label,
				value = weaponList[i].name
			})
		end
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_put_weapon', {
		title    = _U('put_weapon_menu'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'armory_put_weapon_label', {
			title = _U('weapon_label')
		}, function(data2, menu2)
			local label = data2.value or data.current.label
			ESX.TriggerServerCallback('lp_storages:addArmoryWeapon', function()
				if menu2 then
					menu2.close()
				end
				if menu then
					menu.close()
				end
				ESX.SetTimeout(300, function()
					OpenPutWeaponMenu()
				end)
			end, data.current.value, GetAmmoInPedWeapon(GetPlayerPed(-1), GetHashKey(data.current.value)), label, true, currentStorage.name)
		end, function(data2, menu2)
			menu2.close()
		end)
	end, function(data, menu)
		menu.close()
	end)
end

-- Item functions 

function OpenGetStocksMenu()
	ESX.TriggerServerCallback('lp_storages:getStockItems', function(items)
		local elements = {}

		for i=1, #items, 1 do
			table.insert(elements, {
				label = 'x' .. items[i].count .. ' ' .. items[i].label,
				value = items[i].name,
				count = items[i].count
			})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu', {
			title    = _U('storage_stock'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			local itemName = data.current.value
			local itemCount = data.current.count

			ESX.TriggerServerCallback('esx:getAllItems', function(dbItems)
				
				local dbItem = dbItems[itemName]
				local itemWeight = dbItem.weight

				if data.current.count == 1 then
					menu.close()
					TriggerServerEvent('lp_storages:getStockItem', itemName, 1, currentStorage.name)
					Citizen.Wait(300)
					OpenGetStocksMenu()
				else
					ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(itemsPlayer, dbItems)
						ESX.TriggerServerCallback('lp_core:getPlayerMaxWeight', function(maxWeight)

							local currentWeight = 0
							for i=1, #itemsPlayer, 1 do
								local item = itemsPlayer[i]
					
								if item.count > 0 then
									local dbItem = dbItems[item.name]
									if dbItem ~= nil then
										if item.name == itemName then
											itemWeight = item.weight
										end
										currentWeight = currentWeight + (item.weight * item.count)
									end
								end
							end

							local remainingWeight = maxWeight - currentWeight
							local maxQuantity = math.max(math.floor(math.min(remainingWeight / itemWeight, itemCount)), 0)

							ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count', {
								title = _U('quantity_with_max', maxQuantity),
								type = 'number'
							}, function(data2, menu2)
								local count = tonumber(data2.value)

								if not count then
									ESX.ShowNotification(_U('quantity_invalid'))
								else
									menu2.close()
									menu.close()
									TriggerServerEvent('lp_storages:getStockItem', itemName, count, currentStorage.name)

									Citizen.Wait(300)
									OpenGetStocksMenu()
								end
							end, function(data2, menu2)
								menu2.close()
							end)
						end)
					end)
				end
			end)
		end, function(data, menu)
			menu.close()
		end)
	end, currentStorage.name)
end

function OpenPutStocksMenu()
	ESX.TriggerServerCallback('lp_storages:getPlayerInventory', function(inventory)
		local elements = {}

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {
					label = item.label .. ' x' .. item.count,
					type = 'item_standard',
					value = item.name,
					count = item.count
				})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu', {
			title    = _U('inventory'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			local itemName = data.current.value
			local itemCount = data.current.count

			if data.current.count == 1 then
				menu.close()
				TriggerServerEvent('lp_storages:putStockItems', itemName, 1, currentStorage.name)

				Citizen.Wait(300)
				OpenPutStocksMenu()
			else
				ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count', {
					title = _U('quantity_with_max', itemCount),
					type = 'number'
				}, function(data2, menu2)
					local count = tonumber(data2.value)

					if not count then
						ESX.ShowNotification(_U('quantity_invalid'))
					else
						menu2.close()
						menu.close()
						TriggerServerEvent('lp_storages:putStockItems', itemName, count, currentStorage.name)

						Citizen.Wait(300)
						OpenPutStocksMenu()
					end
				end, function(data2, menu2)
					menu2.close()
				end)
			end
		end, function(data, menu)
			menu.close()
		end)
	end)
end

-- Helper functions --

function playerCanUseStorage(storage)
	if storage.type ~= 'public' then
		return ESX.PlayerData.job.name == storage.type and ESX.PlayerData.job.grade >= storage.min_job_grade and isInService
	else
		return true
	end
end

-- --