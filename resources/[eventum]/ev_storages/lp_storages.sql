CREATE TABLE `lp_storages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `coordsX` float NOT NULL,
  `coordsY` float NOT NULL,
  `coordsZ` float NOT NULL,
  `type` varchar(100) NOT NULL,
  `category` varchar(20) NOT NULL DEFAULT 'all',
  `active` tinyint(1) DEFAULT 0,
  `min_job_grade` int DEFAULT 0,
  PRIMARY KEY (`id`)
);

INSERT INTO `lp_storages` (`name`, `label`, `coordsX`, `coordsY`, `coordsZ`, `type`, `category`, `active`, `min_job_grade`) VALUES
('society_police', 'Armurerie LSPD', 452, -980, 29.73, 'police', 'all', 1, 1),
('police_food', 'Réfrigérateur LSPD', 465, -990, 29.73, 'police', 'food', 1, 0),
('police_seize', 'Coffre des saisies LSPD', 477, -989, 23.95, 'police', 'all', 1, 0),
('police_armory2', 'Armurerie Intervention LSPD', 472, -1013, 25.43, 'police', 'weapon', 1, 1),
('society_ambulance', 'Stockage des employés', '298.99', '-598.39', '42.28', 'ambulance', 'all', 1 , 0),
('ambulance_boss', 'Stockage des patrons', '341.69', '-589.26', '42.28', 'ambulance', 'all', 1 , 4),
('ambulance_food', 'Réfrigérateur EMS', 304.5, -600.5, 42.32, 'ambulance', 'food', 1, 0),
('society_butcher', 'Coffre de l\'entreprise', -68.73, 6255.12, 30.09, 'butcher', 'all', 1, 1),
('society_fisherman', 'Coffre de l\'entreprise', 17.47, -2665.04, 5, 'fisherman', 'all', 1, 1),
('society_lumberjack', 'Coffre de l\'entreprise', -585.9, 5344.16, 69.02, 'lumberjack', 'all', 1, 1),
('society_miner', 'Coffre de l\'entreprise', 1050.9, -1958.4, 30.01, 'miner', 'all', 1, 1),
('society_refiner', 'Coffre de l\'entreprise', 251.12, -3075.26, 4.87, 'refiner', 'all', 1, 1),
('society_tailor', 'Coffre de l\'entreprise', 720.78, -965.56, 29.5, 'tailor', 'all', 1, 1),
('society_journaliste', 'Coffre de l\'entreprise', -1051.59, -232.954, 43.05, 'journaliste', 'all', 1, 0),
('society_bennys', 'Coffre de l\'entreprise', -191.13, -1336.66, 29.7, 'bennys', 'all', 1, 0);

insert  into `addon_inventory`(`name`,`label`,`shared`) values 
('police_food','Police',1),
('police_seize','Police',1),
('police_armory2','Police',1),
('ambulance_food','Ambulance',1),
('ambulance_boss', 'Ambulance', 1);

insert  into `datastore`(`name`,`label`,`shared`) values 
('police_food','Police',1),
('police_seize','Police',1),
('police_armory2','Police',1),
('ambulance_food','Ambulance',1),
('ambulance_boss', 'Ambulance', 1);

insert  into `datastore_data`(`name`,`data`) values 
('police_food','{}'),
('police_seize','{}'),
('police_armory2','{}'),
('ambulance_food','{}'),
('ambulance_boss', '{}');