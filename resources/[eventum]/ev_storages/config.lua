Config                      = {}
Config.DrawDistance         = 100
Config.Locale               = "fr"
Config.Storages              = {}
Config.MarkerColor          = {
    police = {r = 50, g = 50, b = 204, a = 100},
	ambulance = {r = 171, g = 60, b = 230, a = 100},
	public = {r = 50, g = 50, b = 204, a = 100}
}
Config.MarkerSize           = {
    all = { x = 1.5, y = 1.5, z = 0.5 },
    weapon = { x = 1.5, y = 1.5, z = 0.5 },
    object = { x = 1.5, y = 1.5, z = 0.5 },
    food = { x = 1.5, y = 1.5, z = 0.5 },
}
Config.MarkerType           = {
    all = 1,
    weapon = 1,
    object = 1,
    food = 1,
}
Config.BlipSprite           = {
    all = 478,
    weapon = 150,
    object = 478,
    food = 52,
}
Config.BlipColor            = {
    police = 27,
	ambulance = 27,
	public = 27
}
Config.BlipSize             = {
    police = 0.7,
	ambulance = 0.7,
	public = 0.7
}

Config.AuthorizedWeapons = {
	police_captain = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, nil}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, nil}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	police_boss = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, nil}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, nil}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	}
}