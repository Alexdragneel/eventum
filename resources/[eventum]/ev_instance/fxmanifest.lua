fx_version 'adamant'

game 'gta5'

description 'Los Plantos Instance'

version '1.0.0'

server_scripts {
    'server/main.lua'
}

dependencies {
    'es_extended',
}
