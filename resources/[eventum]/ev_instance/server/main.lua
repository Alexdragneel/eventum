
ESX = nil
local instances = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('lp_instance:set')
AddEventHandler('lp_instance:set', function(set)
    print('[INSTANCES] Instances now looked like this: ', json.encode(instances))
    local src = source
 
    TriggerClientEvent('DoTheBigRefreshYmaps', src)
    local instanceSource = 0
    if set then
        if set == 0 then
            for k,v in pairs(instances) do
                for k2,v2 in pairs(v) do
                    if v2 == src then
                        table.remove(v, k2)
                        if #v == 0 then
                            instances[k] = nil
                        end
                    end
                end
            end
        end
        instanceSource = set
    else
        instanceSource = math.random(1, 63)

        while instances[instanceSource] and #instances[instanceSource] >= 1 do
            instanceSource = math.random(1, 63)
            Citizen.Wait(1)
        end
    end

    if instanceSource ~= 0 then
        if not instances[instanceSource] then
            instances[instanceSource] = {}
        end
 
        table.insert(instances[instanceSource], src)
    end
 
    SetPlayerRoutingBucket(
        src --[[ string ]], 
        instanceSource
    )
    print('[INSTANCES] Instances now looks like this: ', json.encode(instances))
end)
