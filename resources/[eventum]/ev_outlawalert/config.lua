Config = {}

Config.Locale = 'fr'

Config.timeBetweenTwoCarAlert = 10
Config.timeBetweenTwoMeleeAlert = 60
Config.timeBetweenTwoGunshotAlert = 60

Config.GunshotAlert = true
Config.BlipGunTime = 30
Config.BlipGunRadius = 64.0
Config.DetectGunRadius = 20.0
Config.GunAlertChance = 50

Config.MeleeAlert = false
Config.BlipMeleeTime = 30
Config.BlipMeleeRadius = 64.0
Config.MeleeAlertChance = 25

Config.CarJackingAlert = true
Config.BlipJackingTime = 30
Config.BlipJackingRadius = 64.0
Config.CarJackingAlertChance = 75

Config.CarStealingAlert = true
Config.BlipCarStealingTime = 30
Config.BlipCarStealingRadius = 64.0
Config.CarStealAlertChance = 50


Config.WhitelistedJobs = {
	'police'
}

Config.WhitelistedWeapons = {
    'WEAPON_SNOWBALL',
    'WEAPON_BALL',
    'WEAPON_FIREEXTINGUISHER'
}