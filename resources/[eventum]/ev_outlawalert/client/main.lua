ESX = nil

local isPlayerWhitelisted = false
local streetName = nil
local isInService = false
local isDriving = false
local timeLastCarStolen = 0
local timeLastCombat = 0
local timeLastGunshot = 0
local stolenPlates = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) 
            ESX = obj
        end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	ESX.PlayerData = ESX.GetPlayerData()

	isPlayerWhitelisted = refreshPlayerWhitelisted()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job

	isPlayerWhitelisted = refreshPlayerWhitelisted()
end)

RegisterNetEvent("esx_service:disableService")
AddEventHandler("esx_service:disableService", function()
	isInService = false
    isPlayerWhitelisted = refreshPlayerWhitelisted()
end)

RegisterNetEvent("esx_service:enableService")
AddEventHandler("esx_service:enableService", function()
	isInService = true
    isPlayerWhitelisted = refreshPlayerWhitelisted()
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)

		local playerCoords = GetEntityCoords(PlayerPedId())
		local street1, street2 = GetStreetNameAtCoord(playerCoords.x, playerCoords.y, playerCoords.z)
        if street2 == 0 then
		    streetName = GetStreetNameFromHashKey(street1)
        else
            streetName = GetStreetNameFromHashKey(street1)..'/'..GetStreetNameFromHashKey(street2)
        end
	end
end)

function refreshPlayerWhitelisted()
    if not ESX.PlayerData then
		return false
	end

	if not ESX.PlayerData.job then
		return false
	end

	for k,v in ipairs(Config.WhitelistedJobs) do
		if v == ESX.PlayerData.job.name and isInService then
			return true
		end
	end

	return false
end

RegisterNetEvent('lp_outlawalert:carJackInProgress')
AddEventHandler('lp_outlawalert:carJackInProgress', function(targetCoords)
	if isPlayerWhitelisted and Config.CarJackingAlert then
        local alpha = 250
        local thiefBlip = AddBlipForRadius(targetCoords.x, targetCoords.y, targetCoords.z, Config.BlipJackingRadius)

        SetBlipHighDetail(thiefBlip, true)
        SetBlipColour(thiefBlip, 1)
        SetBlipAlpha(thiefBlip, alpha)
        SetBlipAsShortRange(thiefBlip, true)

        while alpha ~= 0 do
            Citizen.Wait(Config.BlipJackingTime * 4)
            alpha = alpha - 1
            SetBlipAlpha(thiefBlip, alpha)

            if alpha == 0 then
                RemoveBlip(thiefBlip)
                return
            end
        end
	end
end)

RegisterNetEvent('lp_outlawalert:carStealingInProgress')
AddEventHandler('lp_outlawalert:carStealingInProgress', function(targetCoords)
	if isPlayerWhitelisted and Config.CarStealingAlert then
        local alpha = 250
        local thiefBlip = AddBlipForRadius(targetCoords.x, targetCoords.y, targetCoords.z, Config.BlipCarStealingRadius)

        SetBlipHighDetail(thiefBlip, true)
        SetBlipColour(thiefBlip, 1)
        SetBlipAlpha(thiefBlip, alpha)
        SetBlipAsShortRange(thiefBlip, true)

        while alpha ~= 0 do
            Citizen.Wait(Config.BlipCarStealingTime * 4)
            alpha = alpha - 1
            SetBlipAlpha(thiefBlip, alpha)

            if alpha == 0 then
                RemoveBlip(thiefBlip)
                return
            end
        end
	end
end)

RegisterNetEvent('lp_outlawalert:gunshotInProgress')
AddEventHandler('lp_outlawalert:gunshotInProgress', function(targetCoords)
	if isPlayerWhitelisted and Config.GunshotAlert then
		local alpha = 250
		local gunshotBlip = AddBlipForRadius(targetCoords.x, targetCoords.y, targetCoords.z, Config.BlipGunRadius)

		SetBlipHighDetail(gunshotBlip, true)
		SetBlipColour(gunshotBlip, 1)
		SetBlipAlpha(gunshotBlip, alpha)
		SetBlipAsShortRange(gunshotBlip, true)

		while alpha ~= 0 do
			Citizen.Wait(Config.BlipGunTime * 4)
			alpha = alpha - 1
			SetBlipAlpha(gunshotBlip, alpha)

			if alpha == 0 then
				RemoveBlip(gunshotBlip)
				return
			end
		end
	end
end)

RegisterNetEvent('lp_outlawalert:combatInProgress')
AddEventHandler('lp_outlawalert:combatInProgress', function(targetCoords)
	if isPlayerWhitelisted and Config.MeleeAlert then
		local alpha = 250
		local meleeBlip = AddBlipForRadius(targetCoords.x, targetCoords.y, targetCoords.z, Config.BlipMeleeRadius)

		SetBlipHighDetail(meleeBlip, true)
		SetBlipColour(meleeBlip, 1)
		SetBlipAlpha(meleeBlip, alpha)
		SetBlipAsShortRange(meleeBlip, true)

		while alpha ~= 0 do
			Citizen.Wait(Config.BlipMeleeTime * 4)
			alpha = alpha - 1
			SetBlipAlpha(meleeBlip, alpha)

			if alpha == 0 then
				RemoveBlip(meleeBlip)
				return
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(50)

		local playerPed = PlayerPedId()
		local playerCoords = GetEntityCoords(playerPed)

        local pedJustStartedToDriveACar = false
        if not isDriving and playerPed == GetPedInVehicleSeat(GetVehiclePedIsIn(PlayerPedId()), -1) then
            isDriving = true
            pedJustStartedToDriveACar = true
        elseif isDriving and playerPed ~= GetPedInVehicleSeat(GetVehiclePedIsIn(PlayerPedId()), -1) then
            isDriving = false
        end

		if IsPedJacking(playerPed) and Config.CarJackingAlert then

			Citizen.Wait(5000)
			local vehicle = GetVehiclePedIsIn(playerPed, true)

			if vehicle > 0 and IsEntityVisible(playerPed) and not isPlayerWhitelisted then
				
                local plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))

                if not stolenPlates[plate] then

                    stolenPlates[plate] = true

                    ESX.TriggerServerCallback('lp_outlawalert:isVehicleOwnedByAPlayer', function(ownedByAPlayer)
                        if not ownedByAPlayer then

                            if --[[GetGameTimer() - timeLastCarStolen > Config.timeBetweenTwoCarAlert*1000 and]] getRandomBoolean(Config.CarJackingAlertChance) then
                                timeLastCarStolen = GetGameTimer()

                                local vehicleLabel = GetDisplayNameFromVehicleModel(GetEntityModel(vehicle))
                                vehicleLabel = GetLabelText(vehicleLabel)

                                local color1, color2 = GetVehicleColours(vehicle, 1)
                                local color = nil
                                if color1 == color2 then
                                    color = GetColorLabel(color1)
                                else
                                    color = GetColorLabel(color1)..'/'..GetColorLabel(color2)
                                end

                                TriggerServerEvent('lp_outlawalert:carJackInProgress', {
                                    x = ESX.Math.Round(playerCoords.x, 1),
                                    y = ESX.Math.Round(playerCoords.y, 1),
                                    z = ESX.Math.Round(playerCoords.z, 1)
                                }, streetName, vehicleLabel, color, plate)
                            end
                        end
                    end, plate)
                end
			end

		elseif pedJustStartedToDriveACar and Config.CarStealingAlert then

			Citizen.Wait(3000)
			local vehicle = GetVehiclePedIsIn(playerPed, true)

			if vehicle > 0 and IsEntityVisible(playerPed) and not isPlayerWhitelisted then

                local plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))

                if not stolenPlates[plate] then

                    stolenPlates[plate] = true

                    ESX.TriggerServerCallback('lp_outlawalert:isVehicleOwnedByAPlayer', function(ownedByAPlayer)
                        if not ownedByAPlayer then

                            if --[[GetGameTimer() - timeLastCarStolen > Config.timeBetweenTwoCarAlert*1000 and]] getRandomBoolean(Config.CarStealAlertChance) then
                                timeLastCarStolen = GetGameTimer()
                                local vehicleLabel = GetDisplayNameFromVehicleModel(GetEntityModel(vehicle))
                                vehicleLabel = GetLabelText(vehicleLabel)

                                local color1, color2 = GetVehicleColours(vehicle, 1)
                                local color = nil
                                if color1 == color2 then
                                    color = GetColorLabel(color1)
                                else
                                    color = GetColorLabel(color1)..'/'..GetColorLabel(color2)
                                end

                                TriggerServerEvent('lp_outlawalert:carStealingInProgress', {
                                    x = ESX.Math.Round(playerCoords.x, 1),
                                    y = ESX.Math.Round(playerCoords.y, 1),
                                    z = ESX.Math.Round(playerCoords.z, 1)
                                }, streetName, vehicleLabel, color, plate)
                            end
                        end
                    end, plate)
                end
			end

		elseif IsPedInMeleeCombat(playerPed) and Config.MeleeAlert and getNearestPedDistance() < 1.5 then

			Citizen.Wait(3000)

			if not isPlayerWhitelisted then

                if GetGameTimer() - timeLastCombat > Config.timeBetweenTwoMeleeAlert*1000 and getRandomBoolean(Config.MeleeAlertChance) then
                    timeLastCombat = GetGameTimer()
                    TriggerServerEvent('lp_outlawalert:combatInProgress', {
                        x = ESX.Math.Round(playerCoords.x, 1),
                        y = ESX.Math.Round(playerCoords.y, 1),
                        z = ESX.Math.Round(playerCoords.z, 1)
                    }, streetName)
                end
			end

		elseif IsPedShooting(playerPed) and Config.GunshotAlert and getNearestPedDistance() < Config.DetectGunRadius then

            local selectedWeaponHash = GetSelectedPedWeapon(playerPed)
            local hasSilencer = IsPedCurrentWeaponSilenced(playerPed)
            local weaponLabel = nil
            local isWeaponWhitelisted = false

            local weaponList = ESX.GetWeaponList()
            for i=1, #weaponList, 1 do
                if selectedWeaponHash == GetHashKey(weaponList[i].name) then
                    for j=1, #Config.WhitelistedWeapons, 1 do
                        if weaponList[i].name == Config.WhitelistedWeapons[j] then
                            isWeaponWhitelisted = true
                            break
                        end
                    end
                    weaponLabel = weaponList[i].label
                    break
                end
            end

            if not isWeaponWhitelisted then
                Citizen.Wait(3000)

                if not isPlayerWhitelisted then

                    if GetGameTimer() - timeLastGunshot > Config.timeBetweenTwoGunshotAlert*1000 and getRandomBoolean(Config.GunAlertChance) then
                        timeLastGunshot = GetGameTimer()

                        TriggerServerEvent('lp_outlawalert:gunshotInProgress', {
                            x = ESX.Math.Round(playerCoords.x, 1),
                            y = ESX.Math.Round(playerCoords.y, 1),
                            z = ESX.Math.Round(playerCoords.z, 1)
                        }, streetName, weaponLabel)
                    end
                end
            end
		end
	end
end)

function getNearestPedDistance()

    local playerPed    = PlayerPedId()
	local playerCoords = GetEntityCoords(playerPed)
    local closestPed   = ESX.Game.GetClosestAlivePed(playerCoords)
    local distance     = #(playerCoords - GetEntityCoords(closestPed))
    return distance
end

function getRandomBoolean(truePercentage)
    return GetRandomIntInRange(1,101) < truePercentage
end

function GetColorLabel(color)
	local colors = {
        { index = 0, label = _U('black')},
        { index = 1, label = _U('graphite')},
        { index = 2, label = _U('black_metallic')},
        { index = 3, label = _U('caststeel')},
        { index = 11, label = _U('black_anth')},
        { index = 12, label = _U('matteblack')},
        { index = 15, label = _U('darknight')},
        { index = 16, label = _U('deepblack')},
        { index = 21, label = _U('oil')},
        { index = 147, label = _U('carbon')},
        { index = 106, label = _U('vanilla')},
        { index = 107, label = _U('creme')},
        { index = 111, label = _U('white')},
        { index = 112, label = _U('polarwhite')},
        { index = 113, label = _U('beige')},
        { index = 121, label = _U('mattewhite')},
        { index = 122, label = _U('snow')},
        { index = 131, label = _U('cotton')},
        { index = 132, label = _U('alabaster')},
        { index = 134, label = _U('purewhite')},
        { index = 4, label = _U('silver')},
        { index = 5, label = _U('metallicgrey')},
        { index = 6, label = _U('laminatedsteel')},
        { index = 7, label = _U('darkgray')},
        { index = 8, label = _U('rockygray')},
        { index = 9, label = _U('graynight')},
        { index = 10, label = _U('aluminum')},
        { index = 13, label = _U('graymat')},
        { index = 14, label = _U('lightgrey')},
        { index = 17, label = _U('asphaltgray')},
        { index = 18, label = _U('grayconcrete')},
        { index = 19, label = _U('darksilver')},
        { index = 20, label = _U('magnesite')},
        { index = 22, label = _U('nickel')},
        { index = 23, label = _U('zinc')},
        { index = 24, label = _U('dolomite')},
        { index = 25, label = _U('bluesilver')},
        { index = 26, label = _U('titanium')},
        { index = 66, label = _U('steelblue')},
        { index = 93, label = _U('champagne')},
        { index = 144, label = _U('grayhunter')},
        { index = 156, label = _U('grey')},
        { index = 27, label = _U('red')},
        { index = 28, label = _U('torino_red')},
        { index = 29, label = _U('poppy')},
        { index = 30, label = _U('copper_red')},
        { index = 31, label = _U('cardinal')},
        { index = 32, label = _U('brick')},
        { index = 33, label = _U('garnet')},
        { index = 34, label = _U('cabernet')},
        { index = 35, label = _U('candy')},
        { index = 39, label = _U('matte_red')},
        { index = 40, label = _U('dark_red')},
        { index = 43, label = _U('red_pulp')},
        { index = 44, label = _U('bril_red')},
        { index = 46, label = _U('pale_red')},
        { index = 143, label = _U('wine_red')},
        { index = 150, label = _U('volcano')},
        { index = 135, label = _U('electricpink')},
        { index = 136, label = _U('salmon')},
        { index = 137, label = _U('sugarplum')},
        { index = 54, label = _U('topaz')},
        { index = 60, label = _U('light_blue')},
        { index = 61, label = _U('galaxy_blue')},
        { index = 62, label = _U('dark_blue')},
        { index = 63, label = _U('azure')},
        { index = 64, label = _U('navy_blue')},
        { index = 65, label = _U('lapis')},
        { index = 67, label = _U('blue_diamond')},
        { index = 68, label = _U('surfer')},
        { index = 69, label = _U('pastel_blue')},
        { index = 70, label = _U('celeste_blue')},
        { index = 73, label = _U('rally_blue')},
        { index = 74, label = _U('blue_paradise')},
        { index = 75, label = _U('blue_night')},
        { index = 77, label = _U('cyan_blue')},
        { index = 78, label = _U('cobalt')},
        { index = 79, label = _U('electric_blue')},
        { index = 80, label = _U('horizon_blue')},
        { index = 82, label = _U('metallic_blue')},
        { index = 83, label = _U('aquamarine')},
        { index = 84, label = _U('blue_agathe')},
        { index = 85, label = _U('zirconium')},
        { index = 86, label = _U('spinel')},
        { index = 87, label = _U('tourmaline')},
        { index = 127, label = _U('paradise')},
        { index = 140, label = _U('bubble_gum')},
        { index = 141, label = _U('midnight_blue')},
        { index = 146, label = _U('forbidden_blue')},
        { index = 157, label = _U('glacier_blue')},
        { index = 42, label = _U('yellow')},
        { index = 88, label = _U('wheat')},
        { index = 89, label = _U('raceyellow')},
        { index = 91, label = _U('paleyellow')},
        { index = 126, label = _U('lightyellow')},
        { index = 49, label = _U('met_dark_green')},
        { index = 50, label = _U('rally_green')},
        { index = 51, label = _U('pine_green')},
        { index = 52, label = _U('olive_green')},
        { index = 53, label = _U('light_green')},
        { index = 55, label = _U('lime_green')},
        { index = 56, label = _U('forest_green')},
        { index = 57, label = _U('lawn_green')},
        { index = 58, label = _U('imperial_green')},
        { index = 59, label = _U('green_bottle')},
        { index = 92, label = _U('citrus_green')},
        { index = 125, label = _U('green_anis')},
        { index = 128, label = _U('khaki')},
        { index = 133, label = _U('army_green')},
        { index = 151, label = _U('dark_green')},
        { index = 152, label = _U('hunter_green')},
        { index = 155, label = _U('matte_foilage_green')},
        { index = 36, label = _U('tangerine')},
        { index = 38, label = _U('orange')},
        { index = 41, label = _U('matteorange')},
        { index = 123, label = _U('lightorange')},
        { index = 124, label = _U('peach')},
        { index = 130, label = _U('pumpkin')},
        { index = 138, label = _U('orangelambo')},
        { index = 45, label = _U('copper')},
        { index = 47, label = _U('lightbrown')},
        { index = 48, label = _U('darkbrown')},
        { index = 90, label = _U('bronze')},
        { index = 94, label = _U('brownmetallic')},
        { index = 95, label = _U('expresso')},
        { index = 96, label = _U('chocolate')},
        { index = 97, label = _U('terracotta')},
        { index = 98, label = _U('marble')},
        { index = 99, label = _U('sand')},
        { index = 100, label = _U('sepia')},
        { index = 101, label = _U('bison')},
        { index = 102, label = _U('palm')},
        { index = 103, label = _U('caramel')},
        { index = 104, label = _U('rust')},
        { index = 105, label = _U('chestnut')},
        { index = 108, label = _U('brown')},
        { index = 109, label = _U('hazelnut')},
        { index = 110, label = _U('shell')},
        { index = 114, label = _U('mahogany')},
        { index = 115, label = _U('cauldron')},
        { index = 116, label = _U('blond')},
        { index = 129, label = _U('gravel')},
        { index = 153, label = _U('darkearth')},
        { index = 154, label = _U('desert')},
        { index = 71, label = _U('indigo')},
        { index = 72, label = _U('deeppurple')},
        { index = 76, label = _U('darkviolet')},
        { index = 81, label = _U('amethyst')},
        { index = 142, label = _U('mysticalviolet')},
        { index = 145, label = _U('purplemetallic')},
        { index = 148, label = _U('matteviolet')},
        { index = 149, label = _U('mattedeeppurple')},
        { index = 117, label = _U('brushedchrome')},
        { index = 118, label = _U('blackchrome')},
        { index = 119, label = _U('brushedaluminum')},
        { index = 120, label = _U('chrome')},
        { index = 37, label = _U('gold')},
        { index = 158, label = _U('puregold')},
        { index = 159, label = _U('brushedgold')},
        { index = 160, label = _U('lightgold')}
    }
    for i=1, #colors, 1 do
        if colors[i].index == color then
            return colors[i].label
        end
    end
    return ''
end