ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('lp_outlawalert:carJackInProgress')
AddEventHandler('lp_outlawalert:carJackInProgress', function(targetCoords, streetName, vehicleLabel, vehicleColor, plate)
	local notification = {
        title = 'LSPD',
        subject = _U('subject_carjack'),
        picture = 'CHAR_CALL911',
        iconType = 1
    }

    local baseMessage = _('notification_message_location', streetName)
    baseMessage = baseMessage .. '\n' .. _U('carjack', vehicleLabel, vehicleColor)
    baseMessage = baseMessage .. '\n' .. _U('notification_message_plate', plate)

    notification.msg = baseMessage

    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
	TriggerClientEvent('lp_outlawalert:carJackInProgress', -1, targetCoords)
end)

RegisterServerEvent('lp_outlawalert:carStealingInProgress')
AddEventHandler('lp_outlawalert:carStealingInProgress', function(targetCoords, streetName, vehicleLabel, vehicleColor, plate)
	local notification = {
        title = 'LSPD',
        subject = _U('subject_car_stealing'),
        picture = 'CHAR_CALL911',
        iconType = 1
    }

    local baseMessage = _('notification_message_location', streetName)
    baseMessage = baseMessage .. '\n' .. _U('car_stealing', vehicleLabel, vehicleColor)
    baseMessage = baseMessage .. '\n' .. _U('notification_message_plate', plate)

    notification.msg = baseMessage

    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
	TriggerClientEvent('lp_outlawalert:carStealingInProgress', -1, targetCoords)
end)

RegisterServerEvent('lp_outlawalert:combatInProgress')
AddEventHandler('lp_outlawalert:combatInProgress', function(targetCoords, streetName)
	local notification = {
        title = 'LSPD',
        subject = _U('subject_combat'),
        picture = 'CHAR_CALL911',
        iconType = 1
    }

    local baseMessage = _('notification_message_location', streetName)
    baseMessage = baseMessage .. '\n' .. _U('combat')

    notification.msg = baseMessage
    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
	TriggerClientEvent('lp_outlawalert:combatInProgress', -1, targetCoords)
end)

RegisterServerEvent('lp_outlawalert:gunshotInProgress')
AddEventHandler('lp_outlawalert:gunshotInProgress', function(targetCoords, streetName, weaponLabel)
    local notification = {
        title = 'LSPD',
        subject = _U('subject_gunshot'),
        picture = 'CHAR_CALL911',
        iconType = 1
    }

    local baseMessage = _('notification_message_location', streetName)
    baseMessage = baseMessage .. '\n' .. _U('gunshot', weaponLabel)

    notification.msg = baseMessage
    TriggerEvent('esx_service:notifyAllInService', notification, 'police', true)
	TriggerClientEvent('lp_outlawalert:gunshotInProgress', -1, targetCoords)
end)

ESX.RegisterServerCallback('lp_outlawalert:isVehicleOwnedByAPlayer', function(source, cb, plate)
	MySQL.Async.fetchAll('SELECT owner FROM owned_vehicles WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)
		if result[1] then
			cb(true)
		else
			cb(false)
		end
	end)
end)