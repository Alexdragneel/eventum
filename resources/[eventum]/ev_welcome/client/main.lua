local fade, locked = false, false
local delay = 2

RegisterNetEvent('lp_welcome:fadeIn')
AddEventHandler('lp_welcome:fadeIn', function()
    Citizen.CreateThread(function()
        if not locked then
            locked = true
            SendNUIMessage({
                setDisplayWelcomeTitle = true
            })
            while delay > 0 do
                Citizen.Wait(1000)
                delay = delay - 1
            end
            SendNUIMessage({
                setWelcomeTitleOpacity = true,
                welcomeOpacity    = 1
            })
            Citizen.Wait(5000)
            SendNUIMessage({
                setWelcomeTitleOpacity = true,
                welcomeOpacity    = 0
            })
        end
    end)
end)
