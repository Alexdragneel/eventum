fx_version 'adamant'

game 'gta5'

description 'Los Plantos Welcome Airport'

version '1.0.0'

client_scripts {
	'client/main.lua'
}

ui_page 'html/ui.html'

files {
	'html/ui.html',
	'html/losplantos.png',
	'html/css/app.css',
	'html/js/app.js'
}
