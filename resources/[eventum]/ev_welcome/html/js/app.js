(function () {
	window.onData = function (data) {
		if (data.setDisplayWelcomeTitle) {
			$('#welcome').removeAttr('style');
			$('#welcome').addClass('displayWelcome');
		}

		if (data.setWelcomeTitleOpacity) {
			if (data.welcomeOpacity === 1) {
				$('#welcome').addClass('animOpacity');
			} else {
				$('#welcome').removeClass('animOpacity')
			}
		}
	};

	window.onload = function (e) {
		window.addEventListener('message', function (event) {
			onData(event.data);
		});
	};
})();