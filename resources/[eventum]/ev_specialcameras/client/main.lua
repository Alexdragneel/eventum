local usingCam = false
local camera = false
local fov = Config.fov_max
local UI = { 
	x =  0.000 ,
	y = -0.001 ,
}
local usedProp = nil

RegisterNetEvent('lp_specialcameras:activateCamera')
AddEventHandler('lp_specialcameras:activateCamera', function(cameraMode, anUsedProp)

    usedProp = anUsedProp

    usingCamera = true

    SetTimecycleModifier("default")

    SetTimecycleModifierStrength(0.3)
    
    local scaleform = RequestScaleformMovie("security_camera")
    local scaleform2 = RequestScaleformMovie("breaking_news")
    local scaleform3 = RequestScaleformMovie("BINOCULARS")

    if cameraMode == 'BREAKING_NEWS' then
        while not HasScaleformMovieLoaded(scaleform) do
            Citizen.Wait(10)
        end
        while not HasScaleformMovieLoaded(scaleform2) do
            Citizen.Wait(10)
        end
    elseif cameraMode == 'CINEMA' then
        while not HasScaleformMovieLoaded(scaleform) do
            Citizen.Wait(10)
        end
    elseif cameraMode == 'BINOCULARS' then
        while not HasScaleformMovieLoaded(scaleform3) do
            Citizen.Wait(10)
        end
    end

    local lPed = GetPlayerPed(-1)
    local vehicle = GetVehiclePedIsIn(lPed)
    local cam1 = CreateCam("DEFAULT_SCRIPTED_FLY_CAMERA", true)

    AttachCamToEntity(cam1, lPed, 0.0,0.0,0.5, true)
    SetCamRot(cam1, 2.0,1.0,GetEntityHeading(lPed))
    SetCamFov(cam1, fov)
    RenderScriptCams(true, false, 0, 1, 0)

    if cameraMode == 'BREAKING_NEWS' then
        PushScaleformMovieFunction(scaleform, "SET_CAM_LOGO")
        PushScaleformMovieFunction(scaleform2, "breaking_news")
    elseif cameraMode == 'CINEMA' then
        PushScaleformMovieFunction(scaleform, "security_camera")
    elseif cameraMode == 'BINOCULARS' then
        PushScaleformMovieFunction(scaleform3, "SET_CAM_LOGO")
        PushScaleformMovieFunctionParameterInt(1)
    end

    PopScaleformMovieFunctionVoid()

    while usingCamera and not IsEntityDead(lPed) and (GetVehiclePedIsIn(lPed) == vehicle) do
        setPlayerAndItemsInvisible()
        if IsControlJustPressed(1, 177) then
            PlaySoundFrontend(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", false)
            usingCamera = false
        end

        SetEntityRotation(lPed, 0, 0, new_z,2, true)
            
        local zoomvalue = (1.0/(Config.fov_max-Config.fov_min))*(fov-Config.fov_min)
        CheckInputRotation(cam1, zoomvalue)

        HandleZoom(cam1)
        HideHUDThisFrame()

        if cameraMode == 'BREAKING_NEWS' then
            DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255)
            DrawScaleformMovie(scaleform2, 0.5, 0.63, 1.0, 1.0, 255, 255, 255, 255)
            Breaking("BREAKING NEWS")
        elseif cameraMode == 'CINEMA' then
            drawRct(UI.x + 0.0, 	UI.y + 0.0, 1.0,0.15,0,0,0,255) -- Top Bar
            DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255)
            drawRct(UI.x + 0.0, 	UI.y + 0.85, 1.0,0.16,0,0,0,255) -- Bottom Bar
        elseif cameraMode == 'BINOCULARS' then
            DrawScaleformMovieFullscreen(scaleform3, 255, 255, 255, 255)
        end
        
        local camHeading = GetGameplayCamRelativeHeading()
        local camPitch = GetGameplayCamRelativePitch()
        if camPitch < -70.0 then
            camPitch = -70.0
        elseif camPitch > 42.0 then
            camPitch = 42.0
        end
        camPitch = (camPitch + 70.0) / 112.0
        
        if camHeading < -180.0 then
            camHeading = -180.0
        elseif camHeading > 180.0 then
            camHeading = 180.0
        end
        camHeading = (camHeading + 180.0) / 360.0
        
        Citizen.InvokeNative(0xD5BB4025AE449A4E, GetPlayerPed(-1), "Pitch", camPitch)
        Citizen.InvokeNative(0xD5BB4025AE449A4E, GetPlayerPed(-1), "Heading", camHeading * -1.0 + 1.0)
        
        Citizen.Wait(0)
    end
    setPlayerAndItemsVisible()
    usingCamera = false
    ClearTimecycleModifier()
    fov = (Config.fov_max+Config.fov_min)*0.5
    RenderScriptCams(false, false, 0, 1, 0)
    SetScaleformMovieAsNoLongerNeeded(scaleform)
    DestroyCam(cam1, false)
    SetNightvision(false)
    SetSeethrough(false)

    if cameraMode == 'BINOCULARS' then
        ClearPedTasks(PlayerPedId())
    end
end)

-------------------------------------------------------------------------
-- Fonctions Utilitaires --
-------------------------------------------------------------------------

function CheckInputRotation(cam, zoomvalue)
	local rightAxisX = GetDisabledControlNormal(0, 220)
	local rightAxisY = GetDisabledControlNormal(0, 221)
	local rotation = GetCamRot(cam, 2)
	if rightAxisX ~= 0.0 or rightAxisY ~= 0.0 then
		new_z = rotation.z + rightAxisX*-1.0*(Config.speed_ud)*(zoomvalue+0.1)
		new_x = math.max(math.min(70.0, rotation.x + rightAxisY*-1.0*(Config.speed_lr)*(zoomvalue+0.1)), -89.5)
		SetCamRot(cam, new_x, 0.0, new_z, 2)
	end
end

function HandleZoom(cam)
	local lPed = GetPlayerPed(-1)
	if not ( IsPedSittingInAnyVehicle( lPed ) ) then

		if IsControlJustPressed(0,241) then
			fov = math.max(fov - Config.zoomspeed, Config.fov_min)
		end
		if IsControlJustPressed(0,242) then
			fov = math.min(fov + Config.zoomspeed, Config.fov_max)
		end
		local current_fov = GetCamFov(cam)
		if math.abs(fov-current_fov) < 0.1 then
			fov = current_fov
		end
		SetCamFov(cam, current_fov + (fov - current_fov)*0.05)
	else
		if IsControlJustPressed(0,17) then
			fov = math.max(fov - Config.zoomspeed, Config.fov_min)
		end
		if IsControlJustPressed(0,16) then
			fov = math.min(fov + Config.zoomspeed, Config.fov_max)
		end
		local current_fov = GetCamFov(cam)
		if math.abs(fov-current_fov) < 0.1 then
			fov = current_fov
		end
		SetCamFov(cam, current_fov + (fov - current_fov)*0.05)
	end
end

function setPlayerAndItemsInvisible()
    SetEntityLocallyInvisible(GetPlayerPed(-1))
    if usedProp then
        SetEntityLocallyInvisible(usedProp)
    end
end

function setPlayerAndItemsVisible()
    SetEntityLocallyVisible(GetPlayerPed(-1))
    if usedProp then
        SetEntityLocallyVisible(usedProp)
    end
end

-------------------------------------------------------------------------
-- Fonctions Affichage UI --
-------------------------------------------------------------------------

function HideHUDThisFrame()
	HideHelpTextThisFrame()
	HideHudAndRadarThisFrame()
	HideHudComponentThisFrame(1)
	HideHudComponentThisFrame(2)
	HideHudComponentThisFrame(3)
	HideHudComponentThisFrame(4)
	HideHudComponentThisFrame(6)
	HideHudComponentThisFrame(7)
	HideHudComponentThisFrame(8)
	HideHudComponentThisFrame(9)
	HideHudComponentThisFrame(13)
	HideHudComponentThisFrame(11)
	HideHudComponentThisFrame(12)
	HideHudComponentThisFrame(15)
	HideHudComponentThisFrame(18)
	HideHudComponentThisFrame(19)
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end

function Breaking(text)
    SetTextColour(255, 255, 255, 255)
    SetTextFont(8)
    SetTextScale(1.2, 1.2)
    SetTextWrap(0.0, 1.0)
    SetTextCentre(false)
    SetTextDropshadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 205)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(0.2, 0.85)
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if usingCamera then
            DisableControlAction(0, 73, true)
        end
    end
end)