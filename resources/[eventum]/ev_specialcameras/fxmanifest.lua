fx_version "adamant"

game "gta5"

description "Los Plantos Special Cameras"

version "1.0.0"

client_scripts {
	"@es_extended/locale.lua",
	"config.lua",
	"client/main.lua"
}

dependencies {
	"es_extended"
}
