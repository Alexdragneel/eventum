INSERT INTO `lp_items` (`name`, `label`, `sellPrice`, `farmPrice`, `weight`, `actions`, `icon`) VALUES
    ('camera', 'Camera', '500', '0', '5000', '{"trigger":{"type":"client","event":"lp_jobs:journaliste:toggleCam"}}', 'camera.png'),
    ('micro', 'Micro', '100', '0', '1000', '{"trigger":{"type":"client","event":"lp_jobs:journaliste:toggleMic"}}', 'micro.png'),
    ('micro_boom', 'Perche de micro', '200', '0', '2000', '{"trigger":{"type":"client","event":"lp_jobs:journaliste:toggleBMic"}}', 'micro_boom.png'),
    ('binoculars', 'Jumelles', '250', '0', '1000', '{"trigger":{"type":"client","event":"lp_items:binoculars"}}', 'binoculars.png')
;