fx_version "adamant"

game "gta5"

description "Los Plantos HUD"

version "1.0.3"

client_scripts {
	"config.lua",
	"functions.lua",
	"client/main.lua"
}

server_scripts {
	"config.lua",
	"server/main.lua"
}

ui_page 'html/ui.html'

files {
	'html/ui.html',
	'html/css/app.css',
	'html/scripts/app.js',
	'html/img/seatbelt_icon.svg',
	'html/img/vehicle-gear.png',
	'html/sounds/*.ogg',
	'postalcodes.json'
}

postal_file('postalcodes.json')
