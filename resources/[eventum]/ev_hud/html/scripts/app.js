(function () {
	let status = [];
	let fuels = [];
	let audioPlayer = null;
	let loopAudioPlayer = null;
	let previousGear = 'N';

	let setHudScale = function(scale) {
		let zoomValue;
		switch (scale) {
			case 'small':
				zoomValue = 1;
				break;
			case 'medium':
				zoomValue = 1.3;
				break;
			case 'big':
				zoomValue = 1.6;
				break;
		}

		$("#speedometer_container").css('zoom', zoomValue);
		$("#fuel_container").css('zoom', zoomValue);
		$("#rpm_container").css('zoom', zoomValue);
		$("#container_seatbelt").css('zoom', zoomValue);
		$("#container_headlight").css('zoom', zoomValue);
		$("#container_blinker_left").css('zoom', zoomValue);
		$("#container_blinker_right").css('zoom', zoomValue);
		window.localStorage['hud_scale_save'] = scale;
	}

	let renderFuels = function () {

		for (let i = 0; i < fuels.length; i++) {
			const fuelCircle = document.querySelector('#progress-fuel svg circle.fuel');
			const fuelVal = Math.floor(parseFloat(fuels[i].val));
			fuelCircle.style.stroke = fuels[i].color;
			fuelCircle.style.strokeDashoffset = fuelVal;
			$('#vehicle-fuel .fuel-icon').html(fuels[i].icon);
		}

	};

	let renderStatus = function () {

		$('#status_list').html('');

		status.sort((st1 ,st2) => st1.order - st2.order);
		for (let i = 0; i < status.length; i++) {

			if (!status[i].visible) {
				continue;
			}

			let statusDiv = $(
				'<div class="wrapper">' +
					'<span class="status_icon material-icons">' + status[i].icon + '</span>' +
					'<div class="status">' +
						'<div class="status_inner">' +
							'<div class="status_val"></div>' +
						'</div>' +
					'</div>' +
				'</div>');

			statusDiv.find('.status_inner')
				.css({ 'border': '1px solid ' + status[i].color })
				;

			statusDiv.find('.status_val')
				.css({
					'background-color': status[i].color,
					'width': (status[i].val / 10000) + '%'
				})
				;

			$('#status_list').append(statusDiv);
		}

	};

	let getPercentageClass = function (percentage) {
		if (percentage >= 0 && percentage <= 10) {
			return 'zero';
		} else if (percentage > 10 && percentage <= 20) {
			return 'ten'
		} else if (percentage > 20 && percentage <= 30) {
			return 'twenty';
		} else if (percentage > 30 && percentage <= 40) {
			return 'thirty';
		} else if (percentage > 40 && percentage <= 50) {
			return 'forty';
		} else if (percentage > 50 && percentage <= 60) {
			return 'fifty';
		} else if (percentage > 60 && percentage <= 70) {
			return 'sixty';
		} else if (percentage > 70 && percentage <= 80) {
			return 'seventy';
		} else if (percentage > 80 && percentage <= 90) {
			return 'eighty';
		} else if (percentage > 90) {
			return 'ninety';
		} else {
			return 'zero';
		}
	}

	window.addEventListener('animationstart', e => (e.animationName == 'blink' || e.animationName == 'blinksirens1' || e.animationName == 'blinksirens2') && e.target.getAnimations({subtree: true}).forEach(e => e.startTime = 0), true)

	window.onData = function (data) {
		if (data.setStatus) {
			if (data.update) {
				status = []
	
				for (let i = 0; i < data.status.length; i++) {
					status.push(data.status[i]);
				}
	
				renderStatus();
			}
	
			if (data.setDisplay) {
				$('#status_list').css({ 'opacity': data.display });
			}
		} else if (data.setFuel) {
			if (data.update) {
				fuels = []
				for (let i = 0; i < data.fuels.length; i++) {
					fuels.push(data.fuels[i]);
				}
	
				renderFuels();
			}
	
			if (data.setDisplay) {
				if (data.display === 0) {
					$("#fuel_container").removeClass('active');
					$("#fuel_container").addClass('inactive');
				} else {
					$("#fuel_container").removeClass('inactive');
					$("#fuel_container").addClass('active');
				}
			}
		} else if (data.setSeatbelt) {
			$("#container_seatbelt").stop(false, true);
			if (data.displaySeatbelt == 'true') {
				$("#container_seatbelt").removeClass('inactive');
				$("#container_seatbelt").addClass('active');
			} else if (data.displaySeatbelt == 'false') {
				$("#container_seatbelt").removeClass('active');
				$("#container_seatbelt").addClass('inactive');
			}

			if (data.seatbeltOn) {
				$("#container_seatbelt").stop(false, true);
				const seatbeltSVG = document.querySelector('#seatbelt svg');
				if (data.seatbeltOn === 'true') {
					seatbeltSVG.classList.remove('blink-image');
					seatbeltSVG.classList.remove('off');
				} else if (data.seatbeltOn === 'false') {
					seatbeltSVG.classList.add('blink-image');
					seatbeltSVG.classList.add('off');
				}
			}

			if (data.transactionType == "playSound") {
				if (audioPlayer != null) {
					audioPlayer.pause();
				}
				audioPlayer = new Audio("./sounds/" + data.transactionFile + ".ogg");
				audioPlayer.volume = data.transactionVolume;
				audioPlayer.play();
			} else if (data.transactionType == "playSoundLoop") {
				if (loopAudioPlayer != null) {
					loopAudioPlayer.pause();
				}
				loopAudioPlayer = new Audio("./sounds/" + data.transactionFile + ".ogg");
				loopAudioPlayer.volume = data.transactionVolume;
				loopAudioPlayer.play();
			}
		} else if (data.speedometer) {
			if (data.speedometer === "hide") {
				$("#speedometer_container").removeClass('active');
				$("#speedometer_container").addClass('inactive');
				$("#rpm_container").removeClass('active');
				$("#rpm_container").addClass('inactive');
				$("#container_headlight").removeClass('active');
				$("#container_headlight").addClass('inactive');
				$("#container_blinker_left").removeClass('active');
				$("#container_blinker_left").addClass('inactive');
				$("#container_blinker_right").removeClass('active');
				$("#container_blinker_right").addClass('inactive');
			} else {
				let hudScale = window.localStorage['hud_scale_save'] || 'small'
				setHudScale(hudScale);
				$("#speedometer_container").removeClass('inactive');
				$("#speedometer_container").addClass('active');
				$("#rpm_container").removeClass('inactive');
				$("#rpm_container").addClass('active');

				if (data.speedometer === "car") {
					$("#container_headlight").removeClass('inactive');
					$("#container_headlight").addClass('active');
					$("#container_blinker_left").removeClass('inactive');
					$("#container_blinker_left").addClass('active');
					$("#container_blinker_right").removeClass('inactive');
					$("#container_blinker_right").addClass('active');

					if (data.vehicleGear === 0) {
						if (data.carSpeed > 0) {
							data.vehicleGear = 'R'
						} else {
							data.vehicleGear = 'N'
						}
					}

					if (previousGear !== data.vehicleGear) {
						previousGear = data.vehicleGear
						$("#vehicle-gear span").html(data.vehicleGear);
					}
					const speedometerCircle = document.querySelector('#progress-speed svg circle.speed');
					const rpmmeterCircle = document.querySelector('#progress-rpm svg circle.rpm');
					
					$("#vehicle-speed span.speed").html(data.carSpeed);
					$("#vehicle-rpm span.rpm").html(data.vehicleRPM);
					speedometerCircle.classList.remove('zero');
					speedometerCircle.classList.remove('ten');
					speedometerCircle.classList.remove('twenty');
					speedometerCircle.classList.remove('thirty');
					speedometerCircle.classList.remove('forty');
					speedometerCircle.classList.remove('fifty');
					speedometerCircle.classList.remove('sixty');
					speedometerCircle.classList.remove('seventy');
					speedometerCircle.classList.remove('eighty');
					speedometerCircle.classList.remove('ninety');
					rpmmeterCircle.classList.remove('zero');
					rpmmeterCircle.classList.remove('ten');
					rpmmeterCircle.classList.remove('twenty');
					rpmmeterCircle.classList.remove('thirty');
					rpmmeterCircle.classList.remove('forty');
					rpmmeterCircle.classList.remove('fifty');
					rpmmeterCircle.classList.remove('sixty');
					rpmmeterCircle.classList.remove('seventy');
					rpmmeterCircle.classList.remove('eighty');
					rpmmeterCircle.classList.remove('ninety');
					const speedometerClass = getPercentageClass(data.carSpeedPercentage)
					speedometerCircle.classList.add(speedometerClass)
					const rpmmeterClass = getPercentageClass(data.RPMPercentage)
					rpmmeterCircle.classList.add(rpmmeterClass)
					speedometerCircle.style.strokeDashoffset = data.vehicleNailSpeed
					rpmmeterCircle.style.strokeDashoffset = data.vehicleNailRpm
					$('#vehicle-rpm strong.rpmunit').html('RPM')
					const carGearIcon = document.querySelector('#speedometer #vehicle-gear i.car');
					const carGearText = document.querySelector('#speedometer #vehicle-gear span');
					carGearIcon.classList.add('show');
					carGearText.classList.remove('plane');
					carGearText.classList.add('car');
					const planeHeightIcon = document.querySelector('#speedometer #vehicle-gear i.plane');
					planeHeightIcon.classList.remove('show');


					// headlights
					const highbeamSVG = document.querySelector('#headlight svg.highbeam');
					const lowbeamSVG = document.querySelector('#headlight svg.lowbeam');
					if (data.lightsOn || data.highbeamsOn) {
						if (!data.highbeamsOn) {
							lowbeamSVG.classList.remove('hide');
							lowbeamSVG.classList.remove('off');
							highbeamSVG.classList.add('hide');
						} else {
							highbeamSVG.classList.remove('hide');
							highbeamSVG.classList.remove('off');
							lowbeamSVG.classList.add('hide');
						}
					} else {
						highbeamSVG.classList.add('hide');
						lowbeamSVG.classList.remove('hide');
						lowbeamSVG.classList.add('off');
					}

					// blinkers and sirens
					const leftBlinkerSVG = document.querySelector('#container_blinker_left svg');
					const rightlinkerSVG = document.querySelector('#container_blinker_right svg');
					
					if (data.vehicleSirens) {
						rightlinkerSVG.classList.remove('blink-image');
						leftBlinkerSVG.classList.remove('blink-image');
						rightlinkerSVG.classList.remove('blink-sirens1audio');
						leftBlinkerSVG.classList.remove('blink-sirens2audio');
						rightlinkerSVG.classList.remove('blink-sirens1');
						leftBlinkerSVG.classList.remove('blink-sirens2');
						rightlinkerSVG.classList.remove('off');
						leftBlinkerSVG.classList.remove('off');
						
						if (data.vehicleSirensSound) {
							rightlinkerSVG.classList.add('blink-sirens1audio');
							leftBlinkerSVG.classList.add('blink-sirens2audio');
						} else {
							rightlinkerSVG.classList.add('blink-sirens1');
							leftBlinkerSVG.classList.add('blink-sirens2');
						}
					} else {
						rightlinkerSVG.classList.remove('blink-sirens1');
						leftBlinkerSVG.classList.remove('blink-sirens2');
						rightlinkerSVG.classList.remove('blink-sirens1audio');
						leftBlinkerSVG.classList.remove('blink-sirens2audio');
						if (data.blinkerLights) {
							if (data.blinkerLights === 1) {
								rightlinkerSVG.classList.add('off');
								rightlinkerSVG.classList.remove('blink-image');
								leftBlinkerSVG.classList.remove('off');
								leftBlinkerSVG.classList.add('blink-image');
							} else if (data.blinkerLights === 2) {
								leftBlinkerSVG.classList.add('off');
								leftBlinkerSVG.classList.remove('blink-image');
								rightlinkerSVG.classList.remove('off');
								rightlinkerSVG.classList.add('blink-image');
							} else {
								rightlinkerSVG.classList.remove('blink-image');
								leftBlinkerSVG.classList.remove('blink-image');
								leftBlinkerSVG.classList.remove('off');
								rightlinkerSVG.classList.remove('off');
								leftBlinkerSVG.classList.add('blink-image');
								rightlinkerSVG.classList.add('blink-image');
							}
						} else {
							leftBlinkerSVG.classList.remove('blink-image');
							leftBlinkerSVG.classList.add('off');
							rightlinkerSVG.classList.remove('blink-image');
							rightlinkerSVG.classList.add('off');
						}
					}
				} else if (data.speedometer === "plane") {
					const horizontalSpeedometerCircle = document.querySelector('#progress-speed svg circle.speed');
					const verticalSpeedometerCircle = document.querySelector('#progress-rpm svg circle.rpm');
					
					$("#vehicle-speed span.speed").html(data.horizontalSpeed);
					$("#vehicle-rpm span.rpm").html(data.verticalSpeed);
					horizontalSpeedometerCircle.classList.remove('zero');
					horizontalSpeedometerCircle.classList.remove('ten');
					horizontalSpeedometerCircle.classList.remove('twenty');
					horizontalSpeedometerCircle.classList.remove('thirty');
					horizontalSpeedometerCircle.classList.remove('forty');
					horizontalSpeedometerCircle.classList.remove('fifty');
					horizontalSpeedometerCircle.classList.remove('sixty');
					horizontalSpeedometerCircle.classList.remove('seventy');
					horizontalSpeedometerCircle.classList.remove('eighty');
					horizontalSpeedometerCircle.classList.remove('ninety');
					verticalSpeedometerCircle.classList.remove('zero');
					verticalSpeedometerCircle.classList.remove('ten');
					verticalSpeedometerCircle.classList.remove('twenty');
					verticalSpeedometerCircle.classList.remove('thirty');
					verticalSpeedometerCircle.classList.remove('forty');
					verticalSpeedometerCircle.classList.remove('fifty');
					verticalSpeedometerCircle.classList.remove('sixty');
					verticalSpeedometerCircle.classList.remove('seventy');
					verticalSpeedometerCircle.classList.remove('eighty');
					verticalSpeedometerCircle.classList.remove('ninety');
					const horizontalSpeedometerClass = getPercentageClass(data.horizontalSpeedPercentage)
					horizontalSpeedometerCircle.classList.add(horizontalSpeedometerClass)
					const verticalSpeedometerClass = getPercentageClass(data.verticalSpeedPercentage)
					verticalSpeedometerCircle.classList.add(verticalSpeedometerClass)
					horizontalSpeedometerCircle.style.strokeDashoffset = data.vehicleNailHorizontalSpeed
					verticalSpeedometerCircle.style.strokeDashoffset = data.vehicleNailVerticalSpeed

					$("#container_headlight").removeClass('active');
					$("#container_headlight").addClass('inactive');
					$("#container_blinker_left").removeClass('active');
					$("#container_blinker_left").addClass('inactive');
					$("#container_blinker_right").removeClass('active');
					$("#container_blinker_right").addClass('inactive');
					
					$('#vehicle-rpm strong.rpmunit').html('VSI');

					const carGearIcon = document.querySelector('#speedometer #vehicle-gear i.car');
					carGearIcon.classList.remove('show');
					const planeHeightIcon = document.querySelector('#speedometer #vehicle-gear i.plane');
					planeHeightIcon.classList.add('show');
					const planeHeightText = document.querySelector('#speedometer #vehicle-gear span');
					planeHeightText.classList.remove('car');
					planeHeightText.classList.add('plane');
					$("#vehicle-gear span").html(data.entityHeight);
				}
			}
		} else if (data.setHudScale) {
			setHudScale(data.scale);
		}
	};

	window.onload = function (e) {
		window.addEventListener('message', function (event) {
			onData(event.data);
		});
	};

})();