ESX = nil

local hasGPS = false
local forceGPS = false
local nearestPostalText = ""
local hasRoute = false
local postals = nil
local nearest = nil
local pBlip = nil

-- optimizations
local vec = vec
local Wait = Citizen.Wait
local ipairs = ipairs
local upper = string.upper
local format = string.format
local RemoveBlip = RemoveBlip
local PlayerPedId = PlayerPedId
local IsHudHidden = IsHudHidden
local SetTextFont = SetTextFont
local SetTextScale = SetTextScale
local SetTextOutline = SetTextOutline
local GetEntityCoords = GetEntityCoords
local EndTextCommandDisplayText = EndTextCommandDisplayText
local BeginTextCommandDisplayText = BeginTextCommandDisplayText
local AddTextComponentSubstringPlayerName = AddTextComponentSubstringPlayerName
-- end optimizations

AddTextEntry('HUD_LABEL', '~a~')

Citizen.CreateThread(function()
    while true do
        if Config.GPS.useAsItem == false then
            return true
        else
            if ESX ~= nil then
                ESX.TriggerServerCallback('lp_hud:hasGPS', function(hasItem)
                    hasGPS = hasItem
                end)
            else
                hasGPS = false
            end
        end
        Citizen.Wait(1000)
    end
end)

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent("esx:getSharedObject", function(obj)
            ESX = obj
        end)
        Citizen.Wait(100)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        HideHudComponentThisFrame(14) -- Reticule
        HideHudComponentThisFrame(9) -- Street name
        HideHudComponentThisFrame(7) -- Area name
    end
end)

Citizen.CreateThread(function()
    while true do
        local ped = GetPlayerPed(-1)
        local radarEnabled = IsRadarEnabled()
        if IsPedInAnyVehicle(ped) then
            if hasGPS == true or forceGPS == true then
                if not radarEnabled then
                    DisplayRadar(true)
                end
            else
                if radarEnabled then
                    DisplayRadar(false)
                end
            end
        else
            if forceGPS == true then
                if not radarEnabled then
                    DisplayRadar(true)
                end
            else
                if radarEnabled then
                    DisplayRadar(false)
                end
            end
        end
        Citizen.Wait(100)
    end
end)

-- Suppresion de la barre de vie sur de la minimap
Citizen.CreateThread(function()
    local minimap = RequestScaleformMovie("minimap")
    SetRadarBigmapEnabled(false, false)
    while true do
        Wait(0)
        BeginScaleformMovieMethod(minimap, "SETUP_HEALTH_ARMOUR")
        ScaleformMovieMethodAddParamInt(3)
        EndScaleformMovieMethod()
    end
end)

RegisterNetEvent('lp_hud:toggleGPS')
AddEventHandler('lp_hud:toggleGPS', function (toggle)
    forceGPS = toggle
end)

RegisterNetEvent('lp_hud:sendNui')
AddEventHandler('lp_hud:sendNui', function (message)
    SendNUIMessage(message)
end)

-- POSTAL CODES

Citizen.CreateThread(function()
    postals = LoadResourceFile(GetCurrentResourceName(), GetResourceMetadata(GetCurrentResourceName(), 'postal_file'))
    postals = json.decode(postals)
    for i, postal in ipairs(postals) do postals[i] = { vec(postal.x, postal.y), code = postal.code } end
end)


-- recalculate current postal
Citizen.CreateThread(function()
    -- wait for postals to load
    while postals == nil do Wait(1) end

    local delay = 300

    local _postals = postals
    local _total = #_postals

    while true do
        local coords = GetEntityCoords(PlayerPedId())
        local _nearestIndex, _nearestD
        coords = vec(coords[1], coords[2])

        for i = 1, _total do
            local D = #(coords - _postals[i][1])
            if not _nearestD or D < _nearestD then
                _nearestIndex = i
                _nearestD = D
            end
        end

        if pBlip and #(pBlip.p[1] - coords) < 100.0 then
            RemoveBlip(pBlip.hndl)
            pBlip = nil
        end

        local _code = _postals[_nearestIndex].code
        nearest = { code = _code, dist = _nearestD }
        nearestPostalText = format('%s', _code)
        Wait(delay)
    end
end)

-- text display thread
Citizen.CreateThread(function()
    local _string = "STRING"
    local _scale = 0.52
    local _font = 4
    while true do
        local camDegrees = 360.0 - ((GetGameplayCamRot().z + 360.0) % 360.0)
        local cardinalText = degreesToIntercardinalDirection(camDegrees)
        local ped = GetPlayerPed(-1)
        if ((IsPedInAnyVehicle(ped) and (hasGPS or forceGPS)) or forceGPS) and nearest and not IsHudHidden() then
            exports.ft_libs:Text({ text = cardinalText, font = 4, x = 0.165, y = 0.89, scale = 0.64, alpha = 150})
            exports.ft_libs:Text({ text = nearestPostalText, font = 4, x = 0.165, y = 0.926, scale = 0.64, alpha = 150})
        end
        Wait(0)
    end
end)

-- postal code route
RegisterCommand('postal', function(_, args)
    if hasRoute then
        if pBlip then
            RemoveBlip(pBlip.hndl)
            pBlip = nil
        end
        hasRoute = false
        return
    end

    ESX.UI.Menu.Open('dialog', 'lp_hud', 'postal_route', {
        title = 'Code Postal'
    }, function(data, menu)
        menu.close()
        local userPostal = upper(data.value)
        local foundPostal

        for _, p in ipairs(postals) do
            if upper(p.code) == userPostal then
                foundPostal = p
                break
            end
        end

        if foundPostal then
            if pBlip then RemoveBlip(pBlip.hndl) end
            local blip = AddBlipForCoord(foundPostal[1][1], foundPostal[1][2], 0.0)
            pBlip = { hndl = blip, p = foundPostal }
            hasRoute = true
            SetBlipRoute(blip, true)
            SetBlipSprite(blip, 8)
            SetBlipColour(blip, 3)
            SetBlipRouteColour(blip, 3)
            BeginTextCommandSetBlipName('STRING')
            AddTextComponentSubstringPlayerName(format('Code postal: %s', pBlip.p.code))
            EndTextCommandSetBlipName(blip)
        end
    end, function(data, menu)
        menu.close()
    end)
end)
RegisterKeyMapping('postal', 'Aller à un Code Postal', 'keyboard', Config.postalCode.key)
