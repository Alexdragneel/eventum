ESX = nil

TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)

ESX.RegisterServerCallback('lp_hud:hasCompass', function(source, cb)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local hasItem = false

    if xPlayer ~= nil then
        if xPlayer.getInventoryItem('compass') ~= nil then
            hasItem = xPlayer.getInventoryItem('compass').count > 0
        end
    end
    cb(hasItem)
end)

ESX.RegisterServerCallback('lp_hud:hasGPS', function(source, cb)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local hasItem = false

    if xPlayer ~= nil then
        if xPlayer.getInventoryItem('gps') ~= nil then
            hasItem = xPlayer.getInventoryItem('gps').count > 0
        end
    end
    cb(hasItem)
end)
