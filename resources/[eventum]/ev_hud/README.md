# HUD

Nom des rues masqués en bas à droite

Le réticule n'existe plus, il va falloir viser comme un pro

Les notifications d'inventaire (ex `+1 Pain`) ne s'affiche plus (cf `resources/[essential]/es_extended/html/css/app.css:26`)

Nouvelle notification pour les métiers de farm ! Ça s'affiche au milieux de l'écran

# GPS 

Le radar est masqué lorsque l'on est à pied !

Le GPS est un item achetable chez Ginette et affiche le radar en véhicule.

Il est possible de ne plus l'utiliser en tant qu'objet facilement dans le fichier `config.lua`


# FiveM FXServer - Compass and Street Names HUD

> Original ressource from [dzetah](https://github.com/dzetah/fxserver-resource-compass)

> Original resource from MSQuerade, the project is located at [https://gitlab.com/MsQuerade/Compass-and-street-name-HUD](https://gitlab.com/MsQuerade/Compass-and-street-name-HUD)

## Changelog

### 1.3
- Update colors
- Rename files
- Use compass as item

### 1.2
- Changed folder structure.
- Configuration is now separated in it's own `config.lua` !

### 1.1
- Added compass needle with text and central indicator?

### 1.0
- Original work from **MsQuerade** !
