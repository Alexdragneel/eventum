START TRANSACTION;

INSERT INTO `lp_items` (`name`, `label`, `sellPrice`, `farmPrice`, `weight`, `actions`, `canRemove`, `rarity`, `icon`, `stackable`) VALUES
('gps', 'GPS', 250, 0, 250, '{}', 1, 'common', 'gps.png', 1),
('compass', 'Boussole', 150, 0, 150, '{}', 1, 'common', 'compass.png', 1);

UPDATE `lp_shop_types` SET `items`="[\"phone\",\"water\",\"bread\",\"beer\",\"compass\",\"gps\"]" WHERE `name`= "gasstation";
UPDATE `lp_shop_types` SET `items`="[\"phone\",\"water\",\"bread\",\"pizza\",\"hotdog\",\"cheeseburger\",\"beer\",\"compass\",\"gps\"]" WHERE `name`= "store";

COMMIT;