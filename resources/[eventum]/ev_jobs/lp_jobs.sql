USE `fivem_server`;

INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_lumberjack', 'Bûcheron', 1),
	('society_butcher', 'Boucher', 1),
	('society_fisherman', 'Pêcheur', 1),
	('society_miner', 'Mineur', 1),
	('society_refiner', 'Raffineur', 1),
	('society_tailor', 'Couturier', 1)
;

INSERT INTO `addon_account_data` (account_name, money) VALUES
	('society_lumberjack', 100000),
	('society_butcher', 100000),
	('society_fisherman', 100000),
	('society_miner', 100000),
	('society_refiner', 100000),
	('society_tailor', 100000)	
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_lumberjack', 'Bûcheron', 1),
	('society_butcher', 'Boucher', 1),
	('society_fisherman', 'Pêcheur', 1),
	('society_miner', 'Mineur', 1),
	('society_refiner', 'Raffineur', 1),
	('society_tailor', 'Couturier', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_lumberjack', 'Bûcheron', 1),
	('society_butcher', 'Boucher', 1),
	('society_fisherman', 'Pêcheur', 1),
	('society_miner', 'Mineur', 1),
	('society_refiner', 'Raffineur', 1),
	('society_tailor', 'Couturier', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('lumberjack', 'Bûcheron'),
	('butcher', 'Boucher'),
	('fisherman', 'Pêcheur'),
	('miner', 'Mineur'),
	('refiner', 'Raffineur'),
	('tailor', 'Couturier')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('lumberjack',0,'temp','Intérimaire',50,'{"sex":0,"tshirt_1":59,"tshirt_2":0,"torso_1":0,"torso_2":1,"pants_1":105,"pants_2":8,"shoes_1":27,"shoes_2":0,"helmet_1":0,"helmet_2":0,"arms":0,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":36,"tshirt_2":0,"torso_1":88,"torso_2":1,"pants_1":45,"pants_2":1,"shoes_1":52,"shoes_2":5,"helmet_1":0,"helmet_2":0,"arms":14,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('lumberjack',1,'employee','Employé',100,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":95,"torso_2":2,"pants_1":105,"pants_2":9,"shoes_1":27,"shoes_2":0,"helmet_1":0,"helmet_2":0,"arms":11,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":14,"tshirt_2":0,"torso_1":9,"torso_2":13,"pants_1":45,"pants_2":0,"shoes_1":52,"shoes_2":5,"helmet_1":0,"helmet_2":0,"arms":14,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('lumberjack',2,'manager','Manager',150,'{}','{}'),
	('lumberjack',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('lumberjack',4,'boss','Directeur',300,'{}','{}'),
	('fisherman',0,'temp','Intérimaire',50,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":217,"torso_2":0,"pants_1":36,"pants_2":0,"shoes_1":82,"shoes_2":0,"arms":63,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":2,"tshirt_2":0,"torso_1":227,"torso_2":0,"pants_1":45,"pants_2":0,"shoes_1":86,"shoes_2":0,"arms":73,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('fisherman',1,'employee','Employé',100,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":211,"torso_2":19,"pants_1":87,"pants_2":19,"shoes_1":27,"shoes_2":0,"helmet_1":-1,"helmet_2":0,"arms":63,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":2,"tshirt_2":0,"torso_1":215,"torso_2":19,"pants_1":90,"pants_2":19,"shoes_1":26,"shoes_2":0,"helmet_1":-1,"helmet_2":0,"arms":73,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('fisherman',2,'manager','Manager',150,'{}','{}'),
	('fisherman',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('fisherman',4,'boss','Directeur',300,'{}','{}'),
	('butcher',0,'temp','Intérimaire',50,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":56,"torso_2":0,"pants_1":36,"pants_2":0,"shoes_1":82,"shoes_2":2,"arms":63,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":7,"tshirt_2":0,"torso_1":49,"torso_2":0,"pants_1":35,"pants_2":0,"shoes_1":86,"shoes_2":2,"arms":83,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('butcher',1,'employee','Employé',100,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":43,"torso_2":0,"pants_1":0,"pants_2":9,"shoes_1":82,"shoes_2":2,"arms":70,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":7,"tshirt_2":0,"torso_1":86,"torso_2":0,"pants_1":1,"pants_2":4,"shoes_1":86,"shoes_2":2,"arms":83,"arms_2":0,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('butcher',2,'manager','Manager',150,'{}','{}'),
	('butcher',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('butcher',4,'boss','Directeur',300,'{}','{}'),
	('miner',0,'temp','Intérimaire',50,'{"sex":0,"tshirt_1":59,"tshirt_2":1,"torso_1":1,"torso_2":1,"pants_1":4,"pants_2":1,"shoes_1":50,"shoes_2":0,"helmet_1":0,"helmet_2":0,"arms":138,"arms_2":7,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0,"glasses_1":15,"glasses_2":3}','{"sex":1,"tshirt_1":36,"tshirt_2":1,"torso_1":73,"torso_2":0,"pants_1":0,"pants_2":0,"shoes_1":56,"shoes_2":0,"helmet_1":0,"helmet_2":0,"arms":182,"arms_2":7,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0,"glasses_1":9,"glasses_2":1}'),
	('miner',1,'employee','Employé',100,'{"sex":0,"tshirt_1":59,"tshirt_2":1,"torso_1":38,"torso_2":0,"pants_1":8,"pants_2":0,"shoes_1":51,"shoes_2":0,"helmet_1":0,"helmet_2":0,"arms":144,"arms_2":7,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0,"glasses_1":15,"glasses_2":3}','{"sex":1,"tshirt_1":36,"tshirt_2":1,"torso_1":75,"torso_2":1,"pants_1":0,"pants_2":2,"shoes_1":51,"shoes_2":1,"helmet_1":0,"helmet_2":0,"arms":182,"arms_2":7,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0,"glasses_1":9,"glasses_2":1}'),
	('miner',2,'manager','Manager',150,'{}','{}'),
	('miner',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('miner',4,'boss','Directeur',300,'{}','{}'),
	('refiner',0,'temp','Intérimaire',50,'{"sex":0,"tshirt_1":59,"tshirt_2":1,"torso_1":208,"torso_2":19,"pants_1":4,"pants_2":0,"shoes_1":50,"shoes_2":5,"helmet_1":-1,"helmet_2":0,"arms":138,"arms_2":3,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":36,"tshirt_2":1,"torso_1":224,"torso_2":19,"pants_1":4,"pants_2":1,"shoes_1":51,"shoes_2":3,"helmet_1":-1,"helmet_2":0,"arms":182,"arms_2":3,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('refiner',1,'employee','Employé',100,'{"sex":0,"tshirt_1":15,"tshirt_2":0,"torso_1":234,"torso_2":1,"pants_1":4,"pants_2":0,"shoes_1":52,"shoes_2":1,"helmet_1":-1,"helmet_2":0,"arms":145,"arms_2":3,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}','{"sex":1,"tshirt_1":7,"tshirt_2":0,"torso_1":244,"torso_2":0,"pants_1":4,"pants_2":0,"shoes_1":51,"shoes_2":3,"helmet_1":-1,"helmet_2":0,"arms":179,"arms_2":3,"mask_1":0,"mask_2":0,"decals_1":0,"decals_2":0}'),
	('refiner',2,'manager','Manager',150,'{}','{}'),
	('refiner',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('refiner',4,'boss','Directeur',300,'{}','{}'),
	('tailor',0,'temp','Intérimaire',50,'{}','{}'),
	('tailor',1,'employee','Employé',100,'{}','{}'),
	('tailor',2,'manager','Manager',150,'{}','{}'),
	('tailor',3,'vice','Directeur Adjoint',200,'{}','{}'),
	('tailor',4,'boss','Directeur',300,'{}','{}'),
	('unemployed',0,'unemployed','Chômage',50,'{}','{}')
;

ALTER TABLE `owned_vehicles`
	ADD `caution` TINYINT(1) NULL DEFAULT '0'
;
