Config                      = {}
Config.DrawDistance         = 100
Config.Locale               = "fr"
Config.Jobs                 = {}
Config.Marker               = {type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false}
Config.VehicleShop          = vector3(317.36, -541.15, 27.74)
Config.VaultMaxWeight       = 10000
