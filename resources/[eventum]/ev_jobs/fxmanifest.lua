fx_version "adamant"

game "gta5"

description "Los Plantos Jobs"

version "1.0.0"

server_scripts {
	"@mysql-async/lib/MySQL.lua",
	
  "@es_extended/locale.lua",
	"locales/fr.lua",

  "config.lua",

	"jobs/lumberjack.lua",
	"jobs/fisherman.lua",
	"jobs/butcher.lua",
	"jobs/miner.lua",
	"jobs/refiner.lua",
  "jobs/tailor.lua",
	"jobs/bennys/bennys.lua",
	"jobs/journaliste/journaliste.lua",
	"jobs/taxi/taxi.lua",
	"jobs/food_delivery/food_delivery.lua",

	"server/main.lua",
}

client_scripts {
	"@es_extended/locale.lua",
	"locales/fr.lua",

	"config.lua",

  "jobs/lumberjack.lua",
	"jobs/fisherman.lua",
	"jobs/butcher.lua",
	"jobs/miner.lua",
	"jobs/refiner.lua",
  "jobs/tailor.lua",
	"jobs/bennys/bennys.lua",
	"jobs/journaliste/journaliste.lua",
	"jobs/taxi/taxi.lua",
	"jobs/food_delivery/food_delivery.lua",
	
  "client/main.lua",
	"client/job.lua",
	"jobs/bennys/getters.lua",
	"jobs/bennys/client.lua",
	"jobs/journaliste/client.lua",
	"jobs/taxi/client.lua",
	"jobs/food_delivery/client.lua",
}

ui_page 'html/ui.html'

files {
	'html/ui.html',
	'html/style.css',
	'html/img/bennyslogo.png',
}

dependencies {
	"es_extended",
	"esx_skin",
  "esx_billing",
	"esx_vehicleshop"
}
