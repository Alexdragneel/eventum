local CurrentDistance = 0
local CustomerLeavesAt = 0

Citizen.CreateThread(function()
    local blipCoords = vector3(Config.Jobs.taxi.Society.Blip.Pos.x, Config.Jobs.taxi.Society.Blip.Pos.y, Config.Jobs.taxi.Society.Blip.Pos.z)
    local blip = AddBlipForCoord(blipCoords)
    local blipSprite = Config.Jobs.taxi.Society.Blip.Sprite
    local blipColor = Config.Jobs.taxi.Society.Blip.Color

    SetBlipSprite(blip, blipSprite)
    SetBlipDisplay(blip, 4)
    SetBlipScale(blip, 1.0)
    SetBlipColour(blip, blipColor)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentSubstringPlayerName(_U('taxi_society_name'))
    EndTextCommandSetBlipName(blip)
end)

RegisterNetEvent("lp_jobs:taxi:openTaxiActionsMenu")
AddEventHandler("lp_jobs:taxi:openTaxiActionsMenu", function(text, duration, cbServerEvent)
    OpenMobileTaxiActionsMenu()
end)

function OpenMobileTaxiActionsMenu()
	ESX.UI.Menu.CloseAll()

	local startStopJobLabel = _U('start_job')
	if OnJob then
		startStopJobLabel = _U('stop_job')
	end

	local elements = {
		{label = _U('billing'),   value = 'billing'},
		{label = startStopJobLabel, value = 'start_job'}
	}

	if ESX.PlayerData.job.grade ~= nil and ESX.PlayerData.job.grade > 0 then
		table.insert(elements, {label = _U("bill_management"), value = "bill_management"})
	end

	if ESX.PlayerData.job.grade_name == "boss" then
		table.insert(elements, {label = _U("boss_menu"), value = "boss_menu"})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mobile_taxi_actions', {
		title    = 'Taxi',
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if data.current.value == 'billing' then
			TriggerEvent('lp_core:openBillingMenu')
		elseif data.current.value == 'start_job' then
			if OnJob then
				StopTaxiJob()
			else
				if ESX.PlayerData.job ~= nil and ESX.PlayerData.job.name == 'taxi' then
					if IsInAuthorizedVehicle() then
						StartTaxiJob()
					else
						ESX.ShowNotification(_U('must_in_taxi'))
					end
				end
			end
			ESX.UI.Menu.CloseAll()
		elseif data.current.value == "bill_management" then
			TriggerEvent(
				"esx_society:openBillManagement",
				ESX.PlayerData.job.name
			)
		elseif data.current.value == "boss_menu" then
			TriggerEvent(
				"esx_society:openBossMenu",
				ESX.PlayerData.job.name,
				function(data, menu)
					menu.close()
				end,
				{wash = false}
			)
		end
	end, function(data, menu)
		menu.close()
	end)
end

function OpenTaxiActionsMenu()
    local elements = {{
        label = _U("taxi"),
        value = "taxi_menu"
    }, {
        label = _U("billing"),
        value = "billing_menu"
    }}

    if ESX.PlayerData.job.grade_name == "boss" then
        table.insert(elements, {
            label = _U("boss_menu"),
            value = "boss_menu"
        })
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open("default", GetCurrentResourceName(), ESX.PlayerData.job.name .. "_actions", {
        title = ESX.PlayerData.job.label,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        if data.current.value == "billing_menu" then
            OpenBillingMenu()
        elseif data.current.value == "boss_menu" then
            TriggerEvent("esx_society:openBossMenu", ESX.PlayerData.job.name, function(data, menu)
                menu.close()
            end, {
                withdraw = false,
                deposit = false,
                wash = false,
                employees = true,
                grades = true
            }, true)
        elseif data.current.value == "taxi_menu" then
            OpenTaxiActionsMenu()
        end
    end, function(data, menu)
        menu.close()
    end)
end

AddEventHandler('esx:playerLoaded', function()
    ESX.PlayerData = ESX.GetPlayerData()
    if ESX.PlayerData.job then
        if ESX.PlayerData.job.grade_name == 'boss' then
            Citizen.SetTimeout(2500, function()
                TriggerEvent('gcPhone:setEnableApp', "Pacific", true)
                TriggerServerEvent('gcPhone:allUpdate')
            end)
        end
    end
end)

--

RegisterNetEvent("esx:setJob")
AddEventHandler(
	"esx:setJob",
	function(job)
		if ESX.PlayerData.job.name ~= job.name then
			StopTaxiJob()
		end
		ESX.PlayerData.job = job
	end
)

RegisterNetEvent("esx_service:disableService")
AddEventHandler(
	"esx_service:disableService",
	function()
		StopTaxiJob()
	end
)

RegisterNetEvent("esx_service:enableService")
AddEventHandler(
	"esx_service:enableService",
	function()
		if ESX.PlayerData.job.name ~= 'taxi' then
			StopTaxiJob()
		end
	end
)

-- Utilitaires Taxi

function GetRandomWalkingNPC()
	local search = {}
	local peds   = ESX.Game.GetPeds()

	for i=1, #peds, 1 do
		if IsPedHuman(peds[i]) and IsPedWalking(peds[i]) and not IsPedAPlayer(peds[i]) and (LastCustomer == nil or LastCustomer ~= peds[i]) then
			table.insert(search, peds[i])
		end
	end

	if #search > 0 then
		return search[GetRandomIntInRange(1, #search+1)]
	end

	for i=1, 250, 1 do
		local ped = GetRandomPedAtCoord(0.0, 0.0, 0.0, math.huge + 0.0, math.huge + 0.0, math.huge + 0.0, 26)

		if DoesEntityExist(ped) and IsPedHuman(ped) and IsPedWalking(ped) and not IsPedAPlayer(ped) and (LastCustomer == nil or LastCustomer ~= ped) and IsPedModelOk(ped) then
			table.insert(search, ped)
		end
	end

	if #search > 0 then
		return search[GetRandomIntInRange(1, #search+1)]
	end
end

function IsPedModelOk(ped)
	local modelHash = GetEntityModel(ped)
	for i=1, #Config.Jobs.taxi.ExcludedPedModels, 1 do
		if modelHash == GetHashKey(Config.Jobs.taxi.ExcludedPedModels[i]) then
			return false
		end
	end
	return true
end

function ClearCurrentMission()
	if DoesBlipExist(CurrentCustomerBlip) then
		RemoveBlip(CurrentCustomerBlip)
	end

	if DoesBlipExist(DestinationBlip) then
		RemoveBlip(DestinationBlip)
	end

	CurrentCustomer           = nil
	CurrentCustomerBlip       = nil
	DestinationBlip           = nil
	IsNearCustomer            = false
	CustomerIsEnteringVehicle = false
	CustomerEnteredVehicle    = false
	targetCoords              = nil
end

function StartTaxiJob()
	ClearCurrentMission()

	OnJob = true
end

function StopTaxiJob()
	local playerPed = PlayerPedId()

	if CurrentCustomer ~= nil and IsPedInAnyVehicle(CurrentCustomer, false) then
		local vehicle = GetVehiclePedIsIn(CurrentCustomer,  false)
		TaskLeaveVehicle(CurrentCustomer,  vehicle,  0)

		if CustomerEnteredVehicle then
			TaskGoStraightToCoord(CurrentCustomer,  targetCoords.x,  targetCoords.y,  targetCoords.z,  1.0,  -1,  0.0,  0.0)
		end
	end

	TriggerEvent('lp_core:clearTextCenter')
	ClearCurrentMission()
	OnJob = false
	CurrentDistance = 0
end

function OpenTaxiActionsMenu()
	local elements = {
		{label = _U('deposit_stock'), value = 'put_stock'},
		{label = _U('take_stock'), value = 'get_stock'}
	}

	if ESX.PlayerData.job ~= nil and ESX.PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'taxi_actions', {
		title    = 'Taxi',
		align    = 'top-left',
		elements = elements
	}, function(data, menu)

		if data.current.value == 'put_stock' then
			OpenPutStocksMenu()
		elseif data.current.value == 'get_stock' then
			OpenGetStocksMenu()
		elseif data.current.value == 'boss_actions' then
			TriggerEvent('esx_society:openBossMenu', 'taxi', function(data, menu)
				menu.close()
			end)
		end

	end, function(data, menu)
		menu.close()

		CurrentAction     = 'taxi_actions_menu'
		CurrentActionMsg  = _U('press_to_open')
		CurrentActionData = {}
	end)
end

function IsInAuthorizedVehicle()
	local playerPed = PlayerPedId()
	local vehicle = GetVehiclePedIsIn(playerPed, false)

	if GetPedInVehicleSeat(vehicle, -1) == playerPed and vehicle then
		local vehModel  = GetEntityModel(vehicle)

		for i=1, #Config.Jobs.taxi.AuthorizedVehicles, 1 do
			if vehModel == GetHashKey(Config.Jobs.taxi.AuthorizedVehicles[i].model) then
				return true
			end
		end
	end
	
	return false
end

function GetRandomIdleAnimation()
	return Config.Jobs.taxi.IdleAnimations[GetRandomIntInRange(1, #Config.Jobs.taxi.IdleAnimations+1)]
end

function CancelCurrentCustomer()
	ESX.ShowNotification(_U('client_cancel'))

	if DoesBlipExist(CurrentCustomerBlip) then
		RemoveBlip(CurrentCustomerBlip)
	end

	if DoesBlipExist(DestinationBlip) then
		RemoveBlip(DestinationBlip)
	end

	SetEntityAsMissionEntity(CurrentCustomer, false, true)

	CurrentCustomer, CurrentCustomerBlip, DestinationBlip, IsNearCustomer, CustomerIsEnteringVehicle, CustomerEnteredVehicle, targetCoords = nil, nil, nil, false, false, false, nil
end

-- Thread Principal
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		local playerPed = PlayerPedId()

		if OnJob and IsInAuthorizedVehicle() then
			if CurrentCustomer == nil then
				TriggerEvent('lp_core:clearTextCenter')
				TriggerEvent('lp_core:drawTextCenter', _U('wait_search_pass'), 0.5, 0.05, 5000)

				local waitUntil = GetGameTimer() + GetRandomIntInRange(15000, 30000)

				while OnJob and waitUntil > GetGameTimer() do
					Citizen.Wait(500)
				end

				if OnJob and IsInAuthorizedVehicle() then
					CurrentCustomer = GetRandomWalkingNPC()

					if CurrentCustomer ~= nil then
						LastCustomer = CurrentCustomer
						CurrentCustomerBlip = AddBlipForEntity(CurrentCustomer)

						SetBlipAsFriendly(CurrentCustomerBlip, true)
						SetBlipColour(CurrentCustomerBlip, 2)
						SetBlipCategory(CurrentCustomerBlip, 3)
						SetBlipRoute(CurrentCustomerBlip, true)

						SetEntityAsMissionEntity(CurrentCustomer, true, false)
						ClearPedTasksImmediately(CurrentCustomer)
						SetBlockingOfNonTemporaryEvents(CurrentCustomer, true)

						local standTime = GetRandomIntInRange(60000, 180000)
						TaskStandStill(CurrentCustomer, standTime)

						CustomerLeavesAt = GetGameTimer() + 60 * 1000

						ESX.ShowNotification(_U('customer_found'))
					end
				end
			else
				if CurrentCustomer ~= nil then
					local vehicle          = GetVehiclePedIsIn(playerPed, false)
					local playerCoords     = GetEntityCoords(playerPed)
					local customerCoords   = GetEntityCoords(CurrentCustomer)
					local customerDistance = #(playerCoords - customerCoords)

					if IsPedSittingInVehicle(CurrentCustomer, vehicle) then
						if CustomerEnteredVehicle then
							local targetDistance = #(playerCoords - targetCoords)

							if targetDistance <= 10.0 and GetEntitySpeed(playerPed) < 0.1 then
								TaskLeaveVehicle(CurrentCustomer, vehicle, 0)

								ESX.SetTimeout(2000, function()
									SetVehicleDoorShut(vehicle, 3, false, false)
								end)

								ESX.ShowNotification(_U('arrive_dest'))

								SetEntityAsMissionEntity(CurrentCustomer, false, true)
								TriggerServerEvent('lp_jobs:taxi:success', CurrentDistance)
								RemoveBlip(DestinationBlip)

								local scope = function(customer)
									ESX.SetTimeout(2000, function()
										TaskStartScenarioInPlace(customer, GetRandomIdleAnimation(), 0, true)
									end)
									ESX.SetTimeout(60000, function()
										DeletePed(customer)
									end)
								end

								scope(CurrentCustomer)

								CurrentCustomer, CurrentCustomerBlip, DestinationBlip, IsNearCustomer, CustomerIsEnteringVehicle, CustomerEnteredVehicle, targetCoords = nil, nil, nil, false, false, false, nil
							end

							if targetCoords then
								DrawMarker(36, targetCoords.x, targetCoords.y, targetCoords.z + 1.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 234, 223, 72, 155, false, false, 2, true, nil, nil, false)
							end
						else
							RemoveBlip(CurrentCustomerBlip)
							CurrentCustomerBlip = nil
							local distance = -1
							local locationLabel = nil
							while distance < Config.Jobs.taxi.MinimumDistance or distance == -1 do
								Citizen.Wait(5)

								local indexJobLocation = GetRandomIntInRange(1, #Config.Jobs.taxi.JobLocations+1)
								targetCoords = Config.Jobs.taxi.JobLocations[indexJobLocation].Location
								locationLabel = Config.Jobs.taxi.JobLocations[indexJobLocation].Label
								distance = #(playerCoords - targetCoords)
							end
							CurrentDistance = distance

							local street = table.pack(GetStreetNameAtCoord(targetCoords.x, targetCoords.y, targetCoords.z))
							local msg    = nil

							msg = string.format(_U('take_me_to', locationLabel))

							ESX.ShowNotification(msg)

							DestinationBlip = AddBlipForCoord(targetCoords.x, targetCoords.y, targetCoords.z)

							BeginTextCommandSetBlipName('STRING')
							AddTextComponentSubstringPlayerName('Destination')
							EndTextCommandSetBlipName(blip)
							SetBlipRoute(DestinationBlip, true)

							CustomerEnteredVehicle = true
						end
					else
						DrawMarker(36, customerCoords.x, customerCoords.y, customerCoords.z + 1.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 234, 223, 72, 155, false, false, 2, true, nil, nil, false)

						if not CustomerEnteredVehicle then
							if GetGameTimer() < CustomerLeavesAt then
								if customerDistance <= 20.0 then

									if not IsNearCustomer then
										ESX.ShowNotification(_U('close_to_client'))
										IsNearCustomer = true
									end

								end

								if IsHornActive(vehicle) and customerDistance <= 20.0 or customerDistance <= 10.0 then
									if not CustomerIsEnteringVehicle then
										ClearPedTasksImmediately(CurrentCustomer)

										local maxSeats, freeSeat = GetVehicleMaxNumberOfPassengers(vehicle)

										for i=maxSeats - 1, 0, -1 do
											if IsVehicleSeatFree(vehicle, i) then
												freeSeat = i
												break
											end
										end

										if freeSeat then
											while CurrentCustomer ~= nil and not IsPedSittingInVehicle(CurrentCustomer, vehicle) do --on boucle au cas où la porte était fermée
												TaskEnterVehicle(CurrentCustomer, vehicle, -1, freeSeat, 2.0, 0)
												CustomerIsEnteringVehicle = true
												Citizen.Wait(2000)
											end
											ESX.SetTimeout(2000, function()
												SetVehicleDoorShut(vehicle, 3, false, false)
											end)
										end
									end
								end
							else
								CancelCurrentCustomer()
							end
						end
					end
				end
			end
		else
			Citizen.Wait(500)
		end
	end
end)

Citizen.CreateThread(function()
	local pedWasInAuthorizedVehicle = false
	while true do

		Citizen.Wait(200)

		if not pedWasInAuthorizedVehicle then
			pedWasInAuthorizedVehicle = IsInAuthorizedVehicle()
		else
			pedWasInAuthorizedVehicle = IsInAuthorizedVehicle()
			if not pedWasInAuthorizedVehicle and OnJob then
				TriggerEvent('lp_core:clearTextCenter')
				TriggerEvent('lp_core:drawTextCenter', _U('return_to_veh'), 0.5, 0.05, 5000)
			end
		end

		if CurrentCustomer ~= nil and not CustomerEnteredVehicle and not CustomerIsEnteringVehicle then
			local p1 = GetEntityCoords(CurrentCustomer, true)
			local p2 = GetEntityCoords(PlayerPedId(), true)

			local dx = p2.x - p1.x
			local dy = p2.y - p1.y

			local heading = GetHeadingFromVector_2d(dx, dy)
			SetEntityHeading( CurrentCustomer, heading )
		end

		if CurrentCustomer ~= nil and IsPedFatallyInjured(CurrentCustomer) then
			CancelCurrentCustomer()
		end
	end
end)