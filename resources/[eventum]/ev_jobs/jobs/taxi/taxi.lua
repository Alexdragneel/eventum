Config.Jobs.taxi = {
    Society = {
        name = 'taxi',
        label = _U('taxi'),
        db_name = 'society_taxi',
        Blip = {
            Pos     = {x = 894.88, y = -180.23, z = 74.5},
            Sprite  = 198,
            Scale = 1.2,
            Color = 5,
        }
    },
    ActionsMenu = 'lp_jobs:taxi:openTaxiActionsMenu',
    Zones = {
        CloakRooms = {
            {
                Pos = {x = 898.06, y = -170.67, z = 73.17},
                Marker = {
                    Type = 1,
                    x = 1.5, y = 1.5, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Name = _U('cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Vault = {},
        Strongbox = {},
        Farming = {},
        Deliveries = {},
        Special = {}
    },
    TaxiActions = {
        Pos   = {x = 903.32, y = -170.55, z = 74.0},
        Size  = {x = 1.0, y = 1.0, z = 1.0},
        Color = {r = 204, g = 204, b = 0},
        Type  = 20, Rotate = true
    },
    AuthorizedVehicles = {
        {model = 'taxi', label = 'Taxi'}
    },
    Tips = {
        0,
        0,
        20,
        20,
        20,
        30,
        30,
        40,
        50,
        100,
        200
    },
    MinimumDistance = 500,
    IdleAnimations = {
        'WORLD_HUMAN_DRINKING',
        'WORLD_HUMAN_SMOKING',
        'WORLD_HUMAN_STAND_MOBILE'
    },
    ExcludedPedModels = {
        'mp_f_meth_01',
        'mp_m_meth_01',
        'mp_s_m_armoured_01',
        's_m_m_armoured_01',
        's_m_m_armoured_02',
        's_m_m_prisguard_01',
        's_m_m_security_01',
        's_m_y_prismuscl_01',
        's_m_y_prisoner_01',
        's_m_m_marine_01',
        's_m_y_marine_01',
        's_m_y_marine_02',
        's_m_y_marine_03'
    },
    JobLocations = {
        { Location = vector3(333.1, 161.1, 103.28)     , Label = 'au cinéma Doppler'},
        { Location = vector3(-1050.24, -1395.38, 5.42) , Label = 'au restaurant La Spada'},
        { Location = vector3(-376.48, 294.82, 84.89)   , Label = 'au Last Train in Los Santos Diner'},
        { Location = vector3(-420.73, 251.95, 83.08)   , Label = 'au Comedy Club'},
        { Location = vector3(-1020.94, -1465.53, 5.05) , Label = 'au Spa Koi'},
        { Location = vector3(-1330.8, -1095.06, 6.82)  , Label = 'au Venetian'},
        { Location = vector3(-1217.91, -1127.14, 7.74) , Label = 'au Cool Beans de Vespucci'},
        { Location = vector3(229.45, -1524.69, 29.17)  , Label = 'au Porn Crackers Video Store'},
        { Location = vector3(262.09, -1216.74, 29.41)  , Label = 'à la gare routière de Strawberry'},
        { Location = vector3(1379.45, -2074.45, 52.0)  , Label = 'à Convington Engineering'},
        { Location = vector3(1205.86, -1284.66, 35.23)  , Label = 'à Hardwood & Lumber Supply'},
        { Location = vector3(1188.79, -415.39, 67.45)  , Label = 'au Cool Beans de Mirror Park'},
        { Location = vector3(1224.17, -353.01, 68.96)  , Label = 'au Horny\'s de Mirror Park'},
        { Location = vector3(850.37, -218.95, 71.47)   , Label = 'au Skatepark de Vinewood'},
        { Location = vector3(406.31, -991.8, 29.27)    , Label = 'au commissariat de Mission Row'},
        { Location = vector3(660.22, -19.65, 82.71)    , Label = 'au commissariat de Vinewood'},
        { Location = vector3(809.31, -1289.91, 26.18)  , Label = 'au commissariat de La Mesa'},
        { Location = vector3(1844.08, 3665.05, 33.94)  , Label = 'au Sandy Shores Medical Center'},
        { Location = vector3(-229.97, 6313.73, 31.29)  , Label = 'au Bay Care Center'},
        { Location = vector3(1859.34, 2586.32, 45.67)  , Label = 'au pénitentier de Bolingbroke'},
        { Location = vector3(-1531.13, 4954.64, 62.08) , Label = 'au parc national du Mont Chiliad'},
        { Location = vector3(228.71, 1172.97, 225.46)  , Label = 'au Sisyphus Theater'},
        { Location = vector3(-409.92, -1715.98, 19.18) , Label = 'à la casse au sud de la ville'},
        { Location = vector3(3327.93, 5151.95, 18.31)  , Label = 'au phare'},
        { Location = vector3(3503.67, 3768.05, 29.92)  , Label = 'à Human Labs and Research'},
        { Location = vector3(2780.04, -712.56, 5.39)   , Label = 'à la plage à l\'est de la ville'},

        { Location = vector3(-1034.34, -2732.44, 20.17), Label = 'à l\'aéroport' }, -- aeroport
        { Location = vector3(-912.71, -2655.74, 14.12) , Label = 'au parking de l\'aéroport' }, -- parking aeroport
        { Location = vector3(-161.5, -2143.57, 16.7)   , Label = 'au parking de la Maze Bank Arena' }, -- parking arena
        { Location = vector3(-228.11, -2047.21, 27.62) , Label = 'à la Maze Bank Arena' }, -- arena
        { Location = vector3(129.56, -1716.39, 29.07)  , Label = 'au coiffeur de Davis' }, -- barbier 2
        { Location = vector3(410.67, -1636.58, 29.29)  , Label = 'à la fourrière' }, -- fourrière
        { Location = vector3(232.57, -1388.92, 30.47)  , Label = 'à l\'auto-école' }, --auto ecole
        { Location = vector3(294.64, -1439.0, 29.8)    , Label = 'au Central Los Santos Medical Center' }, --hopital davis
        { Location = vector3(133.19, -1305.83, 29.16)  , Label = 'au Vanilla Unicorn' }, -- unicorn
        { Location = vector3(86.15, -1391.63, 29.23)   , Label = 'au magasin de vêtements de Strawberry' }, -- vetements1
        { Location = vector3(-196.81, -1300.13, 31.3)  , Label = 'au Bennys' }, -- bennys
        { Location = vector3(-236.37, -983.78, 29.29)  , Label = 'au Pôle Emploi' }, --pole emploi
        { Location = vector3(-326.42, -767.29, 33.96)  , Label = 'au parking de Pôle Emploi' }, --parking pole emplois
        { Location = vector3(-816.45, -1083.37, 11.03) , Label = 'au magasin de vêtements de Vespucci' }, -- vetements 5
        { Location = vector3(-1019.94, -1421.96, 5.09) , Label = 'à la Puerta' }, --quai vespucci
        { Location = vector3(-1325.59, -1294.19, 4.93) , Label = 'chez Dino' }, --chez dino
        { Location = vector3(-1296.31, -1136.37, 5.88) , Label = 'au coiffeur de Vespucci' }, --coiffeur 3
        { Location = vector3(-1637.69, -984.25, 13.02) , Label = 'à Pleasure Pier' }, --quai parc d'attractions
        { Location = vector3(-871.73, -819.06, 19.38)  , Label = 'au parc près de Little Seoul' }, --parc china town
        { Location = vector3(-454.43, -340.34, 34.36)  , Label = 'au Mount Zonah Medical Center' }, --hopital rockford hills
        { Location = vector3(-1383.79, -577.36, 30.03) , Label = 'au Bahamas Mamas' }, --bahamas mamas
        { Location = vector3(-1474.14, -241.9, 49.91)  , Label = 'au magasin de vêtements de Morningwood' }, --vetements 6
        { Location = vector3(-1686.05, -297.88, 51.81) , Label = 'à l\'église de Pacific Bluffs' }, --eglise
        { Location =  vector3(-1862.38, -352.46, 49.25), Label = 'à l\'hôtel Von Crastenburg' }, --hotel von crastenburg
        { Location = vector3(-1077.62, -265.6, 37.65)  , Label = 'aux bureaux de Life Invader' }, --life invader
        { Location = vector3(-1370.6, 56.38, 53.7)     , Label = 'au Club de golf' }, --golf
        { Location = vector3(-723.6, -160.54, 36.91)   , Label = 'au magasin de vêtements de Rockford Hills' }, --vetements 2
        { Location = vector3(-148.23, -308.21, 38.58)  , Label = 'au magasin de vêtements de Burton' }, --vetements 3
        { Location = vector3(151.81, -911.95, 30.06)   , Label = 'à la Place des Cubes' }, --place des cubes
        { Location = vector3(718.05, -980.28, 24.12)   , Label = 'à la couturerie' }, --couturerie
        { Location = vector3(1172.67, -641.64, 62.44)  , Label = 'au parc de Mirror Park' }, --Mirror park
        { Location = vector3(920.21, 49.18, 80.76)     , Label = 'au casino The Diamond' }, --casino
        { Location = vector3(859.39, 533.66, 125.78)   , Label = 'à l\'amphithéâtre Vinewood Bowl' }, --amphiteatre
        { Location = vector3(-411.08, 1175.95, 325.64) , Label = 'à l\'observatoire Galileo' }, --observatoire
        { Location = vector3(-2295.59, 376.34, 174.47) , Label = 'à Low Rotunda' }, -- Low Rotunda
        { Location = vector3(-3018.07, 85.75, 11.61)   , Label = 'au Pacific Bluffs Country Club' }, -- hotel plage ouest
        { Location = vector3(-3157.12, 1061.19, 20.67) , Label = 'au magasin de vêtements de Chumash' }, -- vetement 13
        { Location = vector3(-1889.25, 2046.42, 140.88), Label = 'au vignoble' }, --vignoble
        { Location = vector3(-1142.73, 2665.47, 18.09) , Label = 'au magasin de vêtements sur la Route 68' }, --vetements 14
        { Location = vector3(597.59, 2721.17, 41.94)   , Label = 'à Harmony' }, --vetmeents 10
        { Location = vector3(1937.02, 3717.86, 32.3)   , Label = 'au coiffeur à Sandy Shores' }, --barbier 4
        { Location = vector3(1678.59, 4832.4, 41.96)   , Label = 'à Grapeseed' }, --grapeseed
        { Location = vector3(2477.57, 5114.37, 46.15)  , Label = 'au pied du Mont Chiliad' }, --pied mont chilliad est
        { Location = vector3(1457.12, 6563.18, 13.88)  , Label = 'à la plage de Procopio' }, --plage nord est
        { Location = vector3(-4.83, 6520.52, 31.29)    , Label = 'au magasin de vêtements de Paleto Bay' }, --vetements 7
        { Location = vector3(-287.58, 6238.27, 31.3)   , Label = 'au coiffeur de Paleto Bay' }, --barbier 7
        { Location = vector3(-441.01, 6033.26, 31.34)  , Label = 'au commissariat de Paleto Bay' }, --BCSO
        { Location = vector3(-1581.32, 5167.73, 19.59) , Label = 'au quai de Paleto Cove' }, --quai nord ouest


        { Location = vector3(345.56, -1552.01, 29.14)  , Label = 'au commissariat de Davis' }, --LSPD Davis
        { Location = vector3(-1115.39, -803.88, 17.42) , Label ='au commissariat de Vespucci' }, --LSPD Vespucci
        { Location = vector3(-555.64, -146.06, 38.11)  , Label ='au commissariat de Rockford Hills' },--LSPD Rockford Hills
        { Location = vector3(1856.17, 3675.43, 33.65)  , Label = 'au commissariat de Sandy Shores' }, --LSPD Sandy Shores
        { Location = vector3(-2295.9, 375.25, 174.47)  , Label = 'au Maze Center' }, --Maze Center
        { Location = vector3(-1601.64, 164.71, 59.37)  , Label = 'à l\'Université de Los Plantos' }, --University
        { Location = vector3(-1798.95, -1176.17, 13.02), Label = 'au restaurant le Pearls' }, --Pearls Restaurant
        { Location = vector3(-1284.66, 294.79, 64.88)  , Label = 'à l\'hôtel Richman' }, --Richman Hotel
        { Location = vector3(-380.26, 230.33, 83.95)   , Label = 'au Hornbills' }, --Strip Tease Hornbills
        { Location = vector3(-727.66, 95.46, 55.58)    , Label = 'au Manoir d\'Epsilon Program' }, --EP Manor
        { Location = vector3(-106.74, -608.01, 36.05)  , Label = 'au Arcadius Business Center' }, --Arcadius
        { Location = vector3(237.29, -371.1, 44.23)    , Label = 'au Tribunal de Vinewood' }, --Vinewood Tribunal
        { Location = vector3(301.16, -238.19, 54.03)   , Label = 'au Pink Cage Motel' }, --PK Motel
        { Location = vector3(205.66, -197.51, 53.83)   , Label = 'à la galerie d\'art de Vinewood' }, --Art Gallery
        { Location = vector3(233.52, 302.01, 105.57)   , Label = 'au Bar le Singleton\'s' }, --Singletons Bar
        { Location = vector3(231.69, 199.73, 105.24)   , Label = 'à la Pacific Bank' }, --Pacific Bank
        { Location = vector3(-75.06, 298.64, 106.33)   , Label = 'à l\'hôtel Gentry Manor' }, --GM Hotel
        { Location = vector3(-677.97, 292.1, 81.97)    , Label = 'à la Eclipse Medical Tower' }, --Eclipse Medical Tower
        { Location = vector3(-942.62, -322.53, 38.76)  , Label = 'à la banque de sperme Eugenics' }, --Eugenics sperm
        { Location = vector3(-1032.61, -497.39, 36.48) , Label = 'aux studios de tournage' }, --Studios Cinema
        { Location = vector3(-735.86, -132.87, 37.29)  , Label = 'au Club de croquet' }, --Croquet Club
        { Location = vector3(-570.5, -384.74, 34.9)    , Label = 'à l\'hôtel Dorset' }, --Dorset Hotel
        { Location = vector3(-735.14, -673.06, 30.14)  , Label = 'au cinéma Valdez' }, --Cinema Valdez
        { Location = vector3(-749.85, -708.14, 29.46)  , Label = 'à l\'église de Little Seoul' }, --LS Church
        { Location = vector3(-300.93, -618.57, 33.41)  , Label = 'aux bureaux du Daily Blog' }, --Daily Blog
        { Location = vector3(-826.71, -1219.55, 6.93)  , Label = 'à l\'hôtel The Viceroy' }, --Viceroy Hotel
        { Location = vector3(-157.88, -162.22, 43.62)  , Label = 'au Rockford Plaza' }, --Rockford Plaza
        { Location = vector3(-279.27, -1064.04, 25.86) , Label = 'à l\'hôtel & spa Banner de Pillbox Hill' }, --Banner Pillbox Hill
        { Location = vector3(-51.21, -786.58, 44.07)   , Label = 'à la Maze Bank Tower' }, --Maze Bank Tower
        { Location = vector3(458.62, -1450.57, 29.15)  , Label = 'au cinéma Beacon Theater' }, --Beacon Cinema
        { Location = vector3(859.16, -1653.41, 29.69)  , Label = 'à l\'entreprise Fridgit' }, --Fridgit
        { Location = vector3(559.96, -1760.56, 29.17)  , Label = 'au Bilingsgate Motel' }, --Bilingsgate Motel
        { Location = vector3(-214.08, -1497.96, 31.27) , Label = 'au centre de loisirs B.J. Smith' }, --BJ Smith Recreation
        { Location = vector3(401.06, -710.71, 29.16)   , Label = 'au cinéma Ten Cent' }, --Ten Cent Cinema
        { Location = vector3(716.71, 1202.2, 325.92)   , Label = 'aux pieds des panneaux-lettres Vinewood' }, --Vinewood Letters
        { Location = vector3(1992.46, 3058.06, 47.06)  , Label = 'à la taverne du Yellow Jack' }, --Yellow Jack Inn
        { Location = vector3(2757.22, 3468.45, 55.73)  , Label = 'au magasin You Tool' }, --You Tool
        { Location = vector3(322.23, 2624.2, 44.49)    , Label = 'au Eastern Motel' }, --Eastern Motel
        { Location = vector3(-315.15, 2741.54, 67.74)  , Label = 'à la chapelle de Great Chaparral' }, --GC Church
        { Location = vector3(-117.17, 6456.32, 31.43)  , Label = 'à la banque de Paleto Bay' }, --PB Bank
        { Location = vector3(-129.07, 6395.81, 31.33)  , Label = 'à la taverne Mojito' }, --Mojito Inn
        { Location = vector3(-343.82, 6159.82, 31.49)  , Label = 'à l\'église de Paleto Bay' }, --PB Church
        { Location = vector3(-294.12, 6249.56, 31.29)  , Label = 'au bar The Hen House' }, --Hen House Bar
        { Location = vector3(-710.6, 5787.1, 17.43)    , Label = 'au Bayview Lodge' }, --Bayview Lodge
        { Location = vector3(-772.81, 5582.91, 33.49)  , Label = 'au téléphérique de Pala Springs' }, --Aerial Tramway
        { Location = vector3(-2208.57, 4279.0, 48.24)  , Label = 'au restaurant Hookies' }, --Hookies Restaurant
        { Location = vector3(-3234.15, 967.37, 13.02)  , Label = 'au ponton de Chumash' }, --Chumash sea
        { Location = vector3(-2965.87, 421.32, 15.26)  , Label = 'à son école de surf' }, --Surf school
        { Location = vector3(-3041.32, 617.26, 7.47)   , Label = 'au Moms Pie Diner' }, --Moms Pie Diner
        { Location = vector3(-862.74, -842.93, 19.23)  , Label = 'à la pagode de Little Seoul' }, --Pagode
        { Location = vector3(-619.79, -934.55, 22.08)  , Label = 'aux bureaux du Weazel News' }, --Weazel News
        { Location = vector3(-866.36, -427.69, 36.64)  , Label = 'au Weazel Plaza' }, --Weazel Plaza
        { Location = vector3(-1355.55, -146.05, 48.58) , Label = 'au Club de tennis' }, --Tennis Club
        { Location = vector3(-1666.94, -541.1, 35.0)   , Label = 'à l\'hôtel & spa Banner de Del Perro' }, --Banner Del Perro
        { Location = vector3(-692.01, -2287.67, 12.97) , Label = 'à l\'hôtel Opium Nights' }, --Opium Nights Hotel
        { Location = vector3(405.14, -1477.34, 29.14)  , Label = 'à la morgue de Strawberry' }, --Morgue
        { Location = vector3(454.6, -684.26, 27.97)    , Label = 'au marché de Simmet Alley' }, --Simmet Alley
        { Location = vector3(-311.31, 225.65, 87.9)    , Label = 'à l\'hôtel Pegasus' }, --Pegasus Hotel
        { Location = vector3(298.99, -1094.57, 29.26)  , Label = 'à l\'hôtel Templar' }, --Templar Hotel
        { Location = vector3(-1095.5, -318.08, 37.67)  , Label = 'à l\'hôtel Archipelago' }, --Archipelago Hotel
        { Location = vector3(-759.49, -35.91, 37.69)   , Label = 'à l\'église de Rockford Hills' }, --RH Church
        { Location = vector3(-532.15, -27.91, 44.4)    , Label = 'au salon de thé de Burton' }, --Tea House
        { Location = vector3(-506.68, -259.17, 35.48)  , Label = 'au parvis de la Mairie' } --City Hall
    }
}