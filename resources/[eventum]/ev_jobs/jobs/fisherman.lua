Config.Jobs.fisherman = {
    Society = {
        name = 'fisherman',
        label = _U('fisherman'),
        db_name = 'society_fisherman'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = 9.58, y = -2663.36, z = 4.98},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 68,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('fs_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = -420.0, y = -3725.0, z = 0.0},
                Marker = {
                    Type = 1,
                    x = 110.0, y = 110.0, z = 10.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "fish",
                    Sprite = 68,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('fs_fish_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'fish',
                        duration = 5,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('fs_fish_hint'),
                HintStop = _U('fs_fish_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = -1012.64, y = -1354.62, z = 4.54},
                Marker = {
                    Type = 1,
                    x = 5.0, y = 5.0, z = 3.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery",
                    Sprite = 68,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('fs_delivery_marker'),
                Type = 'delivery',
                Step = 2,
                Item = {
                    {
                        db_name = nil,
                        duration = 0.5,
                        remove = 1,
                        requires = 'fish',
                    }
                },
                Hint = _U('fs_delivery_hint'),
                HintStop = _U('fs_delivery_hint_stop'),
                InService = true
            },
        },
        Vehicles = {
            Spawner = {
                    {
                        Pos = {x = 25.67, y = -2756.4, z = 5.5},
                        Marker = {
                            Type = 35,
                            x = 1.0, y = 1.0, z = 1.0,
                            r = 171, g = 60, b = 230, a = 100,
                            Rotate = true
                        },
                        Blip = {
                            id = "vehiclespawner_boat",
                            Sprite = 427,
                            Color = 27,
                            Scale = 1.2,
                        },
                        Name = _U('fs_vehiclespawner_boat_marker'),
                        Hint = _U('garage_prompt_boat'),
                        InService = true,
                        Type = "boat"
                    },
                    {
                        Pos = {x = 34.18, y = -2689.79, z = 5.48},
                        Marker = {
                            Type = 36,
                            x = 1.0, y = 1.0, z = 1.0,
                            r = 171, g = 60, b = 230, a = 100,
                            Rotate = true
                        },
                        Blip = {
                            id = "vehiclespawner_car",
                            Sprite = 225,
                            Color = 27,
                            Scale = 1.0,
                        },
                        Name = _U('fs_vehiclespawner_car_marker'),
                        Hint = _U('garage_prompt'),
                        InService = true,
                        Type = "car"
                    }
            },
            SpawnPoints = {
                boat = {
                    {
                        Pos = {x = 35.28, y = -2792.44, z = -1.0},
                        Radius = 4.0,
                        Heading = 179.24
                    }
                },
                car = {
                    {
                        Pos = {x = 33.8, y = -2664.87, z = 5.48},
                        Radius = 4.0,
                        Heading = 354.38
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        boat = {
            temp = {
                {
                    Model = 'tug',
                    Trailer = nil,
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'tug',
                    Trailer = nil,
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'tug',
                    Trailer = nil,
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'tug',
                    Trailer = nil,
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'tug',
                    Trailer = nil,
                    Price = 2000
                }
            }
        },
        car = {
            temp = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 2000
                }
            }
        }
    }
}
