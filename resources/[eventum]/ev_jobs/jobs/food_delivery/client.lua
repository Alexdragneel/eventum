--local OnJob = true
local CurrentDestination = nil
local CurrentStep = 0
local CurrentDistance = 0
local CurrentBlip = nil
local isInService = false
local isInServiceVehicle = false

local deliveryStartedAt = -1
local deliveryTimeLimit = -1

-- Utilitaires Affichage

function DrawPickupDeliveryMarker(pos)
	DrawMarker(
		Config.Jobs.food_delivery.PickupDeliveryMarker.Type,
		pos,
		0.0,
		0.0,
		0.0,
		0.0,
		0.0,
		0.0,
		Config.Jobs.food_delivery.PickupDeliveryMarker.x,
		Config.Jobs.food_delivery.PickupDeliveryMarker.y,
		Config.Jobs.food_delivery.PickupDeliveryMarker.z,
		Config.Jobs.food_delivery.PickupDeliveryMarker.r,
		Config.Jobs.food_delivery.PickupDeliveryMarker.g,
		Config.Jobs.food_delivery.PickupDeliveryMarker.b,
		Config.Jobs.food_delivery.PickupDeliveryMarker.a,
		false,
		false,
		2,
		false,
		nil,
		nil,
		false
	)
end

function RemoveCurrentBlip()
    RemoveBlip(CurrentBlip)
    CurrentBlip = nil
end

--

RegisterNetEvent("esx:setJob")
AddEventHandler(
	"esx:setJob",
	function(job)
		if ESX.PlayerData.job.name ~= job.name then
			isInService = false
		end
		ESX.PlayerData.job = job
	end
)

RegisterNetEvent("esx_service:disableService")
AddEventHandler(
	"esx_service:disableService",
	function()
		isInService = false
        RemoveCurrentBlip()
        CurrentStep = 0
        CurrentDestination = nil
	end
)

RegisterNetEvent("esx_service:enableService")
AddEventHandler(
	"esx_service:enableService",
	function()
		isInService = ESX.PlayerData.job.name == 'food_delivery'
	end
)

-- Utilitaires Food Delivery

function GetRandomPickupLocation()
    return Config.Jobs.food_delivery.PickupLocations[GetRandomIntInRange(1, #Config.Jobs.food_delivery.PickupLocations + 1)]
end

function GetRandomDeliveryLocation()
    return Config.Jobs.food_delivery.DeliveryLocations[GetRandomIntInRange(1, #Config.Jobs.food_delivery.DeliveryLocations + 1)]
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(500)

        if isInService then
            local playerPed = PlayerPedId()
            local vehicle = GetVehiclePedIsIn(playerPed, false)

            if vehicle > 0 then
                isInServiceVehicle = GetEntityModel(vehicle) == 1596918166 -- faggio4 BouffEat
            else
                isInServiceVehicle = false
            end
        else
            isInServiceVehicle = false
        end
    end
end)

-- Thread Principal

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		local playerPed = PlayerPedId()
        local playerCoords = GetEntityCoords(playerPed)

        if isInService then
            if CurrentDestination == nil then
                if isInServiceVehicle then
                    if CurrentStep == 0 then -- recherche resto

                        local waitUntil = GetGameTimer() + GetRandomIntInRange(15000, 30000)
                        TriggerEvent('lp_core:clearTextCenter')
				        TriggerEvent('lp_core:drawTextCenter', _U('waiting_order'), 0.5, 0.05, 5000)
                        while waitUntil > GetGameTimer() do
                            Citizen.Wait(0)
                        end
                        CurrentDestination = GetRandomPickupLocation()
                        TriggerEvent('lp_core:clearTextCenter')
                        TriggerEvent('lp_core:drawTextCenter', _U('go_receive_order'), 0.5, 0.05, 2500)

                    elseif CurrentStep == 1 then -- recherche client

                        CurrentDestination = GetRandomDeliveryLocation()
                        CurrentDistance = #(playerCoords - CurrentDestination)
                        local delai = math.max(CurrentDistance * 150, 60 * 1000)

                        deliveryStartedAt = GetGameTimer()
                        deliveryTimeLimit = GetGameTimer() + delai

                        TriggerEvent('lp_core:clearTextCenter')
                        TriggerEvent('lp_core:drawTextCenter', _U('go_deliver_order'), 0.5, 0.05, 2500)
                    end
                else
                    Citizen.Wait(500)
                end
            else
                if isInServiceVehicle and CurrentBlip == nil then
                    CurrentBlip = AddBlipForCoord(CurrentDestination.x, CurrentDestination.y, CurrentDestination.z)
                    BeginTextCommandSetBlipName('STRING')
                    AddTextComponentSubstringPlayerName('Destination')
                    EndTextCommandSetBlipName(blip)
                    SetBlipRoute(CurrentBlip, true)
                elseif not isInServiceVehicle and CurrentBlip ~= nil then
                    RemoveCurrentBlip()
                end

                if CurrentStep == 1 then
                    local remainingTime = ESX.Math.Round((deliveryTimeLimit - GetGameTimer()) / 1000)
                    if remainingTime >= 0 then
                        local minutes = ESX.Math.Floor(remainingTime/60)
                        if minutes < 10 then minutes = '0'..minutes end
                        local seconds = ESX.Math.Floor(ESX.Math.Mod(remainingTime,60))
                        if seconds < 10 then seconds = '0'..seconds end
                        DrawRect(0.055, 0.774, 0.08, 0.03, 0, 0, 0, 100)
                        exports.ft_libs:Text({ text = 'Temps restant : '..minutes..':'..seconds, font = 4, x = 0.018, y = 0.760, scale = 0.4, alpha = 255})
                    else
                        ESX.ShowNotification(_U("delivery_fail2"))
                        RemoveCurrentBlip()
                        CurrentStep = 0
                        CurrentDestination = nil
                    end
                end

                if CurrentDestination ~= nil then
                    local distance = #(playerCoords - CurrentDestination)
                    if distance < Config.DrawDistance then
                        DrawPickupDeliveryMarker(CurrentDestination)
                    end
                    if distance < Config.Jobs.food_delivery.PickupDeliveryMarker.x and IsPedOnFoot(playerPed) then
                        if CurrentStep == 0 then
                            ESX.ShowHelpNotification(_U('receive_order_hint'))
                            if IsControlJustReleased(0, 38) then
                                ESX.ShowNotification(_U("order_received", GetRandomIntInRange(1000, 9999)))
                                RemoveCurrentBlip()
                                CurrentStep = 1
                                CurrentDestination = nil
                            end
                        else
                            ESX.ShowHelpNotification(_U('deliver_order_hint'))
                            if IsControlJustReleased(0, 38) then
                                RemoveCurrentBlip()
                                CurrentStep = 0
                                CurrentDestination = nil
                                local bonusPercentage = 1 - (GetGameTimer()-deliveryStartedAt)/(deliveryTimeLimit-deliveryStartedAt)
                                TriggerServerEvent('lp_jobs:food_delivery:success', CurrentDistance, bonusPercentage)
                            end
                        end
                    end
                end
            end
        else
            Citizen.Wait(200)
        end
    end
end)