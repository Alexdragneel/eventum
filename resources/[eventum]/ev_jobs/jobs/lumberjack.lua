Config.Jobs.lumberjack = {
    Society = {
        name = 'lumberjack',
        label = _U('lumberjack'),
        db_name = 'society_lumberjack'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = -841.02, y = 5401.87, z = 33.62},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('lj_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = -606.26, y = 5514.64, z = 48.50},
                Marker = {
                    Type = 1,
                    x = 6.0, y = 6.0, z = 1.7,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "wood",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_wood_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'wood',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('lj_wood_hint'),
                HintStop = _U('lj_wood_hint_stop'),
                InService = true
            },
			{
                Pos = {x = -485.64, y = 5576.82, z = 69.56},
                Marker = {
                    Type = 1,
                    x = 6.0, y = 6.0, z = 1.7,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "wood",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_wood_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'wood',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('lj_wood_hint'),
                HintStop = _U('lj_wood_hint_stop'),
                InService = true
            },
			{
                Pos = {x = -590.33, y = 5610.17, z = 37.98},
                Marker = {
                    Type = 1,
                    x = 6.0, y = 6.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "wood",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_wood_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'wood',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('lj_wood_hint'),
                HintStop = _U('lj_wood_hint_stop'),
                InService = true
            },
			{
                Pos = {x = -674.68, y = 5482.28, z = 48.16},
                Marker = {
                    Type = 1,
                    x = 6.0, y = 6.0, z = 1.7,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "wood",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_wood_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'wood',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('lj_wood_hint'),
                HintStop = _U('lj_wood_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -546.03, y = 5377.59, z = 69.56},
                Marker = {
                    Type = 1,
                    x = 4.0, y = 4.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cutwood",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_cutwood_marker'),
                Type = 'farming',
                Step = 2,
                Item = {
                    {
                        db_name = 'wood_ticket',
                        duration = 5,
                        add = 1,
                        remove = 1,
                        requires = 'wood',
                        drop = 100
                    }
                },
                Hint = _U('lj_cutwood_hint'),
                HintStop = _U('lj_cutwood_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -504.22, y = 5275.5, z = 79.61},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "plank",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_planks_marker'),
                Type = 'farming',
                Step = 3,
                Item = {
                    {
                        db_name = 'packaged_plank',
                        duration = 4,
                        add = 5,
                        remove = 1,
                        requires = 'wood_ticket',
                        drop = 100
                    }
                },
                Hint = _U('lj_planks_hint'),
                HintStop = _U('lj_planks_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = 1201.35, y = -1327.51, z = 34.22},
                Marker = {
                    Type = 1,
                    x = 5.0, y = 5.0, z = 3.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery",
                    Sprite = 237,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('lj_delivery_marker'),
                Type = 'delivery',
                Step = 4,
                Item = {
                    {
                        db_name = nil,
                        duration = 0.5,
                        remove = 1,
                        requires = 'packaged_plank',
                    }
                },
                Hint = _U('lj_delivery_hint'),
                HintStop = _U('lj_delivery_hint_stop'),
                InService = true
            },
        },
        Vehicles = {
            Spawner = {
                {
                    Pos = {x = -805 , y = 5392.3, z = 34.52},
                    Marker = {
                        Type = 36,
                        x = 1.0, y = 1.0, z = 1.0,
                        r = 171, g = 60, b = 230, a = 100,
                        Rotate = true
                    },
                    Blip = {
                        id = "vehiclespawner",
                        Sprite = 225,
                        Color = 27,
                        Scale = 1.0,
                    },
                    Name = _U('lj_vehiclespawner_marker'),
                    Type = 'car',
                    Hint = _U('garage_prompt'),
                    InService = true
                }
            },
            SpawnPoints = {
                car = {
                    {
                        Pos = {x = -798.41, y = 5408.77, z = 33.93},
                        Radius = 4.0,
                        Heading = 2.88
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        car = {
            temp = {
                {
                    Model = 'phantom',
                    Trailer = 'trailerlogs',
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'phantom',
                    Trailer = 'trailerlogs',
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'phantom',
                    Trailer = 'trailerlogs',
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'phantom',
                    Trailer = 'trailerlogs',
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'phantom',
                    Trailer = 'trailerlogs',
                    Price = 2000
                }
            }
        }
    }
}
