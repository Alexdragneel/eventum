Config.Jobs.journaliste = {
    Society = {
        name = 'journaliste',
        label = _U('weazel_news'),
        db_name = 'society_journaliste',
        Blip = {
            Pos     = { x = -1087.048, y = -249.330, z = 36.947},
            Sprite  = 184,
            Scale = 1.2,
            Color = 0,
        }
    },
    ActionsMenu = 'lp_jobs:journaliste:openJournalisteActionsMenu',
    Zones = {
        CloakRooms = {
            {
                Pos = { x = -1068.600, y = -241.440, z = 38.73},
                Marker = {
                    Type = 1,
                    x = 1.5, y = 1.5, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Name = _U('cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Strongbox = {
            {
                Pos   = { x = -1054.25, y = -231.89, z = 43.020 },
                Marker = {
                    Type = 1,
                    x = 1.5, y = 1.5, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Name = _U('strongbox'),
                Type = 'vault',
                Hint = _U('access_strongbox'),
                InService = true,
                GradeMin = 3
            }
        },
        Farming = {},
        Deliveries = {},
        Special = {}
    }
}
