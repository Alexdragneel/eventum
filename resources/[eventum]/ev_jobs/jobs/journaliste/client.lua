local holdingCam = false
local holdingMic = false
local usingMic = false
local holdingBmic = false
local usingBmic = false
local camModel = "prop_v_cam_01"
local camanimDict = "missfinale_c2mcs_1"
local camanimName = "fin_c2_mcs_1_camman"
local micModel = "p_ing_microphonel_01"
local micanimDict = "missheistdocksprep1hold_cellphone"
local micanimName = "hold_cellphone"
local bmicModel = "prop_v_bmike_01"
local bmicanimDict = "missfra1"
local bmicanimName = "mcs2_crew_idle_m_boom"
local bmic_net = nil
local mic_net = nil
local cam_net = nil
local camUsed = nil
local isDead = false

Citizen.CreateThread(function()
    local blipCoords = vector3(Config.Jobs.journaliste.Society.Blip.Pos.x, Config.Jobs.journaliste.Society.Blip.Pos.y,
        Config.Jobs.journaliste.Society.Blip.Pos.z)
    local blip = AddBlipForCoord(blipCoords)
    local blipSprite = Config.Jobs.journaliste.Society.Blip.Sprite
    local blipColor = Config.Jobs.journaliste.Society.Blip.Color

    SetBlipSprite(blip, blipSprite)
    SetBlipDisplay(blip, 4)
    SetBlipScale(blip, 1.0)
    SetBlipColour(blip, blipColor)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentSubstringPlayerName(Config.Jobs.journaliste.Society.label)
    EndTextCommandSetBlipName(blip)
end)

RegisterNetEvent("lp_jobs:journaliste:openJournalisteActionsMenu")
AddEventHandler("lp_jobs:journaliste:openJournalisteActionsMenu", function(text, duration, cbServerEvent)
    OpenJournalisteActionsMenu()
end)

function OpenJournalisteActionsMenu()
    local elements = {{
        label = _U("news"),
        value = "news_menu"
    }, {
        label = _U("billing"),
        value = "billing_menu"
    }}

    if ESX.PlayerData.job.grade_name == "boss" or ESX.PlayerData.job.grade_name == "vice" then
        table.insert(elements, {
            label = _U("boss_menu"),
            value = "boss_menu"
        })
    end

    if ESX.PlayerData.job.grade ~= nil and ESX.PlayerData.job.grade > 0 then
        table.insert(elements, {label = _U("bill_management"), value = "bill_management"})
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open("default", GetCurrentResourceName(), ESX.PlayerData.job.name .. "_actions", {
        title = ESX.PlayerData.job.label,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        if data.current.value == "billing_menu" then
            TriggerEvent('lp_core:openBillingMenu')
        elseif data.current.value == "bill_management" then
            TriggerEvent(
                    "esx_society:openBillManagement",
                    ESX.PlayerData.job.name
            )
        elseif data.current.value == "boss_menu" then
            TriggerEvent("esx_society:openBossMenu", ESX.PlayerData.job.name, function(data, menu)
                menu.close()
            end, {
                withdraw = false,
                deposit = false,
                wash = false,
                employees = true,
                grades = true
            }, true)
        elseif data.current.value == "news_menu" then
            OpenNewsMenu()
        end
    end, function(data, menu)
        menu.close()
    end)
end

function OpenNewsMenu()
    ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'news', {
        title = _U('write_news')
    }, function(data, menu)
        if data.value then
            menu.close()
            TriggerServerEvent('lp_jobs:journaliste:advert', data.value)
        end
    end, function(data, menu)
        menu.close()
    end)
end

AddEventHandler('esx:playerLoaded', function()
    ESX.PlayerData = ESX.GetPlayerData()
    if ESX.PlayerData.job then
        if ESX.PlayerData.job.grade_name == 'boss' or ESX.PlayerData.job.grade_name == 'vice' then
            Citizen.SetTimeout(2500, function()
                TriggerEvent('gcPhone:setEnableApp', "Pacific", true)
                TriggerServerEvent('gcPhone:allUpdate')
            end)
        end
    end
end)

AddEventHandler('esx:onPlayerSpawn', function() isDead = false end)
AddEventHandler('esx:onPlayerDeath', function() isDead = true end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)
        
        if holdingCam or holdingMic or holdingBmic then
            ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)

                local hasCamera = false
                local hasMic = false
                local hasBoomMic = false

                for k,v in ipairs(items) do
                    if v.count > 0 then
                        if v.name == 'camera' then
                            hasCamera = true
                        elseif v.name == 'micro' then
                            hasMic = true
                        elseif v.name == 'micro_boom' then
                            hasBoomMic = true
                        end
                    end
                end

                local pedInVehicle = IsPedInAnyVehicle(GetPlayerPed(-1))

                if holdingCam and (isDead or pedInVehicle or not hasCamera) then
                    TriggerEvent("lp_jobs:journaliste:toggleCam")
                end
                if holdingMic and (isDead or pedInVehicle or not hasMic) then
                    TriggerEvent("lp_jobs:journaliste:toggleMic")
                end
                if holdingBmic and (isDead or pedInVehicle or not hasBoomMic) then
                    TriggerEvent("lp_jobs:journaliste:toggleBMic")
                end
            end)
        end
	end
end)

---------------------------------------------------------------------------
-- Camera --
---------------------------------------------------------------------------
RegisterNetEvent("lp_jobs:journaliste:toggleCam")
AddEventHandler("lp_jobs:journaliste:toggleCam", function()
    if not holdingCam then

        if holdingMic then
            TriggerEvent("lp_jobs:journaliste:toggleMic")
        end
        if holdingBmic then
            TriggerEvent("lp_jobs:journaliste:toggleBMic")
        end

        RequestModel(GetHashKey(camModel))
        while not HasModelLoaded(GetHashKey(camModel)) do
            Citizen.Wait(10)
        end
		
        local plyCoords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 0.0, -5.0)
        camUsed = CreateObject(GetHashKey(camModel), plyCoords.x, plyCoords.y, plyCoords.z, 1, 1, 1)
        Citizen.Wait(10)
        local netid = ObjToNet(camUsed)
        SetNetworkIdExistsOnAllMachines(netid, true)
        NetworkSetNetworkIdDynamic(netid, true)
        SetNetworkIdCanMigrate(netid, false)
        AttachEntityToEntity(camUsed, GetPlayerPed(PlayerId()), GetPedBoneIndex(GetPlayerPed(PlayerId()), 28422), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
        TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), camanimDict, camanimName, 1.0, -1, -1, 50, 0, 0, 0, 0)
        cam_net = netid
        holdingCam = true
		DisplayNotification(_U('camera_inputs'))
    else
        ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
        DetachEntity(NetToObj(cam_net), 1, 1)
        DeleteEntity(NetToObj(cam_net))
        cam_net = nil
        holdingCam = false
    end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if holdingCam then
			while not HasAnimDictLoaded(camanimDict) do
				RequestAnimDict(camanimDict)
				Citizen.Wait(10)
			end

			if not IsEntityPlayingAnim(PlayerPedId(), camanimDict, camanimName, 3) then
				TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
				TaskPlayAnim(GetPlayerPed(PlayerId()), camanimDict, camanimName, 1.0, -1, -1, 50, 0, 0, 0, 0)
			end
				
			DisablePlayerFiring(PlayerId(), true)
			DisableControlAction(0,25,true) -- disable aim
			DisableControlAction(0, 44,  true) -- INPUT_COVER
			DisableControlAction(0,37,true) -- INPUT_SELECT_WEAPON
            DisableControlAction(0,23,true) -- INPUT_ENTER : entrée voiture
			SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(10)

		if holdingCam then
            if IsControlJustReleased(1, 244) then
                TriggerEvent("lp_specialcameras:activateCamera", 'CINEMA', camUsed)
            elseif IsControlJustReleased(1, 38) then
                TriggerEvent("lp_specialcameras:activateCamera", 'BREAKING_NEWS', camUsed)
            end
		end

        if IsControlJustReleased(1, 73) then
            if holdingMic then
                TriggerEvent("lp_jobs:journaliste:toggleMic")
            end
            if holdingCam then
                TriggerEvent("lp_jobs:journaliste:toggleCam")
            end
            if holdingBmic then
                TriggerEvent("lp_jobs:journaliste:toggleBMic")
            end
        end
	end
end)

-------------------------------------------------------------------------
-- Microphone --
-------------------------------------------------------------------------
RegisterNetEvent("lp_jobs:journaliste:toggleMic")
AddEventHandler("lp_jobs:journaliste:toggleMic", function()
    if not holdingMic then

        if holdingCam then
            TriggerEvent("lp_jobs:journaliste:toggleCam")
        end
        if holdingBmic then
            TriggerEvent("lp_jobs:journaliste:toggleBMic")
        end

        RequestModel(GetHashKey(micModel))
        while not HasModelLoaded(GetHashKey(micModel)) do
            Citizen.Wait(10)
        end
		
		while not HasAnimDictLoaded(micanimDict) do
			RequestAnimDict(micanimDict)
			Citizen.Wait(10)
		end

        local plyCoords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 0.0, -5.0)
        local micspawned = CreateObject(GetHashKey(micModel), plyCoords.x, plyCoords.y, plyCoords.z, 1, 1, 1)
        Citizen.Wait(10)
        local netid = ObjToNet(micspawned)
        SetNetworkIdExistsOnAllMachines(netid, true)
        NetworkSetNetworkIdDynamic(netid, true)
        SetNetworkIdCanMigrate(netid, false)
        AttachEntityToEntity(micspawned, GetPlayerPed(PlayerId()), GetPedBoneIndex(GetPlayerPed(PlayerId()), 60309), 0.055, 0.05, 0.0, 240.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
        TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), micanimDict, micanimName, 1.0, -1, -1, 50, 0, 0, 0, 0)
        mic_net = netid
        holdingMic = true
    else
        ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
        DetachEntity(NetToObj(mic_net), 1, 1)
        DeleteEntity(NetToObj(mic_net))
        mic_net = nil
        holdingMic = false
        usingMic = false
    end
end)

---------------------------------------------------------------------------
-- Boom Microphone --
---------------------------------------------------------------------------
RegisterNetEvent("lp_jobs:journaliste:toggleBMic")
AddEventHandler("lp_jobs:journaliste:toggleBMic", function()
    if not holdingBmic then

        if holdingMic then
            TriggerEvent("lp_jobs:journaliste:toggleMic")
        end
        if holdingCam then
            TriggerEvent("lp_jobs:journaliste:toggleCam")
        end

        RequestModel(GetHashKey(bmicModel))
        while not HasModelLoaded(GetHashKey(bmicModel)) do
            Citizen.Wait(10)
        end
		
        local plyCoords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 0.0, -5.0)
        local bmicspawned = CreateObject(GetHashKey(bmicModel), plyCoords.x, plyCoords.y, plyCoords.z, true, true, false)
        Citizen.Wait(10)
        local netid = ObjToNet(bmicspawned)
        SetNetworkIdExistsOnAllMachines(netid, true)
        NetworkSetNetworkIdDynamic(netid, true)
        SetNetworkIdCanMigrate(netid, false)
        AttachEntityToEntity(bmicspawned, GetPlayerPed(PlayerId()), GetPedBoneIndex(GetPlayerPed(PlayerId()), 28422), -0.08, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
        TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), bmicanimDict, bmicanimName, 1.0, -1, -1, 50, 0, 0, 0, 0)
        bmic_net = netid
        holdingBmic = true
    else
        ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
        DetachEntity(NetToObj(bmic_net), 1, 1)
        DeleteEntity(NetToObj(bmic_net))
        bmic_net = nil
        holdingBmic = false
        usingBmic = false
    end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if holdingBmic then
			while not HasAnimDictLoaded(bmicanimDict) do
				RequestAnimDict(bmicanimDict)
				Citizen.Wait(100)
			end

			if not IsEntityPlayingAnim(PlayerPedId(), bmicanimDict, bmicanimName, 3) then
				TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
				TaskPlayAnim(GetPlayerPed(PlayerId()), bmicanimDict, bmicanimName, 1.0, -1, -1, 50, 0, 0, 0, 0)
			end
			
			DisablePlayerFiring(PlayerId(), true)
			DisableControlAction(0,25,true) -- disable aim
			DisableControlAction(0, 44,  true) -- INPUT_COVER
			DisableControlAction(0,37,true) -- INPUT_SELECT_WEAPON
			SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
			
			if (IsPedInAnyVehicle(GetPlayerPed(-1), -1) and GetPedVehicleSeat(GetPlayerPed(-1)) == -1) or IsPedCuffed(GetPlayerPed(-1)) or holdingMic then
				ClearPedSecondaryTask(GetPlayerPed(-1))
				DetachEntity(NetToObj(bmic_net), 1, 1)
				DeleteEntity(NetToObj(bmic_net))
				bmic_net = nil
				holdingBmic = false
				usingBmic = false
			end
		end
	end
end)
---------------------------------------------------------------------------------------
-- Fonctions Affichage UI --
---------------------------------------------------------------------------------------

function Notification(message)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(message)
	DrawNotification(0, 1)
end

function DisplayNotification(string)
	SetTextComponentFormat("STRING")
	AddTextComponentString(string)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end