Config.Jobs.refiner = {
    Society = {
        name = 'refiner',
        label = _U('refiner'),
        db_name = 'society_refiner'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = 257.0, y = -3062.44, z = 4.87},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 436,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('rf_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = 609.58, y = 2856.74, z = 38.90},
                Marker = {
                    Type = 1,
                    x = 20.0, y = 20.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "oil",
                    Sprite = 436,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('rf_oil_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'oil',
                        duration = 5,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('rf_oil_hint'),
                HintStop = _U('rf_oil_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 2736.94, y = 1417.99, z = 23.48},
                Marker = {
                    Type = 1,
                    x = 10.0, y = 10.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "refined_oil",
                    Sprite = 436,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('rf_refined_oil_marker'),
                Type = 'farming',
                Step = 2,
                Item = {
                    {
                        db_name = 'refined_oil',
                        duration = 5,
                        add = 1,
                        remove = 2,
                        requires = 'oil',
                        drop = 100
                    }
                },
                Hint = _U('rf_refined_oil_hint'),
                HintStop = _U('rf_refined_oil_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 265.75, y = -3013.39, z = 4.73},
                Marker = {
                    Type = 1,
                    x = 10.0, y = 10.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "gas",
                    Sprite = 436,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('rf_gas_marker'),
                Type = 'farming',
                Step = 3,
                Item = {
                    {
                        db_name = 'gas',
                        duration = 5,
                        add = 2,
                        remove = 1,
                        requires = 'refined_oil',
                        drop = 100
                    }
                },
                Hint = _U('rf_gas_hint'),
                HintStop = _U('rf_gas_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = 304.04, y = -2734.5, z = 5.01},
                Marker = {
                    Type = 1,
                    x = 5.0, y = 5.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery",
                    Sprite = 436,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('rf_delivery_marker'),
                Type = 'delivery',
                Step = 4,
                Item = {
                    {
                        db_name = nil,
                        requires = 'gas',
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('rf_delivery_hint'),
                HintStop = _U('rf_delivery_hint_stop'),
                InService = true
            },
        },
        Vehicles = {
            Spawner = {
                {
                    Pos = {x = 256.25, y = -3056.27, z = 5.6},
                    Marker = {
                        Type = 36,
                        x = 1.0, y = 1.0, z = 1.0,
                        r = 171, g = 60, b = 230, a = 100,
                        Rotate = true
                    },
                    Blip = {
                        id = "vehiclespawner",
                        Sprite = 225,
                        Color = 27,
                        Scale = 1.0,
                    },
                    Name = _U('rf_vehiclespawner_marker'),
                    Hint = _U('garage_prompt'),
                    InService = true,
                    Type = "car"
                }
            },
            SpawnPoints = {
                car = {
                    {
                        Pos = {x = 235.86, y = -3073.64, z = 4.87},
                        Radius = 4.0,
                        Heading = 44.85
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        car = {
            temp = {
                {
                    Model = 'phantom',
                    Trailer = 'tanker',
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'phantom',
                    Trailer = 'tanker',
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'phantom',
                    Trailer = 'tanker',
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'phantom',
                    Trailer = 'tanker',
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'phantom',
                    Trailer = 'tanker',
                    Price = 2000
                }
            }
        }
    }
}
