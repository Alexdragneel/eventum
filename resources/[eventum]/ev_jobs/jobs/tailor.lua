Config.Jobs.tailor = {
    Society = {
        name = 'tailor',
        label = _U('tailor'),
        db_name = 'society_tailor'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = 706.73, y = -960.90, z = 29.39},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 366,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('ta_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = 2414.31, y = 4991.86, z = 45.2},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "wool",
                    Sprite = 366,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('ta_wool_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'wool',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('ta_wool_hint'),
                HintStop = _U('ta_wool_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 713.78, y = -961.74, z = 29.39},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "fabric",
                    Sprite = 366,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('ta_fabric_marker'),
                Type = 'farming',
                Step = 2,
                Item = {
                    {
                        db_name = 'fabric',
                        duration = 5,
                        add = 2,
                        remove = 1,
                        requires = 'wool',
                        drop = 100
                    }
                },
                Hint = _U('ta_fabric_hint'),
                HintStop = _U('ta_fabric_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 712.92, y = -970.58, z = 29.39},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "clothing",
                    Sprite = 366,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('ta_clothing_marker'),
                Type = 'farming',
                Step = 3,
                Item = {
                    {
                        db_name = 'clothing',
                        duration = 4,
                        add = 1,
                        remove = 2,
                        requires = 'fabric',
                        drop = 100
                    }
                },
                Hint = _U('ta_clothing_hint'),
                HintStop = _U('ta_clothing_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = 429.59, y = -807.34, z = 28.49},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery",
                    Sprite = 366,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('ta_delivery_marker'),
                Type = 'delivery',
                Step = 4,
                Item = {
                    {
                        requires = 'clothing',
                        db_name = nil,
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('ta_delivery_hint'),
                HintStop = _U('ta_delivery_hint_stop'),
                InService = true
            },
        },
        Vehicles = {
            Spawner = {
                {
                    Pos = {x = 741.3, y = -966.55, z = 24.49},
                    Marker = {
                        Type = 36,
                        x = 1.0, y = 1.0, z = 1.0,
                        r = 171, g = 60, b = 230, a = 100,
                        Rotate = true
                    },
                    Blip = {
                        id = "vehiclespawner",
                        Sprite = 225,
                        Color = 27,
                        Scale = 1.0,
                    },
                    Name = _U('ta_vehiclespawner_marker'),
                    Hint = _U('garage_prompt'),
                    InService = true,
                    Type = "car"
                }
            },
            SpawnPoints = {
                car = {
                    {
                        Pos = {x = 747.31, y = -966.23, z = 23.70},
                        Radius = 4.0,
                        Heading = 270.1
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        car = {
            temp = {
                {
                    Model = 'youga2',
                    Trailer = nil,
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'youga2',
                    Trailer = nil,
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'youga2',
                    Trailer = nil,
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'youga2',
                    Trailer = nil,
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'youga2',
                    Trailer = nil,
                    Price = 2000
                }
            }
        }
    }
}
