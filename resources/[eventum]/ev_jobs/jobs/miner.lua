Config.Jobs.miner = {
    Society = {
        name = 'miner',
        label = _U('miner'),
        db_name = 'society_miner'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = 1078.72, y = -1973.16, z =  30.47},
                Marker = {
                    Type = 1,
                    x = 1.5, y = 1.5, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('mn_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = 2946.46, y = 2800.1, z = 40.10},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "stone",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_stone_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'stone',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('mn_stone_hint'),
                HintStop = _U('mn_stone_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 289.24, y = 2862.90, z = 42.64},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "washed_stone",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_washed_stone_marker'),
                Type = 'farming',
                Step = 2,
                Item = {
                    {
                        db_name = 'washed_stone',
                        duration = 5,
                        add = 1,
                        remove = 1,
                        requires = 'stone',
                        drop = 100
                    }
                },
                Hint = _U('mn_washed_stone_hint'),
                HintStop = _U('mn_washed_stone_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 1109.14, y = -2007.87, z = 30.01},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "foundry",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_foundry_marker'),
                Type = 'farming',
                Step = 3,
                Item = {
                    {
                        db_name = 'copper',
                        duration = 4,
                        add = 8,
                        remove = 1,
                        requires = 'washed_stone',
                        drop = 100
                    },
                    {
                        db_name = 'iron',
                        add = 6,
                        drop = 100
                    },
                    {
                        db_name = 'gold',
                        add = 3,
                        drop = 100
                    },
                    {
                        db_name = 'diamond',
                        add = 1,
                        drop = 5
                    }
                },
                Hint = _U('mn_foundry_hint'),
                HintStop = _U('mn_foundry_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = -169.481, y = -2659.16, z = 5.00103},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery_copper",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_delivery_copper_marker'),
                Type = 'delivery',
                Step = 4,
                Item = {
                    {
                        db_name = nil,
                        requires = 'copper',
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('mn_delivery_copper_hint'),
                HintStop = _U('mn_delivery_copper_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -148.78, y = -1040.38, z = 26.27},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery_iron",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_delivery_iron_marker'),
                Type = 'delivery',
                Step = 5,
                Item = {
                    {
                        db_name = nil,
                        requires = 'iron',
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('mn_delivery_iron_hint'),
                HintStop = _U('mn_delivery_iron_hint_stop'),
                InService = true
            },
            {
                Pos = {x = 261.48, y = 207.35, z = 109.28},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery_gold",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_delivery_gold_marker'),
                Type = 'delivery',
                Step = 6,
                Item = {
                    {
                        db_name = nil,
                        requires = 'gold',
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('mn_delivery_gold_hint'),
                HintStop = _U('mn_delivery_gold_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -621.04, y = -228.53, z = 37.05},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery_diamond",
                    Sprite = 617,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('mn_delivery_diamond_marker'),
                Type = 'delivery',
                Step = 7,
                Item = {
                    {
                        db_name = nil,
                        requires = 'diamond',
                        duration = 0.5,
                        remove = 1,
                    }
                },
                Hint = _U('mn_delivery_diamond_hint'),
                HintStop = _U('mn_delivery_diamond_hint_stop'),
                InService = true
            }
        },
        Vehicles = {
            Spawner = {
                {
                    Pos = {x = 1081.74, y = -1948.69, z = 31.0},
                    Marker = {
                        Type = 36,
                        x = 1.0, y = 1.0, z = 1.0,
                        r = 171, g = 60, b = 230, a = 100,
                        Rotate = true
                    },
                    Blip = {
                        id = "vehiclespawner",
                        Sprite = 225,
                        Color = 27,
                        Scale = 1.0,
                    },
                    Name = _U('mn_vehiclespawner_marker'),
                    Type = 'car',
                    Hint = _U('garage_prompt'),
                    InService = true
                }
            },
            SpawnPoints = {
                car = {
                    {
                        Pos = {x = 1073.47, y = -1951.62, z = 31.01},
                        Radius = 4.0,
                        Heading = 146.82
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        car = {
            temp = {
                {
                    Model = 'rubble',
                    Trailer = nil,
                    Price = 5000
                }
            },
            employee = {
                {
                    Model = 'rubble',
                    Trailer = nil,
                    Price = 4000
                }
            },
            manager = {
                {
                    Model = 'rubble',
                    Trailer = nil,
                    Price = 3000
                }
            },
            vice = {
                {
                    Model = 'rubble',
                    Trailer = nil,
                    Price = 2500
                }
            },
            boss = {
                {
                    Model = 'rubble',
                    Trailer = nil,
                    Price = 2000
                }
            }
        }
    }
}
