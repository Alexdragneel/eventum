Config.Jobs.butcher = {
    Society = {
        name = 'butcher',
        label = _U('butcher'),
        db_name = 'society_butcher'
    },
    Zones = {
        CloakRooms = {
            {
                Pos = {x = -75.32, y = 6250.56, z = 30.09},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 256,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('bu_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {
            {
                Pos = {x = -67.38, y = 6247.0, z = 30.09},
                Marker = {
                    Type = 1,
                    x = 4.0, y = 4.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "chicken",
                    Sprite = 256,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('bu_chicken_marker'),
                Type = 'farming',
                Step = 1,
                Item = {
                    {
                        db_name = 'chicken',
                        duration = 3,
                        add = 1,
                        remove = 1,
                        requires = nil,
                        drop = 100
                    }
                },
                Hint = _U('bu_chicken_hint'),
                HintStop = _U('bu_chicken_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -87.73, y = 6235.98, z = 30.09},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "slaughtered_chicken",
                    Sprite = 256,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('bu_slaughtered_chicken_marker'),
                Type = 'farming',
                Step = 2,
                Item = {
                    {
                        db_name = 'slaughtered_chicken',
                        duration = 5,
                        add = 1,
                        remove = 1,
                        requires = 'chicken',
                        drop = 100
                    }
                },
                Hint = _U('bu_slaughtered_chicken_hint'),
                HintStop = _U('bu_slaughtered_chicken_hint_stop'),
                InService = true
            },
            {
                Pos = {x = -101.97, y = 6208.79, z = 30.02},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "packaged_chicken",
                    Sprite = 256,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('bu_packaged_chicken_marker'),
                Type = 'farming',
                Step = 3,
                Item = {
                    {
                        db_name = 'packaged_chicken',
                        duration = 5,
                        add = 2,
                        remove = 1,
                        requires = 'slaughtered_chicken',
                        drop = 100
                    }
                },
                Hint = _U('bu_packaged_chicken_hint'),
                HintStop = _U('bu_packaged_chicken_hint_stop'),
                InService = true
            },
        },
        Deliveries = {
            {
                Pos = {x = -596.15, y = -889.32, z = 24.50},
                Marker = {
                    Type = 1,
                    x = 5.0, y = 5.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "delivery",
                    Sprite = 256,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('bu_delivery_marker'),
                Type = 'delivery',
                Step = 4,
                Item = {
                    {
                        db_name = nil,
                        duration = 0.5,
                        remove = 1,
                        requires = 'packaged_chicken',
                    }
                },
                Hint = _U('bu_delivery_hint'),
                HintStop = _U('bu_delivery_hint_stop'),
                InService = true
            },
        },
        Vehicles = {
            Spawner = {
                {
                    Pos = {x = -20.41, y = 6252.1, z = 31.29},
                    Marker = {
                        Type = 36,
                        x = 1.0, y = 1.0, z = 1.0,
                        r = 171, g = 60, b = 230, a = 100,
                        Rotate = true
                    },
                    Blip = {
                        id = "vehiclespawner",
                        Sprite = 225,
                        Color = 27,
                        Scale = 1.0,
                    },
                    Name = _U('bu_vehiclespawner_marker'),
                    Hint = _U('garage_prompt'),
                    InService = true,
                    Type = "car"
                }
            },
            SpawnPoints = {
                car = {
                    {
                        Pos = {x = -16.84, y = 6259.69, z = 31.24},
                        Radius = 4.0,
                        Heading = 33.67
                    }
                }
            },
        },
    },
    AuthorizedVehicles = {
        car = {
            temp = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 5000,
                }
            },
            employee = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 4000,
                }
            },
            manager = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 3000,
                }
            },
            vice = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 2500,
                }
            },
            boss = {
                {
                    Model = 'benson',
                    Trailer = nil,
                    Price = 2000,
                }
            }
        }
    }
}
