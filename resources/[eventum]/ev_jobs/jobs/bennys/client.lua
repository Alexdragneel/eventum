local savedCustoms = nil
local lastPrepared = 0

Citizen.CreateThread(function()
    local blipCoords = vector3(Config.Jobs.bennys.Society.Blip.Pos.x, Config.Jobs.bennys.Society.Blip.Pos.y, Config.Jobs.bennys.Society.Blip.Pos.z)
    local blip = AddBlipForCoord(blipCoords)
    local blipSprite = Config.Jobs.bennys.Society.Blip.Sprite
    local blipColor = Config.Jobs.bennys.Society.Blip.Color
    
    SetBlipSprite (blip, blipSprite)
    SetBlipDisplay(blip, 4)
    SetBlipScale  (blip, 1.0)
    SetBlipColour (blip, blipColor)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentSubstringPlayerName(Config.Jobs.bennys.Society.label)
    EndTextCommandSetBlipName(blip)
end)

RegisterNetEvent("lp_jobs:bennys:openBennysActionsMenu")
AddEventHandler("lp_jobs:bennys:openBennysActionsMenu", function(text, duration, cbServerEvent)
    OpenBennysActionsMenu()
end)

RegisterNetEvent("lp_jobs:bennys:throwVehicleJunkyard")
AddEventHandler("lp_jobs:bennys:throwVehicleJunkyard", function(text, duration, cbServerEvent)
    ThrowVehicleJunkyard()
end)

function ThrowVehicleJunkyard()
    local playerPed = PlayerPedId()
    local playerCoords = GetEntityCoords(playerPed)
    local vehiclesAround = ESX.Game.GetVehiclesInArea(playerCoords, 5.0)
    local foundVehicles = {}
    for k, vehicle in ipairs(vehiclesAround) do
        local plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))
        local model = GetEntityModel(vehicle)
        local vehicleName = GetLabelText(GetDisplayNameFromVehicleModel(model))
        local label = ('%s <span style="color:darkgoldenrod;">%s</span>'):format(vehicleName, plate)
        table.insert(foundVehicles, {
            label = label, value = { vehicle = vehicle, plate = plate }
        })
    end

    if #foundVehicles == 0 then
        ESX.ShowNotification(_U('garage_store_nearby'))
        return
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_junkyard",
		{
			title = _U('junkyard'),
			align = "top-left",
			elements = foundVehicles
		},
		function(data, menu)
            TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_CLIPBOARD", 0, true)
            TriggerEvent('rprogress:custom', {
                Duration = 5000,
                Label = _U('vehicle_junkyard'),
                onComplete = function ()
                    ClearPedTasks(playerPed)
                    ESX.TriggerServerCallback('lp_jobs:bennys:putVehicleJunkyard', function (pawned)
                        if pawned then
                            local attempts = 0
                            ESX.Game.DeleteVehicle(data.current.value.vehicle)

                            -- Workaround for vehicle not deleting when other players are near it.
                            while DoesEntityExist(data.current.value.vehicle) do
                                Citizen.Wait(500)
                                attempts = attempts + 1

                                -- Give up
                                if attempts > 50 then
                                    break
                                end

                                ESX.Game.DeleteVehicle(data.current.value.vehicle)
                            end
                        end
                    end,
                    data.current.value.plate)
                end
            })
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenBennysActionsMenu()
	local elements = {
        {label = _U("vehicle"), value = "vehicle_menu"},
        {label = _U("billing"), value = "billing_menu"}
    }

    if ESX.PlayerData.job.grade ~= nil and ESX.PlayerData.job.grade > 0 then
        table.insert(elements, {label = _U("bill_management"), value = "bill_management"})
    end

	if ESX.PlayerData.job.grade_name == "boss" then
		table.insert(elements, {label = _U("boss_menu"), value = "boss_menu"})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_actions",
		{
			title = ESX.PlayerData.job.label,
			align = "top-left",
			elements = elements
		},
		function(data, menu)
			if data.current.value == "billing_menu" then
				TriggerEvent('lp_core:openBillingMenu')
            elseif data.current.value == "bill_management" then
                TriggerEvent(
                        "esx_society:openBillManagement",
                        ESX.PlayerData.job.name
                )
			elseif data.current.value == "boss_menu" then
				TriggerEvent(
					"esx_society:openBossMenu",
					ESX.PlayerData.job.name,
					function(data, menu)
						menu.close()
					end,
					{wash = false}
				)
			elseif data.current.value == "vehicle_menu" then
                OpenBennysSubMenu()
            end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenBennysSubMenu()
	local playerPed = PlayerPedId()
    local playerCoords = GetEntityCoords(playerPed)
    local vehiclesAround = ESX.Game.GetVehiclesInArea(playerCoords, 5.0)
    local foundVehicles = {}
    for k, vehicle in ipairs(vehiclesAround) do
        local plate = ESX.Math.Trim(GetVehicleNumberPlateText(vehicle))
        local model = GetEntityModel(vehicle)
        local vehicleName = GetLabelText(GetDisplayNameFromVehicleModel(model))
        local label = ('%s <span style="color:darkgoldenrod;">%s</span>'):format(vehicleName, plate)
        table.insert(foundVehicles, {
            label = label, value = { vehicle = vehicle, plate = plate, model = vehicleName }
        })
    end

    if #foundVehicles == 0 then
        ESX.ShowNotification(_U('garage_store_nearby'))
        return
    end

    ESX.UI.Menu.CloseAll()

    local elements = {
        {label = _U("diagnostic"), value = "diagnostic_menu"},
        {label = _U("repair"), value = "repair_menu"},
        {label = _U("customs"), value = "customs_menu"},
        {label = _U("lockpick"), value = "lockpick_vehicle"}
    }

    ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_actions_sub",
		{
			title = ESX.PlayerData.job.label,
			align = "top-left",
			elements = elements
		},
		function(data, menu)
			if data.current.value == 'diagnostic_menu' then
                OpenDiagnosticMenu(foundVehicles)
            elseif data.current.value == 'repair_menu' then
                OpenRepairMenu(foundVehicles)
            elseif data.current.value == 'customs_menu' then
                OpenCustomsMenu(foundVehicles)
            elseif data.current.value == 'lockpick_vehicle' then
                OpenLockpickMenu(foundVehicles)
            end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenDiagnosticMenu(foundVehicles)
	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_diagnostic",
		{
			title = _U('diagnostic'),
			align = "top-left",
			elements = foundVehicles
		},
		function(data, menu)
            OpenDiagnosticSubMenu(data.current.value.vehicle, data.current.value.plate, data.current.value.model)
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenLockpickMenu(foundVehicles)
    ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_diagnostic",
		{
			title = _U('diagnostic'),
			align = "top-left",
			elements = foundVehicles
		},
		function(data, menu)
            TaskStartScenarioInPlace(playerPed, 'WORLD_HUMAN_WELDING', 0, true)
            TriggerEvent('rprogress:custom', {
                Duration = 5000,
                Label = _U('lockpicking'),
                onComplete = function ()
                    ClearPedTasks(playerPed)
                    if DoesEntityExist(data.current.value.vehicle) then
                        local hasControl = ESX.Game.RequestControl(data.current.value.vehicle)
                        if not hasControl then
                            ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
                            return
                        end
                        SetVehicleDoorsLocked(data.current.value.vehicle, 1)
                        ESX.ShowNotification(_U('vehicle_open'))
                    end
                end
            })
        end,
        function(data, menu)
            menu.close()
        end
    )
end

function OpenDiagnosticSubMenu(vehicle, plate, model)
    local playerPed = PlayerPedId()

    local elements = {
        {label = _U("diagnostic_body"), value = "diagnostic_body"},
        {label = _U("diagnostic_engine"), value = "diagnostic_engine"},
        {label = _U("diagnostic_customs"), value = "diagnostic_customs"},
        {label = _U("diagnostic_certificate"), value = "diagnostic_certificate"}
    }

    ESX.UI.Menu.Open(
        "default",
        GetCurrentResourceName(),
        ESX.PlayerData.job.name .. "_diagnostic_sub",
        {
            title = _U('diagnostic'),
            align = "top-left",
            elements = elements
        },
        function(data, menu)
            ESX.TriggerServerCallback('lp_jobs:bennys:getDiagnosticResults', function (diagnosticResults)
                local hasControl = ESX.Game.RequestControl(vehicle)
                if not hasControl then
                    ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
                    return
                end
                if data.current.value == "diagnostic_body" then
                    TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_CLIPBOARD", 0, true)
                    TriggerEvent('rprogress:custom', {
                        Duration = 10000,
                        Label = _U('diagnostic_body'),
                        onComplete = function ()
                            ClearPedTasks(playerPed)
                            local damages = ESX.Game.GetVehicleDamages(vehicle)
                            diagnosticResults.body = ('%d/1000'):format(ESX.Math.Round(damages.bod, 0))
                            
                            if AreAllVehicleWindowsIntact(vehicle) then
                                diagnosticResults.windows = _U('intacts')
                            else
                                diagnosticResults.windows = _U('not_intacts')
                            end

                            local burstTyres = 0
                            local intactTyres = 0
                            if damages.tyr ~= nil then
                                for i = 1, 8 do
                                    if damages.tyr[i] ~= nil then
                                        if damages.tyr[i] == 0 then
                                            intactTyres = intactTyres + 1
                                        else
                                            burstTyres = burstTyres + 1
                                        end
                                    end
                                end	
                            end

                            local numberTyres = GetVehicleNumberOfWheels(vehicle)
                            if intactTyres > numberTyres then
                                intactTyres = numberTyres
                            end

                            diagnosticResults.tyres = ('%d/%d pneus intactes'):format(intactTyres, numberTyres)

                            TriggerServerEvent('lp_jobs:bennys:saveDiagnosticResults', plate, diagnosticResults)
                            
                            ESX.ShowNotification(_U('diagnostic_body_done'))
                        end
                    })
                elseif data.current.value == "diagnostic_engine" then
                    SetVehicleDoorOpen(vehicle, 4, false)
                    Citizen.Wait(500)
                    ESX.Streaming.RequestAnimDict('mini@repair', function()
                        TaskPlayAnim(playerPed, 'mini@repair', 'fixing_a_ped', 8.0, -8.0, -1, 49, 0, false, false, false)
                        RemoveAnimDict('mini@repair')
                    end)
                    TriggerEvent('rprogress:custom', {
                        Duration = 10000,
                        Label = _U('diagnostic_engine'),
                        onComplete = function ()
                            ClearPedTasks(playerPed)
                            local damages = ESX.Game.GetVehicleDamages(vehicle)
                            diagnosticResults.engine = ('%d/1000'):format(ESX.Math.Round(damages.eng, 0))
                            diagnosticResults.tank = ('%d/1000'):format(ESX.Math.Round(damages.tnk, 0))
                            TriggerServerEvent('lp_jobs:bennys:saveDiagnosticResults', plate, diagnosticResults)
                            SetVehicleDoorShut(vehicle, 4, false)
                            ESX.ShowNotification(_U('diagnostic_engine_done'))
                        end
                    })
                elseif data.current.value == "diagnostic_customs" then
                    TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_CLIPBOARD", 0, true)
                    TriggerEvent('rprogress:custom', {
                        Duration = 10000,
                        Label = _U('diagnostic_customs'),
                        onComplete = function ()
                            ClearPedTasks(playerPed)
                            local customs = ESX.Game.GetVehicleProperties(vehicle)
                            diagnosticResults.customs = DiagnosticFormatCustoms(vehicle, customs)
                            TriggerServerEvent('lp_jobs:bennys:saveDiagnosticResults', plate, diagnosticResults)

                            ESX.ShowNotification(_U('diagnostic_customs_done'))
                        end
                    })
                elseif data.current.value == "diagnostic_certificate" then
                    TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_CLIPBOARD", 0, true)
                    OpenDiagnosticCertificate(playerPed, vehicle, plate, model, diagnosticResults)
                end
            end, plate)
        end,
        function(data, menu)
            menu.close()
        end
    )
end

function OpenRepairMenu(foundVehicles)
    local playerPed = PlayerPedId()

    ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_repair",
		{
			title = _U('repair'),
			align = "top-left",
			elements = foundVehicles
		},
		function(data, menu)
            local elements = {
                {label = _U("repair_body"), value = "repair_body"},
                {label = _U("repair_engine"), value = "repair_engine"},
                {label = _U("clean"), value = "clean"}
            }

			ESX.UI.Menu.Open(
                "default",
                GetCurrentResourceName(),
                ESX.PlayerData.job.name .. "_repair_sub",
                {
                    title = _U('repair'),
                    align = "top-left",
                    elements = elements
                },
                function(data2, menu2)
                    local hasControl = ESX.Game.RequestControl(data.current.value.vehicle)
                    if not hasControl then
                        ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
                        return
                    end
                    if data2.current.value == "repair_body" then
                        TaskStartScenarioInPlace(playerPed, 'WORLD_HUMAN_WELDING', 0, true)
                        TriggerEvent('rprogress:custom', {
                            Duration = 10000,
                            Label = _U('repair_body'),
                            onComplete = function ()
                                ClearPedTasks(playerPed)
                                local engineHealth = GetVehicleEngineHealth(data.current.value.vehicle)
                                local petrolHealth = GetVehiclePetrolTankHealth(data.current.value.vehicle)
                                SetVehicleFixed(data.current.value.vehicle)
                                SetVehicleDeformationFixed(data.current.value.vehicle)
                                RemoveDecalsFromVehicle(data.current.value.vehicle)
                                -- we need SetVehicleFixed to fix some deformations but it also fixes engine and tank
                                -- so we reapply previous engine and tank health
                                SetVehicleEngineHealth(data.current.value.vehicle, engineHealth)
                                SetVehiclePetrolTankHealth(data.current.value.vehicle, petrolHealth)

                                ESX.ShowNotification(_U('repair_body_done'))
                            end
                        })
                    elseif data2.current.value == "repair_engine" then
                        SetVehicleDoorOpen(data.current.value.vehicle, 4, false, false)
                        Citizen.Wait(500)
                        ESX.Streaming.RequestAnimDict('mini@repair', function()
                            TaskPlayAnim(playerPed, 'mini@repair', 'fixing_a_ped', 8.0, -8.0, -1, 49, 0, false, false, false)
                            RemoveAnimDict('mini@repair')
                        end)
                        TriggerEvent('rprogress:custom', {
                            Duration = 10000,
                            Label = _U('repair_engine'),
                            onComplete = function ()
                                ClearPedTasks(playerPed)
                                SetVehicleEngineHealth(data.current.value.vehicle, 1000.0)
                                SetVehiclePetrolTankHealth(data.current.value.vehicle, 1000.0)
                                SetVehicleDoorShut(data.current.value.vehicle, 4, false)

                                ESX.ShowNotification(_U('repair_engine_done'))
                            end
                        })
                    elseif data2.current.value == "clean" then
                        TaskStartScenarioInPlace(playerPed, "world_human_maid_clean", 0, true)
                        TriggerEvent('rprogress:custom', {
                            Duration = 10000,
                            Label = _U('clean'),
                            onComplete = function ()
                                ClearPedTasks(playerPed)
                                SetVehicleDirtLevel(data.current.value.vehicle, 0)

                                ESX.ShowNotification(_U('clean_done'))
                            end
                        })
                    end
                end,
                function(data2, menu2)
                    menu2.close()
                end
            )
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function ApplyCustomsAction(vehicle, data, save)
    local hasControl = ESX.Game.RequestControl(vehicle)
    if not hasControl then
        ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
        return
    end
    local props = {}
    if data.wheelType ~= nil then
        props['wheels'] = data.wheelType
        props['modFrontWheels'] = data.value
        if data.wheelType == 6 then
            props['modBackWheels'] = data.value
        end
        ESX.Game.SetVehicleProperties(vehicle, props)
        props = {}
    elseif data.name == 'neonColor' then
        if data.value[1] == 0 and data.value[2] == 0 and data.value[3] == 0 then
            props['neonEnabled'] = { false, false, false, false }
        else
            props['neonEnabled'] = { true, true, true, true }
        end
        ESX.Game.SetVehicleProperties(vehicle, props)
        props = {}
    elseif data.name == 'tyreSmokeColor' then
        if data.value[1] == 0 and data.value[2] == 0 and data.value[3] == 0 then
            props['modSmokeEnabled'] = 0
        else
            props['modSmokeEnabled'] = 1
        end
        ESX.Game.SetVehicleProperties(vehicle, props)
        props = {}
    elseif data.name == 'xenonColor' then
        if data.value == -1 then
            props['modXenon'] = 0
            data.value = 255
        else
            props['modXenon'] = 1
        end
        ESX.Game.SetVehicleProperties(vehicle, props)
        props = {}
    end

    if data.name ~= nil and data.wheelType == nil then
        props[data.name] = data.value

        ESX.Game.SetVehicleProperties(vehicle, props)
    end
    
    if save then
        savedCustoms = ESX.Game.GetVehicleProperties(vehicle)
        savedCustoms.extras = nil
    end
end

function PrepareVehicle(vehicle, modName)
    local hasControl = ESX.Game.RequestControl(vehicle)
    if not hasControl then
        ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
        return
    end
    if (modName == 'modSpeakers' or
    modName == 'modTrunk' or
    modName == 'modHydrolic' or
    modName == 'modEngineBlock' or
    modName == 'modAirFilter' or
    modName == 'modStruts' or
    modName == 'modTank') then
        if lastPrepared ~= 1 then
            lastPrepared = 1
            SetVehicleDoorOpen(vehicle, 4, false)
            SetVehicleDoorOpen(vehicle, 5, false)
        end
    elseif modName == 'modDoorSpeaker' then
        if lastPrepared ~= 2 then
            
            lastPrepared = 2
            SetVehicleDoorOpen(vehicle, 0, false)
            SetVehicleDoorOpen(vehicle, 1, false)
            SetVehicleDoorOpen(vehicle, 2, false)
            SetVehicleDoorOpen(vehicle, 3, false)
        end
    else
        if lastPrepared ~= 3 then
            lastPrepared = 3
            SetVehicleDoorsShut(vehicle, false)
        end
    end

    if not GetIsVehicleEngineRunning(vehicle) then
        SetVehicleEngineOn(vehicle, true, true, false)
        SetVehicleLights(vehicle, 2)
        SetVehicleLights(vehicle, 2)
    end
end

function OpenCustomsSubMenu(vehicle, data)
    local menus = Config.Jobs.bennys.Customs.Menus
    local mainMenuElements = {}
    local parent = nil

    if data ~= nil then
        parent = data.name
    end

    local foundParent = false
    for k,v in pairs(menus) do
        if v.parent == parent then
            foundParent = true
            if v.modType ~= nil then
                local modCount = GetNumVehicleMods(vehicle, v.modType)

                if modCount > 0 then
                    if v.wheelType ~= nil then
                        local vehicleClass = GetVehicleClass(vehicle)
                        if ((vehicleClass == 8 or vehicleClass == 13) and v.wheelType == 6) or (vehicleClass ~= 8 and vehicleClass ~= 13 and v.wheelType ~= 6) then
                            table.insert(mainMenuElements, {
                                label   = v.label,
                                name   = k,
                                modType = v.modType,
                                modName = v.modName,
                                wheelType = v.wheelType
                            })
                        end
                    else
                        table.insert(mainMenuElements, {
                            label   = v.label,
                            name   = k,
                            modType = v.modType,
                            modName = v.modName
                        })
                    end
                end
            else
                table.insert(mainMenuElements, {
                    label   = v.label,
                    name   = k,
                    modName = v.modName
                })
            end
        end
    end
    
    local menuName = parent or 'top'

    if not foundParent and parent ~= nil then
        mainMenuElements = FormatOptionsCustoms(mainMenuElements, parent, data, vehicle)

        if parent ~= data.modName and data.modName ~= nil then
            menuName = menuName .. data.modName
        end
    end
    
    table.sort(mainMenuElements, function(a, b)
        return a.label < b.label
    end)

    if next(mainMenuElements) ~= nil and mainMenuElements[1].name ~= nil and mainMenuElements[1].applyCustoms ~= nil then
        PrepareVehicle(vehicle, mainMenuElements[1].name)
        ApplyCustomsAction(vehicle, mainMenuElements[1], false)
    end

    ESX.UI.Menu.Open(
    "default",
    GetCurrentResourceName(),
    ESX.PlayerData.job.name .. "_customs_main_" .. menuName,
    {
        title = _U('customs'),
        align = "top-left",
        elements = mainMenuElements
    },
    function(data2, menu2)
        local hasControl = ESX.Game.RequestControl(vehicle)
        if not hasControl then
            ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
            return
        end
        lastPrepared = 0
        SetVehicleDoorsShut(vehicle, false)
        if data2.current.applyCustoms == nil then
            OpenCustomsSubMenu(vehicle, data2.current)
        elseif data2.current.applyCustoms ~= nil then
            PrepareVehicle(vehicle, data2.current.name)
            ApplyCustomsAction(vehicle, data2.current, true)
            menu2.close()
        end
    end,
    function(data2, menu2)
        -- reset
        local hasControl = ESX.Game.RequestControl(vehicle)
        if not hasControl then
            ESX.ShowNotification("Erreur. Vérifiez qu'il n'y a personne d'autre que vous à l'intérieur ou autour du véhicule.")
            return
        end
        lastPrepared = 0
        SetVehicleDoorsShut(vehicle, false)
        ESX.Game.SetVehicleProperties(vehicle, savedCustoms)
        if GetIsVehicleEngineRunning(vehicle) then
            SetVehicleEngineOn(vehicle, false, true, false)
            SetVehicleLights(vehicle, 0)
        end
        menu2.close()
    end,
    function(data2, menu2)
        if data2.current.name ~= nil and data2.current.applyCustoms ~= nil then
            PrepareVehicle(vehicle, data2.current.name)
            ApplyCustomsAction(vehicle, data2.current, false)
        end
    end
)
end

function OpenCustomsMenu(foundVehicles)
    -- customs options/upgrades
    local playerPed = PlayerPedId()

	ESX.UI.Menu.Open(
		"default",
		GetCurrentResourceName(),
		ESX.PlayerData.job.name .. "_customs",
		{
			title = _U('customs'),
			align = "top-left",
			elements = foundVehicles
		},
		function(data, menu)
            local vehicle = data.current.value.vehicle
            savedCustoms = ESX.Game.GetVehicleProperties(vehicle)
            savedCustoms.extras = nil
            OpenCustomsSubMenu(vehicle, nil)
        end,
        function(data, menu)
            menu.close()
        end
    )
end

function FormatOptionsCustoms(mainMenuElements, parent, data, vehicle)
    local selectedValue    = 0
    local customs = ESX.Game.GetVehicleProperties(vehicle)

    if data.modType == 14 then -- HORNS
        for j = -1, 51 do
            local _label = ''
            local _modName = _U('default_mod')
            
            if j ~= -1 then
                _modName = GetHornName(j)
            end

            if j == customs.modHorns then
                _label = _modName .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = _modName
            end
            
            table.insert(mainMenuElements, {
                label = _label,
                value = j,
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modName == 'plateIndex' then -- PLATES
        for j = -1, 4 do
            local _label = ''
            local _modName = _U('default_mod')
            
            if j ~= -1 then
                _modName = GetPlatesName(j)
            end

            if j == customs.plateIndex then
                _label = _modName .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                
                _label = _modName
            end

            table.insert(mainMenuElements, {
                label = _label,
                value = j,
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modName == 'neonColor' or data.modName == 'tyreSmokeColor' then -- NEON & SMOKE COLOR
        local neons = GetNeons()
        for j=0, #neons do
            local neonR = 0
            local neonG = 0
            local neonB = 0
            local neonLabel = _U('default_mod')
            if j ~= 0 then
                neonR = neons[j].r
                neonG = neons[j].g
                neonB = neons[j].b
                neonLabel = neons[j].label
            end

            local _label = ''
            if neonR == customs[data.modName][1] and neonG == customs[data.modName][2] and neonB == customs[data.modName][3] then
                _label = neonLabel .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = neonLabel
            end

            table.insert(mainMenuElements, {
                label = _label,
                value = {neonR, neonG, neonB},
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modName == 'windowTint' then -- WINDOWS TINT
        for j = 0, 5 do
            local _label = ''
            local _modName = GetWindowName(j)
            local customValue = customs.windowTint
            
            if customValue == -1 then
                customValue = 0
            end

            if j == customValue then
                _label = _modName .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = _modName
            end
            table.insert(mainMenuElements, {
                label = _label,
                value = j,
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modType == 11 or data.modType == 12 or data.modType == 13 or data.modType == 15 or data.modType == 16 then
        local modCount = GetNumVehicleMods(vehicle, data.modType) - 1 -- UPGRADES
        for j = -1, modCount do
            local _label = ''
            local _modName = _U('default_mod')
            
            if j ~= -1 then
                _modName = _U('level', j + 1)
            end

            if j == customs[parent] then
                _label = _modName .. ' - <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = _modName
            end
            
            table.insert(mainMenuElements, {
                label = _label,
                value = j,
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modName == 'modTurbo' then -- TURBO
        for j = 0, 1 do
            local _label = ''
            local _modName = _U('without')

            if j == 1 then
                _modName = _U('with')
            end

            if (j == 0 and not customs.modTurbo) or j == customs.modTurbo then
                _label = _modName .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = _modName
            end

            table.insert(mainMenuElements, {
                label = _label,
                value = j,
                name = parent,
                applyCustoms = true
            })
        end
    elseif data.modType == 23 then -- WHEELS RIM & TYPE
        local props = {}
        props['wheels'] = data.wheelType
        ESX.Game.SetVehicleProperties(vehicle, props)
        local modCount = GetNumVehicleMods(vehicle, 23) - 1
        local usedModNames = {}
        for j = -1, modCount do
            local _label = ''
            local _modName = nil
            
            if j ~= -1 then
                _modName = GetLabelText(GetModTextLabel(vehicle, 23, j))
            else
                _modName = _U('default_mod')
            end

            if _modName then
                if usedModNames[_modName] then
                    _modName = _modName .. ' 2'
                end
    
                usedModNames[_modName] = true

                local _label = ''
                if j == customs.modFrontWheels and data.wheelType == customs.wheels then
                    _label = _modName .. ' - <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
                else
                    _label = _modName
                end
                
                table.insert(mainMenuElements, {
                    label = _label,
                    value = j,
                    name = 'modFrontWheels',
                    wheelType = data.wheelType,
                    applyCustoms = true
                })
            end
        end
    elseif data.modName == 'color1' or data.modName == 'color2' or data.modName == 'pearlescentColor' or data.modName == 'wheelColor' then
        for i=1, #Config.Jobs.bennys.Customs.Colors, 1 do
            table.insert(mainMenuElements, {
                label = Config.Jobs.bennys.Customs.Colors[i].label,
                modName = Config.Jobs.bennys.Customs.Colors[i].value,
                name = parent
            })
        end
    elseif data.modName == 'xenonColor' then
        local colors = GetXenonColors()
        local isEnabled = customs.modXenon
        for j=1, #colors, 1 do
            local _label = ''
            if isEnabled == 0 then
                if colors[j].index == -1 then
                    _label = colors[j].label .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
                else
                    _label = colors[j].label
                end
            else
                if colors[j].index == customs[parent] then
                    _label = colors[j].label .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
                else
                    _label = colors[j].label
                end
            end

            table.insert(mainMenuElements, {
                name    = parent,
                label   = _label,
                value   = colors[j].index,
                applyCustoms = true
            })
        end
    elseif #GetColors(data.modName) > 0 then -- COLORS
        local colors = GetColors(data.modName)
        for j = 1, #colors, 1 do
            local _label = ''
            if colors[j].index == customs[parent] then
                _label = colors[j].label .. ' <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
            else
                _label = colors[j].label
            end

            table.insert(mainMenuElements, {
                name    = parent,
                label   = _label,
                value   = colors[j].index,
                applyCustoms = true
            })
        end
    else -- BODYPARTS
        local modCount = GetNumVehicleMods(vehicle, data.modType) - 1
        for j = -1, modCount do
            local _label = ''
            local _modName = nil
            
            if j ~= -1 then
                _modName = GetLabelText(GetModTextLabel(vehicle, data.modType, j))
            else
                _modName = _U('default_mod')
            end

            if _modName ~= nil then
                local _label = ''
                if j == customs[parent] then
                    _label = _modName .. ' - <span style="color:cornflowerblue;">'.. _U('installed') ..'</span>'
                else
                    _label = _modName
                end
                
                table.insert(mainMenuElements, {
                    label = _label,
                    value = j,
                    name = parent,
                    applyCustoms = true
                })
            end
        end
    end

    return mainMenuElements
end

function OpenDiagnosticCertificate(playerPed, vehicle, plate, model, diagnosticResults)
    ESX.UI.Menu.CloseAll()

	SendNUIMessage({
		openCertificate = true,
        plate = plate,
        model = model,
        diagnosticResults = json.encode(diagnosticResults)
	})

    local isOpen = true
    while isOpen do
        Citizen.Wait(0)
        ESX.ShowHelpNotification(_U('close_certificate'))
        if IsControlJustReleased(0, 177) then
            CloseDiagnosticCertificate()
            ClearPedTasks(playerPed)
            OpenDiagnosticSubMenu(vehicle, plate, model)
            isOpen = false
        end
    end
end

function CloseDiagnosticCertificate()
	SendNUIMessage({
		closeCertificate = true
	})
end

function DiagnosticFormatCustoms(vehicle, customs)
    local diagnosticCustoms = {}
    local menus = Config.Jobs.bennys.Customs.Menus

    for i,j in pairs(customs) do
        for k,v in pairs(menus) do
            if i == k and (v.modType or v.modName) then
                if v.modType == 14 then -- HORNS
					table.insert(diagnosticCustoms, {
                        label = v.label,
                        value = GetHornName(j)
                    })
				elseif v.modName == 'plateIndex' then -- PLATES
                    table.insert(diagnosticCustoms, {
                        label = v.label,
                        value = GetPlatesName(j)
                    })
				elseif v.modName == 'modTurbo' then -- TURBO AND XENON
                    local value = _U('without')
                    if j == 1 then
                        value = _U('with')
                    end
                    table.insert(diagnosticCustoms, {
                        label = v.label,
                        value = value
                    })
				elseif v.modName == 'neonColor' then
                    if customs.neonEnabled[1] then
                        local neons = GetNeons()
                        for n=1, #neons, 1 do
                            if j[1] == neons[n].r and j[2] == neons[n].g and j[3] == neons[n].b then
                                table.insert(diagnosticCustoms, {
                                    label = v.label,
                                    value = neons[n].label
                                })
                                break
                            end
                        end
                    else
                        table.insert(diagnosticCustoms, {
                            label = v.label,
                            value = _U('none')
                        })
                    end
                elseif v.modName == 'tyreSmokeColor' then -- NEON & SMOKE COLOR
					if customs.modSmokeEnabled then
                        local neons = GetNeons()
                        for n=1, #neons, 1 do
                            if j[1] == neons[n].r and j[2] == neons[n].g and j[3] == neons[n].b then
                                table.insert(diagnosticCustoms, {
                                    label = v.label,
                                    value = neons[n].label
                                })
                                break
                            end
                        end
                    else
                        table.insert(diagnosticCustoms, {
                            label = v.label,
                            value = _U('none')
                        })
                    end
				elseif v.modName == 'xenonColor' then -- XENON COLOR
					if customs.modXenon then
                        local colors = GetXenonColors()
                        for n=1, #colors, 1 do
                            if j == colors[n].index then
                                table.insert(diagnosticCustoms, {
                                    label = v.label,
                                    value = colors[n].label
                                })
                                break
                            end
                        end
                    else
                        table.insert(diagnosticCustoms, {
                            label = v.label,
                            value = _U('none')
                        })
                    end
				elseif v.modName == 'color1' or v.modName == 'color2' or v.modName == 'pearlescentColor' or v.modName == 'wheelColor' then -- RESPRAYS
					local colors = GetColors()
					for c = 1, #colors, 1 do
                        if j == colors[c].index then
                            table.insert(diagnosticCustoms, {
                                label = v.label,
                                value = colors[c].label
                            })
                            break
                        end
					end
				elseif v.modName == 'windowTint' then -- WINDOWS TINT
					for w = 0, 5, 1 do
                        if w == j then
                            table.insert(diagnosticCustoms, {
                                label = v.label,
                                value = GetWindowName(w)
                            })
                        end
					end
				elseif v.modType == 23 then -- WHEELS RIM & TYPE
					local modCount = GetNumVehicleMods(vehicle, v.modType) - 1
					for w = 0, modCount, 1 do
						local modName = GetModTextLabel(vehicle, v.modType, w)
						if modName then
							local _label = ''
							if w == j then
                                table.insert(diagnosticCustoms, {
                                    label = v.label,
                                    value = GetLabelText(modName)
                                })
                                break
							end
						end
					end
				elseif v.modType == 11 or v.modType == 12 or v.modType == 13 or v.modType == 15 or v.modType == 16 then
					local modCount = GetNumVehicleMods(vehicle, v.modType) - 1 -- UPGRADES
					for w = 0, modCount, 1 do
						local _label = ''
						if w == j then
							table.insert(diagnosticCustoms, {
                                label = v.label,
                                value = _U('level', w+1)
                            })
                            break
						end
					end
				else
					local modCount = GetNumVehicleMods(vehicle, v.modType) - 1 -- BODYPARTS
					for w = 0, modCount, 1 do
						local modName = GetModTextLabel(vehicle, v.modType, w)
						if modName then
							if w == j then
                                table.insert(diagnosticCustoms, {
                                    label = v.label,
                                    value = GetLabelText(modName)
                                })
                                break
							end
						end
					end
				end
            end
        end
    end
    
    return diagnosticCustoms
end
