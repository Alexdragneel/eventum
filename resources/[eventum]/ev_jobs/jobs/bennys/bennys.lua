Config.Jobs.bennys = {
    Society = {
        name = 'bennys',
        label = _U('bennys'),
        db_name = 'society_bennys',
        Blip = {
            Pos = {x = -205.45, y = -1316.76, z = 29.7},
            Sprite = 446,
            Color = 27,
            Scale = 1.2
        },
    },
    ActionsMenu = 'lp_jobs:bennys:openBennysActionsMenu',
    Zones = {
        CloakRooms = {
            {
                Pos = {x = -214.4, y = -1337.43, z = 29.7},
                Marker = {
                    Type = 1,
                    x = 3.0, y = 3.0, z = 1.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "cloakroom",
                    Sprite = 446,
                    Color = 27,
                    Scale = 1.2
                },
                Name = _U('be_cloakroom'),
                Type = 'cloakroom',
                Hint = _U('cloak_change'),
                InService = false
            },
        },
        Farming = {},
        Deliveries = {},
        Special = {
            {
                Pos = {x = -458.7, y = -1717.35, z = 17.65},
                Marker = {
                    Type = 1,
                    x = 6.0, y = 6.0, z = 2.0,
                    r = 171, g = 60, b = 230, a = 100,
                    Rotate = false
                },
                Blip = {
                    id = "junkyard",
                    Sprite = 527,
                    Color = 27,
                    Scale = 1.2,
                },
                Name = _U('junkyard'),
                Type = 'special',
                Event = 'lp_jobs:bennys:throwVehicleJunkyard',
                Hint = _U('be_junkyard_hint'),
                InService = true
            }
        }
    },
    Customs = {
        Colors = {
            {label = _U('black'), value = 'black'},
            {label = _U('white'), value = 'white'},
            {label = _U('grey'), value = 'grey'},
            {label = _U('red'), value = 'red'},
            {label = _U('pink'), value = 'pink'},
            {label = _U('blue'), value = 'blue'},
            {label = _U('yellow'), value = 'yellow'},
            {label = _U('green'), value = 'green'},
            {label = _U('orange'), value = 'orange'},
            {label = _U('brown'), value = 'brown'},
            {label = _U('purple'), value = 'purple'},
            {label = _U('chrome'), value = 'chrome'},
            {label = _U('gold'), value = 'gold'}
        },
        Menus = {
            upgrades = {
                label = _U('upgrades')
            },
            modEngine = {
                label = _U('engine'),
                parent = 'upgrades',
                modType = 11
            },
            modBrakes = {
                label = _U('brakes'),
                parent = 'upgrades',
                modType = 12
            },
            modTransmission = {
                label = _U('transmission'),
                parent = 'upgrades',
                modType = 13
            },
            modSuspension = {
                label = _U('suspension'),
                parent = 'upgrades',
                modType = 15
            },
            modArmor = {
                label = _U('armor'),
                parent = 'upgrades',
                modType = 16
            },
            modTurbo = {
                label = _U('turbo'),
                parent = 'upgrades',
                modName = 'modTurbo'
            },
            cosmetics = {
                label				= _U('cosmetics')
            },
            modPlateHolder = {
                label = _U('modplateholder'),
                parent = 'cosmetics',
                modType = 25,
            },
            modVanityPlate = {
                label = _U('modvanityplate'),
                parent = 'cosmetics',
                modType = 26,
            },
            modTrimA = {
                label = _U('interior'),
                parent = 'cosmetics',
                modType = 27,
            },
            modOrnaments = {
                label = _U('trim'),
                parent = 'cosmetics',
                modType = 28
            },
            modDashboard = {
                label = _U('dashboard'),
                parent = 'cosmetics',
                modType = 29
            },
            modDial = {
                label = _U('speedometer'),
                parent = 'cosmetics',
                modType = 30
            },
            modDoorSpeaker = {
                label = _U('door_speakers'),
                parent = 'cosmetics',
                modType = 31
            },
            modSeats = {
                label = _U('seats'),
                parent = 'cosmetics',
                modType = 32
            },
            modSteeringWheel = {
                label = _U('steering_wheel'),
                parent = 'cosmetics',
                modType = 33
            },
            modShifterLeavers = {
                label = _U('gear_lever'),
                parent = 'cosmetics',
                modType = 34
            },
            modAPlate = {
                label = _U('quarter_deck'),
                parent = 'cosmetics',
                modType = 35
            },
            modSpeakers = {
                label = _U('speakers'),
                parent = 'cosmetics',
                modType = 36
            },
            modTrunk = {
                label = _U('trunk'),
                parent = 'cosmetics',
                modType = 37
            },
            modHydrolic = {
                label = _U('hydraulic'),
                parent = 'cosmetics',
                modType = 38
            },
            modEngineBlock = {
                label = _U('engine_block'),
                parent = 'cosmetics',
                modType = 39
            },
            modAirFilter = {
                label = _U('air_filter'),
                parent = 'cosmetics',
                modType = 40
            },
            modStruts = {
                label = _U('struts'),
                parent = 'cosmetics',
                modType = 41
            },
            modArchCover = {
                label = _U('arch_cover'),
                parent = 'cosmetics',
                modType = 42
            },
            modAerials = {
                label = _U('aerials'),
                parent = 'cosmetics',
                modType = 43
            },
            modTrimB = {
                label = _U('wings'),
                parent = 'cosmetics',
                modType = 44
            },
            modTank = {
                label = _U('fuel_tank'),
                parent = 'cosmetics',
                modType = 45
            },
            modWindows = {
                label = _U('windows'),
                parent = 'cosmetics',
                modType = 46
            },
            modLivery = {
                label = _U('stickers'),
                parent = 'cosmetics',
                modType = 48
            },
            wheels = {
                label = _U('wheels'),
                parent = 'cosmetics'
            },
            modFrontWheelsTypes = {
                label				= _U('wheel_type'),
                parent				= 'wheels'
            },
            modFrontWheelsType0 = {
                label = _U('sport'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 0
            },
            modFrontWheelsType1 = {
                label = _U('muscle'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 1
            },
            modFrontWheelsType2 = {
                label = _U('lowrider'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 2
            },
            modFrontWheelsType3 = {
                label = _U('suv'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 3
            },
            modFrontWheelsType4 = {
                label = _U('allterrain'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 4
            },
            modFrontWheelsType5 = {
                label = _U('tuning'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 5
            },
            modFrontWheelsType6 = {
                label = _U('motorcycle'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 6
            },
            modFrontWheelsType7 = {
                label = _U('highend'),
                parent = 'modFrontWheelsTypes',
                modType = 23,
                wheelType = 7
            },
            wheelColor = {
                label = _U('wheel_color'),
                parent = 'wheels',
                modName = 'wheelColor'
            },
            plateIndex = {
                label = _U('licenseplates'),
                parent = 'cosmetics',
                modName = 'plateIndex'
            },
            resprays = {
                label = _U('respray'),
                parent = 'cosmetics'
            },
            color1 = {
                label = _U('primary'),
                parent = 'resprays',
                modName = 'color1'
            },
            color2 = {
                label = _U('secondary'),
                parent = 'resprays',
                modName = 'color2'
            },
            pearlescentColor = {
                label = _U('pearlescent'),
                parent = 'resprays',
                modName = 'pearlescentColor'
            },
            bodyparts = {
                label = _U('bodyparts'),
                parent = 'cosmetics'
            },
            modSpoilers = {
                label = _U('spoilers'),
                parent = 'bodyparts',
                modType = 0
            },
            modFrontBumper = {
                label = _U('frontbumper'),
                parent = 'bodyparts',
                modType = 1
            },
            modRearBumper = {
                label = _U('rearbumper'),
                parent = 'bodyparts',
                modType = 2
            },
            modSideSkirt = {
                label = _U('sideskirt'),
                parent = 'bodyparts',
                modType = 3
            },
            modExhaust = {
                label = _U('exhaust'),
                parent = 'bodyparts',
                modType = 4
            },
            modFrame = {
                label = _U('cage'),
                parent = 'bodyparts',
                modType = 5
            },
            modGrille = {
                label = _U('grille'),
                parent = 'bodyparts',
                modType = 6
            },
            modHood = {
                label = _U('hood'),
                parent = 'bodyparts',
                modType = 7
            },
            modFender = {
                label = _U('leftfender'),
                parent = 'bodyparts',
                modType = 8
            },
            modRightFender = {
                label = _U('rightfender'),
                parent = 'bodyparts',
                modType = 9
            },
            modRoof = {
                label = _U('roof'),
                parent = 'bodyparts',
                modType = 10
            },
            windowTint = {
                label = _U('windowtint'),
                parent = 'cosmetics',
                modName = 'windowTint'
            },
            modHorns = {
                label = _U('horns'),
                parent = 'cosmetics',
                modType = 14
            },
            neonColor = {
                label = _U('neons'),
                parent = 'cosmetics',
                modName = 'neonColor'
            },
            xenonColor = {
                label = _U('xenon'),
                parent = 'cosmetics',
                modName = 'xenonColor'
            },
            tyreSmokeColor = {
                label = _U('tiresmoke'),
                parent = 'wheels',
                modName = 'tyreSmokeColor'
            }
        }
    }
}
