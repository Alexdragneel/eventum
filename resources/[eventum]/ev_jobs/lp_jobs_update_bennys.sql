INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
	('society_bennys', "Mécanicien", 1)
;

INSERT INTO `addon_account_data` (`account_name`, `money`) VALUES
	('society_bennys', 100000)
;

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
	('society_bennys', "Mécanicien", 1)
;

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
	('society_bennys', "Mécanicien", 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
	('bennys', "Mécanicien", 1)
;

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	('bennys',0,'intern','Stagiaire',150,'{}','{}'),
	('bennys',1,'employee','Employé',300,'{}','{}'),
	('bennys',2,'manager','Manager',400,'{}','{}'),
	('bennys',3,'boss','Directeur',500,'{}','{}')
;

INSERT INTO `job_vehicles` (`job_name`,`min_job_grade`,`vehicle_model`,`vehicle_trailer`,`vehicle_livery`,`vehicle_type`,`price`) VALUES
	 ('bennys',0,'flatbed',NULL,NULL,'car','15000')
;

INSERT INTO `job_vehicles` (`job_name`,`min_job_grade`,`vehicle_model`,`vehicle_trailer`,`vehicle_livery`,`vehicle_type`,`price`) VALUES
	 ('bennys',0,'cargobob2',NULL,NULL,'helicopter','500000')
;

INSERT INTO `job_vehicles` (`job_name`,`min_job_grade`,`vehicle_model`,`vehicle_trailer`,`vehicle_livery`,`vehicle_type`,`price`) VALUES
	 ('bennys',0,'phantom','tr2',NULL,'car','45000')
;

INSERT INTO `job_vehicles` (`job_name`,`min_job_grade`,`vehicle_model`,`vehicle_trailer`,`vehicle_livery`,`vehicle_type`,`price`) VALUES
	 ('bennys',0,'phantom','trflat',NULL,'car','45000')
;
