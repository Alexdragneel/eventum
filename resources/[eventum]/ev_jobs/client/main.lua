ESX = nil

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end

		while ESX.GetPlayerData().job == nil do
			Citizen.Wait(100)
		end

		ESX.PlayerData = ESX.GetPlayerData()
		TriggerEvent('lp_jobs:refreshJobBlips')
	end
)
-- --

-- ESX Listeners --
RegisterNetEvent("esx:playerLoaded")
AddEventHandler(
	"esx:playerLoaded",
	function(xPlayer)
		ESX.PlayerData = xPlayer
	end
)

RegisterNetEvent("esx:setJob")
AddEventHandler(
	"esx:setJob",
	function(job)
		ESX.PlayerData.job = job
		TriggerEvent('lp_jobs:refreshJobBlips')
	end
)

AddEventHandler(
	"esx:onPlayerDeath",
	function(data)
		TriggerServerEvent('lp_jobs:stopZoneAction')
	end
)
-- --

-- Interiors for some jobs --
Citizen.CreateThread(function()
	-- Butcher
	RemoveIpl("CS1_02_cf_offmission")
	RequestIpl("CS1_02_cf_onmission1")
	RequestIpl("CS1_02_cf_onmission2")
	RequestIpl("CS1_02_cf_onmission3")
	RequestIpl("CS1_02_cf_onmission4")

	-- Tailor
	RequestIpl("id2_14_during_door")
	RequestIpl("id2_14_during1")
end)

RegisterNetEvent('lp_jobs:displayJobItemNotification')
AddEventHandler('lp_jobs:displayJobItemNotification', function(text)
	TriggerEvent('lp_notif:send', {
		type = 'custom',
		text = text,
		scale = 0.5,
		center = true,
		background = false,
		width = 1000,
	})
end)
-- --