-- Variables --
local inService, letSleep, isInMarker, hasAlreadyEnteredMarker = false, true, false, false
local isSwitchingService = false
local currentZone, currentSubZone, currentMarker = nil, nil, nil
local lastZone, lastSubZone, lastMarker = nil, nil, nil
local jobBlips = {}
isInShopMenu, isFarming = false, false
-- --

-- Draw blips & markers --
Citizen.CreateThread(
	function ()
		while true do
			
			Citizen.Wait(0)

			if HasLPJob() then
				local playerCoords = GetEntityCoords(PlayerPedId())
				local zones = Config.Jobs[ESX.PlayerData.job.name].Zones
				letSleep, isInMarker = true, false
				currentZone, currentSubZone, currentMarker = nil, nil, nil
				local hasExited = false

				-- Draw markers & blips
				DrawZone(zones, "CloakRooms", nil, playerCoords)
				if inService then
					DrawZone(zones, "Farming", nil, playerCoords)
					DrawZone(zones, "Deliveries", nil, playerCoords)
					DrawZone(zones, "Vault", nil, playerCoords)
					DrawZone(zones, "Special", nil, playerCoords)
					DrawZone(zones, "Strongbox", nil, playerCoords)
				end

				-- Marker logic
				if 
					isInMarker and not hasAlreadyEnteredMarker or
					(isInMarker and (lastMarker ~= currentMarker or lastZone ~= currentZone or lastSubZone ~= currentSubZone))
				then
					if
						(lastMarker ~= nil and lastZone ~= nil and lastSubZone ~= nil) and
						(lastMarker ~= currentMarker or lastZone ~= currentZone or lastSubZone ~= currentSubZone)
					then
						TriggerEvent("lp_jobs:hasExitedMarker")
						hasExited = true
					end

					hasAlreadyEnteredMarker, lastMarker, lastZone, lastSubZone = true, currentMarker, currentZone, currentSubZone
				end
				
				if not hasExited and not isInMarker and hasAlreadyEnteredMarker then
					hasAlreadyEnteredMarker = false
					TriggerEvent("lp_jobs:hasExitedMarker")
				end

				if letSleep then
					Citizen.Wait(1000)
				end
			else
				Citizen.Wait(500)
			end
		end
	end
)

AddEventHandler(
	"lp_jobs:hasExitedMarker",
	function()
		if not isInShopMenu then
			ESX.UI.Menu.CloseAll()
		end
		TriggerServerEvent('lp_jobs:stopZoneAction')
		TriggerEvent('lp_jobs:stopFarming')
	end
)

function DrawJobMarker(marker, pos)
	DrawMarker(
		marker.Type,
		pos,
		0.0,
		0.0,
		0.0,
		0.0,
		0.0,
		0.0,
		marker.x,
		marker.y,
		marker.z,
		marker.r,
		marker.g,
		marker.b,
		marker.a,
		false,
		false,
		2,
		marker.Rotate,
		nil,
		nil,
		false
	)
end

function DrawJobBlip(blip, pos, name)
	local mapBlip = AddBlipForCoord(pos.x, pos.y, pos.z)

	SetBlipSprite(mapBlip, blip.Sprite)
	SetBlipDisplay (mapBlip, 4)
	SetBlipScale(mapBlip, blip.Scale)
	SetBlipCategory(mapBlip, 3)
	SetBlipColour(mapBlip, blip.Color)
	SetBlipAsShortRange(mapBlip, true)

	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString(name)
	EndTextCommandSetBlipName(mapBlip)
	table.insert(jobBlips, mapBlip)
end

function RefreshJobBlips()
	DeleteJobBlips()
	if HasLPJob() then
		local playerCoords = GetEntityCoords(PlayerPedId())
		local zones = Config.Jobs[ESX.PlayerData.job.name].Zones
		local grade = ESX.PlayerData.job.grade
		
		for k, v in pairs(zones) do
			local zone = v
			for j, l in pairs(zone) do
				if l.Blip
					and ((l.InService and inService) or not l.InService)
					and (l.GradeMin == nil or l.GradeMin <= grade)
				then
					if l.Step ~= nil then
						l.Blip.Sprite = 502 + (l.Step - 1)  -- 502 = Blip sprite 1
						l.Blip.Scale = 0.75
					end
					DrawJobBlip(l.Blip, l.Pos, l.Name)
				end
			end
		end
	end
end

function DeleteJobBlips()
	if jobBlips[1] ~= nil then
		for i=1, #jobBlips, 1 do
			RemoveBlip(jobBlips[i])
			jobBlips[i] = nil
		end
	end
end

RegisterNetEvent("lp_jobs:refreshJobBlips")
AddEventHandler(
	"lp_jobs:refreshJobBlips",
	function()
		inService = false
		RefreshJobBlips()
	end
)

function DrawZone(zones, zoneName, subZoneName, playerCoords)
	local z = zones[zoneName]
	if subZoneName ~= nil and z ~= nil then
		z = zones[zoneName][subZoneName]
	end
	if z ~= nil then
		for k, v in ipairs(z) do
			local pos = vector3(v.Pos.x, v.Pos.y, v.Pos.z)
			local distance = #(playerCoords - pos)

			if distance < Config.DrawDistance and (v.GradeMin == nil or v.GradeMin <= ESX.PlayerData.job.grade) then
				DrawJobMarker(v.Marker, pos)
				
				letSleep = false
	
				if distance < v.Marker.x then
					isInMarker, currentZone, currentSubZone, currentMarker = true, zoneName, subZoneName, k
				end
			end
		end
	end
end
-- --

-- Key Controls --
Citizen.CreateThread(
	function()
		while true do
			Citizen.Wait(0)

			local zone = GetCurrentZone()
			if zone ~= nil and currentMarker ~= nil then
				local marker = zone[currentMarker]

				if isFarming == false then
					ESX.ShowHelpNotification(marker.Hint)

					if IsControlJustReleased(0, 38) then
						if currentZone == "CloakRooms" then
							OpenCloakRoomsMenu()
						elseif currentZone == "Farming" then
							StartFarming(currentMarker)
						elseif currentZone == "Deliveries" then
							OpenDeliveriesMenu()
						elseif currentZone == "Vault" then
							OpenVaultMenu()
						elseif currentZone == "Special" then
							TriggerEvent(marker.Event)
						elseif currentZone == "Strongbox" then
							TriggerEvent('esx_society:openBossMenu', ESX.PlayerData.job.name, function(data, menu)
								menu.close()
							end, {
								withdraw = true,
								deposit = true,
								wash = true,
								employees = false,
								grades = false
							}, true)
						end
					end
				elseif isFarming == true then
					ESX.ShowHelpNotification(marker.HintStop)

					if IsControlJustReleased(0, 38) then
						if currentZone == "Farming" then
							StopFarming(currentMarker)
						elseif currentZone == "Deliveries" then
							StopFarming(currentMarker)
						end
					end
				end

			elseif HasLPJob() and not isDead and inService then
				if IsControlJustReleased(0, 167) then
					
					if Config.Jobs[ESX.PlayerData.job.name].ActionsMenu ~= nil then
						TriggerEvent(Config.Jobs[ESX.PlayerData.job.name].ActionsMenu)
					else
						OpenActionsMenu()
					end
				end
			else
				Citizen.Wait(500)
			end
		end
	end
)
-- --

-- Helpers --
function HasLPJob()
	if ESX.PlayerData ~= nil
		and ESX.PlayerData.job ~= nil
		and ESX.PlayerData.job.name ~= 'unemployed'
		and ESX.PlayerData.job.name ~= 'police'
		and ESX.PlayerData.job.name ~= 'ambulance' then
		return true
	end

	return false
end

function GetCurrentZone()
	if HasLPJob() and currentZone ~= nil then
		local zone = Config.Jobs[ESX.PlayerData.job.name].Zones[currentZone]
				
		if currentSubZone ~= nil and zone[currentSubZone] ~= nil then
			zone = zone[currentSubZone]
		end

		return zone
	end

	return nil
end

RegisterNetEvent("lp_jobs:toggleService")
AddEventHandler("lp_jobs:toggleService", function()
	if HasLPJob() then
		OpenCloakRoomsMenu()
	else
		ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
			if isInService then
				inService = false
				ESX.ShowNotification(_U('set_out_service'))
				TriggerServerEvent('esx_service:disableService', ESX.PlayerData.job.name)
				RefreshJobBlips()
			else
				ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
					if not canTakeService then
						ESX.ShowNotification(_U('can_not_take_service', inServiceCount, maxInService))
					else
						inService = true
						ESX.ShowNotification(_U('set_in_service'))
						RefreshJobBlips()
					end
					isSwitchingService = false
				end, ESX.PlayerData.job.name)
			end
		end, ESX.PlayerData.job.name)
	end
end)

-- -- 

-- Menus --
function OpenCloakRoomsMenu()
	if not isSwitchingService then
		isSwitchingService = true
		ESX.TriggerServerCallback( "esx_skin:getPlayerSkin", function(skin, jobSkin)
			isFarming = false
			if not inService then
				startAnimAction('switch@franklin@getting_ready', '002334_02_fras_v2_11_getting_dressed_exit')
				Citizen.Wait(1000)
				ClearPedTasks(PlayerPedId())
				if skin.sex == 0 then
					TriggerEvent("skinchanger:loadClothes", skin, jobSkin.skin_male)
				else
					TriggerEvent("skinchanger:loadClothes", skin, jobSkin.skin_female)
				end
				ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
					if not isInService then
						ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
							if not canTakeService then
								ESX.ShowNotification(_U('can_not_take_service', inServiceCount, maxInService))
							else
								inService = true
								ESX.ShowNotification(_U('set_in_service'))
								RefreshJobBlips()
							end
							isSwitchingService = false
						end, ESX.PlayerData.job.name)
					end
				end, ESX.PlayerData.job.name)
			else
				startAnimAction('switch@franklin@getting_ready', '002334_02_fras_v2_11_getting_dressed_exit')
				Citizen.Wait(1000)
				ClearPedTasks(PlayerPedId())
				TriggerEvent("skinchanger:loadSkin", skin)
				ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
					if isInService then
						inService = false
						ESX.ShowNotification(_U('set_out_service'))
						TriggerServerEvent('esx_service:disableService', ESX.PlayerData.job.name)
						RefreshJobBlips()
					end
					isSwitchingService = false
				end, ESX.PlayerData.job.name)
			end
		end)
	end
end

function OpenDeliveriesMenu()
	local zone = GetCurrentZone()
	if currentZone == "Deliveries" and zone ~= nil and currentMarker ~= nil then
		ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
			local currentWeight = 0

			for i=1, #items, 1 do
				local item = items[i]
	
				if item.count > 0 then
					currentWeight = currentWeight + (item.weight * item.count)
				end
			end

			ESX.UI.Menu.CloseAll()
			ESX.TriggerServerCallback('esx:getAllItems', function(dbItems)
				local deliverableItems = {}
				local marker = zone[currentMarker]
				local markerItems = marker.Item
				local anItemKey = nil
				for itemKey, item in ipairs(markerItems) do
					table.insert(deliverableItems, { label = dbItems[item.requires].label, value = itemKey })
					anItemKey = itemKey
				end
		
				if #deliverableItems > 1 then
					ESX.UI.Menu.Open("default", GetCurrentResourceName(), "deliveries", {
						title = _U("deliveries"),
						align = "top-left",
						elements = deliverableItems
					},
					function(data, menu)
						deliverItem(data.current.value, items, dbItems, marker)
						menu.close()
					end, function(data, menu)
						menu.close()
					end)
				elseif #deliverableItems == 1 then
					deliverItem(anItemKey, items, dbItems, marker)
				end
			end)
		end)
	end
end

function deliverItem(itemToDeliver, items, dbItems, marker)
	local zone = GetCurrentZone()
	local dbname = marker.Item[itemToDeliver].requires
	local itemLabel = dbItems[dbname].label
	local ownedQuantity = 0

	for i=1, #items, 1 do
		local item = items[i]
		if item.name == dbname then
			ownedQuantity = item.count
			break
		end
	end

	if ownedQuantity > 0 then
		if ownedQuantity > 0 then
			Citizen.Wait(1000)

			TriggerServerEvent('lp_jobs:startZoneAction', "Deliveries", "", currentMarker, ownedQuantity, itemToDeliver)
			StartFarming(currentMarker)
		else
			ESX.ShowNotification(_U('not_enough', itemLabel))
		end
	else
		ESX.ShowNotification(_U('not_enough', itemLabel))
	end
end

function OpenActionsMenu()
	local elements = {}

	if ESX.PlayerData.job.grade_name ~= 'temp' then
		table.insert(elements, {label = _U("billing"), value = "billing_menu"})
	end

	if ESX.PlayerData.job.grade_name == "boss" then
		table.insert(elements, {label = _U("boss_menu"), value = "boss_menu"})
	end

	ESX.UI.Menu.CloseAll()

	if #elements > 0 then
		ESX.UI.Menu.Open(
			"default",
			GetCurrentResourceName(),
			ESX.PlayerData.job.name .. "_actions",
			{
				title = ESX.PlayerData.job.label,
				align = "top-left",
				elements = elements
			},
			function(data, menu)
				if data.current.value == "billing_menu" then
					TriggerEvent('lp_core:openBillingMenu')
				elseif data.current.value == "boss_menu" then
					TriggerEvent(
						"esx_society:openBossMenu",
						ESX.PlayerData.job.name,
						function(data, menu)
							menu.close()
						end,
						{wash = false}
					)
				end
			end,
			function(data, menu)
				menu.close()
			end
		)
	end
end

function OpenVaultMenu()
	ESX.UI.Menu.CloseAll()

	local elements = {
		{label = _U('get_object'), value = 'get_object'},
		{label = _U('put_object'), value = 'put_object'}
	}

	if ESX.PlayerData.job.grade_name == "boss" then
		table.insert(elements, {label = _U('get_weapon'), value = 'get_weapon'})
		table.insert(elements, {label = _U('put_weapon'), value = 'put_weapon'})
	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vault',
		{
			title    = _U('vault'),
			align    = 'top-left',
			elements = elements,
		},
		function(data, menu)

			if data.current.value == 'put_object' then
				OpenPutStocksMenu()
			end

			if data.current.value == 'get_object' then
				OpenGetStocksMenu()
			end

			if data.current.value == 'get_weapon' then
				OpenGetWeaponMenu()
			end
	
			if data.current.value == 'put_weapon' then
				OpenPutWeaponMenu()
			end

		end,
		
		function(data, menu)
			menu.close()
		end
	)
end

function OpenPutStocksMenu()
	ESX.TriggerServerCallback('lp_core:getPlayerInventory', function(items, dbItems)
		local elements = {}
		local currentWeight = 0

		for i=1, #items, 1 do
			local item = items[i]

			if item.count > 0 then
				local dbItem = dbItems[item.name]
				if dbItem ~= nil then
					currentWeight = currentWeight + (item.weight * item.count)
					local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(item.label, ESX.Math.GroupDigits(item.count))
					if item.useCount > 0 and not item.stackable and dbItem.actions ~= nil and dbItem.actions.useCount ~= nil and dbItem.actions.useCount > 0 then
						label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s %s</span>'):format(item.label, item.useCount, dbItem.actions.countUnit)
					end
					table.insert(elements, {label = label, type = 'item_standard', value = item.name, weight = item.weight })
				end
			end
		end
		
		ESX.TriggerServerCallback('lp_core:getPlayerMaxWeight', function(maxWeight)
			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'stocks_menu',
				{
					title    = _U('inventory', currentWeight, maxWeight),
					align    = 'top-left',
					elements = elements
				},
				function(data, menu)

					local itemName = data.current.value
					
					if dbItems[itemName] ~= nil and dbItems[itemName].stackable then
						local weight = data.current.weight
						local society = 'society_' .. ESX.PlayerData.job.name

						ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
						{
							title = _U('quantity')
						},
						function(data2, menu2)
							local count = tonumber(data2.value)
							if count == nil then
								ESX.ShowNotification(_U('invalid_quantity'))
							else
								menu2.close()
								menu.close()

								ESX.TriggerServerCallback('lp_jobs:putStockItems', function()
									OpenPutStocksMenu()
								end, itemName, weight, count, society)
							end
						end,
						function(data2, menu2)
							menu2.close()
						end)
					else
						ESX.ShowNotification(_U('non_stackable_not_allowed'))
					end
				end,
				function(data, menu)
					menu.close()
				end
			)
		end)
	end)
end

function OpenGetStocksMenu()
	local society = 'society_' .. ESX.PlayerData.job.name
		
	ESX.TriggerServerCallback('lp_jobs:getStockInventory', function(items, dbItems)
		local elements = {}
		local totalVaultWeight = 0

		for i=1, #items, 1 do
			local item = items[i]

			if item.count > 0 then
				local dbItem = dbItems[item.name]
				if dbItem ~= nil then
					totalVaultWeight = totalVaultWeight + (item.weight * item.count)
					local label = ('<span><img src="img/inventory/' .. dbItem.icon .. '" style="width:34px; height:34px; vertical-align:middle; padding-right:10px;" /> %s</span> <span>x%s</span>'):format(item.label, ESX.Math.GroupDigits(item.count))
					table.insert(elements, {label = label, value = item.name})
				end
			end
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'stocks_menu',
			{
				title    = _U('vault_content', totalVaultWeight, Config.VaultMaxWeight),
				elements = elements
			},
			function(data, menu)
				local itemName = data.current.value

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
					{
						title = _U('quantity')
					},
					function(data2, menu2)

						local count = tonumber(data2.value)

						if count == nil then
							ESX.ShowNotification(_U('invalid_quantity'))
						else
							menu2.close()
							menu.close()

							ESX.TriggerServerCallback('lp_jobs:getStockItems', function()
								OpenGetStocksMenu()
							end, itemName, count, society)
						end

					end,
					function(data2, menu2)
						menu2.close()
					end
				)
			end,
			function(data, menu)
				menu.close()
			end
		)
	end, society)
end

function OpenPutWeaponMenu()
	local elements   = {}
	local playerPed  = GetPlayerPed(-1)
	local weaponList = ESX.GetWeaponList()
	local society = 'society_' .. ESX.PlayerData.job.name

	for i=1, #weaponList, 1 do
		local weaponHash = GetHashKey(weaponList[i].name)

		if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
			local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
			table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
		end
	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vault_put_weapon',
		{
			title    = _U('put_weapon_menu'),
			align    = 'top-left',
			elements = elements,
		},
		function(data, menu)
			menu.close()

			ESX.TriggerServerCallback('lp_jobs:addVaultWeapon', function()
				OpenPutWeaponMenu()
			end, data.current.value, society)
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenGetWeaponMenu()
	local society = 'society_' .. ESX.PlayerData.job.name

	ESX.TriggerServerCallback('lp_jobs:getVaultWeapons', function(weapons)
		local elements = {}

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
			end
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'vault_get_weapon',
			{
				title    = _U('get_weapon_menu'),
				align    = 'top-left',
				elements = elements,
			},
			function(data, menu)
				menu.close()

				ESX.TriggerServerCallback('lp_jobs:removeVaultWeapon', function()
					OpenGetWeaponMenu()
				end, data.current.value, society)
			end,
			function(data, menu)
				menu.close()
			end
		)
	end, society)
end
-- --

-- Farming logic --
function StartFarming(marker)
	TriggerServerEvent('lp_jobs:startZoneAction', "Farming", "", marker, nil, nil)
	isFarming = true
end

function StopFarming(marker)
	TriggerServerEvent('lp_jobs:stopZoneAction')
	isFarming = false
end

RegisterNetEvent("lp_jobs:stopFarming")
AddEventHandler("lp_jobs:stopFarming", function()
	isFarming = false
end)

RegisterNetEvent("lp_jobs:startJob")
AddEventHandler("lp_jobs:startJob", function(text, duration, cbServerEvent)
    if IsPedInAnyVehicle(PlayerPedId(), false) then
        ESX.ShowNotification(_U('in_vehicle'))
		TriggerServerEvent('lp_jobs:stopJob')
		isFarming = false
    else
        TriggerEvent('rprogress:start_lpjobs', text, duration, cbServerEvent)
    end
end)

-- --

-- Divers --

function startAnimAction(lib, anim)
	ESX.Streaming.RequestAnimDict(lib, function()
		TaskPlayAnim(PlayerPedId(), lib, anim, 8.0, 1.0, -1, 49, 0, false, false, false)
		RemoveAnimDict(lib)
	end)
end

-- --
