INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_taxi', 'Taxi', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_taxi', 'Taxi', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_taxi', 'Taxi', 1),
	('taxi_food', 'Taxi', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('taxi', 'Taxi')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('taxi',0,'stagiaire','Stagiaire',200,'{"sex":0,"torso_1":32,"tshirt_1":31,"decals_2":0,"pants_2":0,"arms":27,"arms_2":0,"shoes_2":0,"bproof_1":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":10,"pants_1":24}',   '{"sex":1,"torso_1":58,"tshirt_1":38,"decals_2":0,"pants_2":0,"arms":23,"arms_2":0,"shoes_2":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":29,"pants_1":37}'),
	('taxi',1,'driver','Chauffeur',300,   '{"sex":0,"torso_1":32,"tshirt_1":31,"decals_2":0,"pants_2":0,"arms":27,"arms_2":0,"shoes_2":0,"bproof_1":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":10,"pants_1":24}',   '{"sex":1,"torso_1":58,"tshirt_1":38,"decals_2":0,"pants_2":0,"arms":23,"arms_2":0,"shoes_2":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":29,"pants_1":37}'),
	('taxi',2,'manager','Manager',400,    '{"sex":0,"torso_1":32,"tshirt_1":31,"decals_2":0,"pants_2":0,"arms":27,"arms_2":0,"shoes_2":0,"bproof_1":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":10,"pants_1":24}',   '{"sex":1,"torso_1":58,"tshirt_1":38,"decals_2":0,"pants_2":0,"arms":23,"arms_2":0,"shoes_2":0,"decals_1":0,"tshirt_2":0,"torso_2":0,"shoes_1":29,"pants_1":37}'),
	('taxi',3,'boss','Patron',600,        '{"sex":0,"torso_1":29,"tshirt_1":31,"decals_2":0,"pants_2":4,"arms":1 ,"arms_2":0,"shoes_2":0,"bproof_1":0,"decals_1":0,"tshirt_2":0,"torso_2":4,"shoes_1":10,"pants_1":24}',   '{"sex":1,"torso_1":58,"tshirt_1":38,"decals_2":0,"pants_2":4,"arms":1,"arms_2":0,"shoes_2":0,"decals_1":0,"tshirt_2":0,"torso_2":4,"shoes_1":29,"pants_1":37}')
;

INSERT INTO `lp_garages`(`name`,`spawnerCoordsX`,`spawnerCoordsY`,`spawnerCoordsZ`,`spawnpointCoordsX`,`spawnpointCoordsY`,`spawnpointCoordsZ`,`spawnpointCoordsHeading`,`type`,`hidden`,`active`,`category`) VALUES 
	('Garage Taxi',891.67,-153.46,76.89,896.86,-152.94,76.56,327,'taxi',0,1,'car')
;


INSERT INTO `job_vehicles`(`job_name`,`min_job_grade`,`vehicle_model`,`vehicle_trailer`,`vehicle_livery`,`vehicle_type`,`price`) VALUES
	('taxi',0,'taxi',NULL,NULL,'car','20000'),
	('taxi',0,'tourbus',NULL,NULL,'car','60000'),
	('taxi',0,'stretch',NULL,NULL,'car','800000')
;

INSERT INTO `lp_storages` (`name`, `label`, `coordsX`, `coordsY`, `coordsZ`, `type`, `category`, `active`) VALUES
	('society_taxi', 'Coffre de l\'entreprise', '908.94', '-153.3', '73.17', 'taxi', 'all', '1'),
	('taxi_food', 'Réfrigérateur Taxi', '900.23', '-165.97', '73.17', 'taxi', 'food', '1')
;