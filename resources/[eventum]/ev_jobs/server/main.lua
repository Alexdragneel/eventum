-- Variables --
local playersWorking, playersToPay = {}, {}
local diagnosticResults = {}
local lastTaxiPlayerSuccess = {}
ESX = nil
-- --

-- Init ESX stuff --
TriggerEvent(
	"esx:getSharedObject",
	function(obj)
		ESX = obj
	end
)
-- --

-- Register all societies --
for k,v in pairs(Config.Jobs) do
    local society = v.Society
    TriggerEvent('esx_society:registerSociety', society.name, society.label, society.db_name, society.db_name, society.db_name, {type = 'private'})
    TriggerEvent('esx_service:activateService', society.name, 100)
end
-- --

-- Work logic --
Citizen.CreateThread(function()
	while true do
        Citizen.Wait(10)
        for playerId, data in pairs(playersWorking) do
            local xPlayer = ESX.GetPlayerFromId(playerId)
            local playerOwnedQuantity = 0

            -- is player still online?
            if xPlayer then
                --local distance = #(xPlayer.getCoords(true) - data.zoneCoords)
                local currentJobItems = data.jobItems

                -- player still within zone limits?
                --if distance <= data.zoneMaxDistance then --Le check se fait déjà côté client et peut donc afficher le message mais ne pas fonctionner car calcul de distance different
                    local checkIdx = data.itemToDeliver and data.itemToDeliver or 1
                    CanPlayerCompleteAction(xPlayer, data, playerId, checkIdx)
                    
                    if playersWorking[playerId] ~= nil then
                        if not data.isBusy then
                            playersWorking[playerId]['isBusy'] = true
                            -- start progress bar
                            -- when it completes it will call the rest of the job
                            TriggerClientEvent('lp_jobs:startJob', xPlayer.source, data.markerName, currentJobItems[data.itemToDeliver and data.itemToDeliver or 1].duration * 1000, "lp_jobs:processAction")
                        end
                        
                        if data.processAction and data.isBusy then
                            RemoveItemIfNecessary(xPlayer, data, checkIdx)

                            for k,v in ipairs(currentJobItems) do
                                if data.zoneName == "Farming" then
                                    ItemFarmed(v, xPlayer)
                                elseif data.zoneName == "Deliveries" and data.itemToDeliver == k then
                                    ItemDelivered(v, playerId, data)
                                elseif data.zoneName == "Crafting" then
                                    -- TODOWIP
                                end
                            end
    
                            if playersWorking[playerId] ~= nil then
                                playersWorking[playerId]['isBusy'] = false
                                playersWorking[playerId]['processAction'] = false
                            end
                        end
                    end
                --else
                --    playersWorking[playerId] = nil
                --    TriggerClientEvent('rprogress:stop', xPlayer.source)
                --    TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
                --end
            else
                playersWorking[playerId] = nil
            end
        end
	end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(500)
        for playerId, paycheck in pairs(playersToPay) do
            if playersWorking[playerId] == nil then
                local xPlayer = ESX.GetPlayerFromId(playerId)
                -- only pay if player is online
                if xPlayer then
                    GetPaidForDelivery(xPlayer, paycheck)
                    playersToPay[playerId] = nil
                end
            end
        end
    end
end)

ESX.RegisterCommand('toggleService', 'admin', function(xPlayer, args, showError)
    TriggerClientEvent("lp_jobs:toggleService", xPlayer.source)
end, true, {
    help = 'Pour commencer/terminer son service',
    validate = true,
    arguments = {}
})

function CanPlayerCompleteAction(xPlayer, data, playerId, checkIdx)
    if playersWorking[playerId] ~= nil then
        local currentJobItems = data.jobItems
        local currentJobItem = currentJobItems[checkIdx]
        local playerInventory = xPlayer.inventory
        local currentWeight = 0
        for i=1, #playerInventory, 1 do
            local item = playerInventory[i]
    
            if item.count > 0 then
                currentWeight = currentWeight + (item.weight * item.count)
            end
        end
    
        if currentJobItem.requires ~= nil then
            -- deliveries and some farming jobs
            local playerItemInventory = xPlayer.getInventoryItem(currentJobItem.requires)
            if playerItemInventory and playerItemInventory.count >= currentJobItem.remove then
                if currentJobItem.db_name ~= nil then
                    -- farming jobs only
                    local weightToRemove = playerItemInventory.weight * currentJobItem.remove
                    local weightToAdd = ESX.GetItemWeight(currentJobItem.db_name) * currentJobItem.add

                    if currentWeight - weightToRemove + weightToAdd > xPlayer.maxWeight then
                        xPlayer.showNotification(_U('inventory_full'))
                        playersWorking[playerId] = nil
                        TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
                    end
                end
            else
                local itemLabel = ESX.GetItemLabel(currentJobItems[checkIdx].requires)
                xPlayer.showNotification(_U('not_enough', itemLabel))
                playersWorking[playerId] = nil
                TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
            end
        else
            -- some farming jobs
            local weightToAdd = ESX.GetItemWeight(currentJobItem.db_name) * currentJobItem.add
             if currentWeight + weightToAdd > xPlayer.maxWeight then
                    xPlayer.showNotification(_U('inventory_full'))
                    playersWorking[playerId] = nil
                    TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
                end
        end

        if playersWorking[playerId] == nil then
            TriggerClientEvent('rprogress:stop', xPlayer.source)
            TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
        end
    end
end

function RemoveItemIfNecessary(xPlayer, data, checkIdx)
    local currentJobItems = data.jobItems

    if currentJobItems[checkIdx].requires ~= nil then
        xPlayer.removeInventoryItem(currentJobItems[checkIdx].requires, currentJobItems[checkIdx].remove, true)
        local label = string.format('-%d %s', currentJobItems[checkIdx].remove, ESX.GetItemLabel(currentJobItems[checkIdx].requires))
        xPlayer.triggerEvent('lp_jobs:displayJobItemNotification', label)
    end
end

function ItemFarmed(currentJobItem, xPlayer)
    -- chances to drop the item
    local chanceToDrop = math.random(100)

    if currentJobItem.drop == 100 or chanceToDrop <= currentJobItem.drop then
        xPlayer.addInventoryItem(currentJobItem.db_name, currentJobItem.add, false, true)
        local label = string.format('+%d %s', currentJobItem.add, ESX.GetItemLabel(currentJobItem.db_name))
        xPlayer.triggerEvent('lp_jobs:displayJobItemNotification', label)
    end
end

function ItemDelivered(currentJobItem, playerId)
    local xPlayer = ESX.GetPlayerFromId(playerId)
    local item = xPlayer.getInventoryItem(currentJobItem.requires)
    
    if item then
        playersWorking[playerId]['delivered'] = playersWorking[playerId]['delivered'] + 1
        playersToPay[playerId] = playersToPay[playerId] + item.farmPrice

        if item.count == 0 then
            playersWorking[playerId] = nil
            TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
        end
    end
end

function GetPaidForDelivery(xPlayer, amount)
    if amount > 0 then
        if xPlayer.job.grade_name == 'temp' then
            xPlayer.addMoney(amount)
            TriggerClientEvent('esx:showAdvancedNotification', xPlayer.source, xPlayer.job.label, _U('delivery_done'), _U('got_paid2', amount), 'CHAR_DEFAULT', 1)
        else
            TriggerEvent('esx_society:getSociety', xPlayer.job.name, function (society)
                if society ~= nil then
                    TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
                        xPlayer.showNotification(_U('society_got_paid', ESX.Math.GroupDigits(amount)))
                        account.addMoney(amount)
                    end)
                end
            end)
        end
    end
end

RegisterServerEvent('lp_jobs:processAction')
AddEventHandler('lp_jobs:processAction', function()
    if playersWorking[source] then
        playersWorking[source]['processAction'] = true
    end
end)

RegisterServerEvent('lp_jobs:stopJob')
AddEventHandler('lp_jobs:stopJob', function()
    if playersWorking[source] then
        playersWorking[source] = nil
    end
end)

RegisterServerEvent('lp_jobs:startZoneAction')
AddEventHandler('lp_jobs:startZoneAction', function(zoneName, subZoneName, marker, quantity, itemToDeliver)
    if not playersWorking[source] then
		local xPlayer = ESX.GetPlayerFromId(source)

        if xPlayer then
			local jobObject = Config.Jobs[xPlayer.job.name]

			if jobObject then
				local jobZone = jobObject.Zones[zoneName]

                if jobZone then
                    if subZoneName ~= "" then
                        jobZone = jobZone[subZoneName]
                    end

                    local jobMarker = jobZone[marker]

                    if jobMarker and jobMarker.Item then
                        playersWorking[source] = {
                            jobItems = jobMarker.Item,
                            zoneName = zoneName,
                            markerName = jobMarker.Name,
                            zoneCoords = vector3(jobMarker.Pos.x, jobMarker.Pos.y, jobMarker.Pos.z),
                            zoneMaxDistance = jobMarker.Marker.x,
                            processAction = false,
                            isBusy = false
                        }

                        if zoneName == "Deliveries" and quantity then
                            playersWorking[source]['quantity'] = quantity
                        end

                        if zoneName == "Deliveries" and itemToDeliver then
                            playersWorking[source]['itemToDeliver'] = itemToDeliver
                            playersWorking[source]['delivered'] = 0
                            playersToPay[source] = 0
                        end
                    end
                end
			end
		end
    end
end)

RegisterServerEvent('lp_jobs:stopZoneAction')
AddEventHandler('lp_jobs:stopZoneAction', function()
    local xPlayer = ESX.GetPlayerFromId(source)
    if playersWorking[source] then
        playersWorking[source] = nil
        if xPlayer then
            TriggerClientEvent('rprogress:stop', xPlayer.source)
            TriggerClientEvent("lp_jobs:stopFarming", xPlayer.source)
        end
    end
end)
-- --

-- Vault logic --
ESX.RegisterServerCallback('lp_jobs:getStockItems', function(source, cb, itemName, count, society)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
      local item = inventory.getItem(itemName)
  
      if item.count >= count then
          if xPlayer.canCarryItem(itemName, count) then
              inventory.removeItem(itemName, count)
              xPlayer.addInventoryItem(itemName, count, false, true)
              xPlayer.showNotification(_U('have_withdrawn', count, item.label))
          else
              xPlayer.showNotification(_U('inventory_full'))
          end
      else
          xPlayer.showNotification(_U('invalid_quantity'))
      end
    end)

    cb()
end)

function VaultCanCarryItem(vaultInventory, itemWeight, count)
    local maxVaultWeight = Config.VaultMaxWeight
    local currentVaultWeight = 0

    for k,v in ipairs(vaultInventory) do
        if v ~= nil then
            currentVaultWeight = currentVaultWeight + (v.weight * v.count)
        end
    end

    local totalItemWeight = itemWeight * count
    local futureVaultWeight = currentVaultWeight + totalItemWeight

    return futureVaultWeight <= maxVaultWeight
end

ESX.RegisterServerCallback('lp_jobs:putStockItems', function(source, cb, itemName, weight, count, society)
    local xPlayer = ESX.GetPlayerFromId(source)

    TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
        local inventoryWithWeight = {}
        local allItems = ESX.GetItems()
        for k,v in ipairs(inventory.items) do
            if v ~= nil and v.name ~= nil then
                local item = v
                item.weight = allItems[v.name].weight
                table.insert(inventoryWithWeight, item)
            end
        end
        
	    local sourceItem = xPlayer.getInventoryItem(itemName)
        if sourceItem and sourceItem.count >= count and count >= 0 then
            if VaultCanCarryItem(inventoryWithWeight, weight, count) then
                xPlayer.removeInventoryItem(itemName, count)
                inventory.addItem(itemName, count)
                xPlayer.showNotification(_U('have_deposited', count, sourceItem.label))
            else
                xPlayer.showNotification(_U('vault_full'))
            end
        else
            xPlayer.showNotification(_U('invalid_quantity'))
        end

        cb()
    end)
end)

ESX.RegisterServerCallback('lp_jobs:getStockInventory', function(source, cb, society)
    TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
        local inventoryWithWeight = {}
        local allItems = ESX.GetItems()
        for k,v in ipairs(inventory.items) do
            if v ~= nil and v.name ~= nil then
                local item = v
                item.weight = allItems[v.name].weight
                table.insert(inventoryWithWeight, item)
            end
        end

        cb(inventoryWithWeight, allItems)
    end)
end)

ESX.RegisterServerCallback('lp_jobs:getVaultWeapons', function(source, cb, society)
    TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)
      local weapons = store.get('weapons')
  
      if weapons == nil then
        weapons = {}
      end
  
      cb(weapons)
    end)
end)
  
ESX.RegisterServerCallback('lp_jobs:addVaultWeapon', function(source, cb, weaponName, society)
    local xPlayer = ESX.GetPlayerFromId(source)

    xPlayer.removeWeapon(weaponName)

    TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)
        local weapons = store.get('weapons')

        if weapons == nil then
            weapons = {}
        end

        local foundWeapon = false

        for i=1, #weapons, 1 do
            if weapons[i].name == weaponName then
                weapons[i].count = weapons[i].count + 1
                foundWeapon = true
            end
        end

        if not foundWeapon then
            table.insert(weapons, {
                name  = weaponName,
                count = 1
            })
        end

        store.set('weapons', weapons)

        cb()
    end)
end)
  
ESX.RegisterServerCallback('lp_jobs:removeVaultWeapon', function(source, cb, weaponName, society)
    local xPlayer = ESX.GetPlayerFromId(source)

    xPlayer.addWeapon(weaponName, 1000)

    TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)
        local weapons = store.get('weapons')

        if weapons == nil then
        weapons = {}
        end

        local foundWeapon = false

        for i=1, #weapons, 1 do
        if weapons[i].name == weaponName then
            weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
            foundWeapon = true
        end
        end

        if not foundWeapon then
        table.insert(weapons, {
            name  = weaponName,
            count = 0
        })
        end

        store.set('weapons', weapons)

        cb()
    end)
end)
-- --

-- Benny's logic --
RegisterServerEvent('lp_jobs:bennys:saveDiagnosticResults')
AddEventHandler('lp_jobs:bennys:saveDiagnosticResults', function(plate, _diagnosticResults)
    diagnosticResults[plate] = _diagnosticResults
end)

ESX.RegisterServerCallback('lp_jobs:bennys:getDiagnosticResults', function(source, cb, plate)
    if diagnosticResults[plate] == nil then
        diagnosticResults[plate] = {}
    end

    cb(diagnosticResults[plate])
end)

ESX.RegisterServerCallback('lp_jobs:bennys:putVehicleJunkyard', function(source, cb, plate)
    local xPlayer = ESX.GetPlayerFromId(source)
    
    MySQL.Async.execute(
        "UPDATE owned_vehicles SET `garage` = -1 WHERE `plate` = @plate",
        {
            ["@plate"] = plate,
        },
        function(rowsChanged)
            cb(true)
        end
    )
end)

TriggerEvent('esx_phone:registerNumber', 'bennys', _U('contact_bennys'), true, true)
-- --

-- Weazel News logic

RegisterServerEvent('lp_jobs:journaliste:advert')
AddEventHandler('lp_jobs:journaliste:advert', function(ad)
	TriggerClientEvent('esx:showAdvancedNotification', -1, _U('weazel_news'), _U('news'), ad, 'CHAR_LIFEINVADER', 1)
end)

TriggerEvent('esx_phone:registerNumber', 'journaliste', _U('contact_weazel'), true, true)
-- --

-- Taxi logic --

TriggerEvent('esx_society:registerSociety', 'taxi', 'Taxi', 'society_taxi', 'society_taxi', 'society_taxi', {type = 'public'})

RegisterNetEvent('lp_jobs:taxi:success')
AddEventHandler('lp_jobs:taxi:success', function(taxiDistance)
	local xPlayer = ESX.GetPlayerFromId(source)
	local timeNow = os.clock()

	if xPlayer.job.name == 'taxi' then
		if not lastTaxiPlayerSuccess[source] or timeNow - lastTaxiPlayerSuccess[source] > 5 then
			lastTaxiPlayerSuccess[source] = timeNow

			math.randomseed(os.time())
			local total = math.floor(taxiDistance / 10)
            local tip = Config.Jobs.taxi.Tips[math.random(1, #Config.Jobs.taxi.Tips)]

			TriggerEvent('esx_addonaccount:getSharedAccount', 'society_taxi', function(account)
				if account then
                    account.addMoney(total)
				end

                if tip == 0 then
                    TriggerClientEvent('esx:showAdvancedNotification', source, xPlayer.job.label, _U('mission_complete'), _U('taxi_done_notip', total), 'CHAR_TAXI', 1)
                else
                    xPlayer.addAccountMoney('money', tip)
                    TriggerClientEvent('esx:showAdvancedNotification', source, xPlayer.job.label, _U('mission_complete'), _U('taxi_done_tip', total, tip), 'CHAR_TAXI', 1)
                end
			end)
		end
	end
end)

TriggerEvent('esx_phone:registerNumber', 'taxi', _U('taxi_client'), true, true)

-- --

-- Food Delivery logic

TriggerEvent('esx_society:registerSociety', 'food_delivery', 'food_delivery', 'society_food_delivery', 'society_food_delivery', 'society_food_delivery', {type = 'private'})

RegisterNetEvent('lp_jobs:food_delivery:success')
AddEventHandler('lp_jobs:food_delivery:success', function(deliveryDistance, bonusPercentage)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'food_delivery' then

        local total = math.floor(deliveryDistance / 15)
        local bonus = math.floor(total * bonusPercentage)
        local tip = Config.Jobs.food_delivery.Tips[math.random(1, #Config.Jobs.food_delivery.Tips)]

        xPlayer.addAccountMoney('money', total + bonus + tip)
        
        if tip == 0 then
            TriggerClientEvent('esx:showAdvancedNotification', source, xPlayer.job.label, _U('reward'), _U('delivery_done_notip', total, bonus), 'CHAR_DEFAULT', 1)
        else
            TriggerClientEvent('esx:showAdvancedNotification', source, xPlayer.job.label, _U('reward'), _U('delivery_done_tip', total, bonus, tip), 'CHAR_DEFAULT', 1)
        end
    end
end)	

-- --
