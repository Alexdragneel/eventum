INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
    ('society_food_delivery', "Bouff'Eat", '1')
;

INSERT INTO `addon_account_data` (`account_name`, `money`) VALUES
    ('society_food_delivery', '100000')
;

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
    ('society_food_delivery', "Bouff'Eat", '1')
;

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
    ('society_food_delivery', "Bouff'Eat", '1')
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
    ('food_delivery', "Bouff'Eat", '0')
; 

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
    ('food_delivery', '0', 'temp', 'Livreur', '0', '{}', '{}')
;

INSERT INTO `job_vehicles` (`job_name`, `min_job_grade`, `vehicle_model`, `vehicle_type`, `price`) VALUES
    ('food_delivery', '0', 'faggio4', 'car', '1000')
;

INSERT INTO `lp_garages` (`name`, `spawnerCoordsX`, `spawnerCoordsY`, `spawnerCoordsZ`, `spawnpointCoordsX`, `spawnpointCoordsY`, `spawnpointCoordsZ`, `spawnpointCoordsHeading`, `type`) VALUES
    ("Garage Bouff'Eat", '-1353.03', '-888.68', '13.88', '-1353.92', '-890.34', '13.88', '39.55', 'food_delivery')
;