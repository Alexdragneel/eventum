local currentTattoos, cam, CurrentActionData = {}, -1, {}
local HasAlreadyEnteredMarker, CurrentAction, CurrentActionMsg
local isCamFixed = false
ESX = nil
local playerHasBeenUndressed = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

AddEventHandler('skinchanger:modelLoaded', function()
	ESX.TriggerServerCallback('lp_tattooshops:requestPlayerTattoos', function(tattooList)
		if tattooList then
			for k,v in pairs(tattooList) do
				AddPedDecorationFromHashes(PlayerPedId(), GetHashKey(v.collection), GetHashKey(Config.TattooList[v.collection][v.texture].nameHash))
			end

			currentTattoos = tattooList
		end
	end)
end)

RegisterNetEvent('lp_tattooshops:openTattooMenu')
AddEventHandler('lp_tattooshops:openTattooMenu', function(isRemovingByEMS, target)
	ESX.TriggerServerCallback('lp_tattooshops:requestTattoosOfPlayer', function(tattooList)
		if tattooList then
			OpenShopMenu(isRemovingByEMS, tattooList, target)
		end
	end, target)
end)

function OpenShopMenu(isRemovingByEMS, listTattoos, target)
	local elements = {}

	if isRemovingByEMS then
		currentTattoos = listTattoos
	end

	for k,v in pairs(Config.TattooCategories) do
		table.insert(elements, {label= v.name, value = v.value})
	end

	if DoesCamExist(cam) then
		RenderScriptCams(false, false, 0, 1, 0)
		DestroyCam(cam, false)
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'tattoo_shop', {
		title    = _U('tattoos'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		local currentLabel, currentValue = data.current.label, data.current.value

		if data.current.value then
			elements = {{label = _U('go_back_to_menu'), value = nil}}

			for k,v in pairs(Config.TattooList[data.current.value]) do

				local hasAlreadyTattoo = false
				for i, aTattoo in ipairs(currentTattoos) do
					if aTattoo.collection == currentValue and aTattoo.texture == k then
						hasAlreadyTattoo = true
					end
				end

				if not isRemovingByEMS then
					if hasAlreadyTattoo then
						table.insert(elements, {
							--label = _U('erase_tattoo_item', k, _U('money_amount', ESX.Math.GroupDigits(v.price))),
							label = _U('already_have_tattoo_item', k, _U('money_amount', ESX.Math.GroupDigits(v.price))),
							value = k,
							price = v.price,
							toRemove = true
						})
					else
						table.insert(elements, {
							label = _U('tattoo_item', k, _U('money_amount', ESX.Math.GroupDigits(v.price))),
							value = k,
							price = v.price,
							toRemove = false
						})
					end
				else
					if hasAlreadyTattoo then
						table.insert(elements, {
							label = _U('ems_remove_tattoo_item', k),
							value = k
						})
					end
				end
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'tattoo_shop_categories', {
				title    = _U('tattoos') .. ' | '..currentLabel,
				align    = 'top-left',
				elements = elements
			}, function(data2, menu2)
				if data2.current.value ~= nil then

					local tattoo = {collection = currentValue, texture = data2.current.value}

					if not isRemovingByEMS then
						local price = data2.current.price
						if data2.current.toRemove then
							--[[ESX.TriggerServerCallback('lp_tattooshops:eraseTattoo', function(success)
								if success then
									for i, aTattoo in ipairs(currentTattoos) do
										if aTattoo.collection == tattoo.collection and aTattoo.texture == tattoo.texture then
											table.remove(currentTattoos,i)
										end
									end
									cleanPlayer()
								end
							end, currentTattoos, price, tattoo)]]--
							TriggerEvent('esx:showNotification', _U('already_have_tattoo'))
						else
							RenderScriptCams(false, false, 0, 1, 0)
							DestroyCam(cam, false)
							ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'tattoo_confirm', {
								title    = _U('tattoo_confirm'),
								align    = 'top-left',
								elements = {
									{label = _U('yes'), value = 'yes'},
									{label = _U('no'),  value = 'no'}
								}
							}, function(data3, menu3)
								if data3.current.value == 'yes' then
									ESX.TriggerServerCallback('lp_tattooshops:purchaseTattoo', function(success)
										if success then
											table.insert(currentTattoos, tattoo)
											cleanPlayer()
										end
										menu3.close()
										isCamFixed = false
										FreezeEntityPosition(GetPlayerPed(-1), false)
										menu2.close()
										cleanPlayer()
									end, currentTattoos, price, tattoo)
								elseif data3.current.value == 'no' then
									menu3.close()
								end
							end, function(data3, menu3)
								menu3.close()
								cleanPlayer()
							end)
						end
					else
						ESX.TriggerServerCallback('lp_tattooshops:eraseTattooFromEMS', function()
							for i, aTattoo in ipairs(currentTattoos) do
								if aTattoo.collection == tattoo.collection and aTattoo.texture == tattoo.texture then
									table.remove(currentTattoos,i)
								end
							end
							menu2.close()
						end, currentTattoos, tattoo, target)
					end

				else
					menu2.close()
				end

			end, function(data2, menu2)
				if not isRemovingByEMS then
					isCamFixed = false
					FreezeEntityPosition(GetPlayerPed(-1), false)
					menu2.close()
					RenderScriptCams(false, false, 0, 1, 0)
					DestroyCam(cam, false)
					cleanPlayer()
					--setPedSkin()
				else
					menu2.close()
				end
			end, function(data2, menu2) -- when highlighted
				if not isRemovingByEMS then
					if data2.current.value ~= nil then
						isCamFixed = true
						FreezeEntityPosition(GetPlayerPed(-1), true)
						drawTattoo(data2.current.value, currentValue)
						playerHasBeenUndressed = true
					else
						isCamFixed = false
						FreezeEntityPosition(GetPlayerPed(-1), false)
						RenderScriptCams(false, false, 0, 1, 0)
						DestroyCam(cam, false)
					end
				end
			end)
		end
	end, function(data, menu)
		menu.close()
		if not isRemovingByEMS then
			setPedSkin()
		end
	end)
end

Citizen.CreateThread(function()
	for k,v in pairs(Config.Zones) do
		local blip = AddBlipForCoord(v)
		SetBlipSprite(blip, 75)
		SetBlipColour(blip, 1)
		SetBlipAsShortRange(blip, true)

		BeginTextCommandSetBlipName('STRING')
		AddTextComponentString(_U('tattoo_shop'))
		EndTextCommandSetBlipName(blip)
	end
end)

-- Display markers
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local coords, letSleep = GetEntityCoords(PlayerPedId()), true

		for k,v in pairs(Config.Zones) do
			if (Config.Type ~= -1 and GetDistanceBetweenCoords(coords, v, true) < Config.DrawDistance) then
				DrawMarker(Config.Type, v, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Config.Size.x, Config.Size.y, Config.Size.z, Config.Color.r, Config.Color.g, Config.Color.b, 100, false, true, 2, false, false, false, false)
				letSleep = false
			end
		end

		if letSleep then
			Citizen.Wait(500)
		end
	end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(100)

		local coords = GetEntityCoords(PlayerPedId())
		local isInMarker = false
		local currentZone, LastZone

		for k,v in pairs(Config.Zones) do
			if GetDistanceBetweenCoords(coords, v, true) < Config.Size.x then
				isInMarker  = true
				currentZone = 'TattooShop'
				LastZone    = 'TattooShop'
			end
		end

		if isInMarker and not HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = true
			TriggerEvent('lp_tattooshops:hasEnteredMarker', currentZone)
		end

		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerEvent('lp_tattooshops:hasExitedMarker', LastZone)
		end
	end
end)

AddEventHandler('lp_tattooshops:hasEnteredMarker', function(zone)
	if zone == 'TattooShop' then
		CurrentAction     = 'tattoo_shop'
		CurrentActionMsg  = _U('tattoo_shop_prompt')
		CurrentActionData = {zone = zone}
	end
end)

AddEventHandler('lp_tattooshops:hasExitedMarker', function(zone)
	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
	if playerHasBeenUndressed then
		cleanPlayer()
		setPedSkin()
		playerHasBeenUndressed = false
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if CurrentAction then
			ESX.ShowHelpNotification(CurrentActionMsg)

			if IsControlJustReleased(0, 38) then
				if CurrentAction == 'tattoo_shop' then
					OpenShopMenu(false)
				end
				CurrentAction = nil
			end
		else
			Citizen.Wait(500)
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if isCamFixed then
			DisableAllControlActions(0)
			EnableControlAction(0, 172, true) -- UP ARROW
			EnableControlAction(0, 173, true) -- DOWN ARROW
			EnableControlAction(0, 174, true) -- LEFT ARROW
			EnableControlAction(0, 175, true) -- RIGHT ARROW
			EnableControlAction(0, 191, true)
			EnableControlAction(0, 177, true) --BackSpace, Esc, Right MB
			EnableControlAction(0, 1, true)
			EnableControlAction(0, 2, true)
		else
			Citizen.Wait(200)
		end
	end
end)

function setPedSkin()
	ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
		TriggerEvent('skinchanger:loadSkin', skin)
	end)

	Citizen.Wait(1000)

	for k,v in pairs(currentTattoos) do
		AddPedDecorationFromHashes(PlayerPedId(), GetHashKey(v.collection), GetHashKey(Config.TattooList[v.collection][v.texture].nameHash))
	end
end

function drawTattoo(current, collection)
	SetEntityHeading(PlayerPedId(), 297.7296)
	ClearPedDecorations(PlayerPedId())

	for k,v in pairs(currentTattoos) do
		AddPedDecorationFromHashes(PlayerPedId(), GetHashKey(v.collection), GetHashKey(Config.TattooList[v.collection][v.texture].nameHash))
	end

	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			TriggerEvent('skinchanger:loadClothes', skin, {
				['torso_1'] = 15, 
				['torso_2'] = 0,
				['tshirt_1'] = 15,
				['tshirt_2'] = 0,
				['arms'] = 15,
				['arms_2'] = 0,
				['decals_1'] = 0,
				['decals_2'] = 0,
				['bags_1'] = 0,
				['bags_2'] = 0,
				['pants_1'] = 61,
				['pants_2'] = 1,
				['bags_1'] = 0,
				['bags_2'] = 0,
				['shoes_1'] = 34,
				['shoes_2'] = 0,
			})
		else
			TriggerEvent('skinchanger:loadClothes', skin, {
				['torso_1'] = 15, 
				['torso_2'] = 0,
				['tshirt_1'] = 15,
				['tshirt_2'] = 0,
				['arms'] = 15,
				['arms_2'] = 0,
				['decals_1'] = 0,
				['decals_2'] = 0,
				['bags_1'] = 0,
				['bags_2'] = 0,
				['pants_1'] = 15,
				['pants_2'] = 0,
				['bags_1'] = 0,
				['bags_2'] = 0,
				['shoes_1'] = 35,
				['shoes_2'] = 0,
			})
		end
	end)

	AddPedDecorationFromHashes(PlayerPedId(), GetHashKey(collection), GetHashKey(Config.TattooList[collection][current].nameHash))

	if not DoesCamExist(cam) then
		cam = CreateCam('DEFAULT_SCRIPTED_CAMERA', true)

		SetCamCoord(cam, GetEntityCoords(PlayerPedId()))
		SetCamRot(cam, 0.0, 0.0, 0.0)
		SetCamActive(cam, true)
		RenderScriptCams(true, false, 0, true, true)
		SetCamCoord(cam, GetEntityCoords(PlayerPedId()))
	end

	local x,y,z = table.unpack(GetEntityCoords(PlayerPedId()))

	SetCamCoord(cam, x + Config.TattooList[collection][current].addedX, y + Config.TattooList[collection][current].addedY, z + Config.TattooList[collection][current].addedZ)
	SetCamRot(cam, 0.0, 0.0, Config.TattooList[collection][current].rotZ)
end

RegisterNetEvent('lp_tattooshops:removeTattoo')
AddEventHandler('lp_tattooshops:removeTattoo', function(tattoo)
	for i, aTattoo in ipairs(currentTattoos) do
		if aTattoo.collection == tattoo.collection and aTattoo.texture == tattoo.texture then
			table.remove(currentTattoos,i)
		end
	end
	cleanPlayer()
	cleanPlayer()
end)

function cleanPlayer()
	ClearPedDecorations(PlayerPedId())

	for k,v in pairs(currentTattoos) do
		AddPedDecorationFromHashes(PlayerPedId(), GetHashKey(v.collection), GetHashKey(Config.TattooList[v.collection][v.texture].nameHash))
	end
end
