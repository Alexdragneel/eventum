cinematicCamHeight, cinematicCamStep = 0.30, 0
cinematicCamActive = false

-- Events

RegisterNetEvent('lp_cinematiccam:set')
AddEventHandler('lp_cinematiccam:set', function(_cinematicCamActive)
    if cinematicCamActive ~= _cinematicCamActive then
        cinematicCamActive = _cinematicCamActive
        enableCinematicCam(cinematicCamActive)
    end
end)

-- Main thread

Citizen.CreateThread(function()
    -- local minimap = RequestScaleformMovie("minimap")

    -- if not HasScaleformMovieLoaded(minimap) then
    --     RequestScaleformMovie(minimap)
    --     while not HasScaleformMovieLoaded(minimap) do 
    --         Wait(1)
    --     end
    -- end

    while true do
        Citizen.Wait(1)
        if cinematicCamActive then
            HideHud()
        end
        if cinematicCamStep > 0 then
            DrawBars()
        end
    end
end)

-- Functions

function HideHud()
    for i = 0, 22, 1 do
        HideHudComponentThisFrame(i)
    end
    HideHudAndRadarThisFrame()
end

function DrawBars()
    DrawRect(0.0, 0.0, 2.0, cinematicCamStep, 0, 0, 0, 255)
    DrawRect(0.0, 1.0, 2.0, cinematicCamStep, 0, 0, 0, 255)
end

function enableCinematicCam(bool)
    if bool then
        for i = 0, cinematicCamHeight, 0.002 do 
            Wait(1)
            cinematicCamStep = i
        end
    else
        for i = cinematicCamHeight, 0, -0.002 do
            Wait(1)
            cinematicCamStep = i
        end 
    end
end   
