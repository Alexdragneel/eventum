ESX = nil
local timerNotif = 900

-- Init ESX stuff --
Citizen.CreateThread(
	function()
		while ESX == nil do
			TriggerEvent(
				"esx:getSharedObject",
				function(obj)
					ESX = obj
				end
			)
			Citizen.Wait(0)
		end
	end
)
-- --

Citizen.CreateThread(function()
    local pickingSnowball = false

    while true do
        local playerPed = GetPlayerPed(-1)
        local playerId = PlayerId()
        Citizen.Wait(0)
        if IsNextWeatherType('XMAS') then
            local snowballCount = GetAmmoInPedWeapon(playerPed, GetHashKey('WEAPON_SNOWBALL'))
            if snowballCount < 10 and not pickingSnowball and not IsPedInAnyVehicle(playerPed, true) and not IsPlayerFreeAiming(playerId) and not IsPedSwimming(playerPed) and not IsPedSwimmingUnderWater(playerPed) and not IsPedRagdoll(playerPed) and not IsPedFalling(playerPed) and not IsPedRunning(playerPed) and not IsPedSprinting(playerPed) and GetInteriorFromEntity(playerPed) == 0 and not IsPedShooting(playerPed) and not IsPedUsingAnyScenario(playerPed) and not IsPedInCover(playerPed, 0) then -- check if the snowball should be picked up
                if IsControlJustReleased(0, 38) then
                    pickingSnowball = true
                    ESX.Streaming.RequestAnimDict('anim@mp_snowball', function()
                        TaskPlayAnim(playerPed, 'anim@mp_snowball', 'pickup_snowball', 8.0, -1, -1, 0, 1, false, false, false)
                        RemoveAnimDict('anim@mp_snowball')
                    end)
                    TriggerEvent('rprogress:custom', {
                        Duration = Config.DurationPickup,
                        Label = '',
                        onComplete = function ()
                            ClearPedTasks(playerPed)
                            GiveWeaponToPed(playerPed, GetHashKey('WEAPON_SNOWBALL'), 1, false, true)
                            pickingSnowball = false
                        end
                    })
                end
            end
        end
        if GetSelectedPedWeapon(playerPed) == GetHashKey('WEAPON_SNOWBALL') then
            SetPlayerWeaponDamageModifier(playerId, 0.0)
        end
    end
end)
