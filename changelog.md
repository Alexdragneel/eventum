# Changelog

### 28/07/2021

- Correction nombreuses traductions téléphone (gcphone)
- Correction traductions lspd (esx_policejobs)
- Correction traductions Prise de service jobs (lp_jobs)
- Permettre aux LSPD de rendre le téléphone de quelqu'un
- Correction de la modification d'un contact téléphonique. Maintenant c'est possible ! *Vive la technologie !*
- Ne pas prendre en compte l'appuis de la touche **H** dans un véhicule